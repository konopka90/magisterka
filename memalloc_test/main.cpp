
#include <iostream>
#include "MemoryManager.h"
#include "memorypool.h"
#include <vector>
#include <limits>

using namespace std;
using namespace memorypool;

// DWORD MemorySize = 80*10485760; // 800 MB
SIZE_T MemorySize = 10442450944L;

PCHAR Memory, Memory2;

MemoryManager mgr;
int tmp;

struct A
{
    double array[10];
};

void printSizeOfA()
{
    cout << "sizeof(A): " << sizeof(A) << endl;
}

void printPhysicalMemory()
{
    cout << "Free: " << mgr.freePhysicalMemory() / 1024 / 1024 << " MB" << endl;
}

void printPageSize()
{
    cout << "Page size: " << mgr.pageSize() << endl;
}

void vAlloc()
{
    MemorySize = 1*1024*1024*1024ULL;
    MemorySize = mgr.freePhysicalMemory();

    printPageSize();
    printSizeOfA();
    printPhysicalMemory();
   /* if(!SetProcessWorkingSetSize(GetCurrentProcess(), MemorySize, MemorySize))
    {
        printf("SetProcessWorkingSetSize failed\n");
        exit(-1);
    }*/
    Memory = (PCHAR)VirtualAlloc(NULL,
                          MemorySize,
                          MEM_COMMIT | MEM_RESERVE,
                          PAGE_READWRITE);

    unsigned int items = MemorySize / sizeof(A);



    for(unsigned long long int i = 0 ; i < 10000000 ; i+= 4096*2)
    {
        char * ptr = (char*)(Memory + i);
        *ptr = 0;
    }




    /*while(true)
    {

        printf("Memory size = %d\n", MemorySize / 1024L / 1024L );
        if(!SetProcessWorkingSetSize(GetCurrentProcess(), MemorySize, MemorySize + 1024*1024))
        {
            printf("SetProcessWorkingSetSize failed\n");
            exit(-1);
        }

        Memory = (PCHAR)VirtualAlloc(NULL,
                              MemorySize,
                              MEM_COMMIT,
                              PAGE_READWRITE);

        if(Memory == NULL)
        {
            printf("VirtualAlloc failed, error %d\n", GetLastError());
            exit(-1);
        }

        printPhysicalMemory();
        ZeroMemory(Memory,MemorySize);
        printPhysicalMemory();
        Sleep(2000);
        if(!VirtualFree(Memory,0, MEM_RELEASE))
        {
             printf("VirtualFree failed, error %d\n", GetLastError());
        }



        MemorySize += 1024 * 1024 * 50;
    }*/


    cin >> tmp;
}

void nAlloc()
{
     printPhysicalMemory();
    for(int i = 0 ; i < MemorySize / 80 ; ++i)
    {
        A * a = new A();
    }
    printPhysicalMemory();
    cin >> tmp;
}

void customAlloc()
{
    MemoryPool pool(80,131072);

    for(int i = 0 ; i < 1310710 ; ++i)
        void * ptr = pool.allocate();

   /* void * ptr2 = pool.allocate();
    void * ptr3 = pool.allocate();

    pool.deallocate(ptr);

    pool.allocate();

    pool.deallocate(ptr3);

    pool.allocate();*/
    while(1)
    {
        int i = 0;
    }

}


#define DIV (1024*1024)

void info()
{
    MEMORYSTATUSEX statex;

      statex.dwLength = sizeof (statex);

      GlobalMemoryStatusEx (&statex);

      printf ("There is  %ld percent of memory in use.\n",
                statex.dwMemoryLoad);
      printf ("There are %ld total KB of physical memory.\n",
                statex.ullTotalPhys/DIV);
      printf ("There are %ld free  KB of physical memory.\n",
                statex.ullAvailPhys/DIV);
      printf ("There are %ld total KB of paging file.\n",
                statex.ullTotalPageFile/DIV);
      printf ("There are %ld free  KB of paging file.\n",
                statex.ullAvailPageFile/DIV);
      printf ("There are %ld total KB of virtual memory.\n",
                statex.ullTotalVirtual/DIV);
      printf ("There are %ld free  KB of virtual memory.\n",
                statex.ullAvailVirtual/DIV);
      // Show the amount of extended memory available.

      printf ("There are %%ld free  KB of extended memory.\n",
                statex.ullAvailExtendedVirtual/DIV);
}

int main(int argc, char *argv[])
{
    //info();
    //vAlloc();
    customAlloc();
    return 0;
}
