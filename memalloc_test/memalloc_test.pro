#-------------------------------------------------
#
# Project created by QtCreator 2014-02-20T13:04:43
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = memalloc_test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app



SOURCES += main.cpp \
            MemoryManager.cpp \
    memorypool.cpp

HEADERS += \
            MemoryManager.h \
    memorypool.h

DEFINES += PLATFORM_WIN32 \
            #_DEBUG_

