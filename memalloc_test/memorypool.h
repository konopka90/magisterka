/****************************************** 
 * Deniz Koçak <deniz.kocak@gmail.com> 2010
 ******************************************/

#ifndef _MEMORY_POOL_H_
#define _MEMORY_POOL_H_

#include <string>
#include <stdexcept>

/*! \mainpage
 * \author Deniz Koçak  <deniz.kocak@gmail.com>
 * 
 *  \date \b 2010
*/
namespace memorypool {
	
	namespace exception {
		///\brief 
		/// Indicates an error at runtime especially in the constructor.
		class MemoryPoolException: public std::runtime_error {
			public:
			///\param message Accepts a string which will be shown in what() function.
			MemoryPoolException(const std::string &message): std::runtime_error(message) {}
		};
	}	
	
	class MemoryPool {
	private:
		/// \brief 
		/// Memory Unit which stores the address of the memory that will be requested by the user.
		/// All Memory Unit objects are stored in a linked list of MemUnit
		class MemUnit {
			public:
			MemUnit*  m_next;           ///< Next Memory Unit object
			void*     m_address;        ///< Address of the memory which will be given to the client with allocate()
		};
	
		class MemBlock {
			public:
			MemBlock*       m_next;     ///< Next Memory Block object
			unsigned char*  m_address;  ///< Address of the Memory Block allocated by expandPool()
		
			MemBlock(MemBlock *next, unsigned char *address): m_next(next), m_address(address) {}
		};
	
		class MemUnitBlock {
			public:
			MemUnitBlock*   m_next;     ///< Next Memory Unit Block object */
			unsigned char*  m_address;  ///< Address of the Memory Unit Block allocated by expandPool()
		
			MemUnitBlock(MemUnitBlock *next, unsigned char *address): m_next(next), m_address(address) {}
		};
	
		unsigned char*   m_ListMemory;
		unsigned char*   m_RealMemory;
		const int        m_UnitSize;               ///< Size of each Memory Unit -size of allocated memory with allocate() - in bytes
		const int        m_UnitNumber;             ///< Number of Memory Unit in each Memory Block
		MemUnit*         m_FreeMemList;            ///< Linked-list of free Memory Units that contain address information of the allocated block(s)
		MemUnit*         m_AllocMemList;           ///< Linked-list of allocated Memory Units on the block(s)
		MemBlock*        m_AllocMemBlocks;         ///< Linked-list of allocated Memory Blocks
		MemUnitBlock*    m_AllocMemUnitBlocks;     ///< Linked-list of allocated Memory Unit Blocks
	
		MemUnit *allocateNewMemUnit();
	
	public:
		/// \brief Constructor of the MemoryPool class with default arguments 
		///
		/// This constructor calls expandPool() method in order to allocate a 
		/// memory block to be allocated later. 
		///
		/// Accepts two arguments : 
		/// \param unitSize Size of each allocated memory unit in each block in byte
		/// \param unitNumber Number of memory unit in each block
		/// \exception If expandPoll() method fails in this constructor,  
		/// it throws an exception with type memorypool::exception::MemoryPoolException
		MemoryPool(int unitSize = 512, int unitNumber = 32);
		
		/// \brief Destructor of the MemoryPool class
		///
		/// Delete all the allocated memory. Total memory has three parts.
		/// Memory blocks allocated for storage, 
		/// memory blocks allocated for Memory Units which are used for link list,
		/// and each MemBlock & MemUnitBlock object that each were created after a 
		/// successful expandPool() method. 
		~MemoryPool() throw();
		
		/// \brief Allocates memory 
		///
		/// Allocates memory and increase the capacity of the pool for each call.
		///
		/// The allocated memory size is m_UnitSize * m_UnitNumber bytes for 
		/// new Memory Block and Memory Unit Block in m_UnitNumber * sizeof(MemUnit)
		/// - used as storage for linked list which stores free/alloacted Memory Units - bytes.  
		/// \return true if new memory block could be allocated, 
		/// false otherwise.
		bool expandPool();
		
		/// \brief Returns a memory range in m_UnitSize bytes.
		///
		/// \return the address of the allocated memory for the use of client.
		/// If there is not enough memory allocated in advance or expandPool()
		/// could not return true, it returns NULL. 
		/// <b> Please take care of that case before doing any operation with the
		/// return address.</b> 
		void* allocate();
		
		/// \brief Mark the given memory address p as unused in the internal
		/// structure of the class. 
		///
		/// \param p Address of the previously allocated memory with allocate()
		void deallocate(void *p);
	};	
}

#endif
