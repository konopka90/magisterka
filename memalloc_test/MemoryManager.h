#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H


#ifdef PLATFORM_WIN32
    // Disable macro min() and max() from windows.h
    #ifndef NOMINMAX
        #define NOMINMAX
    #endif

    #include <windows.h>
#endif

#ifdef PLATFORM_UNIX
    #include <unistd.h>
#endif

class MemoryManager
{
public:
    MemoryManager();

    // Gets free physical memory in bytes.
    unsigned long long freePhysicalMemory();
    unsigned int pageSize();

private:
#ifdef PLATFORM_WIN32
    MEMORYSTATUSEX statex;
    SYSTEM_INFO state;
#endif

};

#endif // MEMORYMANAGER_H
