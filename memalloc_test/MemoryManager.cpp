#include "MemoryManager.h"

#ifdef PLATFORM_WIN32

MemoryManager::MemoryManager()
{
    statex.dwLength = sizeof (statex);
}

unsigned long long MemoryManager::freePhysicalMemory()
{
    GlobalMemoryStatusEx (&statex);
    return (unsigned long long)statex.ullAvailPhys;
}

unsigned int MemoryManager::pageSize()
{
    GetSystemInfo(&state);
    return state.dwPageSize;
}

#endif


#ifdef PLATFORM_UNIX
MemoryManager::MemoryManager()
{
}

unsigned long MemoryManager::freePhysicalMemory()
{
    long pageSize = sysconf(_SC_PAGESIZE);
    long availablePages = sysconf(_SC_AVPHYS_PAGES);
    long availableMem = availablePages*pageSize;

    return static_cast<unsigned long>(availableMem);
}
#endif
