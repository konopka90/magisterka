/****************************************** 
 * Deniz Koçak <deniz.kocak@gmail.com> 2010
 ******************************************/

#include "memorypool.h"
#include <iostream>

memorypool::MemoryPool::MemoryPool(int unitSize, int unitNumber): m_UnitSize(unitSize), 
                                                                  m_UnitNumber(unitNumber),
                                                                  m_FreeMemList(NULL),
                                                                  m_AllocMemList(NULL),
                                                                  m_AllocMemBlocks(NULL),
                                                                  m_AllocMemUnitBlocks(NULL) {
	
	#ifdef _DEBUG_
	std::cout << "*****************************************************************" << std::endl; 
	std::cout << "Size of MemUnit is : " << sizeof(MemUnit) << std::endl;
	std::cout << "UnitSize : " << m_UnitSize << " UnitNumber : " << m_UnitNumber << std::endl;
	std::cout << "*****************************************************************" << std::endl; 
	#endif
		
	if (expandPool() == false)
	{
		throw memorypool::exception::MemoryPoolException("Not enough memory!");
	}
}


memorypool::MemoryPool::~MemoryPool() throw() {
	
	MemUnitBlock *deletedMemUnitBlock = NULL;
	MemBlock *deletedMemBlock = NULL;
		
	while (m_AllocMemUnitBlocks != NULL) {
		deletedMemUnitBlock = m_AllocMemUnitBlocks;
		m_AllocMemUnitBlocks = m_AllocMemUnitBlocks->m_next;
		delete[] (deletedMemUnitBlock->m_address);
		delete deletedMemUnitBlock;
	}
		
	while (m_AllocMemBlocks != NULL) {
		deletedMemBlock = m_AllocMemBlocks;
		m_AllocMemBlocks = m_AllocMemBlocks->m_next;
		delete[] (deletedMemBlock->m_address);
		delete deletedMemBlock;
	}	
}


bool memorypool::MemoryPool::expandPool() {
	
	#ifdef _DEBUG_
	std::cout << "** MemoryPool::expandPool() start **" << std::endl;
	#endif
		
	m_ListMemory = new(std::nothrow) unsigned char[m_UnitNumber * sizeof(MemUnit)];
	m_RealMemory = new(std::nothrow) unsigned char[m_UnitSize * m_UnitNumber];
		
	if (m_ListMemory == NULL || m_RealMemory == NULL) {
		std::cout << "**** COULD NOT EXPAND THE POOL CAPACITY ****" << std::endl;
		delete[] m_RealMemory;
		delete[] m_ListMemory;
		return false;
	}
		
	#ifdef _DEBUG_
	std::cout << "ListMemory : " << reinterpret_cast<void*>(m_ListMemory) << \
	            " RealMemory : " << reinterpret_cast<void*>(m_RealMemory) << std::endl;
	#endif
		
	//Prepend memory block
	MemBlock *newBlock = NULL;
	if (m_AllocMemBlocks == NULL) {
		newBlock = new MemBlock(NULL, m_RealMemory);
	} else {
		newBlock = new MemBlock(m_AllocMemBlocks, m_RealMemory);
	}
	m_AllocMemBlocks = newBlock;
		
	//Prepend memory unit block
	MemUnitBlock *newUnitBlock = NULL;
	if (m_AllocMemUnitBlocks == NULL) {
		newUnitBlock = new MemUnitBlock(NULL, m_ListMemory);
	} else {
		newUnitBlock = new MemUnitBlock(m_AllocMemUnitBlocks, m_ListMemory);			
	}
	m_AllocMemUnitBlocks = newUnitBlock;
		
	//Create and prepend memory units
	int i;
	MemUnit *newUnit = NULL;
		
	#ifdef _DEBUG_
	std::cout << "UnitSize : " << m_UnitSize << " UnitNumber : " << m_UnitNumber << std::endl;
	#endif
		
	for (i = 0; i < this->m_UnitNumber; i++) {
		newUnit = reinterpret_cast<MemUnit*>(m_ListMemory + (i * sizeof(MemUnit)));
		newUnit->m_address = reinterpret_cast<void*>(m_RealMemory + (i * m_UnitSize));
			
		#ifdef _DEBUG_
		std::cout << "newUnit : " << newUnit << " address : " << newUnit->m_address << std::endl;
		#endif
			
		newUnit->m_next = m_FreeMemList;
		m_FreeMemList = newUnit;
	}
	
	#ifdef _DEBUG_
	std::cout << "** MemoryPool::expandPool() end **" << std::endl;
	#endif
	
	return true;	
}


void* memorypool::MemoryPool::allocate() {
		
	bool isExpanded = true;
		
	if (m_FreeMemList == NULL) {
		isExpanded = expandPool();
	}
		
	if (isExpanded == true) {
		return (allocateNewMemUnit())->m_address;
	} else {
		return NULL;
	}	
}


void memorypool::MemoryPool::deallocate(void *p) {

	if (p == NULL) {
		return;
	}
		
	MemUnit *unit = NULL;
			
	if (m_AllocMemList != NULL) {
		unit = m_AllocMemList;
		m_AllocMemList = m_AllocMemList->m_next;
		
		#ifdef _DEBUG_
		std::cout << "** MemoryPool::deallocate() address : " << p << std::endl;
		#endif
		
		unit->m_address = p;
		unit->m_next = m_FreeMemList;
		m_FreeMemList = unit;
	}
	return;	
}


memorypool::MemoryPool::MemUnit*  
memorypool::MemoryPool::allocateNewMemUnit() {
	
	MemUnit *unit = m_FreeMemList;
	m_FreeMemList = m_FreeMemList->m_next;
		
	#ifdef _DEBUG_
	std::cout << "** MemoryPool::allocate()  unit address : " << unit << " real address : " << unit->m_address <<  std::endl;
	#endif	
		
	unit->m_next = m_AllocMemList;
	m_AllocMemList = unit;
		
	return unit;
}
