# Find PythonQt
#
# Sets PYTHONQT_FOUND, PYTHONQT_INCLUDE_DIR, PYTHONQT_LIBRARY, PYTHONQT_LIBRARIES
#

# Python is required
find_package(PythonLibs)
if(NOT PYTHONLIBS_FOUND)
  message(FATAL_ERROR "error: Python is required to build PythonQt")
endif()

find_path(PYTHONQT_INSTALL_DIR include/PythonQt/PythonQt.h DOC "Directory where PythonQt was installed.")
find_path(PYTHONQT_INCLUDE_DIR PythonQt.h "${PYTHONQT_INSTALL_DIR}/include/PythonQt" "${PYTHONQT_INSTALL_DIR}/include" DOC "Path to the PythonQt include directory")
find_library(PYTHONQT_LIBRARY PythonQt5 PATHS "${PYTHONQT_INSTALL_DIR}/lib" "${PYTHONQT_INSTALL_DIR}" DOC "The PythonQt library.")
find_library(PYTHONQT_QTALL_LIBRARY PythonQt5_QtAll PATHS "${PYTHONQT_INSTALL_DIR}/lib" "${PYTHONQT_INSTALL_DIR}" DOC "The PythonQt_QtAll library.")

mark_as_advanced(PYTHONQT_INSTALL_DIR)
mark_as_advanced(PYTHONQT_INCLUDE_DIR)
mark_as_advanced(PYTHONQT_LIBRARY)
mark_as_advanced(PYTHONQT_QTALL_LIBRARY)

# On linux, also find libutil
if(UNIX AND NOT APPLE)
  find_library(PYTHONQT_LIBUTIL util)
  mark_as_advanced(PYTHONQT_LIBUTIL)
endif()

set(PYTHONQT_FOUND 0)
if(PYTHONQT_INCLUDE_DIR AND PYTHONQT_LIBRARY)
  # Currently CMake'ified PythonQt only supports building against a python Release build.
  # This applies independently of CTK build type (Release, Debug, ...)
  add_definitions(-DPYTHONQT_USE_RELEASE_PYTHON_FALLBACK)
  set(PYTHONQT_FOUND 1)
  set(PYTHONQT_LIBRARIES ${PYTHONQT_LIBRARY}  ${PYTHONQT_QTALL_LIBRARY} ${PYTHONQT_LIBUTIL})

  find_package_handle_standard_args(PythonQt  DEFAULT_MSG
                                  PYTHONQT_LIBRARY PYTHONQT_INCLUDE_DIR)
endif()
