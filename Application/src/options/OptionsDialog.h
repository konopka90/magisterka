#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

class OptionsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit OptionsDialog(QWidget *parent = 0);

signals:

public slots:
    void changePage(QListWidgetItem *current, QListWidgetItem *previous);
    void save();

private:

    enum PageType
    {
        General,
        Tables,
        Plots
    };


    void createOptionList();
    void configureQListWidgetItem(QListWidgetItem&, PageType);
    void updateListWidget();

private:
    QListWidget *contentsWidget;
    QStackedWidget *pagesWidget;
};

#endif // OPTIONSDIALOG_H
