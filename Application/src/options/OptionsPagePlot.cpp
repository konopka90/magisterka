#include "OptionsPagePlot.h"

OptionsPagePlot::OptionsPagePlot()
{
    m_tabGeometry = new OptionsPagePlotGeometry();
    m_tabAxes = new OptionsPagePlotAxes();
    m_tabHistogram = new OptionsPagePlotHistogram();

    QTabWidget * tabs = new QTabWidget();
    tabs->addTab(new OptionsPagePlotGeneral(),"General");
    tabs->addTab(m_tabHistogram, "Histogram");
    tabs->addTab(m_tabAxes, "Axes");
    tabs->addTab(m_tabGeometry, "Geometry");

    QVBoxLayout *mainLayout = createMainLayout();
    mainLayout->addWidget(tabs);
    setLayout(mainLayout);
}

void OptionsPagePlot::save()
{
    m_tabGeometry->save();
    m_tabAxes->save();
    m_tabHistogram->save();
}
