#ifndef OPTIONSPAGEPLOT_H
#define OPTIONSPAGEPLOT_H

#include "OptionsPage.h"
#include "OptionsPagePlotGeneral.h"
#include "OptionsPagePlotGeometry.h"
#include "OptionsPagePlotAxes.h"
#include "OptionsPagePlotHistogram.h"

class OptionsPagePlot : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPagePlot();
    void save();

private:
    OptionsPage * m_tabGeometry;
    OptionsPage * m_tabAxes;
    OptionsPage * m_tabHistogram;
};

#endif // OPTIONSPAGEPLOT_H
