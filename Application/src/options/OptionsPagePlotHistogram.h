#ifndef OPTIONSPAGEPLOTHISTOGRAM_H
#define OPTIONSPAGEPLOTHISTOGRAM_H

#include "OptionsPage.h"

class PlotHistogram;

class OptionsPagePlotHistogram : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPagePlotHistogram();
    void save();

private slots:
    void setBinsSpinboxAvailability();

private:

    QComboBox * m_binsBox;
    QSpinBox * m_bins;
};

#endif // OPTIONSPAGEPLOTHISTOGRAM_H
