#include "OptionsDialog.h"

#include "OptionsPage.h"
#include "OptionsPageGeneral.h"
#include "OptionsPagePlot.h"
#include "OptionsPageTable.h"

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent)
{
    pagesWidget = new QStackedWidget();
    pagesWidget->addWidget(new OptionsPageGeneral());
    pagesWidget->addWidget(new OptionsPageTable());
    pagesWidget->addWidget(new OptionsPagePlot());

    createOptionList();

    QPushButton *saveButton = new QPushButton(tr("Save"));
    QPushButton *closeButton = new QPushButton(tr("Close"));

    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

    QHBoxLayout *horizontalLayout = new QHBoxLayout();
    horizontalLayout->addWidget(contentsWidget);
    horizontalLayout->addWidget(pagesWidget, 1);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(saveButton);
    buttonsLayout->addWidget(closeButton);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(horizontalLayout);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("Default settings"));
}

void OptionsDialog::createOptionList()
{
    contentsWidget = new QListWidget();
    contentsWidget->setIconSize(QSize(32, 32));
    contentsWidget->setSpacing(12);

    contentsWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    contentsWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QListWidgetItem *generalButton = new QListWidgetItem(contentsWidget);
    configureQListWidgetItem(*generalButton, General);

    QListWidgetItem *tableButton = new QListWidgetItem(contentsWidget);
    configureQListWidgetItem(*tableButton, Tables);

    QListWidgetItem *plotButton = new QListWidgetItem(contentsWidget);
    configureQListWidgetItem(*plotButton, Plots);

    connect(contentsWidget,
                 SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
            this, SLOT(changePage(QListWidgetItem*,QListWidgetItem*)));

    contentsWidget->setCurrentRow(0);

    updateListWidget();
}

void OptionsDialog::configureQListWidgetItem(QListWidgetItem & item, PageType type)
{
    item.setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    switch(type)
    {
    case General:
        item.setText(tr("General"));
        item.setIcon(QIcon(":/icons/general.png"));
        break;

    case Tables:
        item.setText(tr("Tables"));
        item.setIcon(QIcon(":/icons/table.png"));
        break;

    case Plots:
        item.setText(tr("Plots"));
        item.setIcon(QIcon(":/icons/plot.png"));
        break;

    default:
        item.setText(tr("Unknown"));
        break;
    }
}

void OptionsDialog::updateListWidget()
{
    QFontMetrics fm(contentsWidget->font());
    int width = 0;
    for(int i = 0; i<contentsWidget->count() ; i++){
        int itemWidth = fm.boundingRect(contentsWidget->item(i)->text()).width();
        if(itemWidth > width)
            width = itemWidth;
    }

    contentsWidget->setFixedWidth(contentsWidget->iconSize().width() + width + 50);
}


void OptionsDialog::changePage(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (!current)
        current = previous;

    pagesWidget->setCurrentIndex(contentsWidget->row(current));
}

void OptionsDialog::save()
{
    for(int i = 0 ; i < pagesWidget->count() ; ++i)
    {
        qobject_cast<OptionsPage*>(pagesWidget->widget(i))->save();
    }

    Options& options = Options::instance();
    options.save();

    close();
}
