#include "OptionsPageGeneral.h"

OptionsPageGeneral::OptionsPageGeneral()
{

    QTabWidget * tabs = new QTabWidget();
    m_tabApp = new OptionsPageGeneralApplication();
    m_tabNetwork = new OptionsPageGeneralNetwork();
    m_tabColors = new OptionsPageGeneralColors();
    tabs->addTab(m_tabApp,"Application");
    tabs->addTab(m_tabNetwork,"Network");
    tabs->addTab(m_tabColors,"Colors");

    QVBoxLayout *mainLayout = createMainLayout();
    mainLayout->addWidget(tabs);
    setLayout(mainLayout);
}


void OptionsPageGeneral::save()
{
    m_tabApp->save();
    m_tabColors->save();
    m_tabNetwork->save();
}
