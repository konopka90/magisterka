#include "Options.h"

using namespace std;

double const Options::DEFAULT_MAXIMUM_MEMORY_LOAD = 10.0f;
unsigned int const Options::DEFAULT_PORT = 60111;
const QColor Options::HIGHLIGHT_WIDGET_COLOR = QColor(240,240,240);

QDataStream& operator << (QDataStream& stream, const AxisVisibility& v)
{
    stream << v.isVisible;
    stream << v.isLabelVisible;

    return stream;
}

QDataStream& operator >> (QDataStream& stream, AxisVisibility& v)
{
    stream >> v.isVisible;
    stream >> v.isLabelVisible;

    return stream;
}

QDataStream& operator << (QDataStream& stream, map<QwtPlot::Axis, AxisVisibility>& myMap)
{
    stream << myMap[QwtPlot::yLeft];
    stream << myMap[QwtPlot::yRight];
    stream << myMap[QwtPlot::xTop];
    stream << myMap[QwtPlot::xBottom];


    return stream;
}

QDataStream& operator >> (QDataStream& stream, map<QwtPlot::Axis, AxisVisibility>& myMap)
{
    stream >> myMap[QwtPlot::yLeft];
    stream >> myMap[QwtPlot::yRight];
    stream >> myMap[QwtPlot::xTop];
    stream >> myMap[QwtPlot::xBottom];

    return stream;
}


Options::OptionsDetails::OptionsDetails() :
    outOfMemoryAction(DoNothing),
    maximumMemoryLoadInPercent(DEFAULT_MAXIMUM_MEMORY_LOAD),
    port(DEFAULT_PORT),
    geometryPlotSize(DEFAULT_GEOMETRY_PLOT_WIDTH,DEFAULT_GEOMETRY_PLOT_HEIGHT),
    saveAsImageSize(DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT),
    numberOfBins(DEFAULT_NUMBER_OF_BINS)
{
    axisVisibility[QwtPlot::yLeft] = AxisVisibility();
    axisVisibility[QwtPlot::yRight] = AxisVisibility();
    axisVisibility[QwtPlot::xTop] = AxisVisibility();
    axisVisibility[QwtPlot::xBottom] = AxisVisibility();
}

void Options::OptionsDetails::serialize(QDataStream& stream)
{
    stream << networkDevices;
    stream << port;
    stream << maximumMemoryLoadInPercent;
    stream << outOfMemoryAction;
    stream << nextPoolSize;
    stream << geometryPlotSize;
    stream << saveAsImageSize;
    stream << axisVisibility;
    stream << numberOfBins;
    stream << binsType;
}

void Options::OptionsDetails::deserialize(QDataStream &stream)
{
    stream >> networkDevices;
    stream >> port;
    stream >> maximumMemoryLoadInPercent;

    int outOfMemoryActionInt;
    stream >> outOfMemoryActionInt;
    outOfMemoryAction = (OutOfMemoryAction) outOfMemoryActionInt;
    stream >> nextPoolSize;
    stream >> geometryPlotSize;
    stream >> saveAsImageSize;
    stream >> axisVisibility;
    stream >> numberOfBins;
    stream >> binsType;
}


Options &Options::instance()
{
    static Options options;
    return options;
}

Options::Options()
{
    prepareDefaultConfiguration();
    prepareDefaultColors();
}

void Options::prepareDefaultColors()
{
    m_optionsDetails.indexedColors[0] = Qt::black;
    m_optionsDetails.indexedColors[1] = Qt::red;
    m_optionsDetails.indexedColors[2] = Qt::green;
    m_optionsDetails.indexedColors[3] = Qt::blue;
    m_optionsDetails.indexedColors[4] = Qt::cyan;
    m_optionsDetails.indexedColors[5] = Qt::magenta;
    m_optionsDetails.indexedColors[6] = Qt::yellow;
    m_optionsDetails.indexedColors[7] = Qt::darkYellow;
    m_optionsDetails.indexedColors[8] = QColor(0,0,128);     // navy
    m_optionsDetails.indexedColors[9] = QColor(128,0,128);   // purple
    m_optionsDetails.indexedColors[10] = QColor(128,0,0);    // wine
    m_optionsDetails.indexedColors[11] = QColor(0,128,0);    // olive
    m_optionsDetails.indexedColors[12] = QColor(0,128,128);  // dark cyan
    m_optionsDetails.indexedColors[13] = QColor(0,0,160);    // royal
    m_optionsDetails.indexedColors[14] = QColor(255,128,0);  // orange
    m_optionsDetails.indexedColors[15] = QColor(0,0,160);    // royal
    m_optionsDetails.indexedColors[16] = QColor(128,0,255);  // violet
    m_optionsDetails.indexedColors[17] = QColor(255,0,128);  // pink
    m_optionsDetails.indexedColors[18] = QColor(192,192,192);// light gray
    m_optionsDetails.indexedColors[19] = QColor(160,160,164);// gray
}

void Options::prepareDefaultConfiguration()
{
    std::string localdir;
    std::string pathSeparator;
#ifdef  PLATFORM_WIN32
    localdir = getenv("USERPROFILE");
    localdir += "\\AppData\\Local\\";
    pathSeparator = "\\";
#else
    localdir = getenv("HOME");
    localdir += "/.config/";
    pathSeparator = "/";
#endif

    m_applicationName = "DAQ";
    m_applicationLocalConfigDir = localdir + m_applicationName;
    m_applicationLocalConfigFilename = "config.xml";
    m_applicationLocalConfigFilepath = m_applicationLocalConfigDir + pathSeparator + m_applicationLocalConfigFilename;

}

void Options::load()
{
    if(!isLocalConfigDirExists())
    {
        QDir().mkdir(QString::fromStdString(m_applicationLocalConfigDir));
    }

    if(!isLocalConfigFileExists())
    {
        save();
    }
    else
    {
        loadLocalConfigFile();
    }
}

void Options::save()
{
    QFile file(m_applicationLocalConfigFilepath.c_str());
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    m_optionsDetails.serialize(out);
}

double Options::getPreallocatedMemoryPercent()
{
    return m_optionsDetails.maximumMemoryLoadInPercent;
}

OutOfMemoryAction Options::getOutOfMemoryAction()
{
    return m_optionsDetails.outOfMemoryAction;
}

QStringList &Options::getNetworkDevices()
{
    return m_optionsDetails.networkDevices;
}

quint32 Options::getPort()
{
    return m_optionsDetails.port;
}

// In bytes
quint64 Options::getNextPoolSize()
{
    return m_optionsDetails.nextPoolSize * 1024 * 1024;
}

QSize Options::getGeometryPlotSize()
{
    return m_optionsDetails.geometryPlotSize;
}

QSize Options::getSaveAsImageSize()
{
    return m_optionsDetails.saveAsImageSize;
}

bool Options::getAxisVisibility(QwtPlot::Axis axis)
{
    return m_optionsDetails.axisVisibility[axis].isVisible;
}

bool Options::getAxisLabelVisibility(QwtPlot::Axis axis)
{
    return m_optionsDetails.axisVisibility[axis].isLabelVisible;
}

unsigned int Options::getNumberOfBins()
{
    return m_optionsDetails.numberOfBins;
}

QString Options::getBinsType()
{
    return m_optionsDetails.binsType;
}

void Options::setMaximumMemoryLoadInPercent(double value)
{
    m_optionsDetails.maximumMemoryLoadInPercent = value;
}

void Options::setOutOfMemoryAction(OutOfMemoryAction action)
{
    m_optionsDetails.outOfMemoryAction = action;
}

void Options::setNetworkDevices(const QStringList &devs)
{
    m_optionsDetails.networkDevices = devs;
}

void Options::setPort(uint32_t port)
{
    m_optionsDetails.port = port;
}

void Options::setNextPoolSize(uint64_t size)
{
    m_optionsDetails.nextPoolSize = size;
}

void Options::setGeometryPlotSize(const QSize &size)
{
    m_optionsDetails.geometryPlotSize = size;
}

void Options::setSaveAsImageSize(const QSize &size)
{
    m_optionsDetails.saveAsImageSize = size;
}

void Options::setAxisVisibility(QwtPlot::Axis position, bool visible)
{
    AxisVisibility & v = m_optionsDetails.axisVisibility[position];

    if(!visible) { setAxisLabelVisibility(position, false); }
    v.isVisible = visible;
}

void Options::setAxisLabelVisibility(QwtPlot::Axis position, bool visible)
{
    AxisVisibility & v = m_optionsDetails.axisVisibility[position];
    v.isLabelVisible = visible;
}

void Options::setNumberOfBins(unsigned int bins)
{
    m_optionsDetails.numberOfBins = bins;
}

void Options::setBinsType(const QString &type)
{
    m_optionsDetails.binsType = type;
}

const QColor& Options::getColor(unsigned short i) const
{
    Q_ASSERT(i < MAX_PLOTS);
    if(i >= MAX_PLOTS)
        return m_optionsDetails.indexedColors[0];

    return m_optionsDetails.indexedColors[i];
}

QColor Options::getKeywordHighlightColor()
{
    return QColor(0, 0, 128);
}

QColor Options::getCommentColor()
{
    return QColor(Qt::darkGreen);
}

QColor Options::getQuotationColor()
{
    return QColor(Qt::darkMagenta);
}

QColor Options::getNumericColor()
{
    return QColor(Qt::magenta);
}

void Options::loadLocalConfigFile()
{
    QFile file(m_applicationLocalConfigFilepath.c_str());
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    m_optionsDetails.deserialize(in);
}

bool Options::isLocalConfigFileExists()
{
    QString file = QString::fromStdString(m_applicationLocalConfigFilepath);
    return QFile(file).exists();
}

bool Options::isLocalConfigDirExists()
{
    return QDir(QString::fromStdString(m_applicationLocalConfigDir)).exists();
}



QDataStream& operator >>(QDataStream &stream, QwtPlot::Axis &axis)
{
    unsigned int value;
    stream >> value;
    axis = static_cast<QwtPlot::Axis>(value);
    return stream;
}
