#include "OptionsPageTable.h"

OptionsPageTable::OptionsPageTable()
{
    // Background group

    QGroupBox *colorGroup = new QGroupBox(tr("Colors"));
    QGridLayout *colorGroupLayout = new QGridLayout();

    // Background

    QLabel * backgroundColorLabel = new QLabel("Background");
    m_backgroundRect = new ColorRect();
    m_backgroundColorButton = new QPushButton("Change...");

    colorGroupLayout->addWidget(backgroundColorLabel, 0, 0);
    colorGroupLayout->addWidget(m_backgroundRect, 0, 1);
    colorGroupLayout->addWidget(m_backgroundColorButton, 0, 2);

    // Text

    QLabel * textColorLabel = new QLabel("Text");
    m_textRect = new ColorRect();
    m_textColorButton = new QPushButton("Change...");

    colorGroupLayout->addWidget(textColorLabel, 1, 0);
    colorGroupLayout->addWidget(m_textRect, 1, 1);
    colorGroupLayout->addWidget(m_textColorButton, 1, 2);

    // Labels

    QLabel * labelColorLabel = new QLabel("Labels");
    m_labelRect = new ColorRect();
    m_labelColorButton = new QPushButton("Change...");

    colorGroupLayout->addWidget(labelColorLabel, 2, 0);
    colorGroupLayout->addWidget(m_labelRect, 2, 1);
    colorGroupLayout->addWidget(m_labelColorButton, 2, 2);

    // Join

    QHBoxLayout * outerColorGroupLayout = new QHBoxLayout;
    outerColorGroupLayout->addLayout(colorGroupLayout);
    outerColorGroupLayout->setAlignment(Qt::AlignLeft);
    colorGroup->setLayout(outerColorGroupLayout);

    // Font group

    QGroupBox *fontGroup = new QGroupBox(tr("Fonts"));
    QHBoxLayout *fontGroupLayout = new QHBoxLayout();

    m_textFontButton = new QPushButton(tr("Text Font"));
    m_labelsFontButton = new QPushButton(tr("Labels Font"));

    fontGroupLayout->addWidget(m_textFontButton);
    fontGroupLayout->addWidget(m_labelsFontButton);

    // Join

    fontGroup->setLayout(fontGroupLayout);


    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addWidget(colorGroup);
    mainLayout->addWidget(fontGroup);
    setLayout(mainLayout);
}
