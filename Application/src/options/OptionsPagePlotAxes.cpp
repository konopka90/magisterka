#include "OptionsPagePlotAxes.h"

OptionsPagePlotAxes::OptionsPagePlotAxes()
{
    QGroupBox * enabledAxesGroup = new QGroupBox(tr("Enabled axes"));
    m_grid = new QGridLayout;

    m_showLeftAxis = new QCheckBox;
    m_labelLeftAxis = new QCheckBox;

    m_showRightAxis = new QCheckBox;
    m_labelRightAxis = new QCheckBox;

    m_showTopAxis = new QCheckBox;
    m_labelTopAxis = new QCheckBox;

    m_showBottomAxis = new QCheckBox;
    m_labelBottomAxis = new QCheckBox;

    m_showLeftAxis->setChecked(m_options.getAxisVisibility(QwtPlot::yLeft));
    m_showRightAxis->setChecked(m_options.getAxisVisibility(QwtPlot::yRight));
    m_showTopAxis->setChecked(m_options.getAxisVisibility(QwtPlot::xTop));
    m_showBottomAxis->setChecked(m_options.getAxisVisibility(QwtPlot::xBottom));

    m_labelLeftAxis->setChecked(m_options.getAxisLabelVisibility(QwtPlot::yLeft));
    m_labelLeftAxis->setEnabled(m_options.getAxisVisibility(QwtPlot::yLeft));

    m_labelRightAxis->setChecked(m_options.getAxisLabelVisibility(QwtPlot::yRight));
    m_labelRightAxis->setEnabled(m_options.getAxisVisibility(QwtPlot::yRight));

    m_labelTopAxis->setChecked(m_options.getAxisLabelVisibility(QwtPlot::xTop));
    m_labelTopAxis->setEnabled(m_options.getAxisVisibility(QwtPlot::xTop));

    m_labelBottomAxis->setChecked(m_options.getAxisLabelVisibility(QwtPlot::xBottom));
    m_labelBottomAxis->setEnabled(m_options.getAxisVisibility(QwtPlot::xBottom));




    QLabel * leftLabel = new QLabel(tr("Left"));
    QLabel * rightLabel = new QLabel(tr("Right"));
    QLabel * topLabel = new QLabel(tr("Top"));
    QLabel * bottomLabel = new QLabel(tr("Bottom"));

    QLabel * showLabel = new QLabel(tr("Show"));
    QLabel * labelsLabel = new QLabel(tr("Labels"));


    m_grid->addWidget(showLabel, 0, 1);
    m_grid->addWidget(labelsLabel, 0, 2);

    // Left
    m_grid->addWidget(leftLabel, 1,0);
    m_grid->addWidget(m_showLeftAxis, 1,1);
    m_grid->addWidget(m_labelLeftAxis, 1,2);

    // Right
    m_grid->addWidget(rightLabel, 2,0);
    m_grid->addWidget(m_showRightAxis, 2,1);
    m_grid->addWidget(m_labelRightAxis, 2,2);

    // Top
    m_grid->addWidget(topLabel, 3,0);
    m_grid->addWidget(m_showTopAxis, 3,1);
    m_grid->addWidget(m_labelTopAxis, 3,2);

    // Bottom
    m_grid->addWidget(bottomLabel, 4,0);
    m_grid->addWidget(m_showBottomAxis, 4,1);
    m_grid->addWidget(m_labelBottomAxis, 4,2);


    enabledAxesGroup->setLayout(m_grid);

    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addWidget(enabledAxesGroup);
    setLayout(mainLayout);

    connect(m_showLeftAxis, SIGNAL(toggled(bool)), this, SLOT(showCheckboxClicked(bool)));
    connect(m_showRightAxis, SIGNAL(toggled(bool)), this, SLOT(showCheckboxClicked(bool)));
    connect(m_showTopAxis, SIGNAL(toggled(bool)), this, SLOT(showCheckboxClicked(bool)));
    connect(m_showBottomAxis, SIGNAL(toggled(bool)), this, SLOT(showCheckboxClicked(bool)));

}

void OptionsPagePlotAxes::save()
{
    m_options.setAxisLabelVisibility(QwtPlot::yLeft, m_labelLeftAxis->isChecked());
    m_options.setAxisLabelVisibility(QwtPlot::yRight, m_labelRightAxis->isChecked());
    m_options.setAxisLabelVisibility(QwtPlot::xTop, m_labelTopAxis->isChecked());
    m_options.setAxisLabelVisibility(QwtPlot::xBottom, m_labelBottomAxis->isChecked());

    m_options.setAxisVisibility(QwtPlot::yLeft, m_showLeftAxis->isChecked());
    m_options.setAxisVisibility(QwtPlot::yRight, m_showRightAxis->isChecked());
    m_options.setAxisVisibility(QwtPlot::xTop, m_showTopAxis->isChecked());
    m_options.setAxisVisibility(QwtPlot::xBottom, m_showBottomAxis->isChecked());
}

void OptionsPagePlotAxes::showCheckboxClicked(bool checked)
{
    QCheckBox * showbox = qobject_cast<QCheckBox*>(QObject::sender());
    int index = m_grid->indexOf(showbox);
    QCheckBox * labelbox = qobject_cast<QCheckBox*>(m_grid->itemAt(index + 1)->widget());
    labelbox->setEnabled(checked);

    if(!checked)
        labelbox->setChecked(false);
}
