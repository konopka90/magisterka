#ifndef OPTIONSPAGE_H
#define OPTIONSPAGE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>
#include <QSpinBox>
#include <QTabWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QSlider>
#include <QSpinBox>
#include <QRadioButton>
#include <QCheckBox>

#include "Options.h"
#include "../widgets/ColorRect.h"

class OptionsPage : public QWidget
{
    Q_OBJECT
public:
    explicit OptionsPage(QWidget *parent = 0);
    virtual ~OptionsPage() { }
    virtual void save() {}

protected:
    QVBoxLayout *createMainLayout();

    void colorButton(QPushButton&, QColor color);

protected:
    Options& m_options;

signals:

public slots:

};

#endif // OPTIONSPAGE_H
