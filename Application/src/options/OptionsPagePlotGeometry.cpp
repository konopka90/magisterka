#include "OptionsPagePlotGeometry.h"

OptionsPagePlotGeometry::OptionsPagePlotGeometry()
{

    QLabel * canvasWidthLabel = new QLabel(tr("Canvas Width (pixels):"));
    QLabel * canvasHeightLabel = new QLabel(tr("Canvas Height (pixels):"));

    m_canvasHeight = new QSpinBox();
    m_canvasWidth = new QSpinBox();

    m_canvasHeight->setMinimum(1);
    m_canvasWidth->setMinimum(1);
    m_canvasHeight->setMaximum(INT_MAX);
    m_canvasWidth->setMaximum(INT_MAX);


    QSize size = m_options.getGeometryPlotSize();
    m_canvasWidth->setValue(size.width());
    m_canvasHeight->setValue(size.height());

    connect(m_canvasWidth, SIGNAL(valueChanged(int)), this, SLOT(canvasWidthChanged(int)));
    connect(m_canvasHeight, SIGNAL(valueChanged(int)), this, SLOT(canvasHeightChanged(int)));


    m_keepAspectCanvas = new QCheckBox(tr("Keep aspect ratio"));
    m_keepAspectCanvas->setChecked(true);
    m_aspectRatioCanvas = (double)m_canvasWidth->value() / (double)m_canvasHeight->value();

    QGridLayout * grid = new QGridLayout();
    grid->addWidget(canvasWidthLabel, 0,0);
    grid->addWidget(m_canvasWidth, 0,1);
    grid->addWidget(canvasHeightLabel, 1,0);
    grid->addWidget(m_canvasHeight, 1,1);
    grid->addWidget(m_keepAspectCanvas,2,1);

    QLabel * imageWidthLabel = new QLabel(tr("Image Width (pixels):"));
    QLabel * imageHeightLabel = new QLabel(tr("Image Height (pixels):"));

    m_imageHeight = new QSpinBox();
    m_imageWidth = new QSpinBox();

    m_imageHeight->setMinimum(1);
    m_imageWidth->setMinimum(1);
    m_imageHeight->setMaximum(INT_MAX);
    m_imageWidth->setMaximum(INT_MAX);
    size = m_options.getSaveAsImageSize();
    m_imageWidth->setValue(size.width());
    m_imageHeight->setValue(size.height());

    m_keepAspectImage = new QCheckBox(tr("Keep aspect ratio"));
    m_keepAspectImage->setChecked(true);
    m_aspectRatioImage = (double)m_imageWidth->value() / (double)m_imageHeight->value();

    connect(m_imageWidth, SIGNAL(valueChanged(int)), this, SLOT(imageWidthChanged(int)));
    connect(m_imageHeight, SIGNAL(valueChanged(int)), this, SLOT(imageHeightChanged(int)));

    QGroupBox * saveAsGroup = new QGroupBox(tr("Save as image"));

    QGridLayout * grid2 = new QGridLayout;
    grid2->addWidget(imageWidthLabel, 0,0);
    grid2->addWidget(m_imageWidth, 0,1);
    grid2->addWidget(imageHeightLabel, 1,0);
    grid2->addWidget(m_imageHeight, 1,1);
    grid2->addWidget(m_keepAspectImage, 2,1);

    saveAsGroup->setLayout(grid2);

    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addLayout(grid);
    mainLayout->addWidget(saveAsGroup);
    setLayout(mainLayout);
}


void OptionsPagePlotGeometry::save()
{
    m_options.setGeometryPlotSize(QSize(m_canvasWidth->value(), m_canvasHeight->value()));
    m_options.setSaveAsImageSize(QSize(m_imageWidth->value(), m_imageHeight->value()));
}

void OptionsPagePlotGeometry::canvasWidthChanged(int value)
{
    if(m_keepAspectCanvas->isChecked())
    {
        m_canvasHeight->blockSignals(true);
        m_canvasHeight->setValue((double)value/m_aspectRatioCanvas);
        m_canvasHeight->blockSignals(false);
    }
    else
        m_aspectRatioCanvas = (double)value / (double)m_canvasHeight->value();
}

void OptionsPagePlotGeometry::canvasHeightChanged(int value)
{
    if(m_keepAspectCanvas->isChecked())
    {
        m_canvasWidth->blockSignals(true);
        m_canvasWidth->setValue((double)value*m_aspectRatioCanvas);
        m_canvasWidth->blockSignals(false);
    }
    else
        m_aspectRatioCanvas = (double)m_canvasWidth->value() / (double)value;
}

void OptionsPagePlotGeometry::imageWidthChanged(int value)
{
    if(m_keepAspectImage->isChecked())
    {
        m_imageHeight->blockSignals(true);
        m_imageHeight->setValue((double)value/m_aspectRatioImage);
        m_imageHeight->blockSignals(false);
    }
    else
        m_aspectRatioImage = (double)value / (double)m_imageHeight->value();
}

void OptionsPagePlotGeometry::imageHeightChanged(int value)
{
    if(m_keepAspectImage->isChecked())
    {
        m_imageWidth->blockSignals(true);
        m_imageWidth->setValue((double)value*m_aspectRatioImage);
        m_imageWidth->blockSignals(false);
    }
    else
        m_aspectRatioImage = (double)m_imageWidth->value() / (double)value;
}
