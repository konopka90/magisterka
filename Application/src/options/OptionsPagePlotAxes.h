#ifndef OPTIONSPAGEPLOTAXES_H
#define OPTIONSPAGEPLOTAXES_H

#include "OptionsPage.h"

class OptionsPagePlotAxes : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPagePlotAxes();
    void save();

private slots:
    void showCheckboxClicked(bool checked);

private:

    QGridLayout * m_grid;

    QCheckBox * m_showLeftAxis;
    QCheckBox * m_labelLeftAxis;

    QCheckBox * m_showRightAxis;
    QCheckBox * m_labelRightAxis;

    QCheckBox * m_showTopAxis;
    QCheckBox * m_labelTopAxis;

    QCheckBox * m_showBottomAxis;
    QCheckBox * m_labelBottomAxis;
};

#endif // OPTIONSPAGEPLOTAXES_H
