#ifndef OPTIONSPAGEPLOTGENERAL_H
#define OPTIONSPAGEPLOTGENERAL_H

#include "OptionsPage.h"

class OptionsPagePlotGeneral : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPagePlotGeneral();
    void save() {}

private:
    ColorRect * m_backgroundRect;
    ColorRect * m_canvasRect;

    QPushButton * m_backgroundColorButton;
    QPushButton * m_canvasColorButton;

    QSlider * m_backgroundOpacitySlider;
    QSlider * m_canvasOpacitySlider;

    QSpinBox * m_backgroundOpacitySpinbox;
    QSpinBox * m_canvasOpacitySpinbox;


};

#endif // OPTIONSPAGEPLOTGENERAL_H
