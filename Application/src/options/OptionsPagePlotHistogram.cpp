#include "OptionsPagePlotHistogram.h"
#include "../plot/PlotHistogram.h"

OptionsPagePlotHistogram::OptionsPagePlotHistogram()
{
    m_binsBox = new QComboBox;

    QVector<QString> binsTypes = Histogram::binsTypes();

    int index = 0;
    foreach(QString s, binsTypes)
    {
        m_binsBox->addItem(s);
        if(m_options.getBinsType() == s)
        {
            m_binsBox->setCurrentIndex(index);
        }

        index++;
    }

    m_bins = new QSpinBox;
    m_bins->setMaximum(INT_MAX);
    m_bins->setValue(m_options.getNumberOfBins());


    QHBoxLayout * binsLayout = new QHBoxLayout;
    binsLayout->addWidget(m_binsBox);
    binsLayout->addWidget(m_bins);

    QGroupBox * groupBox = new QGroupBox(tr("Number of bins"));
    groupBox->setLayout(binsLayout);

    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addWidget(groupBox);
    setLayout(mainLayout);

    connect(m_binsBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setBinsSpinboxAvailability()));
    setBinsSpinboxAvailability();
}


void OptionsPagePlotHistogram::save()
{
    m_options.setNumberOfBins(m_bins->value());
    m_options.setBinsType(m_binsBox->currentText());
}

void OptionsPagePlotHistogram::setBinsSpinboxAvailability()
{
    Histogram::BinsType type = Histogram::qStringToBinsType(m_binsBox->currentText());
    if(type != Histogram::ExplicitValue)
    {
        m_bins->setEnabled(false);
    }
    else
    {
        m_bins->setEnabled(true);
    }
}
