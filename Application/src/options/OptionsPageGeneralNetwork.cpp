#include "OptionsPageGeneralNetwork.h"

using namespace std;


OptionsPageGeneralNetwork::OptionsPageGeneralNetwork()
{
    // Network devices

    m_nicGridLayout = new QGridLayout();
    QGroupBox *nicGroup = new QGroupBox("Network interface card");
    Network network;
    m_devs = network.devices();

    for(int i = 0 ; i < m_devs.size() ; ++i)
    {
        QString realName;
#ifdef PLATFORM_UNIX
        realName = QString::fromStdString(m_devs[i].name);
#else
        realName = QString::fromStdString(m_devs[i].description);
#endif
        QCheckBox * button = new QCheckBox(realName);

        if(m_options.getNetworkDevices().contains(QString::fromStdString(m_devs[i].name)))
            button->setChecked(true);

        m_nicGridLayout->addWidget(button, i, 0);
        m_nicGridLayout->addWidget(new QLabel(QString::fromStdString(m_devs[i].IP)), i, 1);
    }

    nicGroup->setLayout(m_nicGridLayout);

    // Port

    QHBoxLayout * portLayout = new QHBoxLayout();
    QGroupBox * portGroup = new QGroupBox("UDP port");

    QLabel * portLabel = new QLabel("Capture on port:");

    m_portSpinbox = new QSpinBox();
    m_portSpinbox->setMinimum(0);
    m_portSpinbox->setMaximum(65535);
    m_portSpinbox->setValue(m_options.getPort());

    portLayout->addWidget(portLabel);
    portLayout->addWidget(m_portSpinbox);
    portGroup->setLayout(portLayout);

    // Main layout

    QVBoxLayout *mainLayout = createMainLayout();
    mainLayout->addWidget(nicGroup);
    mainLayout->addWidget(portGroup);
    setLayout(mainLayout);
}

void OptionsPageGeneralNetwork::save()
{
    // Network devices

    QStringList selectedDevs;
    for(int i = 0 ; i < m_devs.size() ; ++i)
    {
        QCheckBox * button = qobject_cast<QCheckBox*>(m_nicGridLayout->itemAtPosition(i,0)->widget());
        if(button->isChecked())
            selectedDevs.append(QString::fromStdString(m_devs[i].name));
    }

    m_options.setNetworkDevices(selectedDevs);

    // Port
    m_options.setPort(m_portSpinbox->value());
}

