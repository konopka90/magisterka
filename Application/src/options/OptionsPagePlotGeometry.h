#ifndef OPTIONSPAGEPLOTGEOMETRY_H
#define OPTIONSPAGEPLOTGEOMETRY_H

#include "OptionsPage.h"
#include <limits>

class OptionsPagePlotGeometry : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPagePlotGeometry();
    void save();

private slots:
    void canvasWidthChanged(int value);
    void canvasHeightChanged(int value);

    void imageWidthChanged(int value);
    void imageHeightChanged(int value);



private:
    QSpinBox * m_canvasWidth;
    QSpinBox * m_canvasHeight;
    QSpinBox * m_imageWidth;
    QSpinBox * m_imageHeight;
    QCheckBox * m_keepAspectCanvas;
    QCheckBox * m_keepAspectImage;

    double m_aspectRatioCanvas;
    double m_aspectRatioImage;

};

#endif // OPTIONSPAGEPLOTGEOMETRY_H
