#ifndef OPTIONSPAGEGENERALCOLORS_H
#define OPTIONSPAGEGENERALCOLORS_H

#include "OptionsPage.h"
#include "../widgets/ColorWidget.h"

class OptionsPageGeneralColors : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPageGeneralColors();
    void save() {}

private:
    ColorWidget * m_workspaceColor;
    QPushButton * m_workspaceColorButton;

    ColorRect * m_panelsRect;
    QPushButton * m_panelsColorButton;

    ColorRect * m_panelsTextRect;
    QPushButton * m_panelsTextColorButton;
};

#endif // OPTIONSPAGEGENERALCOLORS_H
