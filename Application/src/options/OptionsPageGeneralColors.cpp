#include "OptionsPageGeneralColors.h"

OptionsPageGeneralColors::OptionsPageGeneralColors()
{
    QGroupBox *groupBox = new QGroupBox;
    QGridLayout *colorLayout = new QGridLayout();

    // Workspace

    m_workspaceColor = new ColorWidget();
    m_workspaceColor->setText(tr("Workspace"));
    colorLayout->addWidget(m_workspaceColor, 0, 1);

    // Panels

    QLabel * panelsColorLabel = new QLabel("Panels");
    m_panelsRect = new ColorRect();
    m_panelsColorButton = new QPushButton("Change...");

    colorLayout->addWidget(panelsColorLabel, 1, 0);
    colorLayout->addWidget(m_panelsRect, 1, 1);
    colorLayout->addWidget(m_panelsColorButton, 1, 2);

    // Panels Text

    QLabel * panelsTextColorLabel = new QLabel("Panels text");
    m_panelsTextRect = new ColorRect();
    m_panelsTextColorButton = new QPushButton("Change...");

    colorLayout->addWidget(panelsTextColorLabel, 2, 0);
    colorLayout->addWidget(m_panelsTextRect, 2, 1);
    colorLayout->addWidget(m_panelsTextColorButton, 2, 2);

    // Join

    QHBoxLayout * outerColorGroupLayout = new QHBoxLayout;
    outerColorGroupLayout->addLayout(colorLayout);
    outerColorGroupLayout->setAlignment(Qt::AlignLeft);
    groupBox->setLayout(outerColorGroupLayout);

    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addWidget(groupBox);
    setLayout(mainLayout);
}
