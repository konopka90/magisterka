
INCLUDEPATH += options/

HEADERS +=  \
    options/OptionsDialog.h \
    options/OptionsPage.h \
    options/Options.h \
    options/OptionsPageGeneral.h \
    options/OptionsPagePlot.h \
    options/OptionsPagePlotGeneral.h \
    options/OptionsPageTable.h \
    options/OptionsPageGeneralApplication.h \
    options/OptionsPageGeneralColors.h \
    options/OptionsPageGeneralNetwork.h \
    options/OptionsPagePlotGeometry.h \
    options/OptionsPagePlotAxes.h \
    options/OptionsPagePlotHistogram.h



SOURCES +=  \
    options/OptionsDialog.cpp \
    options/OptionsPage.cpp \
    options/Options.cpp \
    options/OptionsPageGeneral.cpp \
    options/OptionsPagePlot.cpp \
    options/OptionsPagePlotGeneral.cpp \
    options/OptionsPageTable.cpp \
    options/OptionsPageGeneralApplication.cpp \
    options/OptionsPageGeneralColors.cpp \
    options/OptionsPageGeneralNetwork.cpp \
    options/OptionsPagePlotGeometry.cpp \
    options/OptionsPagePlotAxes.cpp \
    options/OptionsPagePlotHistogram.cpp

