#include "OptionsPageGeneralApplication.h"

OptionsPageGeneralApplication::OptionsPageGeneralApplication()
{
    QGroupBox *configGroup = new QGroupBox(tr("Memory settings"));

    QLabel *memoryLabel = new QLabel(tr("Memory preallocation:"));
    QLabel *memoryPercent = new QLabel(tr("%"));
    m_memorySpinbox = new QSpinBox();
    m_memorySpinbox->setValue(m_options.getPreallocatedMemoryPercent());
    m_memorySpinbox->setMinimum(1);
    m_memorySpinbox->setMaximum(100);


    QHBoxLayout *memoryLayout = new QHBoxLayout();
    memoryLayout->addWidget(memoryLabel);
    memoryLayout->addWidget(m_memorySpinbox);
    memoryLayout->addWidget(memoryPercent);

    // Out of memory

    m_doNothingOption = new QRadioButton("Do nothing");
    m_removeFirstRowAndAppendOption = new QRadioButton("Remove old data");
    m_allocateNextPool = new QRadioButton("Allocate next pool with size:");

    QGroupBox *outOfMemGroup = new QGroupBox(tr("Out of memory settings"));
    QVBoxLayout *outOfMemLayout = new QVBoxLayout();
    outOfMemLayout->addWidget(new QLabel("When user requests more memory for new data:"));
    outOfMemLayout->addWidget(m_doNothingOption);
    outOfMemLayout->addWidget(m_removeFirstRowAndAppendOption);
    m_nextPoolSize = new QSpinBox();
    m_nextPoolSize->setMaximum(MAX_NEXT_POOL_SIZE);
    m_nextPoolSize->setMinimum(MIN_NEXT_POOL_SIZE);
    m_nextPoolSize->setValue(m_options.getNextPoolSize() / 1024 / 1024);

    QHBoxLayout *allocateLayout = new QHBoxLayout();
    allocateLayout->addWidget(m_allocateNextPool);
    allocateLayout->addWidget(m_nextPoolSize);
    allocateLayout->addWidget(new QLabel("MB"));
    outOfMemLayout->addLayout(allocateLayout);

    switch(m_options.getOutOfMemoryAction())
    {
        case DoNothing:
            m_doNothingOption->setChecked(true);
        break;
        case RemoveFirstAndAppend:
            m_removeFirstRowAndAppendOption->setChecked(true);
        break;
        case AllocateNextPool:
            m_allocateNextPool->setChecked(true);
        break;

    }

    outOfMemGroup->setLayout(outOfMemLayout);

    QVBoxLayout *configLayout = new QVBoxLayout();
    configLayout->setStretch(0,1);
    configLayout->addLayout(memoryLayout);
    configGroup->setLayout(configLayout);


    QVBoxLayout *mainLayout = createMainLayout();
    mainLayout->addWidget(configGroup);
    mainLayout->addWidget(outOfMemGroup);
    setLayout(mainLayout);
}

void OptionsPageGeneralApplication::save()
{
    double maximumMemoryLoadInPercent = m_memorySpinbox->value();
    m_options.setMaximumMemoryLoadInPercent(maximumMemoryLoadInPercent);

    if(m_doNothingOption->isChecked())
    {
        m_options.setOutOfMemoryAction(DoNothing);
    }
    else
    {
        if(m_allocateNextPool->isChecked())
        {
            m_options.setOutOfMemoryAction(AllocateNextPool);
            m_options.setNextPoolSize(m_nextPoolSize->value());
        }
        else
            m_options.setOutOfMemoryAction(RemoveFirstAndAppend);
    }

}
