#ifndef OPTIONS_H
#define OPTIONS_H

#include <cstdlib>
#include <map>
#include <vector>
#include <fstream>
#include <stdint.h>

#include <QDebug>
#include <QtDebug>
#include <QColor>
#include <QString>
#include <QDir>
#include <QDataStream>

#include <pcap.h>
#include "../core/Network.h"
#include <qwt_plot.h>



enum OutOfMemoryAction
{
    DoNothing,
    RemoveFirstAndAppend,
    AllocateNextPool
};

struct AxisVisibility
{
    AxisVisibility() :
        isVisible(true),
        isLabelVisible(true)
    {}

    friend QDataStream& operator << (QDataStream& stream, const AxisVisibility &);
    friend QDataStream& operator >> (QDataStream& stream, const AxisVisibility &);

    friend QDataStream & operator >> (QDataStream& stream, QwtPlot::Axis & axis);

    bool isVisible;
    bool isLabelVisible;
};


class Options
{

public :
    static const QColor HIGHLIGHT_WIDGET_COLOR;

    static const double DEFAULT_MAXIMUM_MEMORY_LOAD;
    static const unsigned int DEFAULT_PORT;
    static const unsigned int MAX_PLOTS = 20;
    static const unsigned int DEFAULT_GEOMETRY_PLOT_WIDTH = 400;
    static const unsigned int DEFAULT_GEOMETRY_PLOT_HEIGHT = 300;
    static const unsigned int DEFAULT_IMAGE_WIDTH = 800;
    static const unsigned int DEFAULT_IMAGE_HEIGHT = 600;
    static const unsigned int DEFAULT_NUMBER_OF_BINS = 100;


private:

    class OptionsDetails
    {
        OptionsDetails();

        // Network

        QStringList networkDevices;
        quint32 port;

        // Memory

        quint64 nextPoolSize;
        double maximumMemoryLoadInPercent;
        OutOfMemoryAction outOfMemoryAction;

        QColor indexedColors[MAX_PLOTS];

        // Plots

        QSize geometryPlotSize;
        QSize saveAsImageSize;
        std::map<QwtPlot::Axis, AxisVisibility> axisVisibility;
        unsigned int numberOfBins;
        QString binsType;

        friend class Options;

    public:
        void serialize(QDataStream& stream);
        void deserialize(QDataStream& stream);
    };

private:
    OptionsDetails m_optionsDetails;

    std::string m_applicationName;
    std::string m_applicationLocalConfigDir;
    std::string m_applicationLocalConfigFilename;
    std::string m_applicationLocalConfigFilepath;

public:
    static Options& instance();

    void load();
    void save();

    // Setters

    void setMaximumMemoryLoadInPercent(double value);
    void setOutOfMemoryAction(OutOfMemoryAction action);
    void setNetworkDevices(const QStringList & name);
    void setPort(uint32_t port);
    void setNextPoolSize(uint64_t size);
    void setGeometryPlotSize(const QSize& size);
    void setSaveAsImageSize(const QSize& size);
    void setAxisVisibility(QwtPlot::Axis axis, bool visible);
    void setAxisLabelVisibility(QwtPlot::Axis axis, bool visible);
    void setNumberOfBins(unsigned int bins);
    void setBinsType(const QString & type);

    // Getters

    double getPreallocatedMemoryPercent();
    OutOfMemoryAction getOutOfMemoryAction();
    QStringList &getNetworkDevices();
    quint32 getPort();
    quint64 getNextPoolSize();
    QSize getGeometryPlotSize();
    QSize getSaveAsImageSize();
    bool getAxisVisibility(QwtPlot::Axis axis);
    bool getAxisLabelVisibility(QwtPlot::Axis axis);
    unsigned int getNumberOfBins();
    QString getBinsType();


    // Colors

    const QColor& getColor(unsigned short i) const;
    QColor getKeywordHighlightColor();
    QColor getCommentColor();
    QColor getQuotationColor();
    QColor getNumericColor();

private:

    Options();
    Options(const Options&);
    Options& operator=(const Options&);
    ~Options() {}

    void prepareDefaultColors();
    void prepareDefaultConfiguration();
    bool isLocalConfigFileExists();
    bool isLocalConfigDirExists();
    void loadLocalConfigFile();

};

#endif // OPTIONS_H
