#ifndef OPTIONSPAGEGENERALAPPLICATION_H
#define OPTIONSPAGEGENERALAPPLICATION_H

#include "OptionsPage.h"

class OptionsPageGeneralApplication : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPageGeneralApplication();
    void save();

private:

    static const int MAX_NEXT_POOL_SIZE = 1024;
    static const int MIN_NEXT_POOL_SIZE = 1;


    QSpinBox * m_memorySpinbox;
    QRadioButton * m_removeFirstRowAndAppendOption;
    QRadioButton * m_allocateNextPool;
    QSpinBox * m_nextPoolSize;
    QRadioButton * m_doNothingOption;
};

#endif // OPTIONSPAGEGENERALAPPLICATION_H
