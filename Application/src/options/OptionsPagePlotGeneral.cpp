#include "OptionsPagePlotGeneral.h"

OptionsPagePlotGeneral::OptionsPagePlotGeneral()
{
    // Background

    QGroupBox *backgroundGroup = new QGroupBox(tr("Background"));
    QGridLayout *backgroundGroupLayout = new QGridLayout();

    const int SLIDER_MAXIMUM_WIDTH = 100;
    // Background color

    QLabel * backgroundColorLabel = new QLabel("Backround color");
    m_backgroundRect = new ColorRect();
    m_backgroundColorButton = new QPushButton("Change...");
    QLabel * backgroundOpacityLabel = new QLabel("Opacity");
    m_backgroundOpacitySlider = new QSlider(Qt::Horizontal);
    m_backgroundOpacitySlider->setMinimumWidth(SLIDER_MAXIMUM_WIDTH);
    m_backgroundOpacitySlider->setMaximumWidth(SLIDER_MAXIMUM_WIDTH);
    m_backgroundOpacitySlider->setRange(0,100);
    m_backgroundOpacitySpinbox = new QSpinBox();
    m_backgroundOpacitySpinbox->setSuffix(" %");
    m_backgroundOpacitySpinbox->setRange(0,100);
    m_backgroundOpacitySpinbox->setSpecialValueText(tr("Transparent"));

    connect(m_backgroundOpacitySpinbox, SIGNAL(valueChanged(int)),m_backgroundOpacitySlider,SLOT(setValue(int)) );
    connect(m_backgroundOpacitySlider,SIGNAL(valueChanged(int)),m_backgroundOpacitySpinbox,SLOT(setValue(int)) );

    backgroundGroupLayout->addWidget(backgroundColorLabel,0,0);
    backgroundGroupLayout->addWidget(m_backgroundRect,0,1);
    backgroundGroupLayout->addWidget(m_backgroundColorButton,0,2);
    backgroundGroupLayout->addWidget(backgroundOpacityLabel,0,3);
    backgroundGroupLayout->addWidget(m_backgroundOpacitySlider,0,4,Qt::AlignLeft);
    backgroundGroupLayout->addWidget(m_backgroundOpacitySpinbox,0,5,Qt::AlignLeft);

    // Canvas color

    QLabel * canvasColorLabel = new QLabel("Canvas color");
    m_canvasRect = new ColorRect();
    m_canvasColorButton = new QPushButton("Change...");
    QLabel * canvasOpacityLabel = new QLabel("Opacity");
    m_canvasOpacitySlider = new QSlider(Qt::Horizontal);
    m_canvasOpacitySlider->setMinimumWidth(SLIDER_MAXIMUM_WIDTH);
    m_canvasOpacitySlider->setMaximumWidth(SLIDER_MAXIMUM_WIDTH);
    m_canvasOpacitySlider->setRange(0,100);
    m_canvasOpacitySpinbox = new QSpinBox();
    m_canvasOpacitySpinbox->setSuffix(" %");
    m_canvasOpacitySpinbox->setRange(0,100);
    m_canvasOpacitySpinbox->setSpecialValueText(tr("Transparent"));

    connect(m_canvasOpacitySpinbox, SIGNAL(valueChanged(int)),m_canvasOpacitySlider,SLOT(setValue(int)) );
    connect(m_canvasOpacitySlider,SIGNAL(valueChanged(int)),m_canvasOpacitySpinbox,SLOT(setValue(int)) );


    backgroundGroupLayout->addWidget(canvasColorLabel,1,0);
    backgroundGroupLayout->addWidget(m_canvasRect,1,1);
    backgroundGroupLayout->addWidget(m_canvasColorButton,1,2);
    backgroundGroupLayout->addWidget(canvasOpacityLabel,1,3);
    backgroundGroupLayout->addWidget(m_canvasOpacitySlider,1,4,Qt::AlignLeft);
    backgroundGroupLayout->addWidget(m_canvasOpacitySpinbox,1,5,Qt::AlignLeft);

    // Join

    QHBoxLayout * outerBackgroundGroupLayout = new QHBoxLayout;
    outerBackgroundGroupLayout->addLayout(backgroundGroupLayout);
    outerBackgroundGroupLayout->setAlignment(Qt::AlignLeft);
    backgroundGroup->setLayout(outerBackgroundGroupLayout);


    QVBoxLayout * mainLayout = createMainLayout();
    mainLayout->addWidget(backgroundGroup);
    setLayout(mainLayout);
}
