#ifndef OPTIONSPAGEGENERALNETWORK_H
#define OPTIONSPAGEGENERALNETWORK_H

#include "OptionsPage.h"

class OptionsPageGeneralNetwork: public OptionsPage
{
    Q_OBJECT

public:
    OptionsPageGeneralNetwork();
    void save();

private:
    QGridLayout * m_nicGridLayout;
    QComboBox * m_nicCombobox;
    QSpinBox * m_portSpinbox;
    std::vector<NetworkDevice> m_devs;


};

#endif // OPTIONSPAGEGENERALNETWORK_H
