#ifndef OPTIONSPAGETABLE_H
#define OPTIONSPAGETABLE_H

#include "OptionsPage.h"

class OptionsPageTable : public OptionsPage
{
public:
    OptionsPageTable();

private:
    ColorRect * m_backgroundRect;
    QPushButton * m_backgroundColorButton;

    ColorRect *m_textRect;
    QPushButton * m_textColorButton;

    ColorRect *m_labelRect;
    QPushButton * m_labelColorButton;

    QPushButton * m_textFontButton;
    QPushButton * m_labelsFontButton;

};

#endif // OPTIONSPAGETABLE_H
