#ifndef OPTIONSPAGEGENERAL_H
#define OPTIONSPAGEGENERAL_H

#include "OptionsPage.h"

#include "OptionsPageGeneralApplication.h"
#include "OptionsPageGeneralColors.h"
#include "OptionsPageGeneralNetwork.h"


class OptionsPageGeneral : public OptionsPage
{
    Q_OBJECT

public:
    OptionsPageGeneral();
    void save();

private:
    OptionsPageGeneralApplication * m_tabApp;
    OptionsPageGeneralColors * m_tabColors;
    OptionsPageGeneralNetwork * m_tabNetwork;



};

#endif // OPTIONSPAGEGENERAL_H
