#include "OptionsPage.h"

OptionsPage::OptionsPage(QWidget *parent) :
    QWidget(parent), m_options(Options::instance())
{    
}

QVBoxLayout *OptionsPage::createMainLayout()
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);
    return mainLayout;
}

void OptionsPage::colorButton(QPushButton & button, QColor color)
{
    QString s("background: #"
        + QString(color.red() < 16? "0" : "") + QString::number(color.red(),16)
        + QString(color.green() < 16? "0" : "") + QString::number(color.green(),16)
        + QString(color.blue() < 16? "0" : "") + QString::number(color.blue(),16) + ";");
    button.setStyleSheet(s);
    button.update();
}
