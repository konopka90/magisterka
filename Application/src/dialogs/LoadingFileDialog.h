#ifndef LOADINGFILEDIALOG_H
#define LOADINGFILEDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QCloseEvent>
#include <QApplication>

class LoadingFileDialog : public QDialog
{
    Q_OBJECT
public:
    LoadingFileDialog(const QString& filename, QWidget * parent = 0);


protected:
    void closeEvent(QCloseEvent * e );

public slots:
    void updatePercent(unsigned int status);

signals:
    void closeSignal();

private:
    QString m_filename;
    unsigned int m_percent;

    QLabel * m_filenameLabel;
    QProgressBar * m_progressBar;
    QLabel * m_progressLabel;

};

#endif // LOADINGFILEDIALOG_H
