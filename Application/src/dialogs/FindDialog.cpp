#include "FindDialog.h"

#include "../core/MainWindow.h"

FindDialog::FindDialog(MainWindow * parent) :
    QDialog(parent),
    m_mainWindow(parent)
{
    setWindowTitle(tr("Find"));
    setSizeGripEnabled( true );

    QGridLayout * topLayout = new QGridLayout();
    QGridLayout * bottomLayout = new QGridLayout();

    topLayout->addWidget( new QLabel(tr( "Find" )), 1, 0 );
    m_findComboBox = new QComboBox();
    m_findComboBox->setEditable(true);
    m_findComboBox->setDuplicatesEnabled(false);
    m_findComboBox->setInsertPolicy( QComboBox::InsertAtTop );
    m_findComboBox->setAutoCompletion(true);
    m_findComboBox->setMaxCount ( 10 );
    m_findComboBox->setMaxVisibleItems ( 10 );
    m_findComboBox->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
    topLayout->addWidget( m_findComboBox, 1, 1, 1, 4 );

    m_boxCaseSensitive = new QCheckBox(tr("Case &Sensitive"));
    m_boxCaseSensitive->setChecked(false);
    bottomLayout->addWidget( m_boxCaseSensitive, 0, 0 );

    m_boxPartialMatch = new QCheckBox(tr("&Partial Match Allowed"));
    m_boxPartialMatch->setChecked(true);
    bottomLayout->addWidget( m_boxPartialMatch, 1, 0 );

    m_buttonFind = new QPushButton(tr("&Find"));
    m_buttonFind->setDefault( true );
    bottomLayout->addWidget( m_buttonFind, 0, 1 );

    m_buttonCancel = new QPushButton(tr("&Close"));
    bottomLayout->addWidget( m_buttonCancel, 1, 1 );
    bottomLayout->setColumnStretch(4, 1);


    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);
    mainLayout->addStretch();

    // signals and slots connections
    connect( m_buttonFind, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( m_buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );

}

void FindDialog::accept()
{
    MdiSubWindowWrapper window = m_mainWindow->windowsManager().findWindow(
                m_findComboBox->currentText(),
                m_boxPartialMatch->isChecked(),
                m_boxCaseSensitive->isChecked());

    m_mainWindow->windowsManager().activateMdiWindow(window);

    // add the combo box's current text to the list when the find button is pressed
    QString text = m_findComboBox->currentText();
    if(!text.isEmpty() && m_findComboBox->findText(text) == -1)
    {
            m_findComboBox->insertItem(0, text);
            m_findComboBox->setCurrentIndex(0);
    }
}
