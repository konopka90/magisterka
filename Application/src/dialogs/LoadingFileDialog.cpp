#include "LoadingFileDialog.h"

LoadingFileDialog::LoadingFileDialog(const QString& filename, QWidget *parent) :
    m_filename(filename),
    QDialog(parent)
{

    QVBoxLayout * mainLayout = new QVBoxLayout();

    m_filenameLabel = new QLabel(filename);
    m_progressBar = new QProgressBar();
    m_progressBar->setMinimum(0);
    m_progressBar->setMaximum(100);
    m_progressBar->setMinimumWidth(300);
    m_progressBar->setMaximumWidth(300);


    QGridLayout * grid = new QGridLayout();
    grid->addWidget(new QLabel(tr("Loading:")),0,0,1,1, Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Status:")),1,0,1,1, Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Progress:")),2,0,1,1, Qt::AlignRight);

    grid->addWidget(m_filenameLabel,0,1);
    grid->addWidget(m_progressBar,1,1);

    mainLayout->addLayout(grid);

    QPushButton * m_stopButton = new QPushButton(tr("Stop"));
    m_stopButton->setMinimumSize(100,30);
    m_stopButton->setMaximumSize(100,30);
    connect(m_stopButton, SIGNAL(clicked()), this, SLOT(close()));

    mainLayout->addWidget(m_stopButton,0, Qt::AlignCenter);

    this->setSizeGripEnabled(false);
    this->setLayout(mainLayout);
}

void LoadingFileDialog::closeEvent(QCloseEvent *e)
{
    emit closeSignal();
    e->accept();
}

void LoadingFileDialog::updatePercent(unsigned int percent)
{
    m_percent = percent;
    this->setWindowTitle(QString("%1% of loading: %2").arg(m_percent).arg(m_filename));
    this->m_progressBar->setValue(percent);
}
