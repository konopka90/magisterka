#include "OneSpinboxDialog.h"

OneSpinboxDialog::OneSpinboxDialog(int startingValue) :
    m_startingValue(startingValue)
{
    QHBoxLayout * horizontalLayout = new QHBoxLayout;
    m_rowLabel = new QLabel(tr("Rows:"));
    m_spinbox = new QSpinBox();
    m_spinbox->setMaximum(INT_MAX);
    m_spinbox->setValue(m_startingValue);

    horizontalLayout->addWidget(m_rowLabel);
    horizontalLayout->addWidget(m_spinbox, 1);

    QDialogButtonBox * buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                          | QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    QVBoxLayout * mainLayout = new QVBoxLayout;
    mainLayout->addLayout(horizontalLayout);
    mainLayout->addWidget(buttonBox);

    setLayout(mainLayout);
}

void OneSpinboxDialog::setLabelText(const QString &text)
{
    m_rowLabel->setText(text);
}

void OneSpinboxDialog::setSpinBoxMin(int min)
{
    m_spinbox->setMinimum(min);
}

void OneSpinboxDialog::setSpinBoxMax(int max)
{
    m_spinbox->setMaximum(max);
}

int OneSpinboxDialog::getValue()
{
    return m_spinbox->value();
}
