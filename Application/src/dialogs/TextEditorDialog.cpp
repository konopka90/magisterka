#include "TextEditorDialog.h"
#include "../plot/Plot.h"

TextEditorDialog::TextEditorDialog(Plot * plot) :
    m_plot(plot),
    m_editType(Title),
    QDialog(plot)
{
    createWidgets();
    setup();
}

TextEditorDialog::TextEditorDialog(Plot *plot, QwtScaleWidget *scaleWidget):
    m_plot(plot),
    m_scaleWidget(scaleWidget),
    m_editType(Axis),
    QDialog(plot)
{
    createWidgets();
    setup();
}

void TextEditorDialog::createWidgets()
{
    setModal(true);

    // Color/Alignment/Font

    QGridLayout * grid = new QGridLayout;

    QLabel * colorLabel = new QLabel(tr("Text Color"));
    m_colorRect = new ColorRect();

    QLabel * fontLabel = new QLabel(tr("Font"));
    m_fontButton = new QPushButton(tr("Change..."));

    QLabel * alignmentLabel = new QLabel(tr("Alignment"));
    m_alignmentBox = new QComboBox();
    m_alignmentBox->addItem( tr( "Center" ) );
    m_alignmentBox->addItem( tr( "Left" ) );
    m_alignmentBox->addItem( tr( "Right" ) );

    grid->addWidget(colorLabel,0,0);
    grid->addWidget(m_colorRect,0,1);

    grid->addWidget(fontLabel,1,0);
    grid->addWidget(m_fontButton,1,1);

    grid->addWidget(alignmentLabel,2,0);
    grid->addWidget(m_alignmentBox,2,1);

    QGroupBox * groupA = new QGroupBox;
    groupA->setLayout(grid);


    // Buttons

    m_apply = new QPushButton(tr("Apply"));
    m_close = new QPushButton(tr("Close"));

    QVBoxLayout * verticalA = new QVBoxLayout;
    verticalA->addWidget(m_apply);
    verticalA->addWidget(m_close);
    verticalA->setAlignment(Qt::AlignTop);

    QHBoxLayout * horizontalA = new QHBoxLayout;
    horizontalA->addWidget(groupA);
    horizontalA->addLayout(verticalA);

    m_textEdit = new QTextEdit;

    setFocusPolicy(Qt::StrongFocus);
    setFocusProxy(m_textEdit);

    QVBoxLayout * mainLayout = new QVBoxLayout;
    mainLayout->addLayout(horizontalA);
    mainLayout->addWidget(m_textEdit);
    setLayout(mainLayout);

    connect(m_apply, SIGNAL(clicked()),this, SLOT(apply()));
    connect(m_close, SIGNAL(clicked()), this, SLOT(reject()));
    connect(m_fontButton, SIGNAL(clicked()), this, SLOT(customFont()));
    connect(m_colorRect, SIGNAL(colorChanged(QColor)), this, SLOT(apply()));
    connect(m_alignmentBox, SIGNAL(activated(int)), this, SLOT(apply()));
}

void TextEditorDialog::setup()
{
    QwtText text;
    QFont font;
    QwtPlot & plot = m_plot->qwtPlot();

    if(m_editType == Axis)
    {
        text = m_scaleWidget->title();
        font = m_scaleWidget->font();
        setWindowTitle(tr("Edit Axis Text"));
    }
    else
    {
        text = plot.title();
        font = plot.titleLabel()->font();
        setWindowTitle(tr("Edit Title Text"));
    }

    m_colorRect->setColor(text.color());

    m_selectedFont = font;
    m_textEdit->setText(text.text());
    m_textEdit->setFont(m_selectedFont);
    m_textEdit->setFontPointSize(m_selectedFont.pointSize() + 2);

    setAlignment(text.renderFlags());

}

void TextEditorDialog::apply()
{
    QwtText text;
    QwtTextLabel * label;
    QwtPlot & plot = m_plot->qwtPlot();
    label = plot.titleLabel();

    if(m_editType == Axis)
    {
        text = m_scaleWidget->title();
    }
    else
    {
        text = plot.title();
    }

    text.setColor(m_colorRect->getColor());
    text.setText(m_textEdit->toPlainText().replace("\n", "<br>"));
    text.setRenderFlags(alignment());

    if(m_editType == Axis)
    {
        text.setFont(m_selectedFont);
        m_scaleWidget->setTitle(text);
    }
    else
    {
        text.setFont(m_selectedFont);
        label->setFont(m_selectedFont);
        plot.setTitle(text);
    }

}

void TextEditorDialog::customFont()
{
    bool ok;
    QFont fnt = QFontDialog::getFont( &ok, m_selectedFont, this);
    if (ok && fnt != m_selectedFont)
    {
        m_selectedFont = fnt;
        m_textEdit->setFont(fnt);
        apply();
    }
}

void TextEditorDialog::setAlignment(int align)
{
    m_alignmentBox->blockSignals(true);
    switch(align){
        case Qt::AlignHCenter:
            m_alignmentBox->setCurrentIndex(0);
            break;
        case Qt::AlignLeft:
            m_alignmentBox->setCurrentIndex(1);
            break;
        case Qt::AlignRight:
            m_alignmentBox->setCurrentIndex(2);
            break;
    }
    m_alignmentBox->blockSignals(false);
}

int TextEditorDialog::alignment() const
{
    int align = -1;
    switch (m_alignmentBox->currentIndex())
    {
        case 0:
            align = Qt::AlignHCenter;
            break;

        case 1:
            align = Qt::AlignLeft;
            break;

        case 2:
            align = Qt::AlignRight;
            break;
    }
    return align;
}
