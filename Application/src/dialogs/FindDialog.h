#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QtWidgets>

class MainWindow;

class FindDialog : public QDialog
{
public:
    FindDialog(MainWindow * parent);

private:
    QComboBox * m_findComboBox;
    QCheckBox * m_boxCaseSensitive;
    QCheckBox * m_boxPartialMatch;
    QPushButton * m_buttonFind;
    QPushButton * m_buttonCancel;
    MainWindow * m_mainWindow;
public slots:
    void accept();
};

#endif // FINDDIALOG_H
