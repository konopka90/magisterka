#ifndef ONESPINBOXDIALOG_H
#define ONESPINBOXDIALOG_H

#include <QtWidgets>
#include <climits>

class OneSpinboxDialog : public QDialog
{
    Q_OBJECT
public:
    explicit OneSpinboxDialog(int startingValue);
    void setLabelText(const QString & text);
    void setSpinBoxMin(int min);
    void setSpinBoxMax(int max);
    int getValue();
private:
    QSpinBox * m_spinbox;
    QLabel * m_rowLabel;
    int m_startingValue;

signals:

public slots:

};

#endif // ONESPINBOXDIALOG_H
