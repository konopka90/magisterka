RESOURCES +=

INCLUDEPATH += dialogs/

HEADERS += \ 
    dialogs/LoadingFileDialog.h \
    dialogs/FindDialog.h \
    dialogs/OneSpinboxDialog.h \
    dialogs/TextEditorDialog.h

SOURCES += \ 
    dialogs/LoadingFileDialog.cpp \
    dialogs/FindDialog.cpp \
    dialogs/OneSpinboxDialog.cpp \
    dialogs/TextEditorDialog.cpp
