#ifndef TEXTEDITORDIALOG_H
#define TEXTEDITORDIALOG_H

#include <QtWidgets>
#include "../widgets/ColorRect.h"
#include <qwt_scale_widget.h>

class Plot;
class TextEditorDialog : public QDialog
{
    Q_OBJECT
public:

    enum EditType
    {
        Title,
        Axis
    };

    TextEditorDialog(Plot * plot);
    TextEditorDialog(Plot * plot, QwtScaleWidget * scaleWidget);

private:
    void createWidgets();
    void setup();
    int alignment() const;
    void setAlignment(int align);

    EditType m_editType;
    QwtScaleWidget * m_scaleWidget;
    Plot * m_plot;
    ColorRect * m_colorRect;
    QPushButton * m_fontButton;
    QComboBox * m_alignmentBox;
    QPushButton * m_apply;
    QPushButton * m_close;
    QTextEdit * m_textEdit;
    QFont m_selectedFont;
signals:

private slots:
    void apply();
    void customFont();
public slots:

};

#endif // TEXTEDITORDIALOG_H
