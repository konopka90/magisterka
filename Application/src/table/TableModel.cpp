#include "TableModel.h"
#include "Table.h"
#include "../core/MainWindow.h"

using namespace boost::math;

TableModel::TableModel(Table * table, DataRowCollectionPtr data)
    : QAbstractItemModel(table),
      m_table(table),
      m_data(data),
      m_options(Options::instance()),
      m_mainWindow(table->getMainWindow()),
      m_rows(0)
{
    m_cols = DataRow::cols();
    m_memoryAction = m_options.getOutOfMemoryAction();
}

QModelIndex TableModel::index(int row, int column, const QModelIndex &) const
{
    return createIndex(row, column);
}

QModelIndex TableModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

int TableModel::rowCount(const QModelIndex &) const
{
    return rowCount();
}

int TableModel::rowCount() const
{
    int size = m_data->size();
    return size;
}

int TableModel::columnCount(const QModelIndex &) const
{
    return m_cols;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{    
    QMutexLocker lock(&m_lock);

    char format = 'g';
    int precision = 13;

    if(index.isValid())
    {
        TableColumnPtr column = m_table->columns().at(index.column());
        precision = column->precision();
        format = TableColumn::numericFormatToChar(column->numericFormat());
    }


    switch(role)
    {
        case Qt::TextAlignmentRole:
            return QVariant(Qt::AlignVCenter | Qt::AlignRight);

        case Qt::DisplayRole:
        {
            int size = m_data->size();

            if(size <= 0)
                return QVariant();

            double value = m_data->at(index.row())->values[index.column()];
            if((isnan)(value))
                return QVariant();

            return QVariant(QString::number(value, format, precision));
        }

        case Qt::EditRole:
        {
            double value = data(index.row(), index.column());
            if((isnan)(value))
                return QVariant();

            QString str = QString::number(value, format, precision);
            return str;
        }

    }

    return QVariant::Invalid;
}

double TableModel::data(int row, int col) const
{
    int size = m_data->size();
    if(size > 0 && row < size && col >= 0 )
    {
        double value = m_data->at(row)->values[col];
        return value;
    }

    return NAN;
}

double TableModel::data_nocheck(int row, int col) const
{
    return m_data->at(row)->values[col];
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    //QMutexLocker lock(&m_lock);

    int row = index.row();
    int col = index.column();

    if(row < 0 || row >= m_data->size())
        return false;

    // Empty string

    if(value == "")
    {
        (*m_data)[row]->values[col] = NAN;
        emit dataChanged(index,index);
        return true;
    }

    // Floating point number

    bool castingResult;
    double newValue = value.toDouble(&castingResult);

    if(!castingResult)
        return false;

    (*m_data)[row]->values[col] = newValue;

    //QModelIndex startOfRow = this->index(row, 0, QModelIndex());
    //QModelIndex endOfRow   = this->index(row, m_cols, QModelIndex());

    //emit dataChanged(startOfRow, endOfRow);

    return true;
}

Qt::ItemFlags TableModel::flags (const QModelIndex &index) const
{
    if(m_table->columns().at(index.column())->readOnly())
        return QAbstractItemModel::flags(index) & (~(1 << Qt::ItemIsEditable));

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}


QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QList<TableColumnPtr>& columns = m_table->columns();

    if(orientation == Qt::Horizontal && columns.size() > 0)
    {
        TableColumnPtr column = columns.at(section);

        if(role == Qt::DisplayRole)
        {
            QString headerLabel = *(column);
            return QVariant( column->name() + " - [" + headerLabel + "]");
        }
        else if(role == Qt::SizeHintRole)
        {
            /*
            QTableView & view = m_table->view();
            QSize size = QSize(view.horizontalHeader()->defaultSectionSize(), view.verticalHeader()->defaultSectionSize());
            if(column->width() > 0)
                size.setWidth(column->width());
            size.setWidth(550);
            size.setHeight(50);

            return QVariant(size);*/

        }
    }

    return QAbstractItemModel::headerData(section,orientation,role);
}

void TableModel::emitHeaderDataChanged(Qt::Orientation orientation, int first, int last)
{
    //QMutexLocker lock(&m_lock);

    emit headerDataChanged(orientation,first,last);
}

void TableModel::moveRowUpward(int row)
{
    if(row < 1)
        return;

    DataRow * data1 = m_data->at(row);
    DataRow * data2 = m_data->at(row-1);
    m_data->at(row) = data2;
    m_data->at(row-1) = data1;

    //m_data->replace(row, data2);
    //m_data->replace(row-1, data1);

    emit dataChanged(QModelIndex(), QModelIndex());
}

void TableModel::moveRowDownward(int row)
{
    if(row >= rowCount() - 1)
        return;

    DataRow * data1 = m_data->at(row);
    DataRow * data2 = m_data->at(row+1);

    m_data->at(row) = data2;
    m_data->at(row+1) = data1;

    //m_data->replace(row, data2);
    //m_data->replace(row+1, data1);

    emit dataChanged(QModelIndex(), QModelIndex());
}

void TableModel::clearCells(const QModelIndexList &indexes)
{
    fillCells(EmptyValues,indexes);
}

void TableModel::clearColumns(const QModelIndexList &indexes)
{
    foreach(QModelIndex idx, indexes)
    {
        clearColumn(idx.column());
    }
}

void TableModel::clearColumn(int column)
{
    int size = m_data->size();
    for(int i = 0 ; i < size ; ++i)
    {
        m_data->at(i)->values[column] = NAN;
    }
}

void TableModel::fillCells(FillType type, const QModelIndexList & indexes)
{
    double val;
    switch(type)
    {
        case RowNumbers:
            foreach(const QModelIndex& index, indexes)
            {
                val = index.row() + 1.0;
                m_data->at(index.row())->values[index.column()] = val;
            }
            break;

        case RandomValues:
            foreach(const QModelIndex& index, indexes)
            {
                val = (double)std::rand() / RAND_MAX;
                m_data->at(index.row())->values[index.column()] = val;
            }
            break;

        case NormalRandomValues:
            foreach(const QModelIndex& index, indexes)
            {
                val = MyMath::randomWithNormalDistribution();
                m_data->at(index.row())->values[index.column()] = val;
            }
            break;

        default:
            val = 0.0;
            break;

    }

    emit dataChanged(QModelIndex(), QModelIndex());
}

void TableModel::fillColumn(int col, TableModel::FillType type)
{
    int start = 0;
    int end = m_data->size();

    double val;
    switch(type)
    {
        case RowNumbers:
            for(int row = start; row < end; row++)
            {
                val = row + 1.0;
                m_data->at(row)->values[col]= val;
            }
            break;

        case RandomValues:
            for(int row = start; row < end; row++)
            {
                val = (double)std::rand() / RAND_MAX;
                m_data->at(row)->values[col]= val;
            }
            break;

        case NormalRandomValues:
            for(int row = start; row < end; row++)
            {
                val = MyMath::randomWithNormalDistribution();
                m_data->at(row)->values[col]= val;
            }
            break;

        default:
            val = 0.0;
            break;

    }
    emit dataChanged(QModelIndex(), QModelIndex());
}

void TableModel::normalizeColumn(int col)
{
    int size = m_data->size();

    if(size <= 0)
        return;

    if(col < 0 || col >= DATAROW_SIZE)
        return;

    // Find min and max

    double min = m_data->at(0)->values[col];
    double max = m_data->at(0)->values[col];
    double current;

    for(int i = 1 ; i < size ; ++i)
    {
        current = m_data->at(i)->values[col];
        if(!(isnan)(current))
        {
            if(current < min)
                min = current;

            if(current > max)
                max = current;
        }
    }

    // Normalize

    for(int i = 0 ; i < size ; ++i)
    {
        current = m_data->at(i)->values[col];
        m_data->at(i)->values[col] = (current - min) / (max - min);
    }

}


void TableModel::clear()
{
    std::deque<DataRow*>::iterator it = m_data->begin();
    while(it != m_data->end())
    {
        (*it)->clear();
        it++;
    }

    emit dataChanged(QModelIndex(),QModelIndex());
}

void TableModel::saveNumberOfRows()
{
    m_rows = m_data->size();
}

bool TableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    QMutexLocker locker(&m_lock);

    if(row == LAST_POSITION)
    {
        for(int i = 0 ; i < count ; i++)
        {
            DataRow * data = new DataRow;
            if(data)
                m_data->push_back(data);
        }
    }
    else
    {
        for(int i = 0 ; i < count ; i++)
        {
            DataRow * data = new DataRow;
            if(data)
                m_data->insert(m_data->begin() + row, data);
        }
    }
    return true;
}

bool TableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    QMutexLocker locker(&m_lock);

    int limit = count + row;
    for(int i = row ; i < limit ; ++i)
    {
        DataRow * data = m_data->at(i);
        delete data;

    }

    m_data->erase(m_data->begin() + row, m_data->begin() + row + count);

    emit dataChanged(QModelIndex(), QModelIndex());

    return true;
}
