#include "Table.h"
#include "../core/MainWindow.h"

using namespace boost::math;

bool compareQModelIndexesByRow(const QModelIndex &a, const QModelIndex & b)
{
    return a.row() < b.row();
}

Table::Table(MainWindow *parent, const DataRowCollectionPtr& data) :
    MdiSubWindow(parent, MdiSubWindow::Table),
    m_mainWindow(parent),
    m_memoryPool(parent->memoryPool()),
    m_data(data),
    m_options(Options::instance()),
    m_isInsertingStarted(false),
    m_memoryAction(Options::instance().getOutOfMemoryAction())
{
    qRegisterMetaType<Qt::Orientation>("Qt::Orientation");

    createModel();
    createView();
    createContextMenuColumns();
    createContextMenuRows();
    resize(800,500);
    setWidget(m_view);
    createColumnObjects();

    connect(this, SIGNAL(aboutToActivate()), this, SLOT(refresh()));
}

void Table::save(QDataStream & stream)
{
    MdiSubWindow::save(stream);

    qDebug() << "Start: Serializing Table";

    // Columns
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        stream << *(m_columns[i].get());
        stream << m_view->columnWidth(i);
        stream << m_view->horizontalHeader()->isSectionHidden(i);
    }

    // Number of rows
    int size = m_model->rowCount();
    stream << size;

    // Rows
    for(int i = 0 ; i < size ; ++i)
    {
        stream << *(m_data->at(i));
    }

    qDebug() << "Stop: Serializing Table";
}

bool compareColumnsByVisualIndex(TableColumnPtr & a, TableColumnPtr & b)
{
    return a->visualIndex() < b->visualIndex();
}

void Table::load(QDataStream &stream)
{
    qDebug() << "Start: Deserializing Table";
    MdiSubWindow::load(stream);

    // Columns
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        int width;
        bool isHidden;
        int oldIndex = m_columns[i]->visualIndex();

        stream >> *(m_columns[i].get());
        stream >> width;
        stream >> isHidden;

        m_view->setColumnWidth(i, width);
        m_view->horizontalHeader()->setSectionHidden(i,isHidden);
        m_view->horizontalHeader()->moveSection(oldIndex,m_columns[i]->visualIndex());

    }

    qSort(m_columnsOrderedByView.begin(), m_columnsOrderedByView.end(), compareColumnsByVisualIndex );

    // Get number of rows
    int size;
    stream >> size;

    // Resize
    this->setNumberOfRows(size);

    // Important !
    // If available memory is insufficient load less rows from file.
    size = this->rows();

    // Rows
    for (int i = 0 ; i < size ; ++i)
    {
        stream >> *m_data->at(i);
    }

    qDebug() << "Stop: Deserializing Table";

}

Table::~Table()
{
}

void Table::createModel()
{
    m_model = new TableModel(this, m_data);
    connect(m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(emitDataChanged()));
}

void Table::createView()
{
    m_view = new QTableView(this);
    m_view->setModel(m_model);
    m_view->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    m_view->horizontalHeader()->setSectionsMovable(true);
    m_view->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    m_view->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_view->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::AnyKeyPressed);
    m_view->viewport()->installEventFilter(this);

    connect(m_view->horizontalHeader(),SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onCustomContextMenuRequestedHorizontal(QPoint)));
    connect(m_view->horizontalHeader(),SIGNAL(sectionMoved(int,int,int)),this,SLOT(onSectionMove(int, int, int)));
    connect(m_view->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onCustomContextMenuRequestedVertical(QPoint)));
}

void Table::createContextMenuColumns()
{
    QAction * setAsX = new QAction(tr("Set as X"),&m_columnMenu);
    QAction * setAsY = new QAction(tr("Set as Y"),&m_columnMenu);
    QAction * columnOptions = new QAction(tr("Column Options..."),&m_columnMenu);
    QAction * setColumnValues = new QAction(tr("Set Column Values..."), &m_columnMenu);
    QAction * recalculate = new QAction(tr("Recalculate"), &m_columnMenu);
    QAction * clear = new QAction(tr("Clear Selected"), &m_columnMenu);
    QAction * hide = new QAction(tr("Hide Selected"), &m_columnMenu);
    QAction * showAll = new QAction(tr("Show All Columns"), &m_columnMenu);
    QMenu * fillSelectionWith = new QMenu(tr("Fill Selection With"));
    QAction * fillWithRowNumbers = fillSelectionWith->addAction("Row Numbers");
    QAction * fillWithRandom = fillSelectionWith->addAction("Random Values");
    QAction * fillWithRandomNormalized = fillSelectionWith->addAction("Normal Random Values");
    QMenu * normalize = new QMenu(tr("Normalize"));
    QAction * normalizeColumns = normalize->addAction("Columns");
    QAction * normalizeTable = normalize->addAction("Table");
    QAction * statisticsOnColumns = new QAction(tr("Statistics on Columns"), &m_columnMenu);

    QAction * sortColumnAsc = new QAction(tr("Sort Column Ascending"), &m_columnMenu);
    QAction * sortColumnDesc = new QAction(tr("Sort Column Descending"), &m_columnMenu);



    m_columnMenu.addMenu(&m_mainWindow->menuPlot());
    m_columnMenu.addAction(setAsX);
    m_columnMenu.addAction(setAsY);
    m_columnMenu.addSeparator();
    m_columnMenu.addAction(setColumnValues);
    m_columnMenu.addAction(recalculate);
    m_columnMenu.addMenu(fillSelectionWith);
    m_columnMenu.addMenu(normalize);
    m_columnMenu.addSeparator();
    m_columnMenu.addAction(statisticsOnColumns);
    m_columnMenu.addSeparator();
    m_columnMenu.addAction(clear);
    m_columnMenu.addAction(hide);
    m_columnMenu.addAction(showAll);
    m_columnMenu.addSeparator();
    m_columnMenu.addAction(sortColumnAsc);
    m_columnMenu.addAction(sortColumnDesc);
    m_columnMenu.addAction(columnOptions);


    connect(setAsX,SIGNAL(triggered()), this, SLOT(onSetAsXClick()));
    connect(setAsY,SIGNAL(triggered()), this, SLOT(onSetAsYClick()));
    connect(columnOptions,SIGNAL(triggered()), this, SLOT(showColumnOptions()));
    connect(setColumnValues, SIGNAL(triggered()), this, SLOT(showSetColumnValues()));
    connect(recalculate, SIGNAL(triggered()), this, SLOT(recalculateSelectedColumns()));
    connect(clear,SIGNAL(triggered()), this, SLOT(clearSelectedColumns()));
    connect(hide,SIGNAL(triggered()), this, SLOT(hideSelectedColumns()));
    connect(showAll,SIGNAL(triggered()), this, SLOT(showAllColumns()));
    connect(fillWithRowNumbers, SIGNAL(triggered()), this, SLOT(onFillWithRowNumbers()));
    connect(fillWithRandom, SIGNAL(triggered()), this, SLOT(onFillWithRandomValues()));
    connect(fillWithRandomNormalized, SIGNAL(triggered()), this, SLOT(fillSelectionWithNormalRandomValues()));
    connect(normalizeColumns, SIGNAL(triggered()), this, SLOT(normalizeSelectedColumns()));
    connect(normalizeTable, SIGNAL(triggered()), this, SLOT(normalize()));

    connect(sortColumnAsc, SIGNAL(triggered()), this, SLOT(sortSelectedColumnAsc()));
    connect(sortColumnDesc, SIGNAL(triggered()), this, SLOT(sortSelectedColumnDesc()));
    connect(statisticsOnColumns, SIGNAL(triggered()), this, SLOT(emitShowStatisticsForColumn()));
}

void Table::createContextMenuRows()
{
    QAction * insertRows = new QAction(tr("Insert rows"), &m_rowMenu);
    QAction * deleteRows = new QAction(tr("Delete rows"), &m_rowMenu);
    QAction * clearRows = new QAction(tr("Clear rows"), &m_rowMenu);
    QAction * moveUp = new QAction(tr("Move row upward"), &m_rowMenu);
    QAction * moveDown = new QAction(tr("Move row downward"), &m_rowMenu);

    m_rowMenu.addAction(moveUp);
    m_rowMenu.addAction(moveDown);
    m_rowMenu.addAction(insertRows);
    m_rowMenu.addAction(deleteRows);
    m_rowMenu.addAction(clearRows);

    connect(insertRows, SIGNAL(triggered()), this, SLOT(insertRowsInPlaceOfSelection()));
    connect(deleteRows, SIGNAL(triggered()), this, SLOT(deleteRows()));
    connect(clearRows, SIGNAL(triggered()), this, SLOT(clearRows()));
    connect(moveUp, SIGNAL(triggered()), this, SLOT(moveRowUpward()));
    connect(moveDown, SIGNAL(triggered()), this, SLOT(moveRowDownward()));
}

TableSelectionRange Table::correctRowRange(TableSelectionRange range)
{
    if(range.minimum < 0)
        range.minimum = 0;

    int size = m_data->size();

    if(range.maximum >= size )
        range.maximum = size - 1;

    return range;
}

double Table::statisticalOperationOnColumn(int col, TableSelectionRange range, StatisticalOperation operation)
{
    if(col < 0 || col >= DATAROW_SIZE)
        return NAN;

    range = correctRowRange(range);


    double result = 0.0;
    double avg  = 0.0;
    double stdDev = 0.0;
    int N = 0;
    int i = 0;
    int cnt = 0;
    int start = range.minimum;
    int end = range.maximum;
    DataRow * row;
    QVector<double> medianContainer;


    switch(operation)
    {
        case VALID_VALUES:
            for(int i = start ; i <= end ; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col])) N++;
            }

            result = N;
            break;

        case AVG:
            for(int i = start ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    result += row->values[col];
                    N++;
                }
            }

            result = result / N;
            break;

        case SUM:

            for(i = start ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    result += row->values[col];
                }
            }
            break;

        case MIN:

            // Find first non-nan value

            do
            {
                row = m_data->at(start);
                result = row->values[col];
            }
            while(IS_NAN(result) && (++start <= end));

            for(i = start + 1 ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    if(row->values[col] < result)
                        result = row->values[col];
                }
            }
            break;
        case MAX:

            do
            {
                row = m_data->at(start);
                result = row->values[col];
            }
            while(IS_NAN(result) && (++start <= end));

            for(i = start + 1 ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    if(row->values[col] > result)
                        result = row->values[col];
                }
            }

            break;

        case VARIANCE:
            avg = statisticalOperationOnColumn(col,range,AVG);

            // Sum of squares

            for(int i = start ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    result += pow(row->values[col] - avg,2.0);
                    N++;
                }
            }
            if(N != 1)
                result = result/(N-1);
            break;

        case STANDARD_DEV:

            stdDev = statisticalOperationOnColumn(col,range,VARIANCE);
            result = sqrt(stdDev);
            break;

        case STANDARD_ERROR:

            stdDev = statisticalOperationOnColumn(col,range,STANDARD_DEV);
            N = columnNumberOfValidValues(col, range);
            if(N > 0)
                result = stdDev / sqrt(N);
            break;

        case MEDIAN:

            N = columnNumberOfValidValues(col, range);
            medianContainer.resize(N);
            for(int i = start ; i <= end; ++i)
            {
                row = m_data->at(i);
                if(IS_NOT_NAN(row->values[col]))
                {
                    medianContainer[cnt] = row->values[col];
                    cnt++;
                }
            }

            qSort(medianContainer.begin(), medianContainer.end());

            if(N > 0)
            {
                if(N % 2 == 0)
                {
                    result = (medianContainer[N/2 - 1] + medianContainer[N/2])/2.0;
                }
                else
                {
                    result = medianContainer[N/2];
                }
            }

            break;

        default:
            qDebug() << "Unknown statisticalOperation";

    }
    return result;
}

void Table::emitShowStatisticsForColumn()
{
    QModelIndexList columns = m_view->selectionModel()->selectedColumns();
    emit showStatisticsForColumns(*this,columns);
}

void Table::createColumnObjects()
{
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        TableColumn::ColumnType type;

        if(i == 0)
            type = TableColumn::X;
        else
            type = TableColumn::Y;


        TableColumnPtr ptr(new TableColumn(type, this));
        ptr->setLogicalIndex(i);
        ptr->setVisualIndex(i);
        ptr->setName(QString::number(i+1));

        m_columns.push_back(ptr);
        m_columnsOrderedByView.push_back(ptr);
    }
}

void Table::setColumnWidth(int index, int width)
{
    if(!isColumnIndexValid(index))
        return;

    m_view->setColumnWidth(index,width);
}

void Table::clearSelectedColumns()
{
    m_mutex.lock();
    QModelIndexList cols = m_view->selectionModel()->selectedColumns();
    m_model->clearColumns(cols);
    m_mutex.unlock();
}

void Table::hideSelectedColumns()
{
    QModelIndexList cols = m_view->selectionModel()->selectedColumns();
    foreach(QModelIndex idx, cols)
    {
        int columnIndex = m_columns[idx.column()]->logicalIndex();
        this->setColumnHidden(columnIndex, true);
    }
}

void Table::showAllColumns()
{
    foreach(TableColumnPtr c, m_columns)
    {
        this->setColumnHidden(c->logicalIndex(), false);
    }
}

void Table::sortSelectedColumnAsc()
{
    sortSelectedColumn(Ascending);
}

void Table::sortSelectedColumnDesc()
{
    sortSelectedColumn(Descending);
}

void Table::sortColumn(int column, Table::SortType type)
{
    if(!isColumnIndexValid(column))
        return;

    TableColumnPtr col = this->columns().at(column);
    if(col->readOnly())
        return;

    int numRows = this->rows();
    int nonEmptyCells = 0;

    QVarLengthArray<int> validCell(numRows);
    QVarLengthArray<double> copiedValues(numRows);

    for (int i = 0; i < numRows; i++)
    {
        double val = value(i,col->logicalIndex());
        if(!(isnan)(val))
        {
            copiedValues[nonEmptyCells] = val;
            validCell[nonEmptyCells] = i;
            nonEmptyCells++;
        }
    }

    if (!nonEmptyCells)
        return;

    validCell.resize(nonEmptyCells);
    copiedValues.resize(nonEmptyCells);

    qSort(copiedValues.begin(), copiedValues.end());

    blockSignals(true);

    if(type == Ascending)
    {
        for (int i=0; i< nonEmptyCells; i++)
        {
           this->setValue(validCell[i], col->logicalIndex(), copiedValues[i]);
        }
    }
    else
    {
        for (int i = nonEmptyCells - 1; i>= 0; i--)
        {
           this->setValue(validCell[nonEmptyCells - i - 1], col->logicalIndex(), copiedValues[i]);
        }
    }

    blockSignals(false);
    this->refresh();
    emitDataChanged();
}

void Table::sortColumn(const QString name, Table::SortType type)
{
    int index = columnIndexByName(name);
    sortColumn(index, type);
}

void Table::showSetColumnValues()
{
    TableColumnPtr column = this->firstSelectedColumn();
    if(column)
    {
        this->selectColumn(column->logicalIndex());

        SetColumnValuesDialog dialog(this);
        dialog.exec();
    }
    else
    {
        QMessageBox::warning(this, tr("Warning"), tr("Please select a column first!"));
    }
}

void Table::normalizeSelectedColumns()
{
    QModelIndexList columns = this->m_view->selectionModel()->selectedColumns();
    foreach(QModelIndex idx, columns)
    {
        m_model->normalizeColumn(idx.column());
    }

}

void Table::normalize()
{
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        m_model->normalizeColumn(i);
    }

    this->refresh();
}

void Table::normalizeColumn(int index)
{
    m_model->normalizeColumn(index);
    this->refresh();
}

void Table::normalizeColumn(const QString &name)
{
    int index = this->columnIndexByName(name);
    if(index >= 0)
    {
        m_model->normalizeColumn(index);
    }
    this->refresh();
}

void Table::fillColumn(int index, TableModel::FillType type)
{
   if(!isColumnIndexValid_showWarning(index)) return;

   m_model->fillColumn(index, type);
}

void Table::fillColumn(const QString &name, TableModel::FillType type)
{
    int index = columnIndexByName(name);
    fillColumn(index, type);
}

void Table::recalculateSelectedColumns()
{
    QModelIndexList indexes = m_view->selectionModel()->selectedColumns();
    qSort(indexes.begin(), indexes.end(), qLess<QModelIndex>());

    foreach(QModelIndex i, indexes)
    {
        this->recalculateColumn(i.column());
    }
}


void Table::loadRandomData()
{
    MemoryPool& pool = m_mainWindow->memoryPool();

    DataRowCollectionPtr dataPtr = data();

    for(uint64_t i = 0 ; i < pool.getNumberOfMaxChunksInPool(); ++i)
    {
            DataRow* dataRow = new DataRow();
            if(dataRow)
            {
                dataRow->values[0] = i;
                dataRow->values[1] = i + rand() % 10 + (rand() % 10 * 0.1) ;
                dataRow->values[2] = i + rand() % 50;
                dataRow->values[3] = rand() % 100;
                dataRow->values[4] = 1;
                dataRow->values[5] = -1;
                dataRow->values[6] = rand() % 2;
                dataPtr->push_back(dataRow);

                if(i % 10000000 == 0)
                {
                    m_mainWindow->updateCaption();
                }
            }
    }

    createColumnObjects();
}


void Table::onCustomContextMenuRequestedHorizontal(QPoint point)
{
    int selectedCount = m_view->selectionModel()->selectedColumns().size();
    if(selectedCount <= 1)
    {
        int colIndex = m_view->columnAt(point.x());
        m_view->selectColumn(colIndex);
    }

    m_columnMenu.exec(QCursor::pos());
}

void Table::onCustomContextMenuRequestedVertical(QPoint point)
{
    int row = m_view->rowAt(point.y());
    int selectedCount = m_view->selectionModel()->selectedRows().size();
    if(selectedCount < 2)
        m_view->selectRow(row);

    m_rowMenu.exec(QCursor::pos());
}

void Table::onSectionMove(int logicalIndex, int oldVisualIndex, int visualIndex)
{
    qDebug() << "from " << oldVisualIndex << ", to " << visualIndex;
    m_columnsOrderedByView.move(oldVisualIndex, visualIndex);
    m_columns[logicalIndex]->setVisualIndex(visualIndex);

    for(int i = 0 ; i < DATAROW_SIZE; ++i)
    {
        m_columnsOrderedByView[i]->setVisualIndex(i);
    }
    refresh();
}

void Table::onSetAsXClick()
{
    setColumnType(TableColumn::X);
}

void Table::onSetAsYClick()
{
    setColumnType(TableColumn::Y);
}

void Table::onFillWithRowNumbers()
{
    fillSelectedCellsWith(TableModel::RowNumbers);
}

void Table::onFillWithRandomValues()
{
    fillSelectedCellsWith(TableModel::RandomValues);
}

void Table::fillSelectionWithNormalRandomValues()
{
    fillSelectedCellsWith(TableModel::NormalRandomValues);
}

void Table::showColumnOptions()
{
    QVector<TableColumnPtr> columns = this->selectedFullyOrNotFullyColumns();
    if(columns.empty())
    {
        QMessageBox::warning(this, tr("Warning"), tr("Please select a column first!"));
        return;
    }

    int minVisualIndex = DATAROW_SIZE;
    int logicalIndex = 0;
    if(columns.size() > 0)
    {
        foreach(TableColumnPtr p, columns)
        {
            if(p && p->visualIndex() < minVisualIndex)
            {
                minVisualIndex = p->visualIndex();
                logicalIndex = p->logicalIndex();
            }
        }

        this->clearSelection();
        this->selectColumn(logicalIndex);
    }

    TableColumnOptionsDialog columnOptions(this);
    columnOptions.exec();
}

void Table::emitDataChanged()
{
    emit dataChangedSignal();
}

bool Table::eventFilter(QObject *object, QEvent *event)
{
    // Table mouse press
    if(object == m_view->viewport() && event->type() == QEvent::MouseButtonPress)
    {
        m_view->selectionModel()->clear();
        return false;
    }

    return MdiSubWindow::eventFilter(object, event);
}

void Table::insertRowsInPlaceOfSelection()
{
    m_mutex.lock();

    QModelIndexList list = m_view->selectionModel()->selectedRows();
    int size  = list.size();
    if(size > 0)
    {
        QModelIndex currentRowIdx = list.first();
        m_model->insertRows(currentRowIdx.row(),size, currentRowIdx);
    }

    m_mutex.unlock();

    m_model->emitHeaderDataChanged(Qt::Horizontal,0,0);
}

void Table::moveRowUpward()
{
    m_mutex.lock();
    QModelIndexList rows = m_view->selectionModel()->selectedRows();
    if(rows.size() > 0)
    {
        m_model->moveRowUpward(rows[0].row());
        m_view->selectRow(rows[0].row() - 1);
    }
    m_mutex.unlock();
}

void Table::moveRowDownward()
{
    m_mutex.lock();
    QModelIndexList rows = m_view->selectionModel()->selectedRows();
    if(rows.size() > 0)
    {
        m_model->moveRowDownward(rows[0].row());
        m_view->selectRow(rows[0].row() + 1);
    }
    m_mutex.unlock();
}

void Table::moveColumnLeft()
{
    TableColumnPtr column = this->firstSelectedColumn();
    if(column)
    {
        m_view->horizontalHeader()->moveSection(column->visualIndex(), column->visualIndex() - 1);
    }
}

void Table::moveColumnRight()
{
    TableColumnPtr column = this->firstSelectedColumn();
    if(column)
    {
        m_view->horizontalHeader()->moveSection(column->visualIndex(), column->visualIndex() + 1);
    }
}

void Table::moveColumnToFirst()
{
    TableColumnPtr column = this->firstSelectedColumn();
    if(column)
    {
        m_view->horizontalHeader()->moveSection(column->visualIndex(), 0);
    }
}

void Table::moveColumnToLast()
{
    TableColumnPtr column = this->firstSelectedColumn();
    if(column)
    {
        m_view->horizontalHeader()->moveSection(column->visualIndex(), DATAROW_SIZE - 1);
    }
}

void Table::clearRows()
{
    m_mutex.lock();
    QModelIndexList list = m_view->selectionModel()->selectedIndexes();
    m_model->clearCells(list);
    m_mutex.unlock();
}

void Table::deleteRows()
{
    m_mutex.lock();
    QModelIndexList selectedRows = m_view->selectionModel()->selectedRows();
    qSort(selectedRows.begin(), selectedRows.end(), compareQModelIndexesByRow);
    QModelIndexList::const_iterator it = selectedRows.constEnd();

    // Find min

    int minRow = selectedRows.first().row();
    foreach(QModelIndex idx, selectedRows)
    {
        if(idx.row() < minRow)
            minRow = idx.row();
    }

    int count = selectedRows.count();
    m_model->removeRows(minRow, count , QModelIndex());
    emit signalRemoveRows(minRow,count);
    /*

    while(it != selectedRows.constBegin())
    {
        it--;
        int row = (*it).row();
        m_model->removeRows(row,1,QModelIndex());
        //emit signalRemoveRows(row,1);
    }*/

    m_mutex.unlock();
}

void Table::clearSelection()
{
    m_view->selectionModel()->clearSelection();
}

void Table::clearColumn(int index)
{
    if(!isColumnIndexValid_showWarning(index))
        return;

    m_mutex.lock();
    m_model->clearColumn(index);
    m_mutex.unlock();
}

void Table::clearColumn(const QString &name)
{
    int index = columnIndexByName(name);
    clearColumn(index);
}


void Table::setColumnType(TableColumn::ColumnType type)
{
    QModelIndexList indexes = m_view->selectionModel()->selectedColumns();

    foreach(QModelIndex index, indexes)
    {
        m_columns.at(index.column())->setType(type);
    }

    updateHeaderNumbers();
}

void Table::updateHeaderNumbers()
{    
    int startNumber = 0;
    foreach(TableColumnPtr ptr, m_columnsOrderedByView)
    {
        if(ptr->type() == TableColumn::X)
        {
            startNumber++;
        }

        int number = (startNumber == 0 ? 1 : startNumber);
        ptr->setHeaderNumber(number);
    }

    refresh();
}

void Table::fillSelectedCellsWith(TableModel::FillType type)
{
    if(m_view->selectionModel()->hasSelection())
    {
        QModelIndexList indexes = m_view->selectionModel()->selectedIndexes();
        m_model->fillCells(type,indexes);
    }

    refresh();
}

void Table::refresh()
{
    //emit m_model->headerDataChanged(Qt::Horizontal,0,m_columns.size() - 1);
    m_model->emitHeaderDataChanged(Qt::Horizontal,0,m_columns.size() - 1);

}

void Table::removeRows(int row, int count)
{
    m_model->removeRows(row, count, QModelIndex());
}

DataRowCollectionPtr Table::data() const
{
    return m_data;
}

QList<TableColumnPtr>& Table::columns()
{
    return m_columns;
}

TableColumnPtr Table::columnByVisualIndex(int index)
{
    if(index < 0)
        return m_columnsOrderedByView[0];

    if(index > DATAROW_SIZE)
        return m_columnsOrderedByView.last();

    return m_columnsOrderedByView[index];
}

int Table::columnIndexByName(const QString &name)
{
    foreach(TableColumnPtr c, m_columns)
    {
        if(c->name() == name)
            return c->logicalIndex();
    }

    return -1;
}

TableModel &Table::model() const
{
    return *m_model;
}

QTableView &Table::view() const
{
    return *m_view;
}

MainWindow * Table::getMainWindow() const
{
    return m_mainWindow;
}

QVector<TableColumnPtr> Table::selectedFullyOrNotFullyColumns() const
{
    QItemSelectionModel * select = m_view->selectionModel();
    QVector<TableColumnPtr> result;

    int count_columns = m_columns.size();

    for(int i = 0 ; i < count_columns ; ++i)
    {
         bool isSelected = select->columnIntersectsSelection(i, QModelIndex());
         if(isSelected)
             result.append(m_columns.at(i));
    }

    return result;
}

TableSelectionRange Table::selectedRange() const
{
    QItemSelectionModel * selection = m_view->selectionModel();
    QModelIndexList selectedIndexes = selection->selectedIndexes();

    if(selectedIndexes.size() <= 0 )
        return TableSelectionRange(-1,-1);

    uint64_t min = std::numeric_limits<uint64_t>::max();
    uint64_t max = 0;

    for(QModelIndexList::const_iterator it = selectedIndexes.begin(); it != selectedIndexes.end() ; ++it)
    {
        if((*it).row() < min) min = (*it).row();
        if((*it).row() > max) max = (*it).row();
    }

    return TableSelectionRange(min, max) ;
}

TableSelectionRange Table::fullRange() const
{
    int numRows = rows();
    if(numRows == 0)
        return TableSelectionRange();

    return TableSelectionRange(0,numRows - 1);
}

TableColumnPtr Table::firstSelectedColumn() const
{
    QModelIndexList columns = m_view->selectionModel()->selectedColumns();
    if(columns.size() > 0)
    {
        return m_columns[columns.at(0).column()];
    }

    return TableColumnPtr();
}

TableColumnPtr Table::getColumnByName(const QString &name) const
{
    foreach(TableColumnPtr ptr, m_columns)
    {
        if(ptr->name() == name)
            return ptr;
    }

    return TableColumnPtr();
}

TableColumnPtr Table::getColumnByIndex(int index) const
{
    if(index >= 0 && index < DATAROW_SIZE)
    {
        return m_columns[index];
    }

    return TableColumnPtr();
}

TableStatistics Table::getColumnStatistics(int index)
{
    TableSelectionRange fullRange(0, m_data->size() - 1);
    TableStatistics stats;
    stats.col = index;
    stats.rows = fullRange;
    stats.min = columnMin(index,fullRange);
    stats.max= columnMax(index,fullRange);
    stats.mean = columnAvg(index, fullRange);
    stats.median = columnMedian(index, fullRange);
    stats.standardDev = columnStandardDev(index, fullRange);
    stats.standardError = columnStandardError(index, fullRange);
    stats.sum = columnSum(index, fullRange);
    stats.variance = columnVariance(index, fullRange);

    return stats;
}

void Table::selectColumn(int index, bool appendToSelection)
{
    if(appendToSelection)
    {
        QItemSelection selectedItems = m_view->selectionModel()->selection();

        m_view->selectColumn(index);
        selectedItems.merge(m_view->selectionModel()->selection(), QItemSelectionModel::Select);
        m_view->selectionModel()->clearSelection();
        m_view->selectionModel()->select(selectedItems, QItemSelectionModel::Select);
    }
    else
    {
        m_view->selectColumn(index);
    }
}

void Table::selectColumnByVisualIndex(int index, bool appendToSelection)
{
    selectColumn(m_columnsOrderedByView[index]->logicalIndex(), appendToSelection);
}

void Table::setColumnName(int index, const QString &name)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    if(isColumnNameValid(name))
    {
        m_columns[index]->setName(name);
        refresh();
    }
    else
    {
        QMessageBox::warning(0, tr("Warning"), tr("Column name is not valid!"), QMessageBox::Ok);
    }
}

void Table::setColumnType(int index, TableColumn::ColumnType type)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setType(type);
    refresh();
}

void Table::setColumnPrecision(int index, unsigned int precision)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setPrecision(precision);
    refresh();
}

void Table::setColumnHidden(int index, bool hidden)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setVisibility(!hidden);
    m_view->setColumnHidden(index, hidden);
    refresh();
}

void Table::setColumnComment(int index, const QString &comment)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setComment(comment);
}

void Table::setColumnReadOnly(int index, bool readOnly)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setReadOnly(readOnly);
}

void Table::setColumnNumericFormat(int index, TableColumn::NumericFormat format)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setNumericFormat(format);
}

bool Table::isAnyRowSelected() const
{
    return m_view->selectionModel()->selectedRows().size() > 0;
}

void Table::goToRow(int index)
{
    if(!isRowIndexValid(index))
        return;

    m_view->selectRow(index);
}

void Table::goToColumn(int index)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_view->selectColumn(index);
}

void Table::sortSelectedColumn(Table::SortType type)
{
    QModelIndexList cols = m_view->selectionModel()->selectedColumns();
    foreach(QModelIndex idx, cols)
    {
        int columnIndex = m_columns[idx.column()]->logicalIndex();
        this->sortColumn(columnIndex, type);
    }
}

double Table::value(int row, int col)
{
    QMutexLocker locker(&m_mutex);

    if(row < m_data->size() && row >= 0 && col >= 0)
    {
        DataRow * d_row = m_data.get()->at(row);
        return d_row->values[col];
    };

    return NAN;
}

double Table::columnAvg(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index,range,AVG);
}

double Table::columnSum(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index,range,SUM);
}

double Table::columnMin(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index,range,MIN);
}

double Table::columnMax(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index,range,MAX);
}

double Table::columnVariance(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index,range,VARIANCE);
}

double Table::columnStandardDev(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index, range, STANDARD_DEV);
}

double Table::columnStandardError(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index, range, STANDARD_ERROR);
}

double Table::columnMedian(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index, range, MEDIAN);
}

int Table::columnWidth(int index) const
{
    return m_view->columnWidth(index);
}

int Table::columnNumberOfValidValues(int index, TableSelectionRange range)
{
    return statisticalOperationOnColumn(index, range, VALID_VALUES);
}

void Table::resizeToContent()
{
    m_view->resizeColumnsToContents();
}

void Table::recalculate()
{
    int size = m_model->rowCount();

    foreach(TableColumnPtr column, m_columns)
    {
        this->recalculateColumn(column->logicalIndex(), TableSelectionRange(0,size-1));
    }

    this->refresh();
}



void Table::recalculateColumn(int index, TableSelectionRange range)
{
    QString formula = m_columns[index]->formula();
    if(formula.isEmpty())
        return;



    double loopVariable;

    try
    {
        ScriptingMuParser parser(m_mainWindow, this);
        parser.setFormula(formula);
        parser.addVariable("i", loopVariable);

        QMap<QString, double> constants = m_columns[index]->constants();
        for(QMap<QString, double>::const_iterator it = constants.begin(); it!=constants.end() ; ++it)
        {
            parser.addConstant(it.key(), it.value());
        }

        parser.compile();

        for(int i = range.minimum ; i <= range.maximum ; ++i)
        {
            loopVariable = i + 1;

            double result = parser.execute();
            m_model->setData(i, index, result);
        }
    }
    catch(mu::Parser::exception_type & e)
    {
        QMessageBox::critical(0, tr("Critical Error"), tr(e.GetMsg().c_str()), QMessageBox::Ok);
    }

    this->refresh();
}

void Table::recalculateColumn(int index)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    this->recalculateColumn(index, TableSelectionRange(0,m_data->size()));
}

void Table::recalculateColumn(const QString &name)
{
    int index = columnIndexByName(name);
    this->recalculateColumn(index);
}

int Table::rows() const
{
    return m_model->rowCount();
}

void Table::setNumberOfRows(int num)
{
    int rowsCount = rows();

    if(num == rowsCount)
        return;

    if(num > rowsCount)
    {
        int diff = num - rowsCount;
        this->startInsertingRows();
        for(int i = 0 ; i < diff ; ++i)
        {
            this->appendRow();
        }
        this->stopInsertingRows();
    }
    else
    {
        int toRemove = rowsCount - num;
        this->removeRows(rowsCount - toRemove, toRemove);
        this->stopInsertingRows();
    }
}

bool Table::isColumnNameValid(const QString &name)
{
    if(name.contains(' ')) return false;
    if(name.contains('[')) return false;
    if(name.contains(']')) return false;
    if(name.contains('(')) return false;
    if(name.contains(')')) return false;
    if(name.contains(',')) return false;

    return true;
}

bool Table::isColumnIndexValid_showWarning(int index)
{
    if(!isColumnIndexValid(index))
    {
        QMessageBox::warning(0, tr("Warning"), tr("Column index out of range!"), QMessageBox::Ok);
        return false;
    }

    return true;
}

void Table::refreshAllTables()
{
    const QVector<Table*> & tables = m_mainWindow->windowsManager().tables();
    foreach(Table* table, tables)
    {
        table->refresh();
    }

}

void Table::setColumnHidden(const QString &name, bool hidden)
{
    int index = columnIndexByName(name);
    setColumnHidden(index, hidden);
}

void Table::setColumnFormula(const QString &name, const QString &formula)
{
    int index = columnIndexByName(name);
    setColumnFormula(index, formula);
}

void Table::setColumnFormula(int index, const QString &formula)
{
    if(!isColumnIndexValid_showWarning(index)) return;

    m_columns[index]->setFormula(formula);
}

void Table::setOnCloseStopCapture(bool flag)
{
    m_onCloseStopCaptureFlag = flag;
}

void Table::clear()
{
    m_model->clear();
}

void Table::startInsertingRows()
{
    if(m_isInsertingStarted)
        return;

    m_isInsertingStarted = true;
    m_refreshCounter = 0;

    // Find other tables

    m_otherTables.clear();
    const QVector<Table*> tables = m_mainWindow->windowsManager().tables();
    foreach(Table* table, tables)
    {
        if(table != this)
            m_otherTables.append(table);
    }
}

void Table::stopInsertingRows()
{
    m_isInsertingStarted = false;
    m_otherTables.clear();
    m_model->saveNumberOfRows();
    refreshAllTables();

}

void Table::appendRow()
{
    if(m_memoryAction == RemoveFirstAndAppend && m_memoryPool.isFull())
    {
        Table * table = this;
        if(m_otherTables.size() > 0)
        {
            while(m_otherTables.size() > 0)
            {
                Table * tmpTable = m_otherTables[0];
                if(tmpTable->rows() == 0)
                    m_otherTables.pop_front();
                else
                {
                    table = tmpTable;
                    break;
                }
            }

        }

        table->removeRows(0,1);
    }

    m_model->insertRows(TableModel::LAST_POSITION, 1, QModelIndex());

    if(m_refreshCounter % 100000 == 0)
    {
        QCoreApplication::processEvents();
        refreshAllTables();
    }

    m_refreshCounter++;
}


bool Table::isColumnIndexValid(int index)
{
    if(index < 0 || index >= DATAROW_SIZE)
        return false;

    return true;
}

bool Table::isRowIndexValid(int index)
{
    if(index < 0 || index >= this->rows())
        return false;

    return true;
}


void Table::onWindowDelete()
{
    m_mainWindow->stopCapture();
}
