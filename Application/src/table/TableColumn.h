#ifndef TABLECOLUMN_H
#define TABLECOLUMN_H

#include <QString>
#include <boost/shared_ptr.hpp>
#include "TableTypes.h"

class TableColumn : public QObject
{
    Q_OBJECT

public:

    enum ColumnType
    {
        X,
        Y
    };

    enum NumericFormat
    {
        Default,
        Decimal,
        Scientific
    };

    static char numericFormatToChar(NumericFormat format);

    TableColumn(ColumnType columnType, Table * parent);

    // Getters

    Table& parent() const;
    ColumnType type() const;
    NumericFormat numericFormat() const;
    unsigned int headerNumber() const;
    unsigned int logicalIndex() const;
    unsigned int visualIndex() const;
    const QString &name() const;
    unsigned int precision() const;
    const QString & comment() const;
    const QString & formula() const;
    const QMap<QString, double> & constants() const;
    bool visibility() const;
    bool readOnly() const;

    QString curveTitle() const;
    operator QString() const;

    // Setters

    void setFormula(const QString& formula);
    void setType(ColumnType columnType);
    void setHeaderNumber(unsigned int number);
    void setLogicalIndex(unsigned int index);
    void setVisualIndex(unsigned int index);
    void setName(const QString & name);
    void setPrecision(unsigned int precision);
    void setComment(const QString & comment);
    void setVisibility(bool visible);
    void setReadOnly(bool readOnly);
    void setNumericFormat(NumericFormat format);

    void addConstant(const QString & name, double value);
    void removeConstant(const QString & name);

    friend QDataStream& operator<<(QDataStream& stream, const TableColumn& column);
    friend QDataStream& operator>>(QDataStream& stream, TableColumn& column);
    friend class Table;

private:
    void init();

private:

    QString m_name;
    QString m_comment;
    Table * m_parent;
    ColumnType m_columnType;
    NumericFormat m_numericFormat;
    QString m_formula;
    QMap<QString, double> m_constants;
    bool m_readOnly;
    bool m_visible;
    int m_headerNumber;
    unsigned int m_visualIndex;
    unsigned int m_logicalIndex;
    unsigned int m_precision;


};

typedef boost::shared_ptr<TableColumn> TableColumnPtr;

#endif // TABLECOLUMN_H
