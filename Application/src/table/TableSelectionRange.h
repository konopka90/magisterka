#ifndef TABLESELECTIONRANGE_H
#define TABLESELECTIONRANGE_H

#include <climits>
#include <QDataStream>
struct TableSelectionRange
{
    // Invalid range
    TableSelectionRange() :
        minimum(-1),
        maximum(-1)
    { }

    TableSelectionRange(int min, int max);

    static TableSelectionRange fullRange();

    friend QDataStream & operator<<(QDataStream & stream, const TableSelectionRange& range)
    {
        stream << range.minimum;
        stream << range.maximum;
        return stream;
    }

    friend QDataStream & operator>>(QDataStream & stream, TableSelectionRange& range)
    {
        stream >> range.minimum;
        stream >> range.maximum;
        return stream;
    }

    int minimum;
    int maximum;
};

#endif // TABLESELECTIONRANGE_H
