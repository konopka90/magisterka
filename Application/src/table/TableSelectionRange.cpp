#include "TableSelectionRange.h"

TableSelectionRange::TableSelectionRange(int min, int max)
    : minimum(min), maximum(max)
{}

TableSelectionRange TableSelectionRange::fullRange()
{
    return TableSelectionRange(0, INT_MAX);
}
