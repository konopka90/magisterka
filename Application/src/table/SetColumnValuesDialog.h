#ifndef SETCOLUMNVALUESDIALOG_H
#define SETCOLUMNVALUESDIALOG_H

#include <QtWidgets>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QGroupBox>
#include <QLabel>
#include <QSpinBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QTableView>
#include <QTableWidget>
#include <QMap>

#include "TableColumn.h"

class Table;
class ScriptEditor;
class ScriptingEnvCreator;
class ScriptingMuParser;

class SetColumnValuesDialog : public QDialog
{
    Q_OBJECT
public:
    SetColumnValuesDialog(Table *parent);

private:

    void initTableConstants();
    void updateGUIWithColumn(int index);
    void fillComboboxes();
    QString generateColumnString(const QString & name);
    void addConstantToTable(const QString & name, double value);

private:
    Table * m_table;
    TableColumnPtr m_column;

    QTextEdit * m_description;
    QSpinBox * m_from;
    QSpinBox * m_to;
    QComboBox * m_functionComboBox;
    QComboBox * m_columnComboBox;
    QPushButton * m_nextColumnButton;
    QPushButton * m_previousColumnButton;
    QPushButton * m_addFunctionButton;
    QPushButton * m_addCellButton;
    QPushButton * m_applyButton;
    QPushButton * m_closeButton;
    QPushButton * m_clearButton;
    QLabel * m_actualColumnLabel;
    QTableWidget * m_tableConstants;
    QPushButton * m_addConstantButton;
    QLineEdit * m_constantName;
    QLineEdit * m_constantValue;
    ScriptEditor * m_scriptEditor;

signals:

public slots:

    void insertDescription(int index);
    void addCell();
    void addColumn();
    void addFunction();
    void previousColumn();
    void nextColumn();
    void clearFormulas();
    void apply();

private slots:
    void addConstantClick();
    void removeConstant();
    void reloadConstantsTable();


};

#endif // SETCOLUMNVALUESDIALOG_H
