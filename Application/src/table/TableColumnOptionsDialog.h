#ifndef TABLECOLUMNOPTIONSDIALOG_H
#define TABLECOLUMNOPTIONSDIALOG_H

#include <QtWidgets>
#include "TableColumn.h"

class Table;

class TableColumnOptionsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TableColumnOptionsDialog(Table *table, QWidget * parent = 0);

private:
    Table * m_table;
    TableColumnPtr m_column;

    QLineEdit * m_columnName;
    QCheckBox * m_enumerateAll;
    QPushButton * m_buttonPrev;
    QPushButton * m_buttonNext;
    QPushButton * m_buttonOk;
    QPushButton * m_buttonApply;
    QPushButton * m_buttonCancel;
    QComboBox * m_columnTypeBox;
    QComboBox * m_formatTypeBox;
    QSpinBox * m_precisionBox;
    QSpinBox * m_columnWidth;
    QCheckBox * m_boxReadOnly;
    QCheckBox * m_boxHideColumn;
    QCheckBox * m_applyToRightCols;
    QTextEdit * m_comments;
    QCheckBox * m_applyToAllBox;


signals:

public slots:

private slots:
    void apply();
    void accept();
    void previous();
    void next();
    void updateColumn(unsigned int index);

};

#endif // TABLECOLUMNOPTIONSDIALOG_H
