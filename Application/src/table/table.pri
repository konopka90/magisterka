
INCLUDEPATH += table/

HEADERS +=  table/Table.h \
            table/TableModel.h \
            table/TableColumn.h \
            table/TableSelectionRange.h \
    table/TableTypes.h \
    table/TableColumnOptionsDialog.h \
    table/SetColumnValuesDialog.h \
    table/TableStatistics.h


SOURCES +=  table/Table.cpp \
            table/TableModel.cpp \
            table/TableColumn.cpp \
            table/TableSelectionRange.cpp \
    table/TableColumnOptionsDialog.cpp \
    table/SetColumnValuesDialog.cpp \
    table/TableStatistics.cpp
