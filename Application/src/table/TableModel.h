#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <limits>
#include <QAbstractItemModel>
#include <QMutex>

#include "../options/Options.h"
#include "../core/DataRow.h"
#include "TableColumn.h"


class MainWindow;

class TableModel : public QAbstractItemModel
{
    Q_OBJECT
public:

    explicit TableModel(Table * table, DataRowCollectionPtr data);

    enum FillType { RowNumbers, RandomValues, NormalRandomValues, EmptyValues };

    const static int LAST_POSITION = INT_MAX;

public:

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int rowCount() const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    double data(int row, int col) const;
    double data_nocheck(int row, int col) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    inline bool setData(int row, int col, const QVariant &value)
    {
        if(row < 0 || row >= m_data->size())
            return false;

        // Empty string

        if(value == "")
        {
            (*m_data)[row]->values[col] = NAN;
            return true;
        }

        // Floating point number

        bool castingResult;
        double newValue = value.toDouble(&castingResult);

        if(!castingResult)
            return false;

        (*m_data)[row]->values[col] = newValue;

        return true;
    }

    Qt::ItemFlags flags (const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void emitHeaderDataChanged(Qt::Orientation orientation, int first, int last);
    void refresh()
    {
        emit layoutChanged();
    }

    void moveRowUpward(int row);
    void moveRowDownward(int row);
    void clearCells(const QModelIndexList& indexes);
    void clearColumns(const QModelIndexList& indexes);
    void clearColumn(int column);
    void fillCells(FillType type, const QModelIndexList & indexes);
    void fillColumn(int col, FillType type);
    void normalizeColumn(int col);
    void clear();
    DataRow * last();
private:

    int m_cols;
    int m_rows;
    Table * m_table;
    DataRowCollectionPtr m_data;
    Options &m_options;
    OutOfMemoryAction m_memoryAction;
    MainWindow * m_mainWindow;
    mutable QMutex m_lock;
    QModelIndex m_modelIndex;


    // QAbstractItemModel interface
public:
    inline void beginInsertRows(int row, int count)
    {
        QAbstractItemModel::beginInsertRows(m_modelIndex, row, count);
    }

    inline void endInsertRows()
    {
        QAbstractItemModel::endInsertRows();
    }

    void saveNumberOfRows();

    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);

private:
    void removeRowsDetails(int row, int count);
};

#endif // TABLEMODEL_H
