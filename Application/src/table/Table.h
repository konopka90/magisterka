#ifndef TABLE_H
#define TABLE_H

#include <QMdiSubWindow>
#include <QTableView>
#include <QPushButton>
#include <QMenu>
#include <QHeaderView>
#include <QStyleFactory>
#include <QSet>
#include <QtAlgorithms>
#include <QMutex>
#include <QTimer>

#include <vector>
#include <cstdlib>
#include <limits>
#include <cmath>

#include "../core/MyMath.h"
#include "../core/MdiSubWindow.h"
#include "../core/MemoryPool.h"

#include "../options/Options.h"
#include "TableTypes.h"
#include "TableColumn.h"
#include "TableModel.h"
#include "TableSelectionRange.h"
#include "TableStatistics.h"
#include "TableColumnOptionsDialog.h"
#include "SetColumnValuesDialog.h"
#include "../scripting/ScriptingMuParser.h"

class QApplication;
class TableModel;
class ApplicationWindow;


bool compareQModelIndexesByRow(const QModelIndex & a , const QModelIndex & b);


class Table : public MdiSubWindow
{
    Q_OBJECT
public:

    enum SortType
    {
        Ascending,
        Descending
    };

    enum Axis
    {
        Left,
        Right,
        Bottom,
        Top
    };

    explicit Table(MainWindow *parent, const DataRowCollectionPtr& data);
    void save(QDataStream & stream);
    void load(QDataStream & stream);
    void onWindowDelete();
    ~Table();

public slots:

    void loadRandomData();
    DataRowCollectionPtr data() const;
    QList<TableColumnPtr>& columns();
    TableColumnPtr columnByVisualIndex(int index);
    int columnIndexByName(const QString& name);
    TableModel &model() const;
    QTableView & view() const;
    MainWindow * getMainWindow() const;


    QVector<TableColumnPtr> selectedFullyOrNotFullyColumns() const;
    TableSelectionRange selectedRange() const;
    TableSelectionRange fullRange() const;
    TableColumnPtr firstSelectedColumn() const;
    TableColumnPtr getColumnByName(const QString & name) const;
    TableColumnPtr getColumnByIndex(int index) const;
    TableStatistics getColumnStatistics(int index);

    void refreshAllTables();
    void setOnCloseStopCapture(bool flag);
    void selectColumn(int index, bool appendToSelection = false);
    void selectColumnByVisualIndex(int index, bool appendToSelection = false);
    void setColumnName(int index, const QString& name);
    void setColumnType(int index, TableColumn::ColumnType type);
    void setColumnPrecision(int index, unsigned int precision);
    void setColumnHidden(int index, bool hidden);
    void setColumnHidden(const QString & name, bool hidden);
    void setColumnFormula(const QString & name, const QString & formula);
    void setColumnFormula(int index, const QString & formula);
    void setColumnComment(int index, const QString & comment);
    void setColumnReadOnly(int index, bool readOnly);
    void setColumnNumericFormat(int index, TableColumn::NumericFormat format);
    bool isAnyRowSelected() const;
    void goToRow(int index);
    void goToColumn(int index);
    double value(int row, int col);
    double columnAvg(int index, TableSelectionRange range);
    double columnSum(int index, TableSelectionRange range);
    double columnMin(int index, TableSelectionRange range);
    double columnMax(int index, TableSelectionRange range);
    double columnVariance(int index, TableSelectionRange range);
    double columnStandardDev(int index, TableSelectionRange range);
    double columnStandardError(int index, TableSelectionRange range);
    double columnMedian(int index, TableSelectionRange range);
    int columnWidth(int index) const;
    int columnNumberOfValidValues(int index, TableSelectionRange range);

    void resizeToContent();
    void recalculate();
    void recalculateColumn(int index, TableSelectionRange range);
    void recalculateColumn(int index);
    void recalculateColumn(const QString & name);
    void recalculateSelectedColumns();


    inline void setValue(int row, int col, double value)
    {
        m_mutex.lock();
        m_model->setData(row, col , value);
        m_mutex.unlock();
    }

    int rows() const;
    void clear();

    // Row insert interface

    void startInsertingRows();
    void stopInsertingRows();

    // Appends row at the end of table
    void appendRow();

    // Does not require invoking "startInsertingRows" and "stopInsertingRows" before and after execution.
    void setNumberOfRows(int num);

    void refresh();
    void removeRows(int startRow, int count);
    void insertRowsInPlaceOfSelection();
    void moveRowUpward();
    void moveRowDownward();
    void moveColumnLeft();
    void moveColumnRight();
    void moveColumnToFirst();
    void moveColumnToLast();
    void clearRows();
    void deleteRows();
    void clearSelection();
    void clearColumn(int index);
    void clearColumn(const QString& name);
    void setColumnWidth(int index, int width);
    void clearSelectedColumns();
    void hideSelectedColumns();
    void showAllColumns();
    void sortSelectedColumn(SortType type);
    void sortSelectedColumnAsc();
    void sortSelectedColumnDesc();
    void sortColumn(int column, SortType type);
    void sortColumn(const QString name, SortType type);
    void showSetColumnValues();
    void showColumnOptions();
    void normalizeSelectedColumns();
    void normalize();
    void normalizeColumn(int index);
    void normalizeColumn(const QString & name);

    void fillColumn(int index, TableModel::FillType type);
    void fillColumn(const QString & name, TableModel::FillType type);



    void onCustomContextMenuRequestedHorizontal(QPoint point);
    void onCustomContextMenuRequestedVertical(QPoint point);
    void onSectionMove(int, int, int);
    void onSetAsXClick();
    void onSetAsYClick();
    void onFillWithRowNumbers();
    void onFillWithRandomValues();
    void fillSelectionWithNormalRandomValues();

private:

    enum StatisticalOperation
    {
        AVG,
        SUM,
        MIN,
        MAX,
        STANDARD_DEV,
        STANDARD_ERROR,
        VALID_VALUES,
        MEDIAN,
        VARIANCE
    };

    bool isColumnNameValid(const QString & name);
    bool isColumnIndexValid_showWarning(int index);
    bool isColumnIndexValid(int index);
    bool isRowIndexValid(int index);

    void createModel();
    void createView();
    void createContextMenuColumns();
    void createContextMenuRows();
    void createColumnObjects();

    TableSelectionRange correctRowRange(TableSelectionRange range);
    double statisticalOperationOnColumn(int col, TableSelectionRange range,StatisticalOperation operation);


    void setColumnType(TableColumn::ColumnType type);
    void updateHeaderNumbers();
    void fillSelectedCellsWith(TableModel::FillType type);
    void refreshPeriodically();

    bool eventFilter(QObject* object, QEvent* event);

signals:
    void showStatisticsForColumns(Table& table, QModelIndexList columns);
    void dataChangedSignal();
    void signalRemoveRows(int row,int count);
    void refreshTables();
    void closePlots();

private slots:
    void emitDataChanged();
    void emitShowStatisticsForColumn();

private:

    MainWindow * m_mainWindow;
    QMutex m_mutex;
    Options & m_options;
    MemoryPool & m_memoryPool;
    QTableView * m_view;
    TableModel * m_model;
    DataRowCollectionPtr m_data;
    QList<TableColumnPtr> m_columns;
    QList<TableColumnPtr> m_columnsOrderedByView;
    bool m_isInsertingStarted;
    OutOfMemoryAction m_memoryAction;
    QVector<Table*> m_otherTables;

    QMenu m_columnMenu;
    QMenu m_rowMenu;

    unsigned int m_refreshCounter;
    bool m_onCloseStopCaptureFlag;

};

#endif // TABLE_H
