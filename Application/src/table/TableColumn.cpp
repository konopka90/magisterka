#include "TableColumn.h"
#include "Table.h"

char TableColumn::numericFormatToChar(TableColumn::NumericFormat format)
{
    char result = 'g';

    switch(format)
    {
        case TableColumn::Default:
            result = 'g';
            break;

        case TableColumn::Decimal:
            result = 'f';
            break;

        case TableColumn::Scientific:
            result = 'e';
            break;

        default:
            result = 'g';
            break;
    }

    return result;
}

TableColumn::TableColumn(TableColumn::ColumnType columnType, Table * parent)
    : m_columnType(columnType),
      m_parent(parent),
      m_precision(13),
      m_visible(true),
      m_readOnly(false),
      m_numericFormat(Default)
{
    init();
}

void TableColumn::init()
{
    m_headerNumber = 1;
}

Table &TableColumn::parent() const
{
    return *m_parent;
}

void TableColumn::setType(TableColumn::ColumnType columnType)
{
    m_columnType = columnType;
}

void TableColumn::setHeaderNumber(unsigned int number)
{
    m_headerNumber = number;
}

void TableColumn::setLogicalIndex(unsigned int index)
{
    m_logicalIndex = index;
}

void TableColumn::setVisualIndex(unsigned int index)
{
    m_visualIndex = index;
}

void TableColumn::setName(const QString &name)
{
    m_name = name;
}

void TableColumn::setPrecision(unsigned int precision)
{
    m_precision = precision;
}

void TableColumn::setComment(const QString &comment)
{
    m_comment = comment;
}

void TableColumn::setVisibility(bool visible)
{
    m_visible = visible;
}

void TableColumn::setReadOnly(bool readOnly)
{
    m_readOnly = readOnly;
}

void TableColumn::setNumericFormat(TableColumn::NumericFormat format)
{
    m_numericFormat = format;
}

void TableColumn::addConstant(const QString &name, double value)
{
    m_constants[name] = value;
}

void TableColumn::removeConstant(const QString &name)
{
    m_constants.remove(name);
}

TableColumn::ColumnType TableColumn::type() const
{
    return m_columnType;
}

TableColumn::NumericFormat TableColumn::numericFormat() const
{
    return m_numericFormat;
}

unsigned int TableColumn::logicalIndex() const
{
    return m_logicalIndex;
}

unsigned int TableColumn::visualIndex() const
{
    return m_visualIndex;
}

const QString &TableColumn::name() const
{
    return m_name;
}

unsigned int TableColumn::precision() const
{
    return m_precision;
}

const QString & TableColumn::comment() const
{
    return m_comment;
}

const QString &TableColumn::formula() const
{
    return m_formula;
}

const QMap<QString, double> &TableColumn::constants() const
{
    return m_constants;
}

bool TableColumn::visibility() const
{
    return m_visible;
}

bool TableColumn::readOnly() const
{
    return m_readOnly;
}

QString TableColumn::curveTitle() const
{
    return QString::number(m_logicalIndex+1);
}

void TableColumn::setFormula(const QString &formula)
{
    m_formula = formula;
}

TableColumn::operator QString() const
{
    QString number = QString::number(m_headerNumber);

    if(m_columnType == TableColumn::X)
    {
        return "X" + number;
    }

    return "Y" + number;
}

QDataStream& operator<<(QDataStream& stream, const TableColumn& column)
{
    stream << column.m_name;
    stream << column.m_comment;
    stream << column.m_columnType;
    stream << column.m_headerNumber;
    stream << column.m_numericFormat;
    stream << column.m_formula;
    stream << column.m_constants;
    stream << column.m_readOnly;
    stream << column.m_visualIndex;
    stream << column.m_logicalIndex;
    stream << column.m_precision;

    return stream;
}

QDataStream& operator>>(QDataStream& stream, TableColumn& column)
{
    int enumLoader;

    stream >> column.m_name;
    stream >> column.m_comment;
    stream >> enumLoader;
    column.m_columnType = (TableColumn::ColumnType)enumLoader;
    stream >> column.m_headerNumber;
    stream >> enumLoader;
    column.m_numericFormat = (TableColumn::NumericFormat)enumLoader;
    stream >> column.m_formula;
    stream >> column.m_constants;
    stream >> column.m_readOnly;
    stream >> column.m_visualIndex;
    stream >> column.m_logicalIndex;
    stream >> column.m_precision;

    return stream;
}
