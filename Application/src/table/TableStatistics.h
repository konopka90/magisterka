#ifndef TABLESTATISTICS_H
#define TABLESTATISTICS_H

#include "TableSelectionRange.h"

struct TableStatistics
{
public:

    int col;
    TableSelectionRange rows;

    double mean;
    double min;
    double max;
    double standardDev;
    double standardError;
    double variance;
    double sum;
    double median;
};

#endif // TABLESTATISTICS_H
