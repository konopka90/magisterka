#include "TableColumnOptionsDialog.h"
#include "Table.h"

TableColumnOptionsDialog::TableColumnOptionsDialog(Table *table, QWidget * parent) :
    m_table(table),
    QDialog(parent)
{
    setWindowTitle(tr("Column options"));
    m_column = table->firstSelectedColumn();

    // Column name

    QHBoxLayout *horizontalLayoutA = new QHBoxLayout();
    horizontalLayoutA->addWidget(new QLabel(tr( "Column Name:" )));
    m_columnName = new QLineEdit();
    horizontalLayoutA->addWidget(m_columnName);

    // Buttons

    m_buttonPrev = new QPushButton("&<<");
    m_buttonPrev->setAutoDefault(false);

    m_buttonNext = new QPushButton("&>>");
    m_buttonNext->setAutoDefault(false);

    m_enumerateAll = new QCheckBox(tr("Enumerate all to the right"));
    \
    QHBoxLayout *horizontalLayoutB = new QHBoxLayout();
    horizontalLayoutB->addWidget(m_buttonPrev);
    horizontalLayoutB->addWidget(m_buttonNext);
    horizontalLayoutB->addStretch();

    QVBoxLayout *verticalLayoutA = new QVBoxLayout();
    verticalLayoutA->addLayout(horizontalLayoutA);
    verticalLayoutA->addWidget(m_enumerateAll);
    verticalLayoutA->addLayout(horizontalLayoutB);

    m_buttonOk = new QPushButton(tr( "&OK" ));
    m_buttonOk->setDefault(true);

    m_buttonApply = new QPushButton(tr("&Apply"));
    m_buttonApply->setAutoDefault(false);

    m_buttonCancel = new QPushButton(tr( "&Cancel" ));
    m_buttonCancel->setAutoDefault(false);


    QVBoxLayout  *verticalLayoutB = new QVBoxLayout();
    verticalLayoutB->setSpacing(5);
    verticalLayoutB->setMargin(5);
    verticalLayoutB->addWidget(m_buttonOk);
    verticalLayoutB->addWidget(m_buttonApply);
    verticalLayoutB->addWidget(m_buttonCancel);

    QHBoxLayout  *horizontalLayout1 = new QHBoxLayout();
    horizontalLayout1->setSpacing(5);
    horizontalLayout1->addLayout(verticalLayoutA);
    horizontalLayout1->addLayout(verticalLayoutB);

    // Options

    QGridLayout *gl1 = new QGridLayout();
    gl1->addWidget(new QLabel( tr("Plot Designation:")), 0, 0);

    m_columnTypeBox = new QComboBox();
    m_columnTypeBox->addItem(tr("X (abscissae)"));
    m_columnTypeBox->addItem(tr("Y (ordinates)"));
    gl1->addWidget(m_columnTypeBox, 0, 1);

    m_formatTypeBox = new QComboBox();
    m_formatTypeBox->addItem(tr("Default"));
    m_formatTypeBox->addItem(tr("Decimal: 1000"));
    m_formatTypeBox->addItem(tr("Scientific: 1E3"));

    QLabel * labelFormat = new QLabel(tr("Format:"));
    gl1->addWidget(labelFormat, 2,0);
    gl1->addWidget(m_formatTypeBox, 2,1);


    QLabel * labelNumeric = new QLabel(tr( "Precision:" ));
    gl1->addWidget(labelNumeric, 3, 0);

    m_precisionBox = new QSpinBox();
    m_precisionBox->setRange(0, MAX_PRECISION);
    gl1->addWidget(m_precisionBox, 3, 1);

    m_boxReadOnly = new QCheckBox(tr("&Read-only" ));
    gl1->addWidget(m_boxReadOnly, 4, 0);

    m_boxHideColumn = new QCheckBox(tr("&Hidden" ));
    gl1->addWidget(m_boxHideColumn, 4, 1);

    m_applyToRightCols = new QCheckBox(tr( "Apply to all columns to the right" ));

    QVBoxLayout *verticalLayoutC = new QVBoxLayout();
    verticalLayoutC->addLayout(gl1);
    verticalLayoutC->addWidget(m_applyToRightCols);

    QGroupBox *gb = new QGroupBox(tr("Options"));
    gb->setLayout(verticalLayoutC);

    QHBoxLayout  *horizontalLayout2 = new QHBoxLayout();
    horizontalLayout2->addWidget(new QLabel(tr( "Column Width:" )));

    m_columnWidth = new QSpinBox();
    m_columnWidth->setRange(0, 1000);
    m_columnWidth->setSingleStep(10);
    m_applyToAllBox = new QCheckBox(tr( "Apply to all" ));

    horizontalLayout2->addWidget(m_columnWidth);
    horizontalLayout2->addWidget(m_applyToAllBox);

    m_comments = new QTextEdit();

    // Final layout

    QVBoxLayout* verticalBoxD = new QVBoxLayout();
    verticalBoxD->addLayout(horizontalLayout1);
    verticalBoxD->addWidget(gb);
    verticalBoxD->addLayout(horizontalLayout2);
    verticalBoxD->addWidget(new QLabel(tr( "Comment:" )));
    verticalBoxD->addWidget(m_comments);

    setLayout(verticalBoxD);
    updateColumn(m_column->visualIndex());

    connect(m_buttonNext, SIGNAL(clicked()), this, SLOT(next()));
    connect(m_buttonPrev, SIGNAL(clicked()), this, SLOT(previous()));
    connect(m_buttonOk, SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_buttonApply, SIGNAL(clicked()), this, SLOT(apply()));
    connect(m_buttonCancel,SIGNAL(clicked()), this, SLOT(reject()));
}


void TableColumnOptionsDialog::apply()
{
    int visualIndex = m_column->visualIndex();
    int logicalIndex = m_column->logicalIndex();

    QString name = m_columnName->text();
    m_table->setColumnName(m_column->logicalIndex(), name);

    if(m_enumerateAll->isChecked())
    {
        int inc = 1;
        for(int i = visualIndex ; i < DATAROW_SIZE ; ++i)
        {
            TableColumnPtr column = m_table->columnByVisualIndex(i);
            m_table->setColumnName(column->logicalIndex(), name + QString::number(inc));
            inc++;
        }
    }

    if(m_columnTypeBox->currentIndex() == 0)
        m_table->setColumnType(logicalIndex, TableColumn::X);
    else
        m_table->setColumnType(logicalIndex,TableColumn::Y);

    int prec = m_precisionBox->value();
    m_table->setColumnPrecision(logicalIndex, prec);

    bool hidden = m_boxHideColumn->isChecked();
    m_table->setColumnHidden(logicalIndex, hidden);

    bool readOnly = m_boxReadOnly->isChecked();
    m_table->setColumnReadOnly(logicalIndex, readOnly);

    TableColumn::NumericFormat format = (TableColumn::NumericFormat)m_formatTypeBox->currentIndex();
    m_table->setColumnNumericFormat(logicalIndex, format);

    if(m_applyToRightCols->isChecked())
    {
        for(int i = visualIndex; i < DATAROW_SIZE ; ++i)
        {
            TableColumnPtr column = m_table->columnByVisualIndex(i);

            if(m_columnTypeBox->currentIndex() == 0)
                m_table->setColumnType(logicalIndex, TableColumn::X);
            else
                m_table->setColumnType(logicalIndex,TableColumn::Y);

            m_table->setColumnPrecision(column->logicalIndex(), prec);
            m_table->setColumnHidden(column->logicalIndex(), hidden);
            m_table->setColumnReadOnly(column->logicalIndex(), readOnly);
            m_table->setColumnNumericFormat(column->logicalIndex(), format);
        }
    }

    int width = m_columnWidth->value();

    if(m_applyToAllBox->isChecked())
    {
        for(int i = 0 ; i < DATAROW_SIZE ; ++i)
        {
            TableColumnPtr column = m_table->columnByVisualIndex(i);
            m_table->setColumnWidth(column->logicalIndex(), width);
        }
    }
    else
    {
        m_table->setColumnWidth(logicalIndex, width);
    }

    QString comment = m_comments->toPlainText();
    m_table->setColumnComment(logicalIndex,comment);

}


void TableColumnOptionsDialog::accept()
{
    apply();
    QDialog::accept();
}

void TableColumnOptionsDialog::previous()
{
    apply();
    updateColumn(m_column->visualIndex() - 1);
}

void TableColumnOptionsDialog::next()
{
    apply();
    updateColumn(m_column->visualIndex() + 1);
}

void TableColumnOptionsDialog::updateColumn(unsigned int index)
{
    if (!index)
        m_buttonPrev->setEnabled(false);
    else
        m_buttonPrev->setEnabled(true);

    if (index >= DATAROW_SIZE - 1)
        m_buttonNext->setEnabled(false);
    else
        m_buttonNext->setEnabled(true);


    m_table->selectColumnByVisualIndex(index);
    m_column = m_table->firstSelectedColumn();

    m_columnName->setText(m_column->name());

    if(m_column->type() == TableColumn::X)
        m_columnTypeBox->setCurrentIndex(0);
    else
        m_columnTypeBox->setCurrentIndex(1);

    int width = m_table->columnWidth(m_column->logicalIndex());

    m_boxReadOnly->setChecked(m_column->readOnly());
    m_boxHideColumn->setChecked(!m_column->visibility());
    m_columnWidth->setValue(width);
    m_precisionBox->setValue(m_column->precision());
    m_comments->setText(m_column->comment());

    int formatIndex = m_column->numericFormat();
    m_formatTypeBox->setCurrentIndex(formatIndex);


}

