#include "SetColumnValuesDialog.h"
#include "Table.h"
#include "../scripting/ScriptEditor.h"
#include "../scripting/ScriptingMuParser.h"
#include "../core/MainWindow.h"

SetColumnValuesDialog::SetColumnValuesDialog(Table *parent) :
    QDialog(parent),
    m_table(parent)
{
    setWindowTitle(tr("Set column values"));
    setModal(true);

    m_description = new QTextEdit();
    m_description->setText(tr("Description"));
    m_description->setReadOnly (true);
    m_description->setSizePolicy(QSizePolicy (QSizePolicy::Minimum, QSizePolicy::Minimum));
    m_description->setMaximumHeight(70);

    QGroupBox * groupA = new QGroupBox();
    QLabel * forLabel = new QLabel(tr("For row (i)"));
    QLabel * toLabel = new QLabel(tr("to"));
    m_from = new QSpinBox();
    m_from->setMinimum(1);

    m_to = new QSpinBox();
    m_to->setMinimum(1);

    int rows = m_table->rows();   
    if(rows < 1)
        rows = 1;
    m_to->setMaximum(rows);
    m_to->setValue(rows);

    QHBoxLayout * horizontalLayoutA = new QHBoxLayout;
    horizontalLayoutA->addWidget(forLabel);
    horizontalLayoutA->addWidget(m_from);
    horizontalLayoutA->addWidget(toLabel);
    horizontalLayoutA->addWidget(m_to);


    m_functionComboBox = new QComboBox();
    m_columnComboBox = new QComboBox();

    m_nextColumnButton = new QPushButton(tr(">>"));
    m_previousColumnButton = new QPushButton(tr("<<"));
    m_addFunctionButton = new QPushButton(tr("Add function"));
    m_addCellButton = new QPushButton(tr("Add cell"));

    QHBoxLayout * horizontalLayoutB = new QHBoxLayout;
    horizontalLayoutB->addWidget(m_previousColumnButton);
    horizontalLayoutB->addWidget(m_nextColumnButton);


    QGridLayout * m_gridLayout = new QGridLayout;
    m_gridLayout->addWidget(m_functionComboBox,0,0);
    m_gridLayout->addWidget(m_addFunctionButton,0,1);
    m_gridLayout->addWidget(m_columnComboBox,1,0);
    m_gridLayout->addWidget(m_addCellButton,1,1);
    m_gridLayout->addLayout(horizontalLayoutB,2,0);


    QVBoxLayout * groupLayout = new QVBoxLayout;
    groupLayout->addLayout(horizontalLayoutA);
    groupLayout->addLayout(m_gridLayout);
    groupA->setLayout(groupLayout);

    m_actualColumnLabel = new QLabel(tr("label"));
    m_applyButton = new QPushButton(tr("&Apply"));
    m_closeButton = new QPushButton(tr("&Close"));
    m_clearButton = new QPushButton(tr("Clear &Formulas"));

    QVBoxLayout * verticalLayoutA = new QVBoxLayout;
    verticalLayoutA->addWidget(m_applyButton);
    verticalLayoutA->addWidget(m_clearButton);
    verticalLayoutA->addWidget(m_closeButton);
    verticalLayoutA->setAlignment(Qt::AlignTop);


    QLabel * constantNameLabel = new QLabel(tr("Name:"));
    QLabel * constantValueLabel = new QLabel(tr("Value:"));
    m_addConstantButton = new QPushButton(tr("Add"));
    m_constantName = new QLineEdit;
    m_constantValue = new QLineEdit;
    m_tableConstants = new QTableWidget;

    QHBoxLayout * constantsLayoutHorizontal = new QHBoxLayout;
    constantsLayoutHorizontal->addWidget(constantNameLabel);
    constantsLayoutHorizontal->addWidget(m_constantName);
    constantsLayoutHorizontal->addWidget(constantValueLabel);
    constantsLayoutHorizontal->addWidget(m_constantValue);
    constantsLayoutHorizontal->addWidget(m_addConstantButton);


    QVBoxLayout * constantsLayoutVertical = new QVBoxLayout;
    constantsLayoutVertical->addWidget(m_tableConstants);
    constantsLayoutVertical->addLayout(constantsLayoutHorizontal);

    QGroupBox * groupConstants = new QGroupBox(tr("Constants"));
    groupConstants->setLayout(constantsLayoutVertical);

    QSharedPointer<ScriptingEnv> env = ScriptingEnvCreator::create(m_table->getMainWindow());
    m_scriptEditor = new ScriptEditor(env, m_table->getMainWindow());
    m_scriptEditor->setLineAreaVisibile(false);
    m_scriptEditor->setMaximumHeight(120);

    QHBoxLayout * horizontalLayoutC = new QHBoxLayout;
    horizontalLayoutC->addWidget(m_scriptEditor);
    horizontalLayoutC->addLayout(verticalLayoutA);

    QVBoxLayout * mainLayout = new QVBoxLayout;

    mainLayout->addWidget(groupConstants);
    mainLayout->addWidget(m_description);
    mainLayout->addWidget(groupA);
    mainLayout->addWidget(m_actualColumnLabel);
    mainLayout->addLayout(horizontalLayoutC);
    this->setLayout(mainLayout);

    connect(m_functionComboBox, SIGNAL(activated(int)), this, SLOT(insertDescription(int)));
    connect(m_addCellButton, SIGNAL(clicked()), this, SLOT(addCell()));
    connect(m_addFunctionButton,SIGNAL(clicked()), this, SLOT(addFunction()));
    connect(m_nextColumnButton, SIGNAL(clicked()), this, SLOT(nextColumn()));
    connect(m_previousColumnButton, SIGNAL(clicked()), this, SLOT(previousColumn()));
    connect(m_applyButton, SIGNAL(clicked()), this, SLOT(apply()));
    connect(m_clearButton, SIGNAL(clicked()), this, SLOT(clearFormulas()));
    connect(m_closeButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_addConstantButton, SIGNAL(clicked()), this, SLOT(addConstantClick()));

    m_column = m_table->firstSelectedColumn();
    initTableConstants();
    updateGUIWithColumn(m_column->visualIndex());
    fillComboboxes();
    insertDescription(m_functionComboBox->currentIndex());

}

void SetColumnValuesDialog::initTableConstants()
{
    m_tableConstants->insertColumn(0);
    m_tableConstants->insertColumn(0);
    m_tableConstants->insertColumn(0);

    QStringList labels;
    labels << "Name" << "Value" << "";
    m_tableConstants->setHorizontalHeaderLabels(labels);
    m_tableConstants->setMaximumHeight(120);
}

void SetColumnValuesDialog::updateGUIWithColumn(int index)
{
    reloadConstantsTable();

    if (!index)
        m_previousColumnButton->setEnabled(false);
    else
        m_previousColumnButton->setEnabled(true);

    if (index >= DATAROW_SIZE - 1)
        m_nextColumnButton->setEnabled(false);
    else
        m_nextColumnButton->setEnabled(true);

    m_actualColumnLabel->setText("col(\"" + m_column->name() + "\")=");
    m_scriptEditor->setPlainText(m_column->formula());
}

void SetColumnValuesDialog::fillComboboxes()
{
    // Functions
    const QStringList items = ScriptingMuParser::functionList();
    m_functionComboBox->addItems(items);

    // Columns
    foreach(TableColumnPtr c,m_table->columns())
    {
        m_columnComboBox->addItem(generateColumnString(c->name()));
    }
}

QString SetColumnValuesDialog::generateColumnString(const QString &name)
{
    return tr("col(\"") + name + tr("\",i)");
}

void SetColumnValuesDialog::addConstantToTable(const QString &name, double value)
{
    int rows = m_tableConstants->rowCount();
    m_tableConstants->insertRow(rows);

    QPushButton * remove = new QPushButton("Remove");
    remove->setObjectName(name);
    connect(remove, SIGNAL(clicked()), this, SLOT(removeConstant()));

    m_tableConstants->setCellWidget(rows, 0, new QLabel(name));
    m_tableConstants->setCellWidget(rows, 1, new QLabel(QString::number(value)));
    m_tableConstants->setCellWidget(rows, 2, remove);
}

void SetColumnValuesDialog::insertDescription(int index)
{
    QString functionName = m_functionComboBox->itemText(index);
    QString description = ScriptingMuParser::explainFunction(functionName);
    m_description->clear();
    m_description->insertPlainText(description);
}

void SetColumnValuesDialog::addCell()
{
    m_scriptEditor->insertPlainText(m_columnComboBox->currentText());
}

void SetColumnValuesDialog::addColumn()
{
    m_scriptEditor->insertPlainText(m_columnComboBox->currentText());
}

void SetColumnValuesDialog::addFunction()
{
    m_scriptEditor->insertFunction(m_functionComboBox->currentText());
}

void SetColumnValuesDialog::previousColumn()
{
    int index = m_column->visualIndex();
    m_column = m_table->columnByVisualIndex(--index);
    m_table->selectColumn(m_column->logicalIndex());
    updateGUIWithColumn(index);
}

void SetColumnValuesDialog::nextColumn()
{
    int index = m_column->visualIndex();
    m_column = m_table->columnByVisualIndex(++index);
    m_table->selectColumn(m_column->logicalIndex());
    updateGUIWithColumn(index);
}

void SetColumnValuesDialog::clearFormulas()
{
    m_scriptEditor->clear();
}

void SetColumnValuesDialog::apply()
{
    QString formula = m_scriptEditor->toPlainText();
    m_column->setFormula(formula);
    m_table->recalculateColumn(m_column->logicalIndex(), TableSelectionRange(m_from->value() - 1, m_to->value() - 1));
    m_table->refresh();
}

void SetColumnValuesDialog::addConstantClick()
{
    // Validation

    QMap<QString, double> constants = m_column->constants();
    QString name = m_constantName->text().simplified();

    if(name.isEmpty())
    {
        return;
    }

    if(constants.contains(name))
    {
        QMessageBox::critical(this, tr("Error!"), tr("Cannot add constant - \"") + name + tr("\" already exists!"));
        return;
    }

    QRegExp regexp("[a-zA-Z][a-zA-Z0-9]*");
    if(!regexp.exactMatch(name))
    {
        QMessageBox::critical(this, tr("Error!"), tr("Cannot add constant - invalid name!"));
        return;
    }

    bool result = false;
    double value = m_constantValue->text().toDouble(&result);
    if(!result)
    {
        QMessageBox::critical(this, tr("Error!"), tr("Cannot add constant - value must be type of double!"));
        return;
    }

    addConstantToTable(name, value);
    m_column->addConstant(name,value);
}

void SetColumnValuesDialog::removeConstant()
{
    QPushButton * button =  qobject_cast<QPushButton*>(sender());
    QString name = button->objectName();

    int row = -1;
    for(int i = 0 ; i < m_tableConstants->rowCount() ; ++i)
    {
        QLabel * label = qobject_cast<QLabel*>(m_tableConstants->cellWidget(i,0));
        if(label->text() == name)
        {
            row = i;
            break;
        }
    }

    m_column->removeConstant(name);
    m_tableConstants->removeRow(row);
}

void SetColumnValuesDialog::reloadConstantsTable()
{
    while (m_tableConstants->rowCount() > 0)
    {
        m_tableConstants->removeRow(0);
    }

    QMap<QString, double> constants = m_column->constants();
    QMap<QString, double>::const_iterator it = constants.begin();

    for(; it != constants.end(); ++it)
    {
        addConstantToTable(it.key(), it.value());
    }

}
