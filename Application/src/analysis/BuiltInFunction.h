#ifndef BUILT_IN_H
#define BUILT_IN_H

#include <QObject>
#include <QString>

class BuiltInFunction
{    
public:
    
    enum Type
    {
        Normal,
        MultiPeak,
        Ordered
    };

    BuiltInFunction();
    BuiltInFunction(const QString & tag, const QString & fullName, const QString & explanation, const QString & formula, Type type = Normal);

    operator QString() const
    {
        return fullName;
    }

    QString tag;
    QString fullName;
    QString explanation;
    Type type;

    QString formula();    
private:
    QString m_formula;
};

Q_DECLARE_METATYPE(BuiltInFunction*)

#endif BUILT_IN_H
