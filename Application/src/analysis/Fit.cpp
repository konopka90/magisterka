#include "Fit.h"
#include <QLocale>
#include "../core/MainWindow.h"
#include "../scripting/ScriptingConsole.h"
#include <boost/assign/list_of.hpp>

using namespace mu;
using namespace std;

const gsl_multimin_fminimizer_type *gsl_multimin_fminimizer_nmsimplex2;
const QString Fit::DEFAULT_VARIABLE = "x";
const double Fit::DEFAULT_TOLERANCE = 0.0001;

// Patrzec tu w razie czego http://stackoverflow.com/questions/3701903/initialisation-of-static-vector


QMap<Fit::FitType, BuiltInFunction> makeBuiltInFunctions()
{
    QMap<Fit::FitType, BuiltInFunction> map;
    map.insert(Fit::Boltzman, BuiltInFunction("Boltzman", "Boltzmann (Sigmoidal) Fit", "Boltzmann (Sigmoidal) Fit", "A2+(A1-A2)/(1+exp((x-x0)/dx))"));
    map.insert(Fit::Gauss, BuiltInFunction("Gauss", "Gauss Fit", "Gauss Fit", "y0+A*sqrt(2/PI)/w*exp(-2*((x-xc)/w)^2)"));
    map.insert(Fit::Slope, BuiltInFunction("Slope", "Slope", "Slope", "A*x"));
    map.insert(Fit::Linear, BuiltInFunction("Linear", "Linear", "Linear function", "A*x+b"));
    map.insert(Fit::Logistic, BuiltInFunction("Logistic", "Logistic", "Logistic Fit", "A2+(A1-A2)/(1+(x/x0)^p"));
    map.insert(Fit::Lorentz, BuiltInFunction("Lorentz", "Lorentz Fit", "Lorentz Fit", "y0+2*A/PI*w/(4*(x-xc)^2+w^2)"));
    map.insert(Fit::ExpGrowth, BuiltInFunction("ExpGrowth", "Exponential growth", "Exponential growth", "y0+A*exp(x/t)"));
    map.insert(Fit::ExpDecay1, BuiltInFunction("ExpDecay1", "Exponential First Order", "Exponential decay", "y0+A*exp(-x/t)"));
    map.insert(Fit::ExpDecay2, BuiltInFunction("ExpDecay2", "Exponential Second Order", "Exponential decay", "A1*exp(-x/t1)+A2*exp(-x/t2)+y0"));
    map.insert(Fit::ExpDecay3, BuiltInFunction("ExpDecay3", "Exponential Third Order", "Exponential decay", "A1*exp(-x/t1)+A2*exp(-x/t2)+A3*exp(-x/t3)+y0"));
    map.insert(Fit::MultiPeak, BuiltInFunction("MultiPeak", "Multi Peak Fit", "Multi Peak Fit", "", BuiltInFunction::MultiPeak));
    map.insert(Fit::Polynomial, BuiltInFunction("Polynomial", "Polynomial Fit", "Polynomial Fit", "", BuiltInFunction::Ordered));

    return map;
}

QMap<Fit::FitType, BuiltInFunction> Fit::builtInFunctions = makeBuiltInFunctions();
    
Fit::Fit(QObject *parent) :
    QObject(parent)
{
}

GuessResult Fit::guessParameters(const QString &text)
{
    QString _formula = text;
    QStringList parameters;
    ScriptingMuParser parser;
    ParserTokenReader reader(&parser.getParser());

    try
    {
        std::string stdFormula = _formula.toStdString();
        const char * rawFormula = stdFormula.c_str();

        int length = _formula.length();
        reader.SetFormula(rawFormula);
        reader.IgnoreUndefVar(true);
        int pos = 0;

        while(pos < length)
        {
            ParserToken<value_type, string_type> token = reader.ReadNextToken();
            QString tokenString = QString(token.GetAsString().c_str());

            bool isNumber;
            tokenString.toDouble(&isNumber);


            if (token.GetCode () == cmVAR && tokenString.contains(QRegExp("\\D"))
                && tokenString != DEFAULT_VARIABLE && !parameters.contains(tokenString) && !isNumber)
            {
                if (tokenString.endsWith("e", Qt::CaseInsensitive) &&
                    tokenString.count("e", Qt::CaseInsensitive) == 1){

                    QString aux = tokenString;
                    aux.remove("e", Qt::CaseInsensitive);

                    bool auxIsNumber;
                    aux.toDouble(&auxIsNumber);
                    if (!auxIsNumber)
                        parameters << tokenString;
                } else
                    parameters << tokenString;
            }

            pos = reader.GetPos();
        }

        if(parameters.isEmpty())
        {
            throw ParserError("You must provide at least one parameter!");
        }

    }
    catch(ParserError &e)
    {
            GuessResult result = {parameters, true, e.GetMsg().c_str()};
            return result;
    }

    GuessResult result = {parameters, false, ""};
    return result;
}

FitResult Fit::fit(const FitConfig &config)
{    
    m_config = config;
    m_iterations = config.iterations;

    FitFunctionData data = generateFunctionData(config);

    if(data.parameters.size() == 0)
    {
        FitResult result;
        result = generateResult();
        return result;
    }

    initWorkspace(data.parameters.size());


    switch(m_config.algorithm)
    {
        case Simplex:
            fitSimplex(data);
        break;
        case ScaledLevenberg:
        case UnscaledLevenberg:
            fitLevenberg(data);
        break;
    }

    FitResult result = generateResult();

    logResultsToConsole(result);
    deinitWorkspace();

    return result;
}

FitResult Fit::fit(Plot *plot, Curve *curve, Fit::FitType type)
{
    FitConfig config;
    config.plot = plot;
    config.curve = curve;
    config.from = curve->minX();
    config.to = curve->maxX();
    config.formula = Fit::builtInFunctions[type].formula();
    config.iterations = DEFAULT_ITERATIONS;
    config.tolerance = DEFAULT_TOLERANCE;
    config.points = curve->points();
    config.algorithm = ScaledLevenberg;

    GuessResult guess = Fit::guessParameters(config.formula);

    foreach(QString p, guess.parameters)
    {
        Parameter parameter;
        parameter.name = p;

        config.parameters.append(parameter);
    }

    FitResult result = fit(config);

    return result;
}

void Fit::initWorkspace(int parameters)
{
    if(parameters < 1)
    {
        qDebug() << "No parameters in fit formula!";
        return;
    }

    m_parametersInitGuessess = gsl_vector_alloc(parameters);
    gsl_vector_set_all (m_parametersInitGuessess, 1.0);

    m_covar = gsl_matrix_alloc (parameters, parameters);
    m_results = new double[parameters];
}

void Fit::deinitWorkspace()
{
    if (m_parametersInitGuessess)
    {
        gsl_vector_free(m_parametersInitGuessess);
        m_parametersInitGuessess = NULL;
    }

    if (m_covar)
    {
        gsl_matrix_free (m_covar);
        m_covar = NULL;
    }

    if (m_results)
    {
        delete[] m_results;
        m_results = NULL;
    }
}

FitFunctionData Fit::generateFunctionData(const FitConfig &config)
{
    FitFunctionData data(config);
    return data;
}

FitResult Fit::generateResult()
{
    FitResult result;

    int i = 0;
    foreach(const Parameter & p, m_config.parameters)
    {
        result.parameters.append(p);

        if(!p.isConst)
        {
            result.parameters.last().error = sqrt(gsl_matrix_get(m_covar, i, i));
            result.parameters.last().value = m_results[i];
            i++;
        }
    }

    result.formula = m_config.formula;
    result.from = m_config.from;
    result.to = m_config.to;
    result.points = m_config.points;

    return result;
}

void Fit::fitLevenberg(FitFunctionData & data)
{
    gsl_multifit_function_fdf f;
    f.f = user_f;
    f.df = user_df;
    f.fdf = user_fdf;
    f.n = data.points;
    f.p = data.parameters.size();
    f.params = &data;

    gsl_multifit_fdfsolver *s = fitLevenbergDetails(f, data);

    m_chi2 = pow(gsl_blas_dnrm2(s->f), 2.0);
    gsl_multifit_fdfsolver_free(s);

    qDebug() << "Result 1 : " << m_results[0];
    qDebug() << "Result 2 : " << m_results[1];
}

gsl_multifit_fdfsolver * Fit::fitLevenbergDetails(gsl_multifit_function_fdf f, FitFunctionData & data)
{
    const gsl_multifit_fdfsolver_type *T;
	if (data.algorithm == ScaledLevenberg)
		T = gsl_multifit_fdfsolver_lmsder;
	else
		T = gsl_multifit_fdfsolver_lmder;

    gsl_set_error_handler_off();

	gsl_multifit_fdfsolver *s = gsl_multifit_fdfsolver_alloc (T, data.points, data.parameters.size());
    m_status = gsl_multifit_fdfsolver_set (s, &f, m_parametersInitGuessess);

	size_t iter = 0;
    size_t paramSize = data.parameters.size();
    for (int i=0; i < paramSize ; i++)
    {
        double p = gsl_vector_get(m_parametersInitGuessess, i);
        m_results[i] = p;
    }

    if (m_status != GSL_SUCCESS)
    {
	    gsl_multifit_covar (s->J, 0.0, m_covar);
	    m_iterations = 0;
	    return s;
    }

    do
    {
        iter++;
        m_status = gsl_multifit_fdfsolver_iterate (s);
        if (m_status != GSL_SUCCESS)
            break;

        for (int i = 0; i < paramSize; i++)
        {
            m_results[i] = gsl_vector_get(s->x, i);
        }

        m_status = gsl_multifit_test_delta (s->dx, s->x, data.tolerance, data.tolerance);
    } 
    while (m_status == GSL_CONTINUE && (int)iter < data.iterations);
    
    gsl_multifit_covar (s->J, 0.0, m_covar);
    m_iterations = iter;

    return s;
}

void Fit::fitSimplex(FitFunctionData & data)
{
    gsl_multimin_function f;
    f.f = user_d;
    f.n = data.parameters.size();
    f.params = &data;


    gsl_multimin_fminimizer *s_min = fitSimplexDetails(f, data);

    if (m_status == GSL_SUCCESS)
    {
         // allocate memory and calculate covariance matrix based on residuals
         int numPoints = f.n;
         int numParams = data.parameters.size();

         gsl_matrix *J = gsl_matrix_alloc(numPoints, numParams);
         user_df(s_min->x,(void*)f.params, J);
         gsl_multifit_covar (J, 0.0, m_covar);
         m_chi2 = s_min->fval;

         // free previousely allocated memory
         gsl_matrix_free (J);
    }
    gsl_multimin_fminimizer_free (s_min);

    qDebug() << "Result 1 : " << m_results[0];
    qDebug() << "Result 2 : " << m_results[1];

    for(int i = 0 ; i < 2 ; ++i)
        qDebug() << " sqrt covar: " << sqrt(gsl_matrix_get(m_covar,i,i));
}

gsl_multimin_fminimizer * Fit::fitSimplexDetails(gsl_multimin_function f, FitFunctionData & data)
{
    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex;

    //size of the simplex
    gsl_vector *ss;
    //initial vertex size vector
    ss = gsl_vector_alloc (f.n);
    //set all step sizes to 1 can be increased to converge faster
    gsl_vector_set_all (ss, 1.0);

    gsl_set_error_handler_off();

    gsl_multimin_fminimizer *s_min = gsl_multimin_fminimizer_alloc (T, f.n);
    m_status = gsl_multimin_fminimizer_set (s_min, &f, m_parametersInitGuessess, ss);

    double size;
    size_t iter = 0;

    for (int i=0; i < data.parameters.size() ; i++)
    {
        double p = gsl_vector_get(m_parametersInitGuessess, i);
        m_results[i] = p;
    }

    if (m_status != GSL_SUCCESS)
    {
        m_iterations = 0;
        gsl_vector_free(ss);
        return s_min;
    }

    do
    {
        iter++;
        m_status = gsl_multimin_fminimizer_iterate (s_min);

        if (m_status)
            break;

        for (int i=0; i< data.parameters.size() ; i++)
            m_results[i] = gsl_vector_get(s_min->x, i);

        size = gsl_multimin_fminimizer_size (s_min);
        m_status = gsl_multimin_test_size (size, data.tolerance);
    }
    while (m_status == GSL_CONTINUE && (int)iter < data.iterations);

//     m_iterations = iter;
    gsl_vector_free(ss);
    return s_min;
}

void Fit::logResultsToConsole(FitResult & result)
{
    int prec = 6;
    int dof = m_config.points - result.parameters.size();
    double chi2_dof = m_chi2/dof;
  
    QLocale locale = QLocale::system();
    QStringList log;
    
    // Date and plot name
    
    QDateTime dt = QDateTime::currentDateTime ();
    log << "[" << dt.toString(Qt::LocalDate) << "\t" << tr("Plot:");
    log << m_config.plot->name() << "]\n";
    log << "Non-Linear Fit of dataset:" << m_config.curve->getTitle();
    log << ", using function:" << m_config.formula << "\n";
         
    switch(m_config.algorithm)
    {
        case ScaledLevenberg:
          log << "Scaled Levenberg-Marquardt";
        break;
        case UnscaledLevenberg:
          log << "Unscaled Levenberg-Marquardt";
        break;
        case Simplex:
          log << "Nelder-Mead Simplex";
        break;
        
        default: 
          log << "Unknown";
    }
    
    log << " algorithm with tolerance = " << locale.toString(m_config.tolerance) << "\n";
    log << "From x=" << locale.toString(m_config.from, 'e', prec) << " to x=" << locale.toString(m_config.to, 'e', prec) << "\n";
    log << "Parameters:\n";
    for(int i = 0 ; i < result.parameters.size(); ++i)
    {
        const Parameter & p = result.parameters[i];
        log << p.name << " = " << locale.toString(m_results[i], 'e', prec);
        
        //double error = sqrt(chi_2_dof*gsl_matrix_get(covar, i, i));
        double error = gsl_matrix_get(m_covar, i, i);
        
        log << "+/- " << locale.toString(error, 'e', prec) << "\n";
    }
    
    log << "--------------------------------------------------------------------------------------\n";
    log << "Chi^2/doF = " << locale.toString(chi2_dof, 'e', prec) << "\n";
    log << "--------------------------------------------------------------------------------------\n";
    log << "Iterations = " << QString::number(m_iterations) << "\n";
    log << "Status = " << gsl_strerror (m_status) << "\n";
    log << "--------------------------------------------------------------------------------------\n";
    
    QString finalLog = log.join("");
    MainWindow * window = m_config.plot->getMainWindow();
    ScriptingConsole & console = window->scriptingConsole();
    console.appendText(finalLog);
}
