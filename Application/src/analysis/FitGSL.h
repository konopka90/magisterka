#ifndef FITGSL_H
#define FITGSL_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "../scripting/ScriptingMuParser.h"
#include "FitStructs.h"

double user_d(const gsl_vector * x, void *params);
int user_f(const gsl_vector * x, void *params, gsl_vector * f);
int user_df(const gsl_vector *x, void *params, gsl_matrix *J);
int user_fdf(const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J);

#endif // FITGSL_H
