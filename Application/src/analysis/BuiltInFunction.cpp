#include "BuiltInFunction.h"
   
BuiltInFunction::BuiltInFunction() {}
BuiltInFunction::BuiltInFunction(const QString & _tag, const QString & _fullName, const QString & _explanation, const QString & _formula, Type _type) :
    tag(_tag),
    fullName(_fullName),
    explanation(_explanation),
    m_formula(_formula),
    type(_type)
{}

QString BuiltInFunction::formula()
{
    // Special cases

    switch(type)
    {
        case MultiPeak:
        m_formula = "";
        break;

        case Ordered:
        m_formula = "";
        break;
    }

    return m_formula;
}
