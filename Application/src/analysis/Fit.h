#ifndef FIT_H
#define FIT_H
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_cdf.h>

#include <QStringList>
#include <QObject>
#include <muParser/muParserTokenReader.h>
#include <qwt_plot_curve.h>

#include "../scripting/ScriptingMuParser.h"
#include "../plot/Curve.h"

#include "FitStructs.h"
#include "FitGSL.h"
#include "BuiltInFunction.h"



class Fit : public QObject
{
    Q_OBJECT
public:

    enum FitType
    {
        Boltzman,
        Gauss,
        Linear,
        Slope,
        Logistic,
        Lorentz,
        ExpGrowth,
        ExpDecay1,
        ExpDecay2,
        ExpDecay3,
        MultiPeak,
        Polynomial
    };

    explicit Fit(QObject *parent = 0);

    static const QString DEFAULT_VARIABLE;
    static const int DEFAULT_ITERATIONS = 1000;
    static const int DEFAULT_POINTS = 1000;
    static const double DEFAULT_TOLERANCE;

    static GuessResult guessParameters(const QString& formula);

    FitResult fit(const FitConfig& params);
    FitResult fit(Plot* plot, Curve * curve, FitType type);

    static QMap<FitType, BuiltInFunction> builtInFunctions;

private:

    FitConfig m_config;
    int m_status;
    int m_iterations;
    double m_chi2;
    gsl_matrix *m_covar;

    //! Initial guesses for the fit parameters
    gsl_vector *m_parametersInitGuessess;

    //! Stores the result parameters
    double *m_results;


    void initWorkspace(int parameters);
    void deinitWorkspace();
    void logResultsToConsole(FitResult & result);

    FitFunctionData generateFunctionData(const FitConfig &config);
    FitResult generateResult();
  
    void fitLevenberg(FitFunctionData & data);
    void fitSimplex(FitFunctionData & data);

    gsl_multimin_fminimizer * fitSimplexDetails(gsl_multimin_function f, FitFunctionData & data);
    gsl_multifit_fdfsolver * fitLevenbergDetails(gsl_multifit_function_fdf f, FitFunctionData & data);
};

#endif // FIT_H
