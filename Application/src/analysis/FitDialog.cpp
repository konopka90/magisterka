#include "FitDialog.h"
#include <QVariant>



FitDialog::FitDialog(Plot *plot, QWidget *parent) :
    QDialog(parent),
    m_plot(plot)
{
    init();
}

FitDialog::FitDialog(QWidget *parent) :
    QDialog(parent)
{
    init();
}

void FitDialog::init()
{
    setWindowTitle(tr("Fit Wizard"));

    m_stack = new QStackedWidget;

    initExpressionPage();
    initFitPage();

    m_stack->addWidget(m_expressionPage);
    m_stack->addWidget(m_fitPage);

    QHBoxLayout * layout = new QHBoxLayout;
    layout->addWidget(m_stack);
    this->setLayout(layout);
    this->resize(500, 500);
}

void FitDialog::initExpressionPage()
{
    m_expressionPage = new QWidget();
    QVBoxLayout * layout = new QVBoxLayout;

    QLabel * categoryLabel = new QLabel(tr("Category"));
    QLabel * functionLabel = new QLabel(tr("Function"));
    QLabel * expressionLabel = new QLabel(tr("Expression"));

    m_categoryWidget = new QListWidget;
    m_functionWidget = new QListWidget;
    m_expressionExplanation = new QTextEdit;

    m_categoryWidget->addItem(tr("Basic"));
    m_categoryWidget->addItem(tr("Built-in"));

    QGridLayout * gridLayout = new QGridLayout;
    gridLayout->addWidget(categoryLabel,0,0);
    gridLayout->addWidget(functionLabel,0,1);
    gridLayout->addWidget(expressionLabel,0,2);

    gridLayout->addWidget(m_categoryWidget,1,0);
    gridLayout->addWidget(m_functionWidget,1,1);
    gridLayout->addWidget(m_expressionExplanation,1,2);

    QHBoxLayout * horizontalLayout = new QHBoxLayout;
    QLabel * parametersLabel = new QLabel(tr("Parameters:"));

    m_parametersBox = new QLabel();
    m_parametersBox->setFrameStyle(QFrame::Box | QFrame::Sunken);
    m_parametersBox->setMinimumWidth(200);

    m_orderLayout = new QHBoxLayout;
    m_orderLabel = new QLabel;
    m_orderSpinbox = new QSpinBox;
    m_orderSpinbox->setMinimum(1);
    m_orderSpinbox->setMaximum(99);
    m_orderLabel->setVisible(false);
    m_orderSpinbox->setVisible(false);

    m_orderLayout->addWidget(m_orderLabel);
    m_orderLayout->addWidget(m_orderSpinbox);
    m_orderLayout->setAlignment(Qt::AlignRight);

    m_errorLabel = new QLabel();
    m_errorLabel->setStyleSheet("QLabel { color : red; }");
    m_addExpressionButton = new QPushButton(tr("Add expression"));
    m_cancelButton = new QPushButton(tr("Cancel"));
    m_goToFittingSessionButton = new QPushButton(tr("Go to Fitting Session"));
    //m_goToFittingSessionButton->setEnabled(false);
    m_expressionEditor = new ScriptEditor(this);
    m_expressionEditor->setLineAreaVisibile(false);

    horizontalLayout->addWidget(parametersLabel);
    horizontalLayout->addWidget(m_parametersBox,1,Qt::AlignLeft);
    horizontalLayout->addWidget(m_addExpressionButton,0,Qt::AlignRight);

    QHBoxLayout * horizontalLayout2 = new QHBoxLayout;
    horizontalLayout2->addWidget(m_goToFittingSessionButton);
    horizontalLayout2->addWidget(m_cancelButton);
    horizontalLayout2->setAlignment(Qt::AlignRight);

    layout->addLayout(gridLayout);
    layout->addLayout(horizontalLayout);
    layout->addLayout(m_orderLayout);
    layout->addWidget(m_expressionEditor);
    layout->addWidget(m_errorLabel);
    layout->addLayout(horizontalLayout2);

    m_expressionPage->setLayout(layout);

    connect(m_goToFittingSessionButton, SIGNAL(clicked()), this, SLOT(goToFittingSession()));
    connect(m_cancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_categoryWidget, SIGNAL(currentRowChanged(int)), this, SLOT(showFunctionList(int)));
    connect(m_functionWidget, SIGNAL(currentRowChanged(int)), this, SLOT(showFunctionExplanation(int)));
    connect(m_addExpressionButton, SIGNAL(clicked()), this, SLOT(addExpression()));
    connect(m_expressionEditor, SIGNAL(textChanged()), this, SLOT(guessParameters()));

    m_categoryWidget->setCurrentRow(0);
    m_functionWidget->setCurrentRow(0);
}

void FitDialog::initFitPage()
{
    m_fitPage = new QWidget();
    QVBoxLayout * layout = new QVBoxLayout;

    m_functionDeclarationLabel = new QLabel("f()");
    m_functionLabel = new QLabel("A*x+b");
    m_functionLabel->setFrameStyle(QFrame::Sunken | QFrame::Box);
    m_functionLabel->setMargin(10);
    QHBoxLayout * functionLayout = new QHBoxLayout;

    functionLayout->addWidget(new QLabel("Function:"),0,Qt::AlignLeft);
    functionLayout->addWidget(m_functionDeclarationLabel,0,Qt::AlignRight);


    QGroupBox * initialGroup = new QGroupBox(tr("Initial guesses"));
    QHBoxLayout * paramsLayout = new QHBoxLayout;
    QStringList header;
    header <<  "Parameter" << "Value" << "Constant" << "Error";
    m_parametersTable = new QTableWidget;
    m_parametersTable->setColumnCount(4);
    m_parametersTable->setHorizontalHeaderLabels(header);
    m_parametersTable->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    paramsLayout->addWidget(m_parametersTable);
    initialGroup->setLayout(paramsLayout);


    m_from = new DoubleSpinBox;
    m_to = new DoubleSpinBox;
    m_tolerance = new DoubleSpinBox;
    m_tolerance->setRange(0.0, 1.0);
    m_tolerance->setSingleStep(1e-4);
    m_tolerance->setDecimals(13);
    m_tolerance->setValue(Fit::DEFAULT_TOLERANCE);


    m_points = new QSpinBox;
    m_points->setMinimum(2);
    m_points->setMaximum(std::numeric_limits<int>::max());
    m_points->setValue(Fit::DEFAULT_POINTS);

    m_algorithm = new QComboBox;
    m_iterations = new QSpinBox;
    m_iterations->setMinimum(1);
    m_iterations->setMaximum(std::numeric_limits<int>::max());
    m_iterations->setValue(Fit::DEFAULT_ITERATIONS);

    m_algorithm->addItem(tr("Scaled Levenberg-Marquardt"));
    m_algorithm->addItem(tr("Unscaled Levenberg-Marquardt"));
    m_algorithm->addItem(tr("Nelder-Mead Simplex"));

    // Data set

    QVBoxLayout * dataSetLayout = new QVBoxLayout;
    QHBoxLayout * curveLayout = new QHBoxLayout;
    QGridLayout * gridLayout = new QGridLayout;

    m_curves = new QComboBox;
    connect(m_curves, SIGNAL(currentIndexChanged(int)), this, SLOT(curveChanged(int)));
    curveLayout->addWidget(new QLabel(tr("Curve:")),0,Qt::AlignLeft);
    curveLayout->addWidget(m_curves,1,Qt::AlignLeft);

    gridLayout->addWidget(new QLabel(tr("From x:")),0,0);
    gridLayout->addWidget(new QLabel(tr("To x:")),0,2);
    gridLayout->addWidget(m_from,0,1);
    gridLayout->addWidget(m_to,0,3);

    dataSetLayout->addLayout(curveLayout);
    dataSetLayout->addLayout(gridLayout);

    QGroupBox * groupDataSet = new QGroupBox(tr("Data set"));
    groupDataSet->setLayout(dataSetLayout);

    // Curves

    const QVector<Curve*> curves = m_plot->getCurves();
    foreach(Curve* curve, curves)
    {
        m_curves->addItem(curve->getTitle(), QVariant::fromValue(curve));
    }

    // Algorithm

    QHBoxLayout * horizontalLayout = new QHBoxLayout;
    horizontalLayout->addWidget(new QLabel(tr("Iterations:")));
    horizontalLayout->addWidget(m_iterations);
    horizontalLayout->addWidget(new QLabel(tr("Tolerance:")));
    horizontalLayout->addWidget(m_tolerance);
    horizontalLayout->addWidget(new QLabel(tr("Points:")));
    horizontalLayout->addWidget(m_points);

    QVBoxLayout * verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(m_algorithm);
    verticalLayout->addLayout(horizontalLayout);

    QGroupBox * groupAlgorithm = new QGroupBox(tr("Algorithm"));
    groupAlgorithm->setLayout(verticalLayout);

    // Buttons

    m_goBack = new QPushButton(tr("Go Back"));
    QPushButton * closeButton = new QPushButton(tr("Close"));
    QPushButton * fitButton = new QPushButton(tr("Fit"));
    QPushButton * addFitButton = new QPushButton(tr("Add Fit Curve"));
    QPushButton * deleteFitsButton = new QPushButton(tr("Delete Fit Curves"));


    QHBoxLayout * buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(m_goBack);
    buttonsLayout->addWidget(fitButton);
    buttonsLayout->addWidget(addFitButton);
    buttonsLayout->addWidget(deleteFitsButton);
    buttonsLayout->addWidget(closeButton);


    layout->addLayout(functionLayout);
    layout->addWidget(m_functionLabel);
    layout->addWidget(initialGroup);
    layout->addWidget(groupDataSet);
    layout->addWidget(groupAlgorithm);
    layout->addLayout(buttonsLayout);
    m_fitPage->setLayout(layout);

    connect(m_goBack, SIGNAL(clicked()), this, SLOT(goToExpressionPage()));
    connect(closeButton, SIGNAL(clicked()),this, SLOT(close()));
    connect(fitButton, SIGNAL(clicked()),this, SLOT(fit()));
    connect(addFitButton, SIGNAL(clicked()),this, SLOT(addFitCurve()));
    connect(deleteFitsButton, SIGNAL(clicked()),this, SLOT(deleteFits()));



}

void FitDialog::addExpression()
{
    Category category = (Category)m_categoryWidget->currentRow();
    QString name;
    QString markedText;
    switch(category)
    {
    case Basic:
        name = m_functionWidget->currentItem()->text();
        m_expressionEditor->insertFunction(name,"x");
        break;

    case BuiltIn:
        BuiltInFunction * f = m_functionWidget->currentItem()->data(Qt::UserRole).value<BuiltInFunction*>();
        m_expressionEditor->insertText(f->formula());
        break;

    }
}

void FitDialog::goToFittingSession()
{
    m_stack->setCurrentIndex(1);

    // Set function

    m_functionLabel->setText(m_expressionEditor->toPlainText());

    QStringList declarationParameters;
    declarationParameters << Fit::DEFAULT_VARIABLE << m_parameters;
    m_functionDeclarationLabel->setText("f("+declarationParameters.join(", ")+")");

    // Set params

    m_parametersTable->clearContents();
    m_parametersTable->setRowCount(m_parameters.length());

    int i = 0;
    foreach(QString param, m_parameters)
    {

        // First cell
        QTableWidgetItem * it = new QTableWidgetItem(param);
        it->setFlags(it->flags() & (~Qt::ItemIsEditable));
        it->setBackground(QBrush(Qt::lightGray));
        it->setForeground(Qt::black);
        it->setText(param);
        QFont font = it->font();
        font.setBold(true);
        it->setFont(font);
        m_parametersTable->setItem(i, 0, it);

        // Second cell - initial guess
        DoubleSpinBox *sb = new DoubleSpinBox();
        sb->setDecimals(13);
        sb->setValue(1.0);
        m_parametersTable->setCellWidget(i, 1, sb);

        // Third cell - constant

        QCheckBox *cb = new QCheckBox();
        m_parametersTable->setCellWidget(i, 2, cb);

        // Fourth cell - error
        it = new QTableWidgetItem();
        it->setFlags(it->flags() & (~Qt::ItemIsEditable));
        it->setText("--");
        m_parametersTable->setItem(i, 3, it);

        i++;
    }

}

void FitDialog::goToExpressionPage()
{
    m_stack->setCurrentIndex(0);
}

void FitDialog::showFunctionList(int category)
{
    m_functionWidget->clear();
    m_functionWidget->blockSignals(true);

    Category categoryEnum = (Category)category;
    switch(categoryEnum)
    {
    case Basic:
        showFunctionsBasic();
        break;

    case BuiltIn:
        showFunctionsBuiltIn();
        break;

    default:
        qDebug() << "Wrong category index";
    }

    m_functionWidget->blockSignals(false);
    m_functionWidget->setCurrentRow(0);

}

void FitDialog::showFunctionExplanation(int function)
{
    Category categoryEnum = (Category)m_categoryWidget->currentRow();
    QString explanation = "";

    switch(categoryEnum)
    {
    case Basic:
        explanation = explainBasic(function);
        break;

    case BuiltIn:
        explanation = explainBuiltIn(function);
        break;

    default:
        qDebug() << "Wrong category index";
    }

    m_expressionExplanation->setText(explanation);
}

void FitDialog::guessParameters()
{
    QString text = m_expressionEditor->toPlainText().remove(QRegExp("\\s")).remove(".");
    GuessResult guess = Fit::guessParameters(text);
    m_parametersBox->setText(guess.parameters.join(", "));
    m_errorLabel->clear();

    if(guess.isError)
    {
        m_errorLabel->setText(tr("Error: ") + guess.errorMessage);
    }
    else
    {
        m_goToFittingSessionButton->setEnabled(true);
    }

    if(guess.isError || text.isEmpty())
    {
        m_goToFittingSessionButton->setEnabled(false);
    }

    m_parameters = guess.parameters;
}

void FitDialog::curveChanged(int index)
{
    Curve * curve = m_curves->itemData(index).value<Curve*>();
    m_from->setMinimum(curve->minX());
    m_from->setMaximum(curve->maxX());
    m_from->setValue(curve->minX());

    m_to->setMinimum(curve->minX());
    m_to->setMaximum(curve->maxX());
    m_to->setValue(curve->maxX());
}

FitResult FitDialog::fit()
{
    FitConfig params;

    switch(m_algorithm->currentIndex())
    {
        case 0: params.algorithm = ScaledLevenberg; break;

        case 1: params.algorithm = UnscaledLevenberg; break;

        case 2: params.algorithm = Simplex; break;

        default: params.algorithm = ScaledLevenberg;
    }

    params.curve = m_curves->currentData().value<Curve*>();
    params.formula = m_expressionEditor->toPlainText();
    params.from = m_from->value();
    params.iterations = m_iterations->value();
    params.to = m_to->value();
    params.tolerance = m_tolerance->value();
    params.points = m_points->value();
    params.plot = m_plot;

    for(int i = 0 ; i < m_parametersTable->rowCount() ; ++i)
    {
        QTableWidgetItem * name = dynamic_cast<QTableWidgetItem*>(m_parametersTable->item(i,0));
        DoubleSpinBox * value = dynamic_cast<DoubleSpinBox*>(m_parametersTable->cellWidget(i,1));
        QCheckBox * isConst = dynamic_cast<QCheckBox*>(m_parametersTable->cellWidget(i,2));

        Parameter p;
        p.name = name->text();
        p.value = value->value();
        p.isConst = isConst->isChecked();

        params.parameters.push_back(p);
    }

    Fit fit;
    FitResult result = fit.fit(params);

    int i = 0;
    foreach(const Parameter & parameter, result.parameters )
    {
        // First
        QTableWidgetItem * name = dynamic_cast<QTableWidgetItem*>(m_parametersTable->item(i,0));
        name->setText(parameter.name);

        // Second
        DoubleSpinBox * value = dynamic_cast<DoubleSpinBox*>(m_parametersTable->cellWidget(i,1));
        value->setValue(parameter.value);

        // Third
        QCheckBox * isConst = dynamic_cast<QCheckBox*>(m_parametersTable->cellWidget(i,2));
        isConst->setChecked(parameter.isConst);

        // Fourth
        QTableWidgetItem * error = dynamic_cast<QTableWidgetItem*>(m_parametersTable->item(i,3));
        error->setText(QChar(177) + QString::number(parameter.error));

        i++;
    }

    return result;
}

void FitDialog::addFitCurve()
{
    FitResult result = fit();
    m_plot->addFitCurve(result);
}

void FitDialog::deleteFits()
{
    m_plot->deleteFits();
}

void FitDialog::showFunctionsBasic()
{
    m_functionWidget->addItems(ScriptingMuParser::functionList(false));
}

void FitDialog::showFunctionsBuiltIn()
{
    QMap<Fit::FitType, BuiltInFunction>::iterator i;
    for (i = Fit::builtInFunctions.begin(); i != Fit::builtInFunctions.end(); ++i)
    {
        QListWidgetItem * item = new QListWidgetItem();
        item->setData(Qt::UserRole, QVariant::fromValue(&i.value()) );
        item->setText(i.value().fullName);
        m_functionWidget->addItem(item);
    }
}

QString FitDialog::explainBasic(int function)
{
    if(function < 0)
        return "";

    QString functionName = m_functionWidget->item(function)->text();
    return ScriptingMuParser::explainFunction(functionName);
}

QString FitDialog::explainBuiltIn(int function)
{
    if(function < 0)
        return "";

    BuiltInFunction * f = m_functionWidget->item(function)->data(Qt::UserRole).value<BuiltInFunction*>();
    return f->explanation + "\n\n" + f->formula();
}
