INCLUDEPATH += analysis/

HEADERS += \ 
    analysis/BuiltInFunction.h \
    analysis/FitDialog.h \
    analysis/Fit.h \
    analysis/FitGSL.h \
    analysis/FitStructs.h

SOURCES += \ 
    analysis/BuiltInFunction.cpp \
    analysis/FitDialog.cpp \
    analysis/Fit.cpp \
    analysis/FitGSL.cpp \
    analysis/FitStructs.cpp
