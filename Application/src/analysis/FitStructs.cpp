#include "FitStructs.h"
#include "../plot/Curve.h"

FitFunctionData::FitFunctionData()
{

}

FitFunctionData::FitFunctionData(const FitConfig& config)
{
    curve = config.curve;
    formula = config.formula;
    algorithm = config.algorithm;
    tolerance = config.tolerance;
    iterations = config.iterations;

    foreach(const Parameter& p, config.parameters)
    {
        if(p.isConst)
            constants.append(p);
        else
            parameters.append(p);
    }

    // Find nearest X from start and end

    double lastDiffStart = DBL_MAX;
    double lastDiffEnd = DBL_MAX;
    indexFrom = 0;
    indexTo = 0;
    for(int i = 0 ; i < curve->points() ; ++i)
    {
        double diffStart = fabs(curve->x(i) - config.from);
        double diffEnd = fabs(curve->x(i) - config.to);
        if(diffStart < lastDiffStart)
            indexFrom = i;
        if(diffEnd < lastDiffEnd)
            indexTo = i;

        lastDiffStart = diffStart;
        lastDiffEnd = diffEnd;
    }

    points = indexTo - indexFrom + 1;
}
