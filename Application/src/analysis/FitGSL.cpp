#include "FitGSL.h"
#include "../plot/Curve.h"

double user_d(const gsl_vector * x, void *params)
{
    struct FitFunctionData * data = static_cast<FitFunctionData*>(params);
    int numPoints = data->points;
    int numParams = data->parameters.size();
    int numConstants = data->constants.size();
    Curve * curve = data->curve;

    double val=0;
    ScriptingMuParser parser;

    double *parameters = new double[numParams];

    // Set variable "x"
    double xVariable;
    parser.addVariable("x", xVariable);

    // Set variables
    for (int i=0; i < numParams; i++)
    {
        parameters[i]=gsl_vector_get(x,i);
        parser.addVariable(data->parameters[i].name, parameters[i]);
    }

    // Set constants
    for(int i = 0 ; i < numConstants ; ++i)
    {
        parser.addConstant(data->constants[i].name, data->constants[i].value);
    }

    parser.setFormula(data->formula);

    for (int j = 0; j < numPoints; j++)
    {
        xVariable = curve->x(j + data->indexFrom);
        //double s = 1.0/sqrt(sigma[j]);
        double s = 1.0;

        try
        {
            //double t = (parser.EvalRemoveSingularity(&xVariable) - Y[j])/s;
            double t = (parser.execute() - curve->y(j + data->indexFrom))/s;
            val += t*t;
        }
        catch(mu::Parser::exception_type &e)
        {
            qDebug() << QObject::tr(e.GetMsg().c_str());
            delete[] parameters;
            return GSL_EINVAL; //weird, I know. blame gsl.
        }
    }

    delete[] parameters;

    return val;
}

int user_f(const gsl_vector * x, void *params, gsl_vector * f)
{
    struct FitFunctionData * data = static_cast<FitFunctionData*>(params);
    int numPoints = data->points;
    int numParams = data->parameters.size();
    int numConstants = data->constants.size();
    Curve * curve = data->curve;

    ScriptingMuParser parser;

    double *parameters = new double[numParams];

    // Set variable "x"
    double xVariable;
    parser.addVariable("x", xVariable);

    // Set variables
    for (int i=0; i < numParams; i++)
    {
        parameters[i]=gsl_vector_get(x,i);
        parser.addVariable(data->parameters[i].name, parameters[i]);
    }

    // Set constants
    for(int i = 0 ; i < numConstants ; ++i)
    {
        parser.addConstant(data->constants[i].name, data->constants[i].value);
    }

    parser.setFormula(data->formula);

    //double s = 1.0/sqrt(sigma[j]);
    double s = 1.0;

    for(int j = 0 ; j < numPoints; ++j)
    {
        xVariable = curve->x(j + data->indexFrom);
        try
        {
            gsl_vector_set (f, j, (parser.execute() - curve->y(j + data->indexFrom))/s);
        }
        catch(mu::Parser::exception_type &e)
        {
            qDebug() << QObject::tr(e.GetMsg().c_str());
            delete[] parameters;
            return GSL_EINVAL;
        }
    }

    delete[] parameters;

    return GSL_SUCCESS;
}

int user_df(const gsl_vector *x, void *params, gsl_matrix *J)
{
    struct FitFunctionData * data = static_cast<FitFunctionData*>(params);
    int numPoints = data->points;
    int numParams = data->parameters.size();
    int numConstants = data->constants.size();
    Curve * curve = data->curve;

    ScriptingMuParser parser;

    double *parameters = new double[numParams];

    // Set variable "x"
    double xVariable;
    parser.addVariable("x", xVariable);

    // Set variables
    for (int i=0; i < numParams; i++)
    {
        parameters[i]=gsl_vector_get(x,i);
        parser.addVariable(data->parameters[i].name, parameters[i]);
    }

    // Set constants
    for(int i = 0 ; i < numConstants ; ++i)
    {
        parser.addConstant(data->constants[i].name, data->constants[i].value);
    }

    parser.setFormula(data->formula);

    for (int i = 0; i < numPoints; i++)
    {
        xVariable = curve->x(i + data->indexFrom);
        //double s = 1.0/sqrt(sigma[j]);
        double s = 1.0;

        for(int j = 0 ; j < numParams; ++j)
        {
            try
            {
                // Copied part
                double *a_Var;
                double a_fPos;

                a_Var = &parameters[j];
                a_fPos = parameters[j];

                double fRes(0),
                           fBuf(*a_Var),
                           f[4] = {0,0,0,0},
                           a_fEpsilon( (a_fPos == 0) ? (double)1e-10 : 1e-7 * a_fPos );

                    *a_Var = a_fPos+2 * a_fEpsilon;  f[0] = parser.execute();
                    *a_Var = a_fPos+1 * a_fEpsilon;  f[1] = parser.execute();
                    *a_Var = a_fPos-1 * a_fEpsilon;  f[2] = parser.execute();
                    *a_Var = a_fPos-2 * a_fEpsilon;  f[3] = parser.execute();
                    *a_Var = fBuf; // restore variable

                    fRes = (-f[0] + 8*f[1] - 8*f[2] + f[3]) / (12*a_fEpsilon);

                // End of copied part

                gsl_matrix_set (J, i, j, 1.0/s*fRes);
            }
            catch(mu::Parser::exception_type &e)
            {
                qDebug() << QObject::tr(e.GetMsg().c_str());
                delete[] parameters;
                return GSL_EINVAL;
            }
        }
    }

    delete[] parameters;

    return GSL_SUCCESS;
}

int user_fdf(const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J) 
{
    int status = user_f (x, params, f);
    if (status != GSL_SUCCESS)
    	return status;
    status = user_df (x, params, J);
    return status;
}
