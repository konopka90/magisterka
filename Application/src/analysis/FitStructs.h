#ifndef FITSTRUCTS_H
#define FITSTRUCTS_H

#include <QVector>
#include <QtWidgets>
#include <math.h>

class Curve;
class Plot;

enum FitAlgorithm
{
    ScaledLevenberg,
    UnscaledLevenberg,
    Simplex
};

struct GuessResult
{
    QStringList parameters;
    bool isError;
    QString errorMessage;
};

struct Parameter
{
    Parameter() :
        name(""),
        value(1.0),
        isConst(false),
        error(0.0)
    {}

    QString name;
    double value;
    bool isConst;
    double error;
};

struct FitConfig
{
    Curve * curve;
    Plot * plot;
    QString formula;
    QVector<Parameter> parameters;
    FitAlgorithm algorithm;
    double tolerance;
    int iterations;
    int points;
    double from;
    double to;

};

struct FitFunctionData
{
    Curve * curve;
    QString formula;
    QVector<Parameter> parameters;
    QVector<Parameter> constants;
    FitAlgorithm algorithm;
    double tolerance;
    int iterations;
    int indexFrom;
    int indexTo;
    int points;

    FitFunctionData();
    FitFunctionData(const FitConfig& config);
};

struct FitResult
{
    QVector<Parameter> parameters;
    int points;
    double from;
    double to;
    QString formula;
};



#endif // FITSTRUCTS_H
