#ifndef FITDIALOG_H
#define FITDIALOG_H

#include <QtWidgets>
#include "Fit.h"
#include "BuiltInFunction.h"
#include "../scripting/ScriptingMuParser.h"
#include "../scripting/ScriptEditor.h"
#include "../widgets/DoubleSpinBox.h"
#include "../plot/Plot.h"
#include "../plot/PlotLine.h"
#include "../plot/PlotHistogram.h"


class FitDialog : public QDialog
{
    Q_OBJECT

    enum Category
    {
        Basic,
        BuiltIn
    };

public:
    // Debug purpose
    FitDialog(Plot * plot, QWidget *parent = 0);
    FitDialog(QWidget *parent = 0);

private:

    void init();
    void initExpressionPage();
    void initFitPage();

    static const int SPINBOX_DECIMALS = 4;

    Plot * m_plot;

    QStackedWidget * m_stack;
    QWidget * m_fitPage;
    QWidget * m_expressionPage;

    QListWidget * m_categoryWidget;
    QListWidget * m_functionWidget;
    QTextEdit * m_expressionExplanation;
    QLabel * m_parametersBox;

    QHBoxLayout * m_orderLayout;
    QLabel * m_orderLabel;
    QLabel * m_errorLabel;
    QSpinBox * m_orderSpinbox;

    QPushButton * m_addExpressionButton;
    QPushButton * m_cancelButton;
    QPushButton * m_goToFittingSessionButton;

    ScriptEditor * m_expressionEditor;

    QStringList m_parameters;

    // Fitting page
private:

    QLabel * m_functionDeclarationLabel;
    QLabel * m_functionLabel;
    DoubleSpinBox * m_from;
    DoubleSpinBox * m_to;
    QSpinBox * m_iterations;
    QSpinBox * m_points;
    DoubleSpinBox * m_tolerance;
    QComboBox * m_algorithm;
    QTableWidget * m_parametersTable;
    QPushButton * m_goBack;
    QComboBox * m_curves;



private slots:
    void addExpression();
    void goToFittingSession();
    void goToExpressionPage();
    void showFunctionList(int category);
    void showFunctionExplanation(int function);
    void showFunctionsBasic();
    void showFunctionsBuiltIn();
    QString explainBasic(int function);
    QString explainBuiltIn(int function);
    void guessParameters();
    void curveChanged(int index);

    FitResult fit();
    void addFitCurve();
    void deleteFits();
};

#endif // FITDIALOG_H
