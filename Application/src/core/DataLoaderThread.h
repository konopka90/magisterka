#ifndef DATALOADERTHREAD_H
#define DATALOADERTHREAD_H

#include <QThread>
#include <pcap.h>
#ifdef PLATFORM_UNIX
    #include <netinet/in.h>
#endif

#include "../table/Table.h"
#include "../options/Options.h"
#include "MemoryPool.h"



/* 4 bytes IP address */
typedef struct ip_address{
    u_char byte1;
    u_char byte2;
    u_char byte3;
    u_char byte4;
}ip_address;

/* IPv4 header */
typedef struct ip_header{
    u_char  ver_ihl;        // Version (4 bits) + Internet header length (4 bits)
    u_char  tos;            // Type of service
    u_short tlen;           // Total length
    u_short identification; // Identification
    u_short flags_fo;       // Flags (3 bits) + Fragment offset (13 bits)
    u_char  ttl;            // Time to live
    u_char  proto;          // Protocol
    u_short crc;            // Header checksum
    ip_address  saddr;      // Source address
    ip_address  daddr;      // Destination address
    u_int   op_pad;         // Option + Padding
}ip_header;

/* UDP header*/
typedef struct udp_header{
    u_short sport;          // Source port
    u_short dport;          // Destination port
    u_short len;            // Datagram length
    u_short crc;            // Checksum
}udp_header;

class DataLoaderThread : public QThread
{
    Q_OBJECT

public:
    DataLoaderThread();

protected:
    void appendDataToTable(const u_char *packet);
    virtual pcap_t* getHandle() = 0;

signals:
    void finishSignal();
    void errorSignal(QString);


protected:
    ip_header *ih;
    udp_header *uh;
    u_int ip_len;
    u_short sport,dport;
    DataRow* data_ptr;
    DataRow * row;

    Options & m_options;
    //MemoryPool & m_memoryPool;
    Table * m_table;
    std::string m_source;
    bool m_isFinished;
    char m_errorBuffer[PCAP_ERRBUF_SIZE];
};

#endif // DATALOADERTHREAD_H
