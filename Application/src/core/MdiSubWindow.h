#ifndef MDISUBWINDOW_H
#define MDISUBWINDOW_H

#include <QMdiSubWindow>
#include <QEvent>
#include <QCloseEvent>
#include <QMap>
#include <QDateTime>

#include "PtrWrapper.h"


class MainWindow;
class MdiSubWindow;
class WindowsManager;

typedef PtrWrapper<MdiSubWindow> MdiSubWindowWrapper;

class MdiSubWindow : public QMdiSubWindow
{
    Q_OBJECT
public:

    enum MdiSubWindowType
    {
        Table,
        PlotLine,
        PlotHistogram,
        ScriptWindow
    };

    enum MessageBoxButton
    {
        Hide,
        Delete,
        Cancel
    };

    explicit MdiSubWindow(MainWindow *parent, MdiSubWindowType type);
    virtual ~MdiSubWindow();
    virtual void save(QDataStream & stream);
    virtual void load(QDataStream & stream);
    virtual void saveAs() {}
    MainWindow * getMainWindow() const;
    

    const QString& name();
    MdiSubWindowType type();
    QString typeToStr();
    QString state();
    const QDateTime& creationDateTime();

    void setName(const QString& name);
    void confirmClose(bool);
    virtual void closeEvent( QCloseEvent *);
    virtual void onWindowDelete() {}

protected:
    MessageBoxButton showPopup();

protected:
    MainWindow * m_mainWindow;
    QString m_name;
    MdiSubWindowType m_type;
    QDateTime m_creationDateTime;
    bool m_confirmClose;


signals:

    void closedWindow(MdiSubWindow *);

public slots:
    void closeWithoutAsking();

};

#endif // MDISUBWINDOW_H
