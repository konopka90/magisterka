#ifndef PTRWRAPPER_H
#define PTRWRAPPER_H

#include <QDebug>

template<typename Type>
class PtrWrapper
{
    typedef struct { } NotType;

public:
    PtrWrapper() : m_ptr(NULL) { }
    PtrWrapper(const NotType*) : m_ptr(NULL) { }
    PtrWrapper(Type *p) : m_ptr(p) { }

    void operator delete(void *p) {

        qDebug() << "Niszcze!";
    }

    Type *operator->() const { return m_ptr; }
    Type *get() const { return m_ptr; }
    Type& operator*() const { return *m_ptr; }

    operator bool() const { return (m_ptr == NULL ? false : true); }
    //operator Type*() const { return m_ptr; }



private:
    Type* m_ptr;
};

#endif // PTRWRAPPER_H
