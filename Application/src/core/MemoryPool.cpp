#include "MemoryPool.h"

MemoryPool MemoryPool::m_mainPool;

MemoryPool::MemoryPool() :
    m_prev(0),
    m_next(0),
    m_percentOfMemory(0),
    m_chunkSize(0),
    m_nextPoolSize(0),
    m_TOS(0),
    m_pool(0),
    m_lock()
{}

void MemoryPool::setup(const uint percentOfMemory, uint64_t chunkSize, uint64_t nextPoolSize)
{
    uint64_t freeMemory = getFreePhysicalMemory();
    uint64_t partOfFreeMemory = (uint64_t)(freeMemory * percentOfMemory / 100.0);
    qDebug() << "Free memory: " << freeMemory;
    qDebug() << percentOfMemory << "% free memory: " << partOfFreeMemory;


#ifdef ARCH_DIR_X86
   if(partOfFreeMemory > (uint64_t)800000000)
       partOfFreeMemory = 800000000;
#endif

    m_percentOfMemory = percentOfMemory;
    m_chunkSize = chunkSize;
    m_nextPoolSize = nextPoolSize;
    m_numberOfChunks = partOfFreeMemory / (m_chunkSize + sizeof(uint64_t));
    m_size = m_numberOfChunks * m_chunkSize;

    qDebug()  << "Allocation size " << m_size;

    m_pool = allocateMemory(m_size);
    qDebug() << "Memory address: "  << m_pool;
    if(m_pool)
    {
        m_freeStackSize = m_numberOfChunks * sizeof(uint64_t);
        qDebug()  << "Free stack allocation size " << m_freeStackSize;
        m_freeStack = reinterpret_cast<uchar**>(allocateMemory(m_freeStackSize));
        m_available = m_pool;
        m_last = m_pool + m_chunkSize * m_numberOfChunks;
    }
}

uint64_t MemoryPool::getNumberOfMaxChunksInPool()
{
    return m_numberOfChunks;
}

void MemoryPool::deallocate(void *memory)
{
    m_lock.lock();

    if (memory)
    {
        uchar* chunk = reinterpret_cast<uchar*>(memory);

        // Check if the item being deleted is within this pool's memory range.
        if (chunk < m_pool || chunk >= m_last)
        {
            // If there is another pool in the list then try to delete from it,
            // otherwise call the generalised delete operator.
            if (m_next)
            {
                m_lock.unlock();
                m_next->deallocate(chunk);
            }
            else
            {
                ::operator delete(memory);
                m_lock.unlock();
            }
        }
        else
        {
            // Add a hole to the free stack.
            m_freeStack[m_TOS++] = chunk;



            if(static_cast<uint64_t>(m_TOS * m_chunkSize) == m_available - m_pool)
            {
                // If this pool is empty and it is the last in the linked list
                // then delete it and set the previous pool to the last.
                if (m_prev && !m_next)
                {
                    m_prev->m_next = 0;
                    m_lock.unlock();
                    delete this;
                }
                else
                if(m_prev && m_next)
                {
                    m_next->m_prev = m_prev;
                    m_prev->m_next = m_next;
                    m_lock.unlock();
                    delete this;
                }
                else
                {
                    m_lock.unlock();
                }
            }
            else
            {
                m_lock.unlock();
            }
        }
    }
}

void MemoryPool::clear()
{

}

MemoryPool::MemoryPool(MemoryPool * const prev) :
    m_prev(prev),
    m_next(0),
    m_percentOfMemory(0),
    m_nextPoolSize(prev->m_nextPoolSize),
    m_TOS(0),
    m_chunkSize(prev->m_chunkSize)
{

    m_numberOfChunks = m_nextPoolSize / (m_chunkSize + sizeof(uint64_t));
    m_size = m_numberOfChunks * m_chunkSize;
    qDebug()  << "Allocation size " << m_size;
    m_pool = allocateMemory(m_size);
    if(m_pool)
    {
        m_freeStackSize = m_numberOfChunks * sizeof(uint64_t);
        qDebug()  << "Free stack allocation size " << m_freeStackSize;
        m_freeStack = reinterpret_cast<uchar**>(allocateMemory(m_freeStackSize));
        m_available = m_pool;
        m_last = m_pool + m_chunkSize * m_numberOfChunks;
    }

}


#ifdef PLATFORM_WIN32
uchar *MemoryPool::allocateMemory(uint64_t size)
{
    void* allocatedMemory = VirtualAlloc(NULL,
                          size,
                          MEM_COMMIT,
                          PAGE_READWRITE);

    if(allocatedMemory)
    {
        memset(allocatedMemory, 0, size);
    }
    return reinterpret_cast<uchar*>(allocatedMemory);
}

void MemoryPool::deallocateMemory()
{
    if(!VirtualFree(m_pool,0, MEM_RELEASE))
    {
        qDebug() << "VirtualFree failed, error " << GetLastError();
    }

    if(!VirtualFree(m_freeStack,0, MEM_RELEASE))
    {
        qDebug() << "VirtualFree failed, error " << GetLastError();
    }
}


uint64_t MemoryPool::getFreePhysicalMemory()
{
    m_statex.dwLength = sizeof (m_statex);
    GlobalMemoryStatusEx (&m_statex);
    return (uint64_t)m_statex.ullAvailPhys;
}
#else

uchar *MemoryPool::allocateMemory(uint64_t size)
{
    void* allocatedMemory = new (std::nothrow) uchar[size];
    //mlock(allocatedMemory, size);

    if(allocatedMemory == 0)
    {
        qDebug() << "Memory allocation failed";
        return 0;
    }

    memset(allocatedMemory, 0, size);

    return reinterpret_cast<uchar*>(allocatedMemory);
}

void MemoryPool::deallocateMemory()
{
    qDebug() << "Deallocating memory...";

    qDebug() << "Memory address: "  << m_pool;
    //int result = munlock(m_pool, m_size);

    //qDebug() << "size : " << m_size << " result " << result;

    if(m_pool)
        delete [] m_pool;

    qDebug() << "Pool deallocated!";
    qDebug() << "Deallocating stack...";

    //munlock(m_freeStack, m_freeStackSize);
    if(m_freeStack)
        delete [] m_freeStack;

    qDebug() << "Memory deallocated !";
}


uint64_t MemoryPool::getFreePhysicalMemory()
{
    QFile file ("/proc/meminfo");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
            char buf[1024];
            file.readLine(buf, sizeof(buf));
            file.readLine(buf, sizeof(buf));
                  QString line = buf;
                  QString temp;
                  QString freemem;
                  if(line.startsWith("MemFree"))
                  {
                      QTextStream stream(&line);
                      stream >> temp >> freemem;
                      long freememInt = freemem.toInt();
                      freememInt = freememInt * 1024;
                      return freememInt;
                  }

             file.close();
    }

    return 0;
}

#endif
