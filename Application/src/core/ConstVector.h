#ifndef CONSTVECTOR_H
#define CONSTVECTOR_H

#include <stdexcept>
#include <exception>
#include <string>

template<class T>
class ConstVector
{
public:

    ConstVector(unsigned int percent)
        : m_percent(percent), m_sizeOfType(sizeof(T)), m_size(0)
    {
        m_mms = m_memoryManager.allocatePercentOfPhysicalMemory(percent);
        m_maxSize = m_mms.memorySize / m_sizeOfType;
    }

    ~ConstVector()
    {
        m_memoryManager.deallocateMemory(m_mms);
    }

    T& operator[](unsigned int i)
    {
        if(i >= m_maxSize)
            throw std::out_of_range("Index out of range");

        return at(i);
    }

    T& at(unsigned int i)
    {
        return *(reinterpret_cast<T*>((char*)m_mms.memoryPtr + i*m_sizeOfType));
    }

    uint64_t size()
    {
        return m_size;
    }

    uint64_t maxSize()
    {
        return m_maxSize;
    }

    void increaseSize()
    {
        ++m_size;
    }

private:

    ConstVector(const ConstVector&);
    ConstVector& operator=(const ConstVector&);

private:

    unsigned int m_percent;
    size_t m_sizeOfType;
    uint64_t m_maxSize;
    uint64_t m_size;


};

#endif // CONSTVECTOR_H
