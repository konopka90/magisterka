#include "WindowsManager.h"
#include "MainWindow.h"

WindowsManager::WindowsManager(MainWindow *parent) :
    QDockWidget(tr("Windows"),(QWidget*)parent), m_mainWindow(*parent)
{
    m_proxyModel.setSourceModel(&m_model);

    m_table = new QTableView(this);
    m_table->setModel(&m_proxyModel);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_table->setSelectionMode(QAbstractItemView::SingleSelection);
    m_table->setSortingEnabled(true);
    m_table->setContextMenuPolicy(Qt::CustomContextMenu);
    m_table->horizontalHeader()->setHighlightSections(false);

    prepareContextMenu();
    setAllowedAreas(Qt::LeftDockWidgetArea  | Qt::BottomDockWidgetArea | Qt::RightDockWidgetArea | Qt::TopDockWidgetArea);
    setWidget(m_table);
}

void WindowsManager::clear()
{
    m_uniqueNameCounters = QMap<MdiSubWindow::MdiSubWindowType, int>();

    foreach(MdiSubWindow* window, m_model.windows())
    {
        window->closeWithoutAsking();
        delete window;
    }
}

void WindowsManager::addWindow(MdiSubWindow* window)
{
    m_model.append(window);
    m_table->resizeColumnsToContents();
    m_table->update();
}

void WindowsManager::removeWindow(MdiSubWindow* window)
{
    m_model.remove(window);
    m_table->resizeColumnsToContents();
    m_table->update();
}

MdiSubWindow* WindowsManager::getWindowByName(const QString &name)
{
    MdiSubWindow* result = NULL;

    const QVector<MdiSubWindow*> subWindows = windows();
    foreach(MdiSubWindow* w, subWindows)
    {
        if(w->name() == name)
        {
            result = w;
        }
    }

    return result;
}


MdiSubWindow* WindowsManager::findWindow(const QString &search, bool isPartialMatch, bool isCaseSentensive, bool showPopup)
{
    foreach(MdiSubWindow* w, windows())
    {
        QString s = search;
        QString n = w->name();
        if(!isCaseSentensive)
        {
            n = n.toLower();
            s = s.toLower();
        }

        if(isPartialMatch && n.contains(s))
        {
            return w;
        }
        else
            if(s == n)
            {
                return w;
            }
    }

    if(showPopup)
        QMessageBox::warning(this, tr("No match found"),
            tr("Sorry, no match found for string: '%1'").arg(search));

    return NULL;
}

void WindowsManager::refreshList()
{
    m_model.refresh();
    m_table->resizeColumnsToContents();
    m_table->update();
}

QString WindowsManager::generateUniqueName(MdiSubWindow::MdiSubWindowType type)
{
    QString name = "";
    switch (type)
    {
    case MdiSubWindow::PlotLine:
        name = "Line";
        break;
    case MdiSubWindow::PlotHistogram:
        name = "Histogram";
        break;
    case MdiSubWindow::Table:
        name = "Table";
        break;
    case MdiSubWindow::ScriptWindow:
        name = "Script";
        break;

    default:
        qDebug() << "WindowsManager::generateUniqueName";
        name = "Unknown";
        break;
    }

    m_uniqueNameCounters[type]++;

    name = name.append(QString::number(m_uniqueNameCounters[type]));

    if(!isNameUnique(name))
        name = generateUniqueName(type);

    return name;
}

Table *WindowsManager::getTableWithRowsOtherThan(Table * table)
{
    const QVector<Table*> allTables = tables();

    foreach(Table* t, allTables)
    {
        if(t->name() != table->name() && t->rows() > 0)
        {
            return t;
        }
    }

    return table;
}

void WindowsManager::prepareContextMenu()
{
    m_contextMenu = new QMenu(this);
    m_activateAction = m_contextMenu->addAction("Activate");
    m_minimizeAction = m_contextMenu->addAction("Minimize");
    m_maximizeAction = m_contextMenu->addAction("Maximize");
    m_contextMenu->addSeparator();
    m_hideAction = m_contextMenu->addAction("Hide Window");
    m_deleteAction =  m_contextMenu->addAction("Delete Window");
    m_renameAction = m_contextMenu->addAction("Rename Window");

    connect(m_table,SIGNAL(doubleClicked(QModelIndex)),this, SLOT(doubleClickedActivateWindow(QModelIndex)));
    connect(m_table, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customMenuRequested(QPoint)));
    connect(m_activateAction,SIGNAL(triggered()),this, SLOT(activateMdiWindow()));
    connect(m_minimizeAction,SIGNAL(triggered()),this, SLOT(minimizeMdiWindow()));
    connect(m_maximizeAction,SIGNAL(triggered()),this, SLOT(maximizeMdiWindow()));
    connect(m_hideAction,SIGNAL(triggered()),this, SLOT(hideMdiWindow()));
    connect(m_deleteAction,SIGNAL(triggered()),this, SLOT(deleteMdiWindow()));
    connect(m_renameAction,SIGNAL(triggered()),this, SLOT(renameMdiWindow()));
}

bool WindowsManager::isNameUnique(const QString& name)
{
    const QVector<MdiSubWindow*>& windows = m_model.windows();
    QVector<MdiSubWindow*>::const_iterator i = std::find_if(windows.begin(), windows.end(), NameMatch(name));

    return i == windows.end();
}

bool WindowsManager::isNameValid(const QString &name)
{
    QRegExp regExp("^[a-zA-Z0-9][a-zA-Z0-9-_]*$");
    bool result = regExp.exactMatch(name);

    return result;
}

MdiSubWindowWrapper WindowsManager::selectedWindow()
{
    QModelIndexList rows = m_table->selectionModel()->selectedRows();
    if(rows.size() < 1) return MdiSubWindowWrapper();

    QModelIndex index = m_proxyModel.mapToSource(rows.first());
    MdiSubWindowWrapper window = m_model.windows().at(index.row());

    return window;
}

void WindowsManager::activateMdiWindow(MdiSubWindowWrapper window)
{
    if(window)
    {
        window.get()->setFocus();
        window.get()->activateWindow();
        window.get()->showNormal();
        m_table->update();
    }
}

void WindowsManager::customMenuRequested(QPoint point)
{
    QModelIndex itemClicked = m_table->indexAt(point);
    if(itemClicked.isValid())
    {
        m_contextMenu->popup(m_table->viewport()->mapToGlobal(point));
    }
}

void WindowsManager::activateMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    activateMdiWindow(window);
}

void WindowsManager::minimizeMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    if(window)
    {
        window.get()->showMinimized();
        m_table->update();
    }
}

void WindowsManager::maximizeMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    if(window)
    {
        window.get()->showMaximized();
        m_table->update();
    }
}

void WindowsManager::hideMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    if(window)
    {
        window.get()->hide();
        m_table->update();
    }
}

void WindowsManager::renameMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    renameMdiWindow(window.get());
}

void WindowsManager::renameMdiWindow(MdiSubWindow *window)
{
    if(window)
    {
        RenameDialog dialog(this, window->name());
        int code = dialog.exec();
        if(code == QDialog::Accepted)
        {
            window->setName(dialog.name());
        }
    }
}

void WindowsManager::deleteMdiWindow()
{
    MdiSubWindowWrapper window = selectedWindow();
    if(window)
    {
        window.get()->confirmClose(false);
        window.get()->close();
    }
}

void WindowsManager::doubleClickedActivateWindow(QModelIndex index)
{
    if(index.isValid())
    {
        QVariant name = m_proxyModel.data(m_proxyModel.index(index.row(),0));
        MdiSubWindowWrapper subWindow = getWindowByName(name.toString());
        activateMdiWindow(subWindow);
    }
}


