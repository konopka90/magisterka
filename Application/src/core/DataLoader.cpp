#include "DataLoader.h"

#include "MainWindow.h"

DataLoader::DataLoader()
    : QWidget(0)
{
    // Setup capture worker
    connect(&m_dataLoaderFromDev, SIGNAL(finishSignal()), this, SLOT(stopCapture()));
    connect(&m_dataLoaderFromDev, SIGNAL(errorSignal(QString)), this, SLOT(onError(QString)));
    connect(&m_dataLoaderFromDev, SIGNAL(updateHeader()), this, SLOT(onRefreshTable()));
}

DataLoader::~DataLoader()
{
    stopCapture();
}

void DataLoader::loadFile(const QString& filepath, Table * table)
{
    m_table = table;
    m_sourceFile = filepath;

    QFile file(m_sourceFile);
    QFileInfo fileInfo(file.fileName());

    if(!file.exists())
    {
        QMessageBox::critical(this,tr("Error"),tr("File \"%1\" does not exist!").arg(fileInfo.fileName()), QMessageBox::Ok,0);
        return;
    }

    QString filename(fileInfo.fileName());

    m_loadingFileDialog = new LoadingFileDialog(filename, this);
    m_loadingFileDialog->setModal(true);


    DataLoaderThreadFromFile worker;
    worker.setup(m_sourceFile, table);
    connect(&worker, SIGNAL(updateStatusSignal(unsigned int)), m_loadingFileDialog, SLOT(updatePercent(unsigned int)));
    connect(&worker, SIGNAL(finishSignal()), this, SLOT(onFinishLoadingFile()));
    connect(&worker, SIGNAL(errorSignal(QString)), this, SLOT(onError(QString)));

    connect(m_loadingFileDialog, SIGNAL(closeSignal()), &worker, SLOT(onCloseDialog()));

    worker.start();
    m_loadingFileDialog->exec();
    worker.wait();
}

void DataLoader::startCapture(const QStringList &devices, Table * table)
{
    m_table = table;
    m_sourceDevs = devices;

    for(int i = 0 ; i < devices.size() ; ++i)
    {
        m_dataLoaderFromDevsCollection.push_back(new DataLoaderThreadFromDev());
    }

    int i = 0;
    foreach(DataLoaderThreadFromDev * loader, m_dataLoaderFromDevsCollection)
    {
        loader->setup(devices[i], table);
        loader->start();
        ++i;
    }

    connect(&timer, SIGNAL(timeout()), this, SLOT(onRefreshTable()));

    timer.start(1000);

}

bool DataLoader::isCapturing() const
{
    return m_dataLoaderFromDevsCollection.size() > 0;
}

void DataLoader::stopCapture()
{
    if(m_dataLoaderFromDevsCollection.size() > 0)
        disconnect(&timer, SIGNAL(timeout()), this, SLOT(onRefreshTable()));

    foreach(DataLoaderThreadFromDev * loader, m_dataLoaderFromDevsCollection)
    {
        loader->stop();
        loader->wait();
        delete loader;
    }

    m_dataLoaderFromDevsCollection.clear();
}

void DataLoader::onFinishLoadingFile()
{
    m_loadingFileDialog->close();
    delete m_loadingFileDialog;

    onRefreshTable();
}

void DataLoader::onRefreshTable()
{
    m_table->refreshAllTables();
}

void DataLoader::onError(QString error)
{
    QMessageBox::critical(this, tr("Error"), tr("Error: ") + error);

    emit errorSignal();
}


