#ifndef MYMATH_H
#define MYMATH_H

#include <time.h>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

class MyMath
{
public:

    static double randomWithNormalDistribution()
    {
        static boost::variate_generator<boost::mt19937, boost::normal_distribution<> >
                generator(boost::mt19937(time(0)), boost::normal_distribution<>());

        return generator();
    }

    static double pixelsToMilimeters(unsigned int pixels, unsigned int resolution)
    {
        double _resolution = resolution;
        double _pixels = pixels;

        return _pixels/_resolution * 25.4;
    }

    static double Log2(double n)
    {
        return log(n) / log(2.0);
    }

};

#endif // MYMATH_H
