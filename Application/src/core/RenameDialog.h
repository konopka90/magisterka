#ifndef RENAMEDIALOG_H
#define RENAMEDIALOG_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QSizePolicy>
#include <QMessageBox>

class WindowsManager;

class RenameDialog : public QDialog
{
    Q_OBJECT
public:
    explicit RenameDialog(WindowsManager *parent, const QString&);
    const QString& name() const;

private:
    QLineEdit * m_lineEdit;
    QString m_name;
    WindowsManager * m_windowsManager;

signals:
    void checkNameSignal();

public slots:
    void applyButtonClicked();

};

#endif // RENAMEDIALOG_H
