#include "MdiSubWindow.h"
#include "MainWindow.h"
#include "WindowsManager.h"

MdiSubWindow::MdiSubWindow(MainWindow *parent, MdiSubWindowType type) :
    QMdiSubWindow(parent), m_mainWindow(parent), m_type(type), m_confirmClose(true)
{
    m_name = m_mainWindow->windowsManager().generateUniqueName(type);
    m_creationDateTime = QDateTime::currentDateTime();
    setWindowTitle(m_name);
}

MdiSubWindow::~MdiSubWindow()
{}

MainWindow * MdiSubWindow::getMainWindow() const
{
    return m_mainWindow;
}

void MdiSubWindow::save(QDataStream &stream)
{
    stream << pos();
    stream << size();
    stream << isHidden();
    stream << isMaximized();
    stream << isMinimized();
    stream << m_name;
    stream << m_creationDateTime;
}

void MdiSubWindow::load(QDataStream &stream)
{
    QPoint pos;
    QSize size;
    bool isHidden;
    bool isMaximized;
    bool isMinimized;

    stream >> pos;
    stream >> size;
    stream >> isHidden;
    stream >> isMaximized;
    stream >> isMinimized;
    stream >> m_name;
    stream >> m_creationDateTime;

    this->setGeometry(pos.x(), pos.y(), size.width(), size.height());
    this->setName(m_name);

    if(isMaximized)
        this->showMaximized();

    if(isMinimized)
        this->showMinimized();

    if(isHidden)
        this->hide();

}

const QString &MdiSubWindow::name()
{
    return m_name;
}

MdiSubWindow::MdiSubWindowType MdiSubWindow::type()
{
    return m_type;
}

QString MdiSubWindow::typeToStr()
{
    switch(m_type)
    {
        case MdiSubWindow::Table:
        return tr("Table");

        case MdiSubWindow::PlotLine:
        return tr("Line");

        case MdiSubWindow::PlotHistogram:
        return tr("Histogram");

        case MdiSubWindow::ScriptWindow:
        return tr("Script window");
    }

    return tr("Unknown");
}

QString MdiSubWindow::state()
{
    if(!this->isVisible())
        return tr("Hidden");

    if(this->isMaximized())
        return tr("Maximized");

    if(this->isMinimized())
        return tr("Minimized");


    return tr("Normal");
}

const QDateTime &MdiSubWindow::creationDateTime()
{
    return m_creationDateTime;
}

void MdiSubWindow::setName(const QString &name)
{
    m_name = QString(name);
    setWindowTitle(m_name);
}

void MdiSubWindow::confirmClose(bool confirm)
{
    m_confirmClose = confirm;
}

void MdiSubWindow::closeEvent( QCloseEvent *e )
{
    if(m_confirmClose)
    {
        MessageBoxButton buttonPressed = showPopup();

        switch(buttonPressed)
        {
            case MdiSubWindow::Hide:
                e->ignore();
                this->hide();
                break;

            case MdiSubWindow::Delete:
                onWindowDelete();
                e->accept();
                emit closedWindow(this);
                break;

            default:
                e->ignore();
                break;
        }
    }
    else
    {
        emit closedWindow(this);
        e->accept();
    }
}

MdiSubWindow::MessageBoxButton MdiSubWindow::showPopup()
{
    QString text = QString("Do you want to hide or delete '")
            .append(this->name())
            .append("' ?");

    QMessageBox msg;
    msg.setText(text);
    msg.setWindowTitle("Question");
    msg.setIcon(QMessageBox::Question);
    QAbstractButton *hideButton = msg.addButton(tr("Hide"), QMessageBox::AcceptRole);
    QAbstractButton *deleteButton = msg.addButton(tr("Delete"), QMessageBox::DestructiveRole);
    QAbstractButton *cancelButton = msg.addButton(tr("Cancel"), QMessageBox::RejectRole);

    msg.exec();

    if(msg.clickedButton() == hideButton)
    {
        return MdiSubWindow::Hide;
    }

    if(msg.clickedButton() == deleteButton)
    {
        return MdiSubWindow::Delete;
    }

    return MdiSubWindow::Cancel;
}

void MdiSubWindow::closeWithoutAsking()
{
    confirmClose(false);
    this->close();
    //emit close();
}

