#include "DataLoaderThread.h"

DataLoaderThread::DataLoaderThread()
    : m_options(Options::instance()),
      m_isFinished(false)
{
}

void DataLoaderThread::appendDataToTable(const u_char *packet)
{

    // retireve the position of the ip header
    ih = (ip_header *) (packet +
        14); //length of ethernet header

    // retireve the position of the udp header
    ip_len = (ih->ver_ihl & 0xf) * 4;
    uh = (udp_header *) ((u_char*)ih + ip_len);

    // convert from network byte order to host byte order
    sport = ntohs( uh->sport );
    dport = ntohs( uh->dport );

    data_ptr = (DataRow*)((u_char*)uh+ sizeof(udp_header));


    m_table->appendRow();
}
