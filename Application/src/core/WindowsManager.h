#ifndef WINDOWSMANAGER_H
#define WINDOWSMANAGER_H

#include <QDockWidget>
#include <QListWidget>
#include <QTableView>
#include <QSortFilterProxyModel>
#include <QMenu>
#include <QRegExp>
#include <algorithm>

#include "WindowsManagerModel.h"
#include "MdiSubWindow.h"
#include "RenameDialog.h"
#include "../scripting/ScriptWindow.h"


class WindowsManager : public QDockWidget
{
    Q_OBJECT
public:
    explicit WindowsManager(MainWindow *parent = 0);
    void clear();
    void addWindow(MdiSubWindow*);
    void removeWindow(MdiSubWindow*);

    MdiSubWindow * getWindowByName(const QString & name);
    MdiSubWindow * findWindow(const QString& name, bool isPartialMatch = true, bool isCaseSentensive = false, bool showPopup = true);
    void refreshList();
    QString generateUniqueName(MdiSubWindow::MdiSubWindowType);
    Table * getTableWithRowsOtherThan(Table*);

    inline const QVector<MdiSubWindow*>& windows() const
    {
        return m_model.windows();
    }

    inline const QVector<Table*>& tables() const
    {
        return m_model.tables();
    }

    inline const QVector<ScriptWindow*> & scripts() const
    {
        return m_model.scripts();
    }

    inline const QVector<PlotHistogram*> & histograms() const
    {
        return m_model.histograms();
    }

    inline const QVector<PlotLine*> & plotsLine() const
    {
        return m_model.plotsLine();
    }

private:
    void prepareContextMenu();

    bool isNameUnique(const QString& name);
    bool isNameValid(const QString& name);
    MdiSubWindowWrapper selectedWindow();


private:
    MainWindow& m_mainWindow;
    QMap<MdiSubWindow::MdiSubWindowType, int> m_uniqueNameCounters;
    WindowsManagerModel m_model;
    QSortFilterProxyModel m_proxyModel;
    QTableView * m_table;

    QMenu * m_contextMenu;
    QAction * m_activateAction;
    QAction * m_maximizeAction;
    QAction * m_minimizeAction;
    QAction * m_hideAction;
    QAction * m_deleteAction;
    QAction * m_renameAction;

signals:

public slots:
    void customMenuRequested(QPoint);
    void activateMdiWindow(MdiSubWindowWrapper);
    void activateMdiWindow();
    void minimizeMdiWindow();
    void maximizeMdiWindow();
    void hideMdiWindow();
    void renameMdiWindow();
    void renameMdiWindow(MdiSubWindow * window);
    void deleteMdiWindow();

private slots:
    void doubleClickedActivateWindow(QModelIndex);

    friend class RenameDialog;
};

#endif // WINDOWSMANAGER_H
