#include "RenameDialog.h"

#include "WindowsManager.h"

RenameDialog::RenameDialog(WindowsManager *parent, const QString& name) :
    QDialog(parent), m_windowsManager(parent)
{
    QHBoxLayout * layout = new QHBoxLayout();
    QLabel * label = new QLabel(tr("Window Title: "),this);
    m_lineEdit = new QLineEdit(this);
    m_lineEdit->setText(name);

    layout->addWidget(label);
    layout->addWidget(m_lineEdit);

    QHBoxLayout * layoutButtons = new QHBoxLayout();
    QPushButton * cancelButton = new QPushButton(tr("Cancel"));
    QPushButton * applyButton = new QPushButton(tr("Apply"));

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored);

    layoutButtons->addWidget(spacer);
    layoutButtons->addWidget(applyButton);
    layoutButtons->addWidget(cancelButton);

    QVBoxLayout * mainLayout = new QVBoxLayout();
    mainLayout->addLayout(layout);
    mainLayout->addLayout(layoutButtons);

    QSize size = this->sizeHint();
    size.setWidth(300);
    this->resize(size);
    this->setLayout(mainLayout);
    this->setWindowTitle("Rename window");

    connect(applyButton, SIGNAL(clicked()), this, SLOT(applyButtonClicked()));
    connect(cancelButton,SIGNAL(clicked()), this, SLOT(reject()));
}

const QString &RenameDialog::name() const
{
    return m_name;
}

void RenameDialog::applyButtonClicked()
{
    QString name = m_lineEdit->text();
    QString errorTitle = tr("Error");
    QString errorMsg;

    if(!m_windowsManager->isNameUnique(name))
    {
        errorMsg = QString("Name '").append(name).append("' already exists!\n\n")
                .append("Please choose another name!");
        QMessageBox::critical(this,errorTitle,errorMsg);
        return;
    }

    if(!m_windowsManager->isNameValid(name))
    {
        errorMsg = QString("Please enter valid name!");
        QMessageBox::critical(this,errorTitle,errorMsg);
        return;
    }

    m_name = name;
    emit accept();
}
