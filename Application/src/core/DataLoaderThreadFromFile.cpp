#include "DataLoaderThreadFromFile.h"

DataLoaderThreadFromFile::DataLoaderThreadFromFile()
{
}


pcap_t *DataLoaderThreadFromFile::getHandle()
{
    pcap_t * handle = NULL;
    handle = pcap_open_offline(m_source.c_str(), m_errorBuffer);
    return handle;
}

void DataLoaderThreadFromFile::updateStatus()
{
    int percentOfLoading = int(double(++m_actualPacketNumber) / m_totalNumberOfPackets * 100.0);
    if(percentOfLoading != m_previousPercentOfLoading)
    {
        emit updateStatusSignal(percentOfLoading);
        m_previousPercentOfLoading = percentOfLoading;
    }
}

void DataLoaderThreadFromFile::onCloseDialog()
{
    m_isFinished = true;
}

uint64_t DataLoaderThreadFromFile::countPackets()
{
    pcap_t *handle;
    struct pcap_pkthdr header;
    handle = getHandle();
    if(!handle)
        return 0;

    const u_char *packetPtr;
    uint64_t counter = 0;
    while(packetPtr = pcap_next(handle,&header))
    {
        if(packetPtr)
            counter++;
    }

    pcap_close(handle);

    return counter;
}

void DataLoaderThreadFromFile::setup(const QString &filename, Table * table)
{
    m_source = filename.toStdString();
    m_table = table;
}

void DataLoaderThreadFromFile::run()
{
    struct pcap_pkthdr header;
    pcap_t *handle;

    handle = getHandle();
    if(!handle)
    {
        emit errorSignal(QString::fromStdString(m_errorBuffer));
        emit finishSignal();
        return;
    }

    m_actualPacketNumber = 0;
    m_previousPercentOfLoading = 0;
    m_totalNumberOfPackets = countPackets();

    const u_char * packet;
    m_table->startInsertingRows();
    while(packet = pcap_next(handle,&header))
    {
        if(m_isFinished) break;

        appendDataToTable(packet);
        updateStatus();
    }
    m_table->stopInsertingRows();
    pcap_close(handle);

    emit finishSignal();
}

