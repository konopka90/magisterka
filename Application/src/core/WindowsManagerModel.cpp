#include "WindowsManagerModel.h"
#include "../table/Table.h"

WindowsManagerModel::WindowsManagerModel(QObject *parent) :
    QAbstractTableModel(parent), COLUMNS(4)
{
}


QModelIndex WindowsManagerModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row,column);
}

QModelIndex WindowsManagerModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}

int WindowsManagerModel::rowCount(const QModelIndex &parent) const
{
    return m_windows.size();
}

int WindowsManagerModel::columnCount(const QModelIndex &parent) const
{
    return COLUMNS;
}

QVariant WindowsManagerModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::EditRole) return QVariant();
    if (index.row() >= m_windows.size()) return QVariant();

    //qDebug() << "data - vec size: " <<  m_windows.size() << ", index.row(): " << index.row() << ", index.col(): " << index.column();

    MdiSubWindow* window = m_windows[index.row()];

    switch(index.column())
    {
    case 0:
        return window->name();

    case 1:
        return window->typeToStr();

    case 2:
        return window->state();

    case 3:
        return window->creationDateTime().toString("yyyy-MM-dd HH:mm:ss");
    }

    return QVariant();
}


QVariant WindowsManagerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
            case 0:
                return tr("Name");
            case 1:
                return tr("Type");
            case 2:
                return tr("View");
            case 3:
                return tr("Creation time");

            default:
                return tr("Unknown");
        }
    }

    if(orientation == Qt::Vertical)
    {
        return QVariant();
    }

    return QVariant();
}

void WindowsManagerModel::append(MdiSubWindow* window)
{
    beginInsertRows(QModelIndex(), m_windows.size(), m_windows.size());
    m_windows.append(window);

    if(window->inherits("Table"))
    {
        Table * table = qobject_cast<Table*>(window);
        m_tables.append(table);
    }
    else if(window->inherits("Plot"))
    {
        Plot * plot = qobject_cast<Plot*>(window);
        m_plots.append(plot);

        if(window->inherits("PlotLine"))
        {
            PlotLine * plotLine = qobject_cast<PlotLine*>(window);
            m_plotsLine.append(plotLine);
        }
        else if(window->inherits("PlotHistogram"))
        {
            PlotHistogram * histogram = qobject_cast<PlotHistogram*>(window);
            m_histograms.append(histogram);
        }
    }
    else if(window->inherits("ScriptWindow"))
    {
        ScriptWindow * script = qobject_cast<ScriptWindow*>(window);
        m_scripts.append(script);
    }

    endInsertRows();
}

void WindowsManagerModel::remove(MdiSubWindow* window)
{
    int size = m_windows.size();

    for(int i = 0 ; i < size ; ++i)
    {
        if(m_windows[i]->name() == window->name())
        {
            beginRemoveRows(QModelIndex(), i, i);
            qDebug() << "usuwam " << i;

            if(window->inherits("Table"))
            {
                Table * table = qobject_cast<Table*>(window);
                m_tables.removeAt(m_tables.indexOf(table));
            }
            else if(window->inherits("Plot"))
            {                
                Plot * plot = qobject_cast<Plot*>(window);
                m_plots.removeAt(m_plots.indexOf(plot));

                if(window->inherits("PlotLine"))
                {
                    PlotLine * plotLine = qobject_cast<PlotLine*>(window);
                    m_plotsLine.removeAt(m_plotsLine.indexOf(plotLine));
                }
                else if(window->inherits("PlotHistogram"))
                {
                    PlotHistogram * histogram = qobject_cast<PlotHistogram*>(window);
                    m_histograms.removeAt(m_histograms.indexOf(histogram));
                }
            }
            else if(window->inherits("ScriptWindow"))
            {
                ScriptWindow * script = qobject_cast<ScriptWindow*>(window);
                m_scripts.removeAt(m_scripts.indexOf(script));
            }


            m_windows.remove(i);
            endRemoveRows();
            break;
        }
    }
}

const QVector<MdiSubWindow*> &WindowsManagerModel::windows() const
{
    return m_windows;
}

const QVector<Table *> &WindowsManagerModel::tables() const
{
    return m_tables;
}

const QVector<Plot *> &WindowsManagerModel::plots() const
{
    return m_plots;
}

const QVector<ScriptWindow*> &WindowsManagerModel::scripts() const
{
    return m_scripts;
}

const QVector<PlotHistogram *> &WindowsManagerModel::histograms() const
{
    return m_histograms;
}

const QVector<PlotLine *> &WindowsManagerModel::plotsLine() const
{
    return m_plotsLine;
}

void WindowsManagerModel::refresh()
{
    emit this->dataChanged(createIndex(0,0),createIndex(m_windows.size()-1,COLUMNS-1));
}
