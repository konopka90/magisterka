#ifndef MEMORYPOOLV2_H
#define MEMORYPOOLV2_H

#include <stdint.h>
#include <QDebug>
#include <QMutex>
#include <QFile>
#include <errno.h>

#ifdef PLATFORM_WIN32
    #include <windows.h>
#endif

#ifdef PLATFORM_UNIX
    #include <unistd.h>
    #include <sys/mman.h>
    #include <sys/types.h>
    #include <sys/param.h>
#endif

// Reference: http://www.flipcode.com/archives/Fast_Allocation_Pool.shtml

class MemoryPool
{
public:


    MemoryPool() ;
    inline static MemoryPool & getMainPool() {

        return m_mainPool;
    }

    void setup(const uint percentOfMemory, uint64_t chunkSize, uint64_t nextPoolSize = 0);

    uint64_t getNumberOfMaxChunksInPool();

    // Gets free physical memory in bytes.
    uint64_t getFreePhysicalMemory();


    inline bool isFull() const
    {
        return m_available >= m_last;
    }

    inline void* allocate()
    {
        QMutexLocker locker(&m_lock);

        // If there are any holes in the free stack then fill them up.
        if (m_TOS == 0)
        {
            // If there is any space left in this pool then use it, otherwise move
            // on to the next in the linked list.
            if (!isFull())
            {
                uchar*	pReturn = m_available;
                m_available += m_chunkSize;
                return reinterpret_cast<void*>(pReturn);
            }
            else
            {
                // If there is another pool in the list then pass the request on to
                // it, otherwise try to create a new pool.
                if (m_next)
                {
                    return m_next->allocate();
                }
                else
                {
                    if (m_nextPoolSize > 0)
                    {
                        m_next = new MemoryPool(this);
                        if (m_next)
                            return m_next->allocate();
                    }
                    // If we cannot allocate a new pool then return 0.
                    return 0;
                }
            }
        }
        else
            return reinterpret_cast<void*>(m_freeStack[--m_TOS]);
    }

    void deallocate(void* chunk);
    void clear();

private:
   friend class MainWindow;
    static MemoryPool m_mainPool;

    MemoryPool(MemoryPool* const prev);
    uchar *allocateMemory(uint64_t size);
    void deallocateMemory();

    // New allocation pools are created and inserted into a doubly-linked list.
    MemoryPool* m_prev;
    MemoryPool*	m_next;

    uint64_t        m_nextPoolSize;
    uint64_t		m_percentOfMemory;
    size_t          m_chunkSize;     // The size of the contained item.
    uchar*			m_available;	// Next available item.
    uchar*			m_last;         // End of the pool.
    uint64_t    	m_TOS;			// Top of the free stack.
    uint64_t        m_numberOfChunks;
    uchar*			m_pool;         // The allocation pool of items.
    uchar**			m_freeStack;	// The stack of deleted items.
    uint64_t        m_size;
    uint64_t        m_freeStackSize;
    QMutex          m_lock;


#ifdef PLATFORM_WIN32
    MEMORYSTATUSEX m_statex;
#endif


};

#endif // MEMORYPOOLV2_H
