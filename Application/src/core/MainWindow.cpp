#include "MainWindow.h"
#include "../plot/PlotFactory.h"

MainWindow::MainWindow()
    : QMainWindow(),
      m_activeTable(NULL),
      m_activePlot(NULL),
      m_options(Options::instance()),
      m_memoryPool(MemoryPool::getMainPool()),
      m_plotFactory(this),
      m_projectManager(this)
{
    srand (time(NULL));
    //emoryPool::setMainPool(&m_memoryPool);

    qDebug() << "Loading options...";
    m_options.load();
    qDebug() << "Options loaded!";

    m_windowsManager = m_projectManager.windowsManager();

    connect(&m_dataLoader, SIGNAL(errorSignal()), this, SLOT(stopCapture()));

    qDebug() << "Preparing memory pool...";
    prepareMemoryPool();

    qDebug() << "Memory pool ready!";
    qDebug() << "Preparing menu...";
    prepareMenu();
    qDebug() << "Menu ready!";
    qDebug() << "Preparing workspace...";
    prepareWorkspace();
    qDebug() << "Workspace ready!";
    prepareCaptionTimer();
    prepareWindowsManager();
    prepareScriptingConsole();
    updateCaption();
    showMaximized();

    //runDebugCode();

    connect(this, SIGNAL(signalStartCapture()), this, SLOT(startCapture()));
}

MainWindow::~MainWindow()
{
    m_projectManager.closeProject();
    m_memoryPool.deallocateMemory();
}
MemoryPool& MainWindow::memoryPool()
{
    return m_memoryPool;
}

void MainWindow::prepareMenu()
{
    // Menu File

    m_menuFile = this->menuBar()->addMenu(tr("&File"));
    connect(m_menuFile, SIGNAL(aboutToShow()), this, SLOT(aboutToShowFileMenu()));

    // Menu View

    m_menuView = this->menuBar()->addMenu(tr("&View"));

    m_menuActionWindows = new QAction(tr("Windows"),this);
    m_menuActionWindows->setCheckable(true);
    m_menuActionWindows->setChecked(true);

    m_menuActionScriptingConsole = new QAction(tr("Scripting Console"),this);
    m_menuActionScriptingConsole->setCheckable(true);
    m_menuActionScriptingConsole->setChecked(true);

    m_menuView->addAction(m_menuActionScriptingConsole);
    m_menuView->addAction(m_menuActionWindows);

    connect(m_menuActionScriptingConsole, SIGNAL(changed()), this, SLOT(toggleDockScriptingConsole()));
    connect(m_menuActionWindows, SIGNAL(changed()), this, SLOT(toggleDockWindows()));

    // Menu Capture

    m_menuCapture = this->menuBar()->addMenu((tr("&Capture")));

    m_menuCaptureStart = new QAction(tr("Start"), this);
    m_menuCaptureStop = new QAction(tr("Stop"), this);
    m_menuCaptureStop->setEnabled(false);

    m_menuCapture->addAction(m_menuCaptureStart);
    m_menuCapture->addAction(m_menuCaptureStop);

    connect(m_menuCaptureStart, SIGNAL(triggered()), this, SLOT(startCapture()));
    connect(m_menuCaptureStop, SIGNAL(triggered()), this, SLOT(stopCapture()));

    // Menu Plot

    m_menuPlot = this->menuBar()->addMenu(tr("&Plot"));

    m_menuPlotLineTogetherAction = new QAction(tr("Line"), this);
    m_menuPlotLineSeparatelyAction = new QAction(tr("Line"), this);
    m_menuPlotHistogramTogetherAction = new QAction(tr("Histogram"), this);
    m_menuPlotHistogramSeparatelyAction = new QAction(tr("Histogram"), this);

    m_menuPlotSeparately = m_menuPlot->addMenu(tr("Separately"));
    m_menuPlotSeparately->addAction(m_menuPlotLineSeparatelyAction);
    m_menuPlotSeparately->addAction(m_menuPlotHistogramSeparatelyAction);

    m_menuPlotTogether = m_menuPlot->addMenu(tr("Together"));
    m_menuPlotTogether->addAction(m_menuPlotLineTogetherAction);
    m_menuPlotTogether->addAction(m_menuPlotHistogramTogetherAction);

    m_menuPlot->addMenu(m_menuPlotSeparately);
    m_menuPlot->addMenu(m_menuPlotTogether);
    
    QAction * actionDeleteFits = m_menuPlot->addAction(tr("Delete Fits"));

    connect(m_menuPlotLineTogetherAction, SIGNAL(triggered()), this, SLOT(plotLineTogether()));
    connect(m_menuPlotLineSeparatelyAction, SIGNAL(triggered()), this, SLOT(plotLineSeparately()));

    connect(m_menuPlotHistogramTogetherAction, SIGNAL(triggered()), this, SLOT(plotHistogramTogether()));
    connect(m_menuPlotHistogramSeparatelyAction, SIGNAL(triggered()), this, SLOT(plotHistogramSeparately()));

    connect(actionDeleteFits, SIGNAL(triggered()), this, SLOT(deleteFits()));
    
    // Menu Table

    m_menuTable = new QMenu(tr("&Table"));
    connect(m_menuTable, SIGNAL(aboutToShow()),this, SLOT(aboutToShowTableMenu()));

    // Menu Analysis
    m_menuAnalysis = new QMenu(tr("&Analysys"));
    connect(m_menuAnalysis, SIGNAL(aboutToShow()),this, SLOT(aboutToShowAnalysisMenu()));

    // Menu Windows

    m_menuWindows = this->menuBar()->addMenu(tr("&Windows"));
    connect(m_menuWindows, SIGNAL(aboutToShow()), this, SLOT(aboutToShowWindowsMenu()));


}

void MainWindow::prepareWorkspace()
{
   m_workspace = new QMdiArea();
   m_workspace->setOption(QMdiArea::DontMaximizeSubWindowOnActivation);
   m_workspace->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
   m_workspace->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
   m_workspace->setActivationOrder(QMdiArea::ActivationHistoryOrder);
   connect(m_workspace, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(subWindowActivated(QMdiSubWindow*)));
   this->setCentralWidget(m_workspace);
}

void MainWindow::prepareCaptionTimer()
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateCaption()));
    timer->start(1000);
}

void MainWindow::prepareWindowsManager()
{
    addDockWidget(Qt::BottomDockWidgetArea, m_windowsManager);
    connect(m_windowsManager, SIGNAL(visibilityChanged(bool)),this,SLOT(windowsManagerVisibilityChanged(bool)));
}

void MainWindow::prepareScriptingConsole()
{
    try
    {
        m_scriptingConsole = new ScriptingConsole(this);
        addDockWidget(Qt::TopDockWidgetArea, m_scriptingConsole);
        connect(m_scriptingConsole, SIGNAL(visibilityChanged(bool)),this,SLOT(scriptingConsoleVisibilityChanged(bool)));

        ScriptingPython::init();
        connect(PythonQt::self(),SIGNAL(pythonStdOut(QString)), m_scriptingConsole, SLOT(appendText(QString)));
        connect(PythonQt::self(),SIGNAL(pythonStdErr(QString)), m_scriptingConsole, SLOT(appendText(QString)));
    }
    catch(QException & e)
    {
        QMessageBox::critical(this, tr("error"), tr(e.what()));
    }
}

void MainWindow::aboutToShowWindowsMenu()
{
    m_menuWindows->clear();

    if(m_windowsManager->windows().size() > 0)
    {
        QAction * actionCascade = m_menuWindows->addAction(tr("Cascade"));
        QAction * actionTile = m_menuWindows->addAction(tr("Tile"));
        m_menuWindows->addSeparator();
        QAction * actionNext = m_menuWindows->addAction(tr("Next"));
        actionNext->setShortcut(QKeySequence(tr("F5")));
        QAction * actionPrev = m_menuWindows->addAction(tr("Previous"));
        actionPrev->setShortcut(QKeySequence(tr("F6")));
        m_menuWindows->addSeparator();

        QAction * actionFind = m_menuWindows->addAction(tr("Find..."));
        QAction * actionRenameWindow = m_menuWindows->addAction(tr("Rename Window..."));

        m_menuWindows->addSeparator();

        QAction * actionHideWindow = m_menuWindows->addAction(tr("Hide Window"));
        QAction * actionCloseWindow = m_menuWindows->addAction(tr("Close Window"));

        const QVector<MdiSubWindow*> & subWindows = m_windowsManager->windows();
        if(subWindows.size() > 0)
        {
            m_menuWindows->addSeparator();
        }


        foreach(MdiSubWindow* window, subWindows)
        {
            QAction * actionWindow = m_menuWindows->addAction(window->name());
            actionWindow->setCheckable(true);

            MdiSubWindow * currentWindow = qobject_cast<MdiSubWindow*>(m_workspace->activeSubWindow());
            if(window == currentWindow )
            {
                actionWindow->setChecked(true);
            }

            connect(actionWindow,SIGNAL(triggered()),this, SLOT(activateWindowSelectedByQAction()));
        }

        connect(actionCascade, SIGNAL(triggered()), this, SLOT(showWindowsInCascade()));
        connect(actionTile, SIGNAL(triggered()), this, SLOT(showWindowsInTile()));
        connect(actionNext, SIGNAL(triggered()), this, SLOT(nextWindow()));
        connect(actionPrev, SIGNAL(triggered()), this, SLOT(previousWindow()));
        connect(actionFind, SIGNAL(triggered()), this, SLOT(findWindow()));
        connect(actionRenameWindow, SIGNAL(triggered()), this, SLOT(renameWindow()));
        connect(actionHideWindow, SIGNAL(triggered()), this, SLOT(hideWindow()));
        connect(actionCloseWindow, SIGNAL(triggered()), this, SLOT(closeWindow()));
    }
}

void MainWindow::aboutToShowTableMenu()
{
    m_menuTable->clear();

    m_menuTableSetColumnsAs = m_menuTable->addMenu(tr("Set Columns As"));
    m_menuActionSetAsX = m_menuTableSetColumnsAs->addAction(tr("X"));
    m_menuActionSetAsY = m_menuTableSetColumnsAs->addAction(tr("Y"));

    connect(m_menuActionSetAsX, SIGNAL(triggered()), this, SLOT(setTableColumnsAsX()));
    connect(m_menuActionSetAsY, SIGNAL(triggered()), this, SLOT(setTableColumnsAsY()));

    QAction * actionColumnOptions = m_menuTable->addAction(tr("Column Options..."));
    connect(actionColumnOptions, SIGNAL(triggered()), this, SLOT(showColumnOptions()));

    m_menuTable->addSeparator();

    QAction * actionSetColumnValues = m_menuTable->addAction(tr("Set Column Values..."));
    QAction * actionRecalculate = m_menuTable->addAction(tr("Recalculate"));



    m_menuTableFillSelectionWith = m_menuTable->addMenu(tr("Fill Selection With"));
    QAction * actionRowNumbers = m_menuTableFillSelectionWith->addAction(tr("Row Numbers"));
    QAction * actionRandomValues = m_menuTableFillSelectionWith->addAction(tr("Random Values"));
    QAction * actionNormalRandomValues = m_menuTableFillSelectionWith->addAction(tr("Normal Random Values"));

    connect(actionSetColumnValues, SIGNAL(triggered()), this, SLOT(showSetColumnValues()));
    connect(actionRecalculate, SIGNAL(triggered()), this, SLOT(recalculateTable()));
    connect(actionRowNumbers, SIGNAL(triggered()), this, SLOT(fillTableWithRowNumbers()));
    connect(actionRandomValues, SIGNAL(triggered()), this, SLOT(fillTableWithRandomValues()));
    connect(actionNormalRandomValues, SIGNAL(triggered()), this, SLOT(fillTableWithNormalRandomValues()));

    QAction * actionClearTable = m_menuTable->addAction(tr("Clear"));
    connect(actionClearTable, SIGNAL(triggered()), this, SLOT(clearTable()));

    m_menuTable->addSeparator();

    QAction * actionHideSelected = m_menuTable->addAction(tr("Hide Selected Columns"));
    QAction * actionClearSelected = m_menuTable->addAction(tr("Clear Selected Columns"));
    QAction * actionShowAll = m_menuTable->addAction(tr("Show All Columns"));

    connect(actionHideSelected, SIGNAL(triggered()), this, SLOT(hideSelectedColumns()));
    connect(actionClearSelected, SIGNAL(triggered()), this, SLOT(clearSelectedColumns()));
    connect(actionShowAll, SIGNAL(triggered()), this, SLOT(showAllColumns()));

    m_menuTable->addSeparator();

    QAction * actionMoveToFirst = m_menuTable->addAction(tr("Move To First"));
    QAction * actionMoveLeft = m_menuTable->addAction(tr("Move Left"));
    QAction * actionMoveRight = m_menuTable->addAction(tr("Move Right"));
    QAction * actionMoveToLast = m_menuTable->addAction(tr("Move To Last"));

    actionMoveToFirst->setEnabled(false);
    actionMoveLeft->setEnabled(false);
    actionMoveRight->setEnabled(false);
    actionMoveToLast->setEnabled(false);

    if(m_activeTable)
    {
        TableColumnPtr column = m_activeTable->firstSelectedColumn();
        if(column)
        {
            if(column->visualIndex() < DATAROW_SIZE - 1)
            {
                actionMoveRight->setEnabled(true);
                actionMoveToLast->setEnabled(true);
            }

            if(column->visualIndex() > 0)
            {
                actionMoveLeft->setEnabled(true);
                actionMoveToFirst->setEnabled(true);
            }
        }
    }

    connect(actionMoveLeft, SIGNAL(triggered()), this, SLOT(moveColumnLeft()));
    connect(actionMoveRight, SIGNAL(triggered()), this, SLOT(moveColumnRight()));
    connect(actionMoveToFirst, SIGNAL(triggered()), this, SLOT(moveColumnToFirst()));
    connect(actionMoveToLast, SIGNAL(triggered()), this, SLOT(moveColumnToLast()));

    m_menuTable->addSeparator();

    QAction * actionRows = m_menuTable->addAction(tr("Rows..."));
    QAction * actionDeleteRowsInterval = m_menuTable->addAction(tr("Delete Rows Interval..."));
    QAction * actionMoveRowUpward = m_menuTable->addAction(tr("Move Row Upward"));
    QAction * actionMoveRowDownward = m_menuTable->addAction(tr("Move Row Downward"));

    if(m_activeTable)
    {
        if(m_activeTable->rows() == 0)
        {
            actionDeleteRowsInterval->setDisabled(true);
        }

        if(!m_activeTable->isAnyRowSelected())
        {
            actionMoveRowDownward->setDisabled(true);
            actionMoveRowUpward->setDisabled(true);
        }
    }

    connect(actionRows, SIGNAL(triggered()), this, SLOT(appendOrRemoveRowsInTable()));
    connect(actionDeleteRowsInterval, SIGNAL(triggered()), this, SLOT(deleteRowsInterval()));
    connect(actionMoveRowUpward, SIGNAL(triggered()), this, SLOT(moveRowUpward()));
    connect(actionMoveRowDownward, SIGNAL(triggered()), this, SLOT(moveRowDownward()));

    m_menuTable->addSeparator();

    QAction * actionGoToRow = m_menuTable->addAction(tr("Go To Row..."));
    QAction * actionGoToColumn = m_menuTable->addAction(tr("Go To Column..."));

    connect(actionGoToRow, SIGNAL(triggered()), this, SLOT(goToRow()));
    connect(actionGoToColumn, SIGNAL(triggered()), this, SLOT(goToColumn()));
}

void MainWindow::aboutToShowFileMenu()
{
    m_menuFile->clear();

    m_menuNew = m_menuFile->addMenu(tr("New"));

    QAction * newProject =  m_menuNew->addAction(tr("New Project"));
    QAction * newTable =  m_menuNew->addAction(tr("New Table"));
    QAction * newScript = m_menuNew->addAction(tr("New Script Window"));
    QAction * openProject = m_menuFile->addAction(tr("Open..."));
    QAction * openPcap = m_menuFile->addAction(tr("&Open PCAP..."));
    QAction * openScript = m_menuFile->addAction(tr("&Open Script..."));
    QAction * close = m_menuFile->addAction(tr("Close"));

    m_menuFile->addSeparator();

    QAction * saveProject = m_menuFile->addAction(tr("&Save Project"));
    QAction * saveProjectAs = m_menuFile->addAction(tr("Save Project &As..."));
    QAction * saveWindowAs = m_menuFile->addAction(tr("Save Window As..."));

    if(!m_projectManager.isFileAssigned())
        saveProject->setEnabled(false);

    m_menuFile->addSeparator();

    QAction * options  = m_menuFile->addAction(tr("&Preferences..."));
    QAction * quit = m_menuFile->addAction(tr("&Quit"));

    newProject->setShortcut(QKeySequence("Ctrl+P"));
    newTable->setShortcut(QKeySequence("Ctrl+T"));
    saveProject->setShortcut(QKeySequence("Ctrl+S"));
    saveProjectAs->setShortcut(QKeySequence("Ctrl+Shift+S"));


    if(!m_workspace->activeSubWindow())
    {
        saveWindowAs->setDisabled(true);
    }

    connect(newProject, SIGNAL(triggered()), this, SLOT(newProject()));
    connect(newTable, SIGNAL(triggered()), this, SLOT(createTableWithEmptyRows()));
    connect(newScript, SIGNAL(triggered()), this, SLOT(createScriptWindow()));
    connect(openProject, SIGNAL(triggered()), this, SLOT(openProject()));
    connect(openPcap, SIGNAL(triggered()), this, SLOT(showOpenFileDialogPcap()));
    connect(openScript, SIGNAL(triggered()), this, SLOT(showOpenFileDialogScript()));
    connect(saveProject, SIGNAL(triggered()),this, SLOT(saveProject()));
    connect(saveProjectAs, SIGNAL(triggered()),this, SLOT(saveProjectAs()));
    connect(options, SIGNAL(triggered()), this, SLOT(openOptions()));
    connect(quit, SIGNAL(triggered()), this, SLOT(closeApplication()));
    connect(saveWindowAs, SIGNAL(triggered()), this, SLOT(saveWindowAs()));
    connect(close, SIGNAL(triggered()), this, SLOT(closeProject()));
}

void MainWindow::aboutToShowAnalysisMenu()
{
    m_menuAnalysis->clear();

    // Table specific

    if(m_workspace->activeSubWindow()->inherits("Table"))
    {
        QAction * statisticsOnColumn = m_menuAnalysis->addAction(tr("Statistics on Columns"));
        QMenu * sortColumn = m_menuAnalysis->addMenu(tr("Sort Column"));
        QAction * sortAsc = sortColumn->addAction(tr("Ascending"));
        QAction * sortDesc = sortColumn->addAction(tr("Descending"));
        QMenu * normalize = m_menuAnalysis->addMenu(tr("Normalize"));
        QAction * normalizeColumn = normalize->addAction(tr("Column"));
        QAction * normalizeTable = normalize->addAction(tr("Table"));
        m_menuAnalysis->addSeparator();

        connect(statisticsOnColumn, SIGNAL(triggered()), this, SLOT(showStatisticsForActiveTableColumns()));
        connect(sortAsc, SIGNAL(triggered()), m_activeTable, SLOT(sortSelectedColumnAsc()));
        connect(sortDesc, SIGNAL(triggered()), m_activeTable, SLOT(sortSelectedColumnDesc()));
        connect(normalizeColumn, SIGNAL(triggered()), m_activeTable, SLOT(normalizeSelectedColumns()));
        connect(normalizeTable, SIGNAL(triggered()), m_activeTable, SLOT(normalize()));
    }
    else
    if(m_workspace->activeSubWindow()->inherits("Plot"))
    {
        QAction * fitSlope= m_menuAnalysis->addAction(tr("Fit Slope"));
        QAction * fitLinear = m_menuAnalysis->addAction(tr("Fit Linear"));
        QAction * fitPolynomial = m_menuAnalysis->addAction(tr("Fit Polynomial"));
         m_menuAnalysis->addSeparator();
        QMenu * menuExp = m_menuAnalysis->addMenu(tr("Fit Exponential Decay"));
        QAction * fitExpDecay1 = menuExp->addAction(tr("First Order"));
        QAction * fitExpDecay2 = menuExp->addAction(tr("Second Order"));
        QAction * fitExpDecay3 = menuExp->addAction(tr("Third Order"));
        
        QAction * fitBoltzman = m_menuAnalysis->addAction(tr("Fit Boltzmann (Sigmoidal)"));
        QAction * fitGaussian = m_menuAnalysis->addAction(tr("Fit Gaussian"));
        QAction * fitLore = m_menuAnalysis->addAction(tr("Fit Lorentzian"));
        m_menuAnalysis->addSeparator();
        QAction * fitWizard = m_menuAnalysis->addAction(tr("Fit Wizard..."));
        
        connect(fitSlope,SIGNAL(triggered()), this, SLOT(fitSlope()));
        connect(fitLinear,SIGNAL(triggered()), this, SLOT(fitLinear()));
        connect(fitPolynomial,SIGNAL(triggered()), this, SLOT(fitPolynomial()));
        connect(fitBoltzman,SIGNAL(triggered()), this, SLOT(fitBoltzman()));
        connect(fitGaussian,SIGNAL(triggered()), this, SLOT(fitGaussian()));
        connect(fitLore,SIGNAL(triggered()), this, SLOT(fitLore()));
        connect(fitExpDecay1,SIGNAL(triggered()), this, SLOT(fitExpDecay1()));
        connect(fitExpDecay2,SIGNAL(triggered()), this, SLOT(fitExpDecay2()));
        connect(fitExpDecay3,SIGNAL(triggered()), this, SLOT(fitExpDecay3()));
        
        
        connect(fitWizard,SIGNAL(triggered()), this, SLOT(showFitWizard()));
    }

}

void MainWindow::activateWindowSelectedByQAction()
{
    QAction * action = qobject_cast<QAction*>(QObject::sender());
    if(action)
    {
        QString name = action->text();
        MdiSubWindowWrapper window = m_windowsManager->getWindowByName(name);
        if(window)
        {
            m_windowsManager->activateMdiWindow(window);
        }
    }
}

void MainWindow::findWindow()
{
    FindDialog findDialog(this);
    findDialog.exec();
}

void MainWindow::hideSelectedColumns()
{
    if(m_activeTable)
        m_activeTable->hideSelectedColumns();
}

void MainWindow::clearSelectedColumns()
{
    if(m_activeTable)
        m_activeTable->clearSelectedColumns();
}

void MainWindow::showAllColumns()
{
    if(m_activeTable)
        m_activeTable->showAllColumns();
}

void MainWindow::deleteRowsInterval()
{
    if(m_activeTable)
    {
        int rows = m_activeTable->rows();

        OneSpinboxDialog rowDialog1(1);
        QString title = tr("Delete rows interval");
        rowDialog1.setWindowTitle(title);
        rowDialog1.setLabelText(tr("Start row"));
        rowDialog1.setSpinBoxMin(1);
        rowDialog1.setSpinBoxMax(rows);

        if(!rowDialog1.exec())
            return;

        OneSpinboxDialog rowDialog2(rows);
        rowDialog2.setWindowTitle(title);
        rowDialog2.setLabelText(tr("End row"));
        rowDialog2.setSpinBoxMin(1);
        rowDialog2.setSpinBoxMax(rows);

        if(!rowDialog2.exec())
            return;

        int startRow = rowDialog1.getValue() - 1;
        int count = rowDialog2.getValue() - startRow;
        m_activeTable->removeRows(startRow, count);
        m_activeTable->refresh();

    }
}

void MainWindow::moveRowUpward()
{
    if(m_activeTable)
        m_activeTable->moveRowUpward();
}

void MainWindow::moveRowDownward()
{
    if(m_activeTable)
        m_activeTable->moveRowDownward();
}

void MainWindow::moveColumnLeft()
{
    if(m_activeTable)
        m_activeTable->moveColumnLeft();
}

void MainWindow::moveColumnRight()
{
    if(m_activeTable)
        m_activeTable->moveColumnRight();
}


void MainWindow::moveColumnToLast()
{
    if(m_activeTable)
        m_activeTable->moveColumnToLast();
}

void MainWindow::goToRow()
{
    if(m_activeTable)
    {
        OneSpinboxDialog dialog(1);
        dialog.setWindowTitle(tr("Go to row"));
        dialog.setSpinBoxMin(1);
        dialog.setSpinBoxMax(m_activeTable->rows());
        if(dialog.exec())
        {
            m_activeTable->goToRow(dialog.getValue() - 1);
        }

    }
}

void MainWindow::goToColumn()
{
    if(m_activeTable)
    {
        OneSpinboxDialog dialog(1);
        dialog.setWindowTitle(tr("Go to column"));
        dialog.setSpinBoxMin(1);
        dialog.setSpinBoxMax(DATAROW_SIZE);
        if(dialog.exec())
        {
            m_activeTable->goToColumn(dialog.getValue() - 1);
        }

    }
}

void MainWindow::showStatisticsForActiveTableColumns()
{
    if(m_activeTable)
    {
        QModelIndexList columns = m_activeTable->view().selectionModel()->selectedColumns();
        showStatisticsForTableColumns(*m_activeTable, columns);
    }
}

void MainWindow::showStatisticsForTableColumns(Table & table, QModelIndexList columns)
{
    Table * statisticTable = createEmptyTable();
    statisticTable->setColumnName(0,tr("Column"));
    statisticTable->setColumnName(1,tr("Rows"));
    statisticTable->setColumnName(2,tr("Mean"));
    statisticTable->setColumnName(3,tr("StandardDev"));
    statisticTable->setColumnName(4,tr("StandardError"));
    statisticTable->setColumnName(5,tr("Variance"));
    statisticTable->setColumnName(6,tr("Sum"));
    statisticTable->setColumnName(7,tr("Min"));
    statisticTable->setColumnName(8,tr("Max"));

    int rowCounter = 0;

    foreach(QModelIndex index, columns)
    {
        statisticTable->appendRow();
        TableStatistics stats = table.getColumnStatistics(index.column());
        statisticTable->setValue(rowCounter,0,stats.col + 1);
        statisticTable->setValue(rowCounter,1,stats.rows.maximum - stats.rows.minimum + 1);
        statisticTable->setValue(rowCounter,2,stats.mean);
        statisticTable->setValue(rowCounter,3,stats.standardDev);
        statisticTable->setValue(rowCounter,4,stats.standardError);
        statisticTable->setValue(rowCounter,5,stats.variance);
        statisticTable->setValue(rowCounter,6,stats.sum);
        statisticTable->setValue(rowCounter,7,stats.min);
        statisticTable->setValue(rowCounter,8,stats.max);

        rowCounter++;
    }

    for(int i = 9 ; i < DATAROW_SIZE ; ++i)
        statisticTable->setColumnHidden(i,true);

    statisticTable->resizeToContent();
}

void MainWindow::moveColumnToFirst()
{
    if(m_activeTable)
        m_activeTable->moveColumnToFirst();
}

void MainWindow::openWindow(MdiSubWindow* window)
{
    m_windowsManager->addWindow(window);
    m_workspace->addSubWindow(window);
    window->setAttribute(Qt::WA_DeleteOnClose);
    window->show();

    connect(window, SIGNAL(closedWindow(MdiSubWindow*)), this, SLOT(closeWindow(MdiSubWindow*)));
    connect(window, SIGNAL(windowStateChanged(Qt::WindowStates,Qt::WindowStates)), this, SLOT(onWindowStateChanged(Qt::WindowStates,Qt::WindowStates)));
    QApplication::processEvents();
}

void MainWindow::runDebugCode()
{
    Table * table = createEmptyTable();
    table->setNumberOfRows(5000);
    table->fillColumn(0,TableModel::RowNumbers);
    table->fillColumn(1,TableModel::RowNumbers);
    table->setColumnFormula(1,"col(\"1\",i)*2.5 + 6.25");
    table->recalculate();
    table->fillColumn(2,TableModel::RandomValues);
    table->fillColumn(3,TableModel::NormalRandomValues);

    //table->selectColumn(0,true);
    table->selectColumn(3,true);
    //table->selectColumn(2,true);

    this->plotHistogramSeparately();

    showFitWizard();
}

void MainWindow::openFilePcap(QString filename)
{
    Table * table = createEmptyTable();
    m_dataLoader.loadFile(filename, table);
}

void MainWindow::openFileScript(QString filename)
{
    ScriptWindow * sw = createScriptWindow();
    sw->openScript(filename);
}

void MainWindow::closeWindow(MdiSubWindow* window)
{
    if(window->inherits("Table"))
        m_activeTable = NULL;

    if(window->inherits("Plot"))
        m_activePlot = NULL;

    m_windowsManager->removeWindow(window);

    window->close();
}

void MainWindow::toggleDockWindows()
{
    if(!this->isMinimized())
    {
        if(m_menuActionWindows->isChecked())
            m_windowsManager->show();
        else
            m_windowsManager->hide();
    }
}

void MainWindow::toggleDockScriptingConsole()
{
    if(!this->isMinimized())
    {
        if(m_menuActionScriptingConsole->isChecked())
            m_scriptingConsole->show();
        else
            m_scriptingConsole->hide();
    }
}

void MainWindow::onWindowStateChanged(Qt::WindowStates oldState, Qt::WindowStates newState)
{
    m_windowsManager->refreshList();
}

void MainWindow::subWindowActivated(QMdiSubWindow * window)
{
    menuBar()->removeAction(m_menuTable->menuAction());
    menuBar()->removeAction(m_menuAnalysis->menuAction());
    if(window)
    {
        if (window->inherits("Table") || window->inherits("Plot"))
        {

            menuBar()->insertMenu(m_menuWindows->menuAction(),m_menuAnalysis);
        }

        if(window->inherits("Table"))
        {
            m_activeTable = static_cast<Table*>(window);
            menuBar()->insertMenu(m_menuWindows->menuAction(),m_menuTable);

        }

        if(window->inherits("Plot"))
        {
            m_activePlot = static_cast<Plot*>(window);
        }
    }

}

void MainWindow::windowsManagerVisibilityChanged(bool visible)
{
    m_menuActionWindows->setChecked(visible);
}

void MainWindow::scriptingConsoleVisibilityChanged(bool visible)
{
    m_menuActionScriptingConsole->setChecked(visible);
}

void MainWindow::setTableColumnsAsX()
{
    if(m_activeTable)
    {
        m_activeTable->onSetAsXClick();
    }
}

void MainWindow::setTableColumnsAsY()
{
    if(m_activeTable)
    {
        m_activeTable->onSetAsYClick();
    }
}

void MainWindow::showFitWizard()
{
    if(m_activePlot)
    {
        FitDialog fitDialog(m_activePlot);
        fitDialog.exec();
    }
}

void MainWindow::recalculateTable()
{
    if(m_activeTable)
    {
        m_activeTable->recalculate();
    }
}

void MainWindow::showColumnOptions()
{
    if(m_activeTable)
    {
        m_activeTable->showColumnOptions();
    }
}

void MainWindow::showSetColumnValues()
{
    if(m_activeTable)
    {
        m_activeTable->showSetColumnValues();
    }
}

void MainWindow::fillTableWithRowNumbers()
{
    if(m_activeTable)
    {
        m_activeTable->onFillWithRowNumbers();
    }
}

void MainWindow::fillTableWithRandomValues()
{
    if(m_activeTable)
    {
        m_activeTable->onFillWithRandomValues();
    }
}

void MainWindow::fillTableWithNormalRandomValues()
{
    if(m_activeTable)
    {
        m_activeTable->fillSelectionWithNormalRandomValues();
    }
}

Table* MainWindow::createEmptyTable()
{
    DataRowCollectionPtr dataPtr(new DataRowCollection() , DataRowDestructor);
    Table * table = new Table(this, dataPtr);

    connect(table, SIGNAL(showStatisticsForColumns(Table&,QModelIndexList)), this, SLOT(showStatisticsForTableColumns(Table&,QModelIndexList)));

    openWindow(table);

    return table;
}

Table *MainWindow::createTableWithEmptyRows()
{
    Table * table = createEmptyTable();
    table->setNumberOfRows(30);
    return table;
}

void MainWindow::setValueInActiveTable(int row, int col, double value)
{
    if(m_activeTable)
    {
        m_activeTable->setValue(row - 1,col - 1,value);
    }
}

int MainWindow::rowsInActiveTable() const
{
    int rows = 0;

    if(m_activeTable)
    {
        QCoreApplication::processEvents();
        rows = m_activeTable->rows();
    }

    return rows;
}

void MainWindow::prepareTableWithSampleData()
{
    Table * table = createEmptyTable();
    table->loadRandomData();
}

void MainWindow::openOptions()
{
    OptionsDialog dialog;
    dialog.exec();
}

void MainWindow::deleteFits()
{
    if(m_activePlot)
    {
        m_activePlot->deleteFits();
    }
}

void MainWindow::plotLineTogether()
{
    plot(PlotType::LINE, PlotMode::TOGETHER);
}

void MainWindow::plotLineSeparately()
{
    plot(PlotType::LINE, PlotMode::SEPARATELY);
}

void MainWindow::plotHistogramTogether()
{
    plot(PlotType::HISTOGRAM, PlotMode::TOGETHER);
}

void MainWindow::plotHistogramSeparately()
{
    plot(PlotType::HISTOGRAM, PlotMode::SEPARATELY);
}

void MainWindow::plot(PlotType::Type type, PlotMode::Mode mode)
{
    try
    {        
        if(!activeTable())
            throw PlotFactoryException("Select table!");

        Table * table = activeTable();

        QVector<Plot*> plots = m_plotFactory.buildPlotsFromSelection(*table,type, mode);
        foreach(Plot* plot, plots)
        {
            openWindow(plot);
        }
    }
    catch(PlotFactoryException& e)
    {
        QMessageBox::warning(this, tr("Warning"), e.what(), QMessageBox::Ok);
    }
}

void MainWindow::updateCaption()
{
    unsigned long long DIV = 1024 * 1024;
    unsigned long long freeMem = m_memoryPool.getFreePhysicalMemory() / DIV;

    this->setWindowTitle(QString("DAQ - %1 - free physical memory %2 MB").arg(m_projectManager.projectName()).arg(freeMem));
}

void MainWindow::activateWindow(Table * window)
{
    m_workspace->setActiveSubWindow(window);
}

void MainWindow::showOpenFileDialogPcap()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Open pcap"),
                "",
                "All files (*.*);;Pcap file (*.pcap)");

    if(filename != "")
    {
        openFilePcap(filename);
    }

}

void MainWindow::showOpenFileDialogScript()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Open script"),
                "",
                "All files (*.*);;Script file (*.py)");

    if(filename != "")
    {
        openFileScript(filename);
    }
}

void MainWindow::openProject()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                "Open Project",
                "",
                "Project file (*.prj);;All files (*.*)");

    m_projectManager.open(filename);
    updateCaption();
}

Table* MainWindow::startCapture()
{
    if(m_dataLoader.isCapturing())
        return NULL;

    QStringList devices = m_options.getNetworkDevices();

    if(devices.size() < 1)
    {
        QMessageBox::critical(this, tr("Error"), tr("Select at least one capture device."));
        return 0;
    }

    Table * table = createEmptyTable();
    table->setOnCloseStopCapture(true);
    //connect(table, SIGNAL(closedWindow(MdiSubWindow*)), this, SLOT(stopCapture()));

    m_menuCaptureStart->setEnabled(false);
    m_menuCaptureStop->setEnabled(true);
    m_dataLoader.startCapture(devices, table);


    return table;
}

void MainWindow::startCapture2()
{
    emit signalStartCapture();
}

void MainWindow::stopCapture()
{
    if(m_dataLoader.isCapturing())
    {
        m_dataLoader.stopCapture();

        m_menuCaptureStart->setEnabled(true);
        m_menuCaptureStop->setEnabled(false);

        m_activeTable->refresh();
    }
}

ScriptWindow * MainWindow::createScriptWindow()
{
    ScriptWindow * scriptWindow = new ScriptWindow(this);
    openWindow(scriptWindow);
    return scriptWindow;
}

void MainWindow::appendOrRemoveRowsInTable()
{
    if(m_activeTable)
    {
        OneSpinboxDialog rowsDialog(m_activeTable->rows());
        rowsDialog.setWindowTitle(tr("Enter rows number"));
        if(rowsDialog.exec() == QDialog::Accepted)
        {
            int rows = rowsDialog.getValue();
            m_activeTable->setNumberOfRows(rows);
        }

    }
}

void MainWindow::clearTable()
{
    if(m_activeTable)
    {
        m_activeTable->clear();
    }
}

void MainWindow::showWindowsInCascade()
{
    foreach(MdiSubWindowWrapper window, m_windowsManager->windows())
        window->showNormal();

    m_workspace->cascadeSubWindows();
}

void MainWindow::showWindowsInTile()
{
    foreach(MdiSubWindowWrapper window, m_windowsManager->windows())
        window->showNormal();

    m_workspace->tileSubWindows();
}

QMdiSubWindow * MainWindow::activeWindow()
{
    return m_workspace->activeSubWindow();
}

Plot *MainWindow::activePlot()
{
    QMdiSubWindow * window = m_workspace->activeSubWindow();
    if(window && window->inherits("Plot"))
    {
        return dynamic_cast<Plot*>(window);
    }

    return NULL;
}

void MainWindow::nextWindow()
{
    m_workspace->activateNextSubWindow();
    m_workspace->activeSubWindow()->showNormal();
}

void MainWindow::previousWindow()
{
    m_workspace->activatePreviousSubWindow();
    m_workspace->activeSubWindow()->showNormal();
}

void MainWindow::hideWindow()
{
    m_workspace->activeSubWindow()->hide();
    m_windowsManager->refreshList();
}

void MainWindow::closeWindow()
{
    m_workspace->activeSubWindow()->close();
}

void MainWindow::renameWindow()
{
    MdiSubWindow * window = qobject_cast<MdiSubWindow*>(m_workspace->activeSubWindow());
    m_windowsManager->renameMdiWindow(window);
}

void MainWindow::newProject()
{
    m_projectManager.newProject();
}

void MainWindow::closeProject()
{
    m_projectManager.closeProject();
    updateCaption();
}

void MainWindow::saveProjectAs()
{
    QString filename = QFileDialog::getSaveFileName(
                this,
                tr("Save Project As..."),
                "",
                "Project file (*.prj);;All files (*.*)");

    m_projectManager.save(filename);
    updateCaption();
}

void MainWindow::saveProject()
{
    if(m_projectManager.isFileAssigned())
    {
        m_projectManager.save();
    }
    else
    {
        saveProjectAs();
    }
}

void MainWindow::saveWindowAs()
{
    MdiSubWindow * window = qobject_cast<MdiSubWindow*>(m_workspace->activeSubWindow());
    if(window)
    {
        window->saveAs();
    }
}

const QMdiArea& MainWindow::workspace() const
{
    return *m_workspace;
}

Table * MainWindow::activeTable()
{
    return m_activeTable;
}


QMenu &MainWindow::menuPlot() const
{
    return *m_menuPlot;
}
/*
MainWindowScriptAccessor &MainWindow::scriptAccessor()
{
    return m_scriptAccessor;
}
*/
WindowsManager &MainWindow::windowsManager() const
{
    return *m_windowsManager;
}

ScriptingConsole &MainWindow::scriptingConsole() const
{
    return *m_scriptingConsole;
}

PlotFactory &MainWindow::plotFactory()
{
    return m_plotFactory;
}

ProjectManager &MainWindow::projectManager()
{
    return m_projectManager;
}

void MainWindow::prepareMemoryPool()
{
    uint percent = m_options.getPreallocatedMemoryPercent();
    uint64_t nextPoolBytes = 0;
    if(m_options.getOutOfMemoryAction() == AllocateNextPool)
    {
        nextPoolBytes = m_options.getNextPoolSize();
    }

    m_memoryPool.setup(percent, sizeof(DataRow), nextPoolBytes);
}

void MainWindow::closeApplication()
{
    this->close();
}


void MainWindow::closeEvent(QCloseEvent * e)
{
    m_dataLoader.stopCapture();
    e->accept();
}


void MainWindow::fitSlope()
{
    fitFunction(Fit::Slope);
}
void MainWindow::fitLinear()
{
    fitFunction(Fit::Linear);
}
void MainWindow::fitBoltzman()
{
    fitFunction(Fit::Boltzman);
}
void MainWindow::fitGaussian()
{
    fitFunction(Fit::Gauss);
}
void MainWindow::fitLore()
{
    fitFunction(Fit::Lorentz);
}
void MainWindow::fitExpDecay1()
{
    fitFunction(Fit::ExpDecay1);
}
void MainWindow::fitExpDecay2()
{
    fitFunction(Fit::ExpDecay2);
}
void MainWindow::fitExpDecay3()
{
    fitFunction(Fit::ExpDecay3);
}

void MainWindow::fitFunction(Fit::FitType type)
{
    if(!m_activePlot)
        return;

    Curve * curve;

    if(m_activePlot->numPlots() == 1)
        curve = m_activePlot->getCurves().at(0);
    else
    {
        // Open dialog instead of
        curve = m_activePlot->getCurves().at(0);
    }

    Fit fit;
    FitResult result = fit.fit(m_activePlot, curve, type);

    m_activePlot->addFitCurve(result);
        
    // Get number of curves (without fit curves)
    // If n = 1 select first curve, otherwise
    // open curve selection dialog

    // Create object FitConfig 
    // Use class Fit and method fit
    // and make m_activePlot->addFitCurve(result);

}
    
