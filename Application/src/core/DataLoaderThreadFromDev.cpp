#include "DataLoaderThreadFromDev.h"

DataLoaderThreadFromDev::DataLoaderThreadFromDev()
    : TIMEOUT(500)
{
}

void DataLoaderThreadFromDev::setup(const QString &dev, Table * table)
{
    m_source = dev.toStdString();
    m_table = table;
    m_isFinished = false;
}

void DataLoaderThreadFromDev::stop()
{
    m_isFinished = true;
}

void DataLoaderThreadFromDev::run()
{
    struct pcap_pkthdr* header;
    const u_char * packet;
    pcap_t *handle;

    handle = getHandle();

    if(!handle)
    {
        emit errorSignal(QString::fromStdString(m_errorBuffer));
        emit finishSignal();
        return;
    }


    if(m_options.getPort() > 0)
    {
        if(!prepareFilter(handle))
        {
            pcap_close(handle);
            emit errorSignal(QString::fromStdString(m_errorBuffer));
            emit finishSignal();
            return;
        }
    }

    m_table->startInsertingRows();

    int errorCode = 0;
    while((errorCode = pcap_next_ex(handle, &header, &packet)) >= 0)
    {
        if(m_isFinished)
        {
            break;
        }

        if(errorCode == 1)
            appendDataToTable(packet);
    }

    m_table->stopInsertingRows();
    qDebug() << "Closing pcap handle";
    pcap_close(handle);

    emit finishSignal();
}

void DataLoaderThreadFromDev::runTest()
{

    uchar data[200];

    int rand = qrand() % 3 + 1 ;
    while(true)
    {
        if(m_isFinished) break;

        appendDataToTable(data);
        //QThread::msleep(rand);
    }

    qDebug() << "Closing pcap handle";

    emit finishSignal();
}


pcap_t *DataLoaderThreadFromDev::getHandle()
{
    pcap_t * handle = NULL;
    handle = pcap_open_live(m_source.c_str(), BUFSIZ, 1, TIMEOUT, m_errorBuffer);
    return handle;
}

bool DataLoaderThreadFromDev::prepareFilter(pcap_t *handle)
{
    m_filterString = QString("udp port %1").arg(m_options.getPort()).toStdString();

    bpf_u_int32 mask;
    bpf_u_int32 net;

    if (pcap_lookupnet(m_source.c_str(), &net, &mask, m_errorBuffer) == -1) {
        return false;
    }

    if (pcap_compile(handle, &m_bpfProgram, m_filterString.c_str(), 0, net) == -1) {
        strncpy(m_errorBuffer, pcap_geterr(handle), PCAP_ERRBUF_SIZE);
        return false;
    }

    if (pcap_setfilter(handle, &m_bpfProgram) == -1) {
        strncpy(m_errorBuffer, pcap_geterr(handle), PCAP_ERRBUF_SIZE);
        return false;
    }

    return true;
}
