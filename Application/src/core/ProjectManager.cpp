#include "ProjectManager.h"
#include "MainWindow.h"

ProjectManager::ProjectManager(MainWindow * mainWindow)
    : m_mainWindow(mainWindow),
      m_isFileAssigned(false),
      DEFAULT_PROJECT_NAME(tr("Untitled"))
{
    m_projectName = DEFAULT_PROJECT_NAME;
    m_windowsManager = new WindowsManager(mainWindow);
}

WindowsManager *ProjectManager::windowsManager() const
{
    return m_windowsManager;
}

bool ProjectManager::isFileAssigned() const
{
    return m_isFileAssigned;
}

void ProjectManager::newProject()
{
    closeProject();
}

void ProjectManager::open(const QString &filepath)
{
    if(filepath.isEmpty())
        return;

    this->closeProject();

    QFile file(filepath);
    if(!file.open(QIODevice::ReadOnly))
    {
        QString message = tr("Cannot open file \"") + filepath + tr("\" !");
        QMessageBox::critical(m_mainWindow,
                              tr("Open Project"),
                              message);
        return;
    }

    try
    {
        QDataStream stream(&file);
        deserializeTables(stream);
        deserializeScripts(stream);
        deserializeHistograms(stream);
        deserializePlotsLine(stream);
    }
    catch(QException & e)
    {
        closeProject();

        QString message = tr("Error occurred while opening the project: ") + tr(e.what());
        QMessageBox::critical(m_mainWindow,
                              tr("Open Project"),
                              message);

    }

    m_windowsManager->refreshList();

    setFileInfo(filepath);
}

void ProjectManager::save(const QString &filepath)
{
    if(filepath.isEmpty())
        return;

    QFile file(filepath);
    if(!file.open(QIODevice::WriteOnly))
    {
        QString message = tr("Cannot create file \"") + filepath + tr("\" !");
        QMessageBox::critical(m_mainWindow,
                              tr("Save Project"),
                              message);
        return;
    }

    m_mainWindow->scriptingConsole().appendLine("Saving project...");


    setFileInfo(filepath);

    try
    {
        QDataStream stream(&file);
        serializeTables(stream);
        serializeScripts(stream);
        serializeHistograms(stream);
        serializePlotsLine(stream);

        m_mainWindow->scriptingConsole().appendLine("Saving done!");

    }
    catch(QException & e)
    {
        QString message = tr("Error occurred while saving the project: ") + tr(e.what());
        QMessageBox::critical(m_mainWindow,
                              tr("Save Project"),
                              message);
    }
}

void ProjectManager::save()
{
    if(m_isFileAssigned)
        save(m_projectFile);
}

QString ProjectManager::projectName() const
{
    return m_projectName;
}

void ProjectManager::closeProject()
{
    m_windowsManager->clear();

    clearFileInfo();
}

void ProjectManager::setFileInfo(const QString &filepath)
{
    m_projectFile = filepath;
    m_projectName = QFileInfo(filepath).completeBaseName ();
    m_isFileAssigned = true;
}

void ProjectManager::clearFileInfo()
{
    m_projectFile = "";
    m_projectName = DEFAULT_PROJECT_NAME;
    m_isFileAssigned = false;
}

void ProjectManager::serializeTables(QDataStream &stream)
{
    const QVector<Table*> tables = m_windowsManager->tables();

    stream << tables.size();

    foreach(Table* table, tables)
    {
        table->save(stream);
    }
}

void ProjectManager::serializeScripts(QDataStream &stream)
{
    const QVector<ScriptWindow*> scripts = m_windowsManager->scripts();

    int size = scripts.size();

    stream << size;
    foreach(ScriptWindow* s, scripts)
    {
        s->save(stream);
    }

}

void ProjectManager::serializeHistograms(QDataStream &stream)
{
    const QVector<PlotHistogram*> histograms = m_windowsManager->histograms();

    int size = histograms.size();

    stream << size;

    foreach(PlotHistogram * h, histograms)
    {
        h->save(stream);
    }

}

void ProjectManager::serializePlotsLine(QDataStream &stream)
{
    const QVector<PlotLine*> plots = m_windowsManager->plotsLine();

    int size = plots.size();

    stream << size;

    foreach(PlotLine * p, plots)
    {
        Table * table = p->table();
        QString tableName = table->name();
        stream << tableName;
        p->save(stream);
    }

}

void ProjectManager::deserializeTables(QDataStream &stream)
{
    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        Table * table = m_mainWindow->createEmptyTable();
        table->load(stream);
    }
}

void ProjectManager::deserializeScripts(QDataStream &stream)
{
    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        ScriptWindow * script = m_mainWindow->createScriptWindow();
        script->load(stream);
    }
}

void ProjectManager::deserializeHistograms(QDataStream &stream)
{
    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        PlotHistogram * histogram = new PlotHistogram(m_mainWindow);
        m_mainWindow->openWindow(histogram);
        histogram->load(stream);

    }
}

void ProjectManager::deserializePlotsLine(QDataStream &stream)
{
    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        QString tableName;
        stream >> tableName;
        MdiSubWindow * window = m_windowsManager->findWindow(tableName,false,true,false);

        if(window)
        {
            Table * table = qobject_cast<Table*>(window);
            PlotLine * plot = new PlotLine(m_mainWindow,table);
            m_mainWindow->openWindow(plot);
            plot->load(stream);
        }
    }
}
