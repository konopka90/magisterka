#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QMenuBar>
#include <QMdiArea>
#include <QTimer>
#include <QMessageBox>
#include <QFileDialog>
#include <QDockWidget>
#include <QListWidget>

#include <cstdlib>
#include <ctime>

//#include "MainWindowScriptAccessor.h"
#include "MemoryPool.h"
#include "ProjectManager.h"
#include "DataLoader.h"

#include "../options/OptionsDialog.h"
#include "../table/Table.h"
#include "../plot/PlotFactory.h"
#include "../plot/PlotMode.h"
#include "../plot/PlotType.h"
#include "../dialogs/OneSpinboxDialog.h"
#include "../dialogs/FindDialog.h"
#include "../dialogs/LoadingFileDialog.h"
#include "../scripting/ScriptingPython.h"
#include "../scripting/ScriptingConsole.h"
#include "../scripting/ScriptWindow.h"
#include "../analysis/FitDialog.h"

class PlotFactory;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow();
    ~MainWindow();
    const QMdiArea& workspace() const;

    MemoryPool& memoryPool();

    QMenu& menuPlot() const;
    //MainWindowScriptAccessor & scriptAccessor();
    WindowsManager& windowsManager() const;
    ScriptingConsole& scriptingConsole() const;
    PlotFactory & plotFactory();
    ProjectManager & projectManager();

    void openWindow(MdiSubWindow* window);

private:
    void runDebugCode();
    void closeEvent(QCloseEvent *);

    void prepareMemoryPool();
    void prepareMenu();
    void prepareWorkspace();
    void prepareCaptionTimer();
    void prepareWindowsManager();
    void prepareScriptingConsole();


    void openFilePcap(QString filename);
    void openFileScript(QString filename);

public slots:
    void setValueInActiveTable(int row, int col, double value);
    int rowsInActiveTable() const;

    Table* createEmptyTable();
    Table* createTableWithEmptyRows();


    void deleteFits();
    void plotLineTogether();
    void plotLineSeparately();
    void plotHistogramTogether();
    void plotHistogramSeparately();
    void updateCaption();
    void activateWindow(Table * window);
    void showOpenFileDialogPcap();
    void showOpenFileDialogScript();
    void openProject();
    Table* startCapture();
    void startCapture2();
    void stopCapture();
    ScriptWindow * createScriptWindow();
    void appendOrRemoveRowsInTable();
    void clearTable();
    void showWindowsInCascade();
    void showWindowsInTile();
    QMdiSubWindow * activeWindow();
    Table * activeTable();
    Plot * activePlot();
    void nextWindow();
    void previousWindow();
    void hideWindow();
    void closeWindow();
    void renameWindow();
    void newProject();
    void closeProject();
    void saveProjectAs();
    void saveProject();
    void saveWindowAs();
    void recalculateTable();

private slots:

    void plot(PlotType::Type, PlotMode::Mode);
    void prepareTableWithSampleData();
    void openOptions();
    void closeApplication();
    void closeWindow(MdiSubWindow* window);
    void toggleDockWindows();
    void toggleDockScriptingConsole();
    void onWindowStateChanged(Qt::WindowStates, Qt::WindowStates);
    void subWindowActivated(QMdiSubWindow*);
    void windowsManagerVisibilityChanged(bool);
    void scriptingConsoleVisibilityChanged(bool);
    void setTableColumnsAsX();
    void setTableColumnsAsY();

    void showFitWizard();
    void showColumnOptions();
    void showSetColumnValues();
    void fillTableWithRowNumbers();
    void fillTableWithRandomValues();
    void fillTableWithNormalRandomValues();

    void aboutToShowWindowsMenu();
    void aboutToShowTableMenu();
    void aboutToShowFileMenu();
    void aboutToShowAnalysisMenu();

    void activateWindowSelectedByQAction();
    void findWindow();
    void hideSelectedColumns();
    void clearSelectedColumns();
    void showAllColumns();
    void deleteRowsInterval();
    void moveRowUpward();
    void moveRowDownward();
    void moveColumnLeft();
    void moveColumnRight();
    void moveColumnToFirst();
    void moveColumnToLast();
    void goToRow();
    void goToColumn();
    
    void fitSlope();
    void fitLinear();
    void fitBoltzman();
    void fitGaussian();
    void fitLore();
    void fitExpDecay1();
    void fitExpDecay2();
    void fitExpDecay3();
    void fitFunction(Fit::FitType);


    void showStatisticsForActiveTableColumns();
    void showStatisticsForTableColumns(Table& table, QModelIndexList columns);

signals:
    void signalStartCapture();


private:
    MemoryPool & m_memoryPool;
    Options & m_options;
    DataLoader m_dataLoader;

    QMdiArea * m_workspace;
    WindowsManager * m_windowsManager;
    PlotFactory m_plotFactory;
    ProjectManager m_projectManager;
    ScriptingConsole * m_scriptingConsole;
    Table * m_activeTable;
    Plot * m_activePlot;

    //MainWindowScriptAccessor m_scriptAccessor;

    QMenu * m_menuNew;
    QMenu * m_menuCapture;
    QMenu * m_menuFile;
    QMenu * m_menuPlot;
    QMenu * m_menuPlotSeparately;
    QMenu * m_menuPlotTogether;
    QMenu * m_menuView;
    QMenu * m_menuTable;
    QMenu * m_menuTableSetColumnsAs;
    QMenu * m_menuTableFillSelectionWith;
    QMenu * m_menuWindows;
    QMenu * m_menuAnalysis;

    QAction * m_menuActionScriptingConsole;
    QAction * m_menuActionWindows;
    QAction * m_menuPlotLineTogetherAction;
    QAction * m_menuPlotLineSeparatelyAction;
    QAction * m_menuPlotHistogramTogetherAction;
    QAction * m_menuPlotHistogramSeparatelyAction;
    QAction * m_menuCaptureStart;
    QAction * m_menuCaptureStop;
    QAction * m_menuActionSetAsX;
    QAction * m_menuActionSetAsY;

    friend class plotFactory;
    friend class MainWindowScriptAccessor;
};

#endif // MAINWINDOW_H
