#ifndef PCAPLOADER_H
#define PCAPLOADER_H

#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QThread>
#include <QDebug>
#include <QTimer>

#include <pcap.h>

#include "../table/Table.h"
#include "../dialogs/LoadingFileDialog.h"
#include "MemoryPool.h"


#include "DataLoaderThreadFromDev.h"
#include "DataLoaderThreadFromFile.h"

class MainWindow;

class DataLoader : public QWidget
{
    Q_OBJECT
public:
    DataLoader();
    ~DataLoader();

    void loadFile(const QString& filepath, Table * table);
    void startCapture(const QStringList & devices, Table * table);
    bool isCapturing() const;

public slots:
    void stopCapture();
    void onFinishLoadingFile();
    void onRefreshTable();
    void onError(QString);

signals:
    void errorSignal();

private:
    QTimer timer;
    QVector<DataLoaderThreadFromDev*> m_dataLoaderFromDevsCollection;

    DataLoaderThreadFromDev m_dataLoaderFromDev;
    QString m_sourceFile;
    QStringList m_sourceDevs;

    LoadingFileDialog * m_loadingFileDialog;
    Table * m_table;
};

#endif // PCAPLOADER_H
