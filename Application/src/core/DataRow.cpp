#include "DataRow.h"

unsigned int DataRow::cols()
{
    return DATAROW_SIZE;
}

void DataRow::clear()
{
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
        this->values[i] = NAN;
}

QDataStream & operator<<(QDataStream& stream, const DataRow& row)
{
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        stream << row.values[i];
    }

    return stream;
}

QDataStream & operator>>(QDataStream& stream, DataRow& row)
{
    for(int i = 0 ; i < DATAROW_SIZE ; ++i)
    {
        stream >> row.values[i];
    }

    return stream;
}


void DataRowDestructor(DataRowCollection* collection)
{
    int i = 0 ;
    DataRowCollection::iterator it = collection->begin();
    while(it != collection->end())
    {
        delete (*it);
        it++;
        ++i;
    }

    qDebug() << "Deleted" << i << "rows";

    collection->clear();

    qDebug() << "After cleaning deque";
}
