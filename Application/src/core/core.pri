RESOURCES +=

INCLUDEPATH += core/

HEADERS  += \
    core/MdiSubWindow.h \
    core/MainWindow.h \
    core/DataRow.h \
    core/PtrWrapper.h \
    core/WindowsManager.h \
    core/WindowsManagerModel.h \
    core/RenameDialog.h \
    core/Network.h \
    core/DataLoader.h \
    core/DataLoaderThread.h \
    core/DataLoaderThreadFromFile.h \
    core/DataLoaderThreadFromDev.h \
    core/Tests.h \
    core/MemoryPool.h \
    core/MyMath.h \
    core/ProjectManager.h
SOURCES += core/main.cpp\
        core/MdiSubWindow.cpp \
        core/MainWindow.cpp \
        core/DataRow.cpp \
        core/WindowsManager.cpp \
        core/WindowsManagerModel.cpp \
		core/RenameDialog.cpp \
		core/Network.cpp \
		core/DataLoader.cpp \
		core/DataLoaderThread.cpp \
		core/DataLoaderThreadFromFile.cpp \
		core/DataLoaderThreadFromDev.cpp \
		core/Tests.cpp \
		core/MemoryPool.cpp \
		core/MyMath.cpp \
		core/ProjectManager.cpp
