#ifndef DATALOADERTHREADFROMDEV_H
#define DATALOADERTHREADFROMDEV_H

#include "DataLoaderThread.h"


class DataLoaderThreadFromDev : public DataLoaderThread
{
    Q_OBJECT

public:
    DataLoaderThreadFromDev();
    void setup(const QString & dev, Table * table);
    void stop();


signals:
    void updateHeader();

private:
    void run();
    void runTest();
    pcap_t *getHandle();
    bool prepareFilter(pcap_t * handle);

private:
    const int TIMEOUT;
    std::string m_filterString;
    struct bpf_program m_bpfProgram;

};

#endif // DATALOADERTHREADFROMDEV_H
