#include "Network.h"
#include <QDebug>
using namespace std;

Network::Network()
{
}

vector<NetworkDevice> Network::devices()
{
    vector<NetworkDevice> result;
    pcap_if_t *allDevs, *dev;
    char errbuf[PCAP_ERRBUF_SIZE];

    if (pcap_findalldevs(&allDevs, errbuf) == 0)
    {
        qDebug() << "Devs found...";

        for(dev = allDevs; dev; dev = dev->next)
        {
            // Get IPv4 address

            string address;
            pcap_addr_t *a;
            for(a = dev->addresses; a ; a = a->next)
            {
                if(a->addr->sa_family == AF_INET)
                {
                    address = IP2String(((struct sockaddr_in *)a->addr)->sin_addr.s_addr);
                    qDebug() << "address:" << address.c_str();
                    break;
                }
            }

            string name = dev->name;
            string desc;
#ifdef PLATFORM_WIN32
             desc = dev->description;
#endif
            qDebug() << address.c_str();
            qDebug() << "vec";


            if(!name.empty())
            {
                NetworkDevice nd(address, name, desc);
                qDebug() << "insert";
                result.push_back(nd);
            }
        }

        pcap_freealldevs(allDevs);
    }
    else
    {
        qDebug() << "Devices not found";
    }
    return result;
}

string Network::IP2String(unsigned long IP)
{
    stringstream str;
    str << int(IP&0xFF) << "."
        << int((IP&0xFF00)>>8) << "."
        << int((IP&0xFF0000)>>16) << "."
        << int((IP&0xFF000000)>>24);

    return str.str();
}

