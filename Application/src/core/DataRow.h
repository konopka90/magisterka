#ifndef DATAROW_H
#define DATAROW_H

#include <QVector>
#include <deque>
#include <boost/shared_ptr.hpp>
#include <memory>
#include "../Precompiled.h"
#include "MemoryPool.h"


#define DATAROW_SIZE (15)

class MemoryPool;

struct DataRow
{
public:
    double values[DATAROW_SIZE];

public:

    DataRow() {
        clear();
    }

    inline void * operator new(size_t size) {
        return MemoryPool::getMainPool().allocate();
    }

    inline void * operator new(size_t size, MemoryPool & pool) {
        return pool.allocate();
    }


    inline void operator delete(void* object) {
        MemoryPool::getMainPool().deallocate(object);
    }


    static unsigned int cols();

    inline void load(void * data)
    {
        memcpy(values, data, DATAROW_SIZE*sizeof(double));
    }

    friend QDataStream & operator<<(QDataStream& stream, const DataRow& row);
    friend QDataStream & operator>>(QDataStream& stream, DataRow& row);

    void clear();


};

typedef std::deque<DataRow*> DataRowCollection;
typedef boost::shared_ptr<DataRow> DataRowPtr;
typedef boost::shared_ptr<DataRowCollection> DataRowCollectionPtr;
void DataRowDestructor(DataRowCollection* collection);

#endif // DATAROW_H
