#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QtWidgets>
#include <QDataStream>
#include <QFile>

#include "WindowsManager.h"

#include "MdiSubWindow.h"
#include "../table/Table.h"

class MainWindow;

class ProjectManager : public QObject
{
    Q_OBJECT

public:
    ProjectManager(MainWindow * mainWindow);

    WindowsManager * windowsManager() const;

    bool isFileAssigned() const;
    void newProject();
    void open(const QString & filepath);
    void save(const QString & filepath);
    void save();

    QString projectName() const;

public slots:
    void closeProject();

private:

    void setFileInfo(const QString & filepath);
    void clearFileInfo();
    void serializeTables(QDataStream & stream);
    void serializeScripts(QDataStream & stream);
    void serializeHistograms(QDataStream & stream);
    void serializePlotsLine(QDataStream & stream);

    void deserializeTables(QDataStream & stream);
    void deserializeScripts(QDataStream & stream);
    void deserializeHistograms(QDataStream & stream);
    void deserializePlotsLine(QDataStream & stream);

    const QString DEFAULT_PROJECT_NAME;
    bool m_isFileAssigned;
    QString m_projectFile;
    QString m_projectName;
    MainWindow * m_mainWindow;
    WindowsManager * m_windowsManager;
    QFile m_file;
    QDataStream m_stream;
};

#endif // PROJECTMANAGER_H
