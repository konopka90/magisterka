#ifndef DATALOADERTHREADFROMFILE_H
#define DATALOADERTHREADFROMFILE_H

#include "DataLoaderThread.h"

class DataLoaderThreadFromFile : public DataLoaderThread
{
    Q_OBJECT

public:
    DataLoaderThreadFromFile();

    void setup(const QString & filename, Table * table);
    void run();

private:
    pcap_t *getHandle();
    void updateStatus();
    uint64_t countPackets();

public slots:
    void onCloseDialog();

signals:
    void updateStatusSignal(unsigned int);

private:
    uint64_t m_actualPacketNumber;
    uint64_t m_totalNumberOfPackets;
    uint64_t m_previousPercentOfLoading;


};

#endif // DATALOADERTHREADFROMFILE_H
