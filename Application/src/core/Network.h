#ifndef NETWORK_H
#define NETWORK_H

#ifdef PLATFORM_UNIX
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
#endif


#include <vector>
#include <string>
#include <sstream>
#include <pcap.h>

struct NetworkDevice
{
    std::string IP;
    std::string name;
    std::string description;

    NetworkDevice()
    {}

    NetworkDevice(std::string IP, std::string name, std::string description) :
        IP(IP), name(name), description(description){}
};


class Network
{
public:
    Network();
    std::vector<NetworkDevice> devices();

private:
    std::string IP2String(unsigned long IP);

};

#endif // NETWORK_H
