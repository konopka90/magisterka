#ifndef WINDOWSMANAGERMODEL_H
#define WINDOWSMANAGERMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include "MdiSubWindow.h"
#include "../scripting/ScriptWindow.h"
#include "../plot/PlotHistogram.h"
#include "../plot/PlotLine.h"


#include <QDebug>

struct NameMatch
{
    NameMatch(const QString& name) : m_name(name) {}

    bool operator()(MdiSubWindowWrapper window)
    {
        return window.get()->name() == m_name;
    }

private:
    QString m_name;
};

class Table;
class WindowsManagerModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit WindowsManagerModel(QObject *parent = 0);

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void refresh();
    void append(MdiSubWindow*);
    void remove(MdiSubWindow*);

    const QVector<MdiSubWindow*>& windows() const;
    const QVector<Table*>& tables() const;
    const QVector<Plot*>& plots() const;
    const QVector<ScriptWindow*>& scripts() const;
    const QVector<PlotHistogram*>& histograms() const;
    const QVector<PlotLine*>& plotsLine() const;




private:
    const unsigned int COLUMNS;
    QVector<MdiSubWindow*> m_windows;
    QVector<Table*> m_tables;
    QVector<Plot*> m_plots;
    QVector<ScriptWindow*> m_scripts;
    QVector<PlotHistogram*> m_histograms;
    QVector<PlotLine*> m_plotsLine;


};


#endif // WINDOWSMANAGERMODEL_H
