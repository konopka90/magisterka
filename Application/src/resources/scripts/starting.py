from PythonQt import *

def startCapture():
        table = __mainWindow__.startCapture()
        return table
	
def stopCapture():
	__mainWindow__.stopCapture()
	
def newTable():
        return __mainWindow__.createEmptyTable()

def plot2(table,x,y):
        print "tu nie mialem wejsc"
        __mainWindow__.activateWindow(table)
        table.clearSelection()
        table.select(x)
        if isinstance(y,list):
            for col in y:
                table.select(col)
        else:
            table.select(y)
        __mainWindow__.plotLineSeparately()

def histogram(table,x):
        __mainWindow__.activateWindow(table)
        table.clearSelection()
        if isinstance(x,list):
            for col in x:
                table.select(col)
        else:
            table.select(x)
        __mainWindow__.plotHistogramSeparately()

print "(remove me) starting.py"
