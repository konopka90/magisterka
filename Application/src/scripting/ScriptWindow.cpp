#include "ScriptWindow.h"
#include "../core/MainWindow.h"

ScriptWindowCommunicator ScriptWindow::m_communicator;
QSharedPointer<ScriptWorker> ScriptWindow::m_currentScriptWorker;

ScriptWindow::ScriptWindow(MainWindow *parent)
    :MdiSubWindow(parent, MdiSubWindow::ScriptWindow)
{
    QVBoxLayout * layout = new QVBoxLayout();

    // Actions

    QToolBar * toolBar = new QToolBar();

    QAction * actionNew = new QAction(QIcon(":/icons/document.png"),tr("New Script"),this);
    QAction * actionOpen = new QAction(QIcon(":/icons/folder.png"),tr("Open Script"),this);
    QAction * actionSave = new QAction(QIcon(":/icons/save.png"),tr("Save"),this);
    QAction * actionSaveAs = new QAction(QIcon(":/icons/saveas.png"),tr("Save As..."),this); 
    m_actionExecuteAll = new QAction(QIcon(":/icons/playForward.png"),tr("Execute All"),this);
    m_actionExecuteSelectedLines = new QAction(QIcon(":/icons/play.png"),tr("Execute selected lines"),this);
    m_actionCancel = new QAction(QIcon(":/icons/delete.png"), tr("Cancel"), this);

    m_actionCancel->setEnabled(false);

    toolBar->addAction(actionNew);
    toolBar->addAction(actionOpen);
    toolBar->addAction(actionSave);
    toolBar->addAction(actionSaveAs);
    toolBar->addSeparator();
    toolBar->addAction(m_actionExecuteAll);
    toolBar->addAction(m_actionExecuteSelectedLines);
    toolBar->addAction(m_actionCancel);

    connect(actionNew, SIGNAL(triggered()),this, SLOT(addEmptyTab()));
    connect(actionOpen, SIGNAL(triggered()), this, SLOT(openScript()));
    connect(actionSave, SIGNAL(triggered()), this, SLOT(save()));
    connect(actionSaveAs, SIGNAL(triggered()), this, SLOT(saveAs()));
    connect(m_actionExecuteAll, SIGNAL(triggered()), this, SLOT(executeAll()));
    connect(m_actionExecuteSelectedLines, SIGNAL(triggered()), this, SLOT(executeSelectedLines()));
    connect(m_actionCancel, SIGNAL(triggered()), this, SLOT(cancel()));


    m_tabWidget = new QTabWidget(this);
    connect(m_tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));


    // Plus button

    QPushButton * buttonAdd = new QPushButton("+");
    buttonAdd->setMaximumWidth(30);

    QWidget *addWidget = new QWidget;
    QHBoxLayout *hb = new QHBoxLayout(addWidget);
    hb->setMargin(0);
    hb->setSpacing(0);
    hb->addWidget(buttonAdd);
    hb->addStretch();

    connect(buttonAdd, SIGNAL(clicked()), this, SLOT(addEmptyTab()));

    // Shortcuts

    QShortcut *saveShortcut = new QShortcut(QKeySequence("Ctrl+S"), this);
    QObject::connect(saveShortcut, SIGNAL(activated()), this, SLOT(save()));


    // Settings

    m_mainWindow = parent;
    m_tabWidget->setCornerWidget(addWidget);
    m_tabWidget->setTabsClosable(false);


    // Connect communicator
    QObject::connect(&m_communicator, SIGNAL(runButtonsEnabled(bool)), this, SLOT(setRunButtonsEnabled(bool)));

    this->addEmptyTab();

    ScriptEditor & se = getCurrentScriptEditor();
    ScriptingEnv & env  = *se.getScriptEnv();
    if(env.isBusy())
    {
        m_communicator.setRunButtonsEnabled(false);
    }

    QWidget *mainWidget = new QWidget;
    layout->addWidget(toolBar);
    layout->addWidget(m_tabWidget);
    mainWidget->setLayout(layout);
    setWidget(mainWidget);
    resize(700, 300);
}

void ScriptWindow::addEmptyTab()
{
    QSharedPointer<ScriptingEnv> env = ScriptingEnvCreator::create(m_mainWindow);
    ScriptEditor * se = new ScriptEditor(env, m_mainWindow);
    int idx = m_tabWidget->addTab(se, tr("untitled"));
    m_tabWidget->setCurrentIndex(idx);
    checkAndHandleTabsCloseable();

    connect(se, SIGNAL(textChanged()), this, SLOT(onTextChange()));

}

void ScriptWindow::openScript()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open script"),
                                                     "",
                                                     tr("Files (*.*);;Python (*.py)"));
    if(!filename.isEmpty())
    {
        openScript(filename);
    }
}

void ScriptWindow::openScript(const QString &filename,bool showPopup)
{
    if(filename.isEmpty())
        return;

    if(m_tabWidget->count() == 1)
    {
        ScriptEditor * se = qobject_cast<ScriptEditor*>(m_tabWidget->currentWidget());
        if(se->isEmpty())
            m_tabWidget->removeTab(0);
    }

    addEmptyTab();
    ScriptEditor & scriptEditor = getCurrentScriptEditor();

    scriptEditor.load(filename, showPopup);
    renameCurrentTab(getFilenameFromFilepath(filename));
}

void ScriptWindow::saveAs()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save script"),
                                                     "",
                                                     tr("Python (*.py);;Files (*.*)"));
    if(!filename.isEmpty())
    {
        ScriptEditor & scriptEditor = getCurrentScriptEditor();
        scriptEditor.save(filename);
        renameCurrentTab(getFilenameFromFilepath(filename));
    }
}

void ScriptWindow::save()
{
    ScriptEditor & se = getCurrentScriptEditor();
    if(se.getFilepath().isEmpty())
        saveAs();
    else
    {
        se.save(se.getFilepath());
        renameCurrentTab(getFilenameFromFilepath(se.getFilepath()));
    }
}

void ScriptWindow::save(QDataStream &stream)
{
    qDebug() << "Start: Serializing ScriptingWindow";
    int size = m_tabWidget->count();

    stream << size;

    for(int i = 0 ; i < size ; ++i)
    {
        QWidget * page = m_tabWidget->widget(i);
        ScriptEditor & se = getScriptEditor(*page);
        QString filepath = se.getFilepath();
        stream << filepath;
    }

    qDebug() << "Stop: Serializing ScriptingWindow";
}

void ScriptWindow::load(QDataStream &stream)
{
    qDebug() << "Start: Deserializing ScriptingWindow";

    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        QString filepath;
        stream >> filepath;
        this->openScript(filepath, false);
    }

    qDebug() << "Stop: Deserializing ScriptingWindow";
}

void ScriptWindow::executeAll()
{
    ScriptEditor & se = getCurrentScriptEditor();
    ScriptingEnv & env  = *se.getScriptEnv();

    if(env.isBusy())
        return;

    QString code = se.toPlainText();
    executeCode(code);

}

void ScriptWindow::executeSelectedLines()
{
    ScriptEditor & se = getCurrentScriptEditor();
    ScriptingEnv & env  = *se.getScriptEnv();

    if(env.isBusy())
        return;

    QTextCursor cursor = se.textCursor();
    QString str = "";
    if(!cursor.selection().isEmpty())
    {
        str = cursor.selection().toPlainText();
    }
    else
    {
        str = cursor.block().text();
    }

    executeCode(str);
}

void ScriptWindow::closeTab(int index)
{
    m_tabWidget->removeTab(index);
    checkAndHandleTabsCloseable();
}

ScriptEditor &ScriptWindow::getCurrentScriptEditor()
{
    return getScriptEditor(*(m_tabWidget->currentWidget()));
}

void ScriptWindow::cancel()
{
    ScriptEditor & se = getCurrentScriptEditor();
    ScriptingEnv & env  = *se.getScriptEnv();
    env.stop();
}

void ScriptWindow::onTextChange()
{
    ScriptEditor & se = getCurrentScriptEditor();
    QString filename = se.getFilepath();
    if(!filename.isEmpty())
    {
        renameCurrentTab("* " + getFilenameFromFilepath(filename));
    }
}

QString ScriptWindow::getFilenameFromFilepath(const QString &filepath)
{
    return QFileInfo(filepath).fileName();
}

void ScriptWindow::checkAndHandleTabsCloseable()
{
    if(m_tabWidget->count() == 1)
        m_tabWidget->setTabsClosable(false);
    else
        m_tabWidget->setTabsClosable(true);

}

void ScriptWindow::renameCurrentTab(const QString &name)
{
    int idx = m_tabWidget->currentIndex();
    m_tabWidget->setTabText(idx,name);
}

void ScriptWindow::executeCode(const QString &code)
{
    ScriptEditor & se = getCurrentScriptEditor();
    QSharedPointer<ScriptingEnv> sp = se.getScriptEnv();

    m_communicator.setRunButtonsEnabled(false);
    sp->execute(code);
    m_communicator.setRunButtonsEnabled(true);

/*

    m_currentScriptWorker = QSharedPointer<ScriptWorker>(new ScriptWorker(*sp));
    connect(&(*m_currentScriptWorker), SIGNAL(workFinished()), &m_communicator, SLOT(setRunButtonsEnabled()) );

    m_communicator.setRunButtonsEnabled(false);
    m_currentScriptWorker->start(code);*/
}

bool ScriptWindow::canExecuteCode(ScriptingEnv &env)
{
    return !env.isBusy();
}

void ScriptWindow::setRunButtonsEnabled(bool enabled)
{
    m_actionExecuteAll->setEnabled(enabled);
    m_actionExecuteSelectedLines->setEnabled(enabled);
    m_actionCancel->setEnabled(!enabled);
}

ScriptEditor &ScriptWindow::getScriptEditor(QWidget &widget)
{
    return *qobject_cast<ScriptEditor*>(&widget);
}
