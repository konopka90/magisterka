#ifndef THREADEDPYTHONCONTEXT_H
#define THREADEDPYTHONCONTEXT_H

#include <PythonQt/PythonQt.h>

#include <QtCore/QMutexLocker>
#include <QtCore/QQueue>
#include <QtCore/QThread>
#include <QtCore/QWaitCondition>

// http://stackoverflow.com/questions/14141699/non-locking-calls-via-python-console-for-pythonqt-library

class ThreadedPythonContext : public QThread
{
    Q_OBJECT
public:
    ThreadedPythonContext(const PythonQtObjectPtr &context) :
        QThread(),
        _context(context),
        _running(true)
    {
    }

    ~ThreadedPythonContext() {
        _running = false;
        wait();
    }
    void issue(const QString &code) {
        _lock.lock();
        _commands.enqueue(code);
        _lock.unlock();

        _CommandQueued.wakeOne();
    }

    bool isCommandQueueEmpty() {
        QMutexLocker lock(&_lock);
        return _commands.isEmpty();
    }

protected:

    QString dequeue() {
        QMutexLocker lock(&_lock);
        QString cmd( _commands.dequeue() );

        return cmd.isEmpty() ? "\n" : cmd;
    }

    void run() {

        QMutex signal;
        PyGILState_STATE state;

        while(_running) {

            // wait to be signaled ...
            signal.lock();
            _CommandQueued.wait(&signal,1);
            signal.unlock();

            if ( isCommandQueueEmpty() ) {
                continue;
            }

            Py_BEGIN_ALLOW_THREADS
            while ( !isCommandQueueEmpty() )
            {

                PythonQtObjectPtr p;
                PyObject* dict = NULL;

                state = PyGILState_Ensure();
                dict = PyModule_GetDict(_context);


/*
                if (PyModule_Check(_context)) {
                    dict = PyModule_GetDict(_context);
                } else if (PyDict_Check(_context)) {
                    dict = _context;
                }
*/
                if (dict) {
                    // this command blocks until the code has completed execution
                    emit python_busy(true);
                    p.setNewRef(PyRun_String(dequeue().toLatin1().data(), Py_file_input, dict, dict));
                    emit python_busy(false);
                }

                // error in the kernel
                if (!p) {
                    PythonQt::self()->handleError();
                }
                PyGILState_Release(state);
            }

            Py_END_ALLOW_THREADS
        }
    }

    PythonQtObjectPtr _context;

    QMutex _lock;
    QQueue<QString> _commands;

    QWaitCondition _CommandQueued;
    bool _running;

signals:
    void python_busy(bool);

};
#endif // THREADEDPYTHONCONTEXT_H
