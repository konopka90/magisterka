#ifndef SCRIPTINGENVCREATOR_H
#define SCRIPTINGENVCREATOR_H

#include <QSharedPointer>

#include "ScriptingPython.h"

class MainWindow;

class ScriptingEnvCreator
{
public:
    static QSharedPointer<ScriptingEnv> create(MainWindow *);

private:
    ScriptingEnvCreator(){}
    ScriptingEnvCreator(const ScriptingEnvCreator &);
    ScriptingEnvCreator & operator= (const ScriptingEnvCreator &);

};

#endif // SCRIPTINGENVCREATOR_H
