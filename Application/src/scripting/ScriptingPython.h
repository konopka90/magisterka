#ifndef SCRIPTPYTHON_H
#define SCRIPTPYTHON_H

#define MS_NO_COREDLL
#include <PythonQt/PythonQt.h>
#include <PythonQt/PythonQt_QtAll.h>
#include <QObject>
#include <QSharedPointer>

#include "DAQ_API.h"
#include "DecoratorDataRow.h"
#include "DecoratorTable.h"
#include "ThreadedPythonContext.h"
#include "ScriptingEnv.h"

class MainWindow;


class ScriptingPython : public ScriptingEnv
{
    Q_OBJECT
public:

    ScriptingPython(MainWindow *);
    ~ScriptingPython();

    static void init();
    bool isBusy();
    void stop();

    void execute(const QString& code);

private slots:
    void handleOutput(QString output);

signals:
    void output(QString output);

private:
    void initScripting();

    void evalFile(const QString & filename);

    static QSharedPointer<ThreadedPythonContext> m_context;
    static QMutex m_mutex;

    PythonQtObjectPtr m_module;
    MainWindow * m_mainWindow;
    QSharedPointer<DAQ_API> m_apiPointer;
    bool m_isInitFileLoaded;
};


#endif // SCRIPTPYTHON_H
