#include "ScriptingConsole.h"
#include "../core/MainWindow.h"

ScriptingConsole::ScriptingConsole(MainWindow *parent)
    :   QDockWidget(tr("Scripting Console"),parent)
{
    m_plainText= new QPlainTextEdit();

    setWidget(m_plainText);
    setMinimumHeight(50);
}

void ScriptingConsole::appendLine(QString line)
{
    m_plainText->appendPlainText(line);
}

void ScriptingConsole::appendText(QString text)
{
    m_plainText->moveCursor(QTextCursor::End);
    m_plainText->textCursor().insertText(text);
    m_plainText->moveCursor(QTextCursor::End);
}


