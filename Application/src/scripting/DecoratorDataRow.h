#ifndef DECORATORDATAROW_H
#define DECORATORDATAROW_H

#include <QObject>

#include "../core/DataRow.h"

class DecoratorDataRow : public QObject
{
    Q_OBJECT

public slots:
    double get(DataRow * row, unsigned int index) {
        if(index > 0 && index <= DATAROW_SIZE)
            return row->values[index-1];
    }

    void set(DataRow * row, unsigned int index, double value) {
        if(index > 0 && index <= DATAROW_SIZE)
            row->values[index-1] = value;
    }
};

#endif // DECORATORDATAROW_H
