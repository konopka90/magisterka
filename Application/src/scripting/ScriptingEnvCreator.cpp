#include "ScriptingEnvCreator.h"
#include "../options/Options.h"

QSharedPointer<ScriptingEnv> ScriptingEnvCreator::create(MainWindow * mainWindow)
{
    Options & options = Options::instance();

    return QSharedPointer<ScriptingEnv>(new ScriptingPython(mainWindow));
}
