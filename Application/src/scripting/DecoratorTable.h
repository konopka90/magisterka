#ifndef DECORATORTABLE_H
#define DECORATORTABLE_H

#include <QObject>
#include "../table/Table.h"

class DecoratorTable : public QObject
{
    Q_OBJECT

public slots:

    DataRow * newRow(Table* table)
    {
        //return table->appendEmptyRow();
        throw std::exception();
        return NULL;
    }

    void close(Table* table)
    {
        table->confirmClose(false);
        table->close();
    }

    void select(Table* table, int column)
    {
        table->selectColumn(column - 1, true);
    }

    void clearSelection(Table * table)
    {
        table->clearSelection();
    }

    double get(Table * table, unsigned int x, unsigned int y)
    {
        return table->value(x-1,y-1);
    }

    void set(Table * table, unsigned int x, unsigned int y, double value)
    {
        table->setValue(x-1,y-1,value);
    }

    int rows(Table * table)
    {
        return table->rows();
    }

};

#endif // DECORATORTABLE_H
