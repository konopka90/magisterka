#ifndef SCRIPTEDITOR_H
#define SCRIPTEDITOR_H

#include <QPlainTextEdit>
#include <QObject>
#include <QtWidgets>
#include <QSharedPointer>

class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
class LineNumberArea;
class MainWindow;

#include "ScriptingPython.h"
#include "PythonSyntaxHighlighter.h"

class ScriptEditor : public QPlainTextEdit
{
    Q_OBJECT

public:
     ScriptEditor(QWidget * parent);
     ScriptEditor(QSharedPointer<ScriptingEnv> env, QWidget * parent);

     void init();
     void load(const QString & filename, bool showPopup = true);
     void save(const QString & filename);
     void setLineAreaVisibile(bool visible);
     void setHighlightingCurrentLineVisible(bool visible);
     void insertFunction(const QString & name, const QString & args = "");
     void insertText(const QString &name);
     bool isEmpty();
     void lineNumberAreaPaintEvent(QPaintEvent *event);
     int lineNumberAreaWidth();

     const QString &getFilepath();
     QSharedPointer<ScriptingEnv> getScriptEnv();


protected:
     void resizeEvent(QResizeEvent *event);

private slots:
     void updateLineNumberAreaWidth(int newBlockCount);
     void updateLineNumberArea(const QRect &, int);
     void setupFont();
     void matchParentheses();

private:

     void createParenthesisSelection(int pos);
     bool matchRightParenthesis(QTextBlock currentBlock, int i, int numRightParentheses);
     bool matchLeftParenthesis(QTextBlock currentBlock, int i, int numLeftParentheses);

private:
     //ScriptingPython m_scriptPython;
     QSharedPointer<ScriptingEnv> m_scriptEnv;
     QWidget *m_lineNumberArea;
     QString m_filepath;
     PythonSyntaxHighlighter * m_syntaxHighlighter;

     const int TAB_WIDTH;
};

//---------------------------------------------------------------------

class LineNumberArea : public QWidget
{
public:
    LineNumberArea(ScriptEditor *editor) : QWidget(editor) {
        scriptEditor = editor;
    }

    QSize sizeHint() const {
        return QSize(scriptEditor->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) {
        scriptEditor->lineNumberAreaPaintEvent(event);
    }

private:
    ScriptEditor *scriptEditor;
};



//! Structure used for parentheses matching
struct ParenthesisInfo
{
    char character;
    int position;
};

//! Help class used for parentheses matching (code taken from Qt Quarterly Issue 31 · Q3 2009)
class TextBlockData : public QTextBlockUserData
{
public:
    TextBlockData(){}

    QVector<ParenthesisInfo *> parentheses(){return m_parentheses;}
    void insert(ParenthesisInfo *info)
    {
        int i = 0;
        while (i < m_parentheses.size() &&
            info->position > m_parentheses.at(i)->position)
            ++i;

        m_parentheses.insert(i, info);
    }

private:
    QVector<ParenthesisInfo *> m_parentheses;
};

#endif // SCRIPTEDITOR_H
