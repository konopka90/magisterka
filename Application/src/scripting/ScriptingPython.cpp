#include "ScriptingPython.h"
#include "../core/MainWindow.h"

QSharedPointer<ThreadedPythonContext> ScriptingPython::m_context;
QMutex ScriptingPython::m_mutex;

ScriptingPython::ScriptingPython(MainWindow * window)
    : ScriptingEnv(window),
      m_apiPointer(new DAQ_API(window)),
      m_isInitFileLoaded(false)
{
    qDebug() << "Creating ScriptingPython";
}

ScriptingPython::~ScriptingPython()
{
    qDebug() << "Removing ScriptingPython";
}

void ScriptingPython::init()
{
    PyEval_InitThreads();
    PythonQt::init(PythonQt::RedirectStdOut);
    PythonQt_QtAll::init();
    PythonQt::self()->addDecorators(new DecoratorDataRow());
    PythonQt::self()->addDecorators(new DecoratorTable());
    //PythonQt::self()->addDecorators(new DecoratorMainWindow());
    PythonQt::self()->addDecorators(new DAQ_API());
    PythonQt::self()->setSystemExitExceptionHandlerEnabled(true);

    PythonQt::self()->registerCPPClass("DataRow");

}



void ScriptingPython::execute(const QString &code)
{
    // issue some commands into the module

    // http://stackoverflow.com/questions/14141699/non-locking-calls-via-python-console-for-pythonqt-library

   /* initScripting();
    m_context = QSharedPointer<ThreadedPythonContext>(new ThreadedPythonContext(m_module));
    m_context->start();
    m_context->issue(code);*/

    // Release the global interpreter lock (if it has been created and thread support
    // is enabled) and reset the thread state to NULL, returning the previous thread
    // state (which is not NULL). If the lock has been created, the current thread must
    // have acquired it. (This function is available even when thread support is
    // disabled at compile time.)

    // give up control of the GIL

    if(isBusy())
        return;

    m_mutex.lock();

    if(!m_isInitFileLoaded)
    {
        initScripting();
        m_isInitFileLoaded = true;
    }



    m_apiPointer->setIsRunning(true);
    m_module.evalScript(code);
    m_mutex.unlock();

   // PythonQt::cleanup();
}


void ScriptingPython::handleOutput(QString line)
{
    qDebug() << line;
    emit output(line);
}

void ScriptingPython::initScripting()
{
    m_module = PythonQt::self()->createUniqueModule();
    m_module.addObject("__mainWindow__",getMainWindow());
    m_module.addObject("DAQ", &(*m_apiPointer));

    PythonQt::self()->registerClass(&Table::staticMetaObject);

    /*MainWindowScriptAccessor * accessor  = &getMainWindow()->scriptAccessor();
    DAQ_API * DAQ = m_apiPointer.operator ->();

    Qt::ConnectionType connectionType = Qt::QueuedConnection;

    connect(DAQ, SIGNAL(startCapture()), accessor, SLOT(startCapture()),connectionType);
    connect(DAQ, SIGNAL(stopCapture()), accessor, SLOT(stopCapture()),connectionType);

    connect(DAQ, SIGNAL(clearTable()), accessor, SLOT(clearTable()), connectionType);
    connect(DAQ, SIGNAL(setRows(int)), accessor, SLOT(setRows(int)),connectionType);

    connect(DAQ, SIGNAL(setColumnName(int,QString)), accessor, SLOT(setColumnName(int, QString)), connectionType);
    connect(DAQ, SIGNAL(setColumnConstant(int,QString, double)), accessor, SLOT(setColumnConstant(int, QString,double)), connectionType);
    connect(DAQ, SIGNAL(setColumnConstant(QString,QString,double)), accessor, SLOT(setColumnConstant(QString, QString,double)), connectionType);
    connect(DAQ, SIGNAL(setColumnFormula(int,QString)), accessor, SLOT(setColumnFormula(int, QString)), connectionType);
    connect(DAQ, SIGNAL(setColumnFormula(QString,QString)), accessor, SLOT(setColumnFormula(QString, QString)), connectionType);
    connect(DAQ, SIGNAL(clearColumn(int)), accessor, SLOT(clearColumn(int)), connectionType);
    connect(DAQ, SIGNAL(clearColumn(QString)), accessor, SLOT(clearColumn(QString)), connectionType);
    connect(DAQ, SIGNAL(hideColumn(int)), accessor, SLOT(hideColumn(int)), connectionType);
    connect(DAQ, SIGNAL(hideColumn(QString)), accessor, SLOT(hideColumn(QString)), connectionType);
    connect(DAQ, SIGNAL(fillColumn(QString,QString)), accessor, SLOT(fillColumn(QString,QString)), connectionType);
    connect(DAQ, SIGNAL(fillColumn(int,QString)), accessor, SLOT(fillColumn(int,QString)), connectionType);
    connect(DAQ, SIGNAL(showColumn(int)), accessor, SLOT(showColumn(int)), connectionType);
    connect(DAQ, SIGNAL(showColumn(QString)), accessor, SLOT(showColumn(QString)), connectionType);
    connect(DAQ, SIGNAL(sortColumnAscending(int)), accessor, SLOT(sortColumnAscending(int)), connectionType);
    connect(DAQ, SIGNAL(sortColumnAscending(QString)), accessor, SLOT(sortColumnAscending(QString)), connectionType);
    connect(DAQ, SIGNAL(sortColumnDescending(int)), accessor, SLOT(sortColumnDescending(int)), connectionType);
    connect(DAQ, SIGNAL(sortColumnDescending(QString)), accessor, SLOT(sortColumnDescending(QString)), connectionType);
    connect(DAQ, SIGNAL(showColumns()), accessor, SLOT(showColumns()), connectionType);
    connect(DAQ, SIGNAL(normalizeColumn(int)), accessor, SLOT(normalizeColumn(int)), connectionType);
    connect(DAQ, SIGNAL(normalizeColumn(QString)), accessor, SLOT(normalizeColumn(QString)), connectionType);
    connect(DAQ, SIGNAL(normalizeTable()), accessor, SLOT(normalizeTable()), connectionType);
    connect(DAQ, SIGNAL(recalculateTable()), accessor, SLOT(recalculateTable()), connectionType);
    connect(DAQ, SIGNAL(recalculateColumn(int)), accessor, SLOT(recalculateColumn(int)), connectionType);
    connect(DAQ, SIGNAL(recalculateColumn(QString)), accessor, SLOT(recalculateColumn(QString)), connectionType);

    connect(DAQ, SIGNAL(setWindowName(QString)), accessor, SLOT(setWindowName(QString)), connectionType);
    connect(DAQ, SIGNAL(selectWindow(QString)), accessor, SLOT(selectWindow(QString)), connectionType);
    connect(DAQ, SIGNAL(closeWindow()), accessor, SLOT(closeWindow()), connectionType);
    connect(DAQ, SIGNAL(closeWindow(QString)), accessor, SLOT(closeWindow(QString)), connectionType);

    connect(DAQ, SIGNAL(setTitle(QString)), accessor, SLOT(setTitle(QString)), connectionType);
    connect(DAQ, SIGNAL(setAxisXTitle(QString)), accessor, SLOT(setAxisXTitle(QString)), connectionType);
    connect(DAQ, SIGNAL(setAxisYTitle(QString)), accessor, SLOT(setAxisYTitle(QString)), connectionType);


    connect(DAQ, SIGNAL(plot(int,int)), accessor, SLOT(plotLineTogether(int, int)), connectionType);
    connect(DAQ, SIGNAL(plot(int,QList<int>)), accessor, SLOT(plotLineTogether(int, QList<int>)), connectionType);
    connect(DAQ, SIGNAL(plot(QString,QString)), accessor, SLOT(plotLineTogether(QString, QString)), connectionType);

    connect(DAQ, SIGNAL(histogram(int)), accessor, SLOT(histogramTogether(int)), connectionType);
    connect(DAQ, SIGNAL(histogram(QList<int>)), accessor, SLOT(histogramTogether(QList<int>)), connectionType);
    connect(DAQ, SIGNAL(histogram(QString)), accessor, SLOT(histogramTogether(QString)), connectionType);

    connect(DAQ, SIGNAL(savePlotAsImage(QString)), accessor, SLOT(savePlotAsImage(QString)), connectionType );
    connect(DAQ, SIGNAL(savePlotAsImage(QString,QSize)), accessor, SLOT(savePlotAsImage(QString,QSize)), connectionType );
*/

    // Doesn;t work because tuple of strings is recognized as one string
    // connect(DAQ,  SIGNAL(plot(QString,QList<QString>)), accessor, SLOT(plotLineTogether(QString, QList<QString>)), Qt::BlockingQueuedConnection);


    //PythonQt::self()->addDecorators(new DAQ_API(m_mainWindow));

   // PythonQt::self()->registerClass(&DataRow::staticMetaObject);

    evalFile(":/scripts/starting.py");
}

void ScriptingPython::evalFile(const QString &filename)
{
    //m_module.evalFile(filename);
    //return;

    QFile script(filename);
    if(script.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&script);
        m_module.evalScript(stream.readAll());
    }
}

bool ScriptingPython::isBusy()
{
    bool isLocked = m_mutex.tryLock();
    if(isLocked)
        m_mutex.unlock();

    return !isLocked;
}

void ScriptingPython::stop()
{
    m_apiPointer->setIsRunning(false);
}
