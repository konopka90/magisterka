#ifndef SCRIPTWINDOW_H
#define SCRIPTWINDOW_H

#include "../core/MdiSubWindow.h"

#include <QTabWidget>
#include <QTabBar>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include "ScriptingPython.h"
#include "ScriptEditor.h"
#include "ScriptingEnvCreator.h"
#include "ScriptWorker.h"

class ScriptWindowCommunicator : public QObject
{
    Q_OBJECT

public slots:

    void setRunButtonsEnabled(bool enabled)
    {
        emit runButtonsEnabled(enabled);
    }

    void setRunButtonsEnabled()
    {
        emit runButtonsEnabled(true);
    }

signals:
    void runButtonsEnabled(bool enabled);
};

class ScriptWindow : public MdiSubWindow
{
    Q_OBJECT

public:
    explicit ScriptWindow(MainWindow *parent);

public slots:
    void addEmptyTab();
    void openScript();
    void openScript(const QString& filename, bool showPopup = true);
    void saveAs();
    void save();
    void save(QDataStream & stream);
    void load(QDataStream & stream);

    void executeAll();
    void executeSelectedLines();
    void closeTab(int);
    ScriptEditor & getCurrentScriptEditor();

private slots:
    void cancel();
    void onTextChange();
    void setRunButtonsEnabled(bool enabled);

private:

    ScriptEditor & getScriptEditor(QWidget & tab);

    QString getFilenameFromFilepath(const QString & filepath);
    void checkAndHandleTabsCloseable();
    void renameCurrentTab(const QString& name);
    void executeCode(const QString & code);
    bool canExecuteCode(ScriptingEnv & env);



private:
    static ScriptWindowCommunicator m_communicator;
    static QSharedPointer<ScriptWorker> m_currentScriptWorker;

    QTabWidget * m_tabWidget;
    MainWindow * m_mainWindow;
    QAction * m_actionExecuteAll;
    QAction * m_actionExecuteSelectedLines;
    QAction * m_actionCancel;


};

#endif // SCRIPTWINDOW_H
