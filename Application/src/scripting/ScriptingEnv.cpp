#include "ScriptingEnv.h"
#include "../core/MainWindow.h"

ScriptingEnv::ScriptingEnv(MainWindow *parent) :
    QObject(parent),
    m_mainWindow(parent)
{
}

MainWindow *ScriptingEnv::getMainWindow()
{
    return m_mainWindow;
}
