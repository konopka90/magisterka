#ifndef SCRIPTINGCONSOLE_H
#define SCRIPTINGCONSOLE_H

#include <QtWidgets>

class MainWindow;

class ScriptingConsole : public QDockWidget
{
    Q_OBJECT

public:
    ScriptingConsole(MainWindow *parent = 0);

public slots:
    void appendLine(QString line);
    void appendText(QString text);

private:
    QPlainTextEdit * m_plainText;
};

#endif // SCRIPTINGCONSOLE_H
