#ifndef SCRIPTINGENV_H
#define SCRIPTINGENV_H

#include <QObject>

class MainWindow;

class ScriptingEnv : public QObject
{
    Q_OBJECT
public:

    ScriptingEnv(MainWindow *mainWindow);
    virtual ~ScriptingEnv() {}

    MainWindow * getMainWindow();

    virtual void execute(const QString& code) {}
    virtual bool isBusy() { return false; }
    virtual void stop() { }
private:

    MainWindow * m_mainWindow;

signals:

public slots:

};

#endif // SCRIPTINGENV_H
