#include "DAQ_API.h"

#include "../core/MainWindow.h"


void DAQ_API::set(int row, int col, double val)
{
    if(m_mainWindow->activeTable())
        m_mainWindow->activeTable()->setValue(row - 1,col - 1,val);
}


int DAQ_API::rows() const
{
    return m_mainWindow->rowsInActiveTable();
}

double DAQ_API::get(int row, int col) const
{
    if(m_mainWindow->activeTable())
        return m_mainWindow->activeTable()->value(row - 1,col - 1);

    return NAN;
}

bool DAQ_API::isRunning() const
{
    QApplication::processEvents();
    return m_isRunning;
}

void DAQ_API::startCapture()
{
    m_mainWindow->startCapture();
}

void DAQ_API::stopCapture()
{
    m_mainWindow->stopCapture();
}

void DAQ_API::selectWindow(QString name)
{
    MdiSubWindowWrapper window = m_mainWindow->windowsManager().findWindow(name,false,true,false);
    m_mainWindow->windowsManager().activateMdiWindow(window);
}

void DAQ_API::setRows(int rows)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setNumberOfRows(rows);
}

void DAQ_API::setColumnName(int col, QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnName(col - 1, name);
}

void DAQ_API::setColumnFormula(QString column, QString formula)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnFormula(column, formula);
}

void DAQ_API::setColumnFormula(int index, QString formula)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnFormula(index, formula);
}

void DAQ_API::setColumnConstant(QString column, QString constant, double value)
{
    RETURN_IF_NO_TABLE

    int index = m_mainWindow->activeTable()->columnIndexByName(column) + 1;
    setColumnConstant(index, constant, value);
}

void DAQ_API::setColumnConstant(int index, QString constant, double value)
{
    RETURN_IF_NO_TABLE
    TableColumnPtr columnPtr = m_mainWindow->activeTable()->getColumnByIndex(index - 1);
    if(columnPtr)
        columnPtr->addConstant(constant,value);
}

void DAQ_API::clearColumn(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->clearColumn(name);
    m_mainWindow->activeTable()->refresh();

}

void DAQ_API::clearColumn(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->clearColumn(index - 1);
    m_mainWindow->activeTable()->refresh();

}

void DAQ_API::clearTable()
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->clear();
}

void DAQ_API::hideColumn(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnHidden(name, true);
}

void DAQ_API::hideColumn(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnHidden(index - 1,true);
}

void DAQ_API::fillColumn(int index, QString type)
{
    RETURN_IF_NO_TABLE

    TableModel::FillType filltype = qStringToFillType(type);
    m_mainWindow->activeTable()->fillColumn(index - 1, filltype);
}

void DAQ_API::fillColumn(QString name, QString type)
{
    RETURN_IF_NO_TABLE

    TableModel::FillType filltype = qStringToFillType(type);
    m_mainWindow->activeTable()->fillColumn(name, filltype);
}

void DAQ_API::showColumn(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnHidden(name, false);
}

void DAQ_API::showColumn(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->setColumnHidden(index - 1,false);
}

void DAQ_API::showColumns()
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->showAllColumns();
}

void DAQ_API::sortColumnAscending(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->sortColumn(index - 1, Table::Ascending);
}

void DAQ_API::sortColumnAscending(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->sortColumn(name, Table::Ascending);
}

void DAQ_API::sortColumnDescending(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->sortColumn(index - 1, Table::Descending);
}

void DAQ_API::sortColumnDescending(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->sortColumn(name, Table::Descending);
}

void DAQ_API::normalizeTable()
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->normalize();
}

void DAQ_API::recalculateTable()
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->recalculate();
}

void DAQ_API::recalculateColumn(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->recalculateColumn(name);
}

void DAQ_API::recalculateColumn(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->recalculateColumn(index - 1);
}

void DAQ_API::normalizeColumn(QString name)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->normalizeColumn(name);
}

void DAQ_API::normalizeColumn(int index)
{
    RETURN_IF_NO_TABLE
    m_mainWindow->activeTable()->normalizeColumn(index - 1);
}

void DAQ_API::setWindowName(QString name)
{
    QMdiSubWindow * window = m_mainWindow->activeWindow();
    if(window)
    {
        window->setWindowTitle(name);
    }
}

void DAQ_API::closeWindow()
{
    MdiSubWindow * window = qobject_cast<MdiSubWindow*>(m_mainWindow->activeWindow());
    if(window)
    {
        window->confirmClose(false);
        window->close();
    }
}

void DAQ_API::closeWindow(QString name)
{
    MdiSubWindow * window = m_mainWindow->windowsManager().findWindow(name);
    if(window)
    {
        window->confirmClose(false);
        window->close();
    }
}

void DAQ_API::setTitle(QString title)
{
    Plot * plot = m_mainWindow->activePlot();
    if(plot)
    {
        plot->setTitle(title);
    }
}

void DAQ_API::setAxisXTitle(QString title)
{
    Plot * plot = m_mainWindow->activePlot();
    if(plot)
    {
        plot->setAxisXTitle(title);
    }
}

void DAQ_API::setAxisYTitle(QString title)
{
    Plot * plot = m_mainWindow->activePlot();
    if(plot)
    {
        plot->setAxisYTitle(title);
    }
}

void DAQ_API::plot(int x, int y)
{
    QList<int> columnsY;
    columnsY.append(y);
    plot(x, columnsY);
}

void DAQ_API::plot(int x, QList<int> y)
{
    plotDetails(x,y);
}

void DAQ_API::plot(QString x, QString y)
{
    QList<QString> columnsY;

    // Little hack because PythonQt does not recognize string tuples
    // and makes one string, something like
    // ['2','3',...,'n'] or
    // ('2','3',...,'n')

    if(isTuple(y))
    {
        columnsY = tupleToColumnNames(y);
    }
    else
    {
        columnsY.append(y);
    }

    plotDetails(x, columnsY);
}

template<class T>
void DAQ_API::plotDetails(T x, QList<T> y)
{
    try
    {
        Table * table = m_mainWindow->activeTable();
        if(table)
        {
            PlotLineWrapper plotLine = m_mainWindow->plotFactory().buildPlotLine(*table, x, y.toVector());
            m_mainWindow->openWindow(plotLine.get());
        }
    }
    catch(PlotFactoryException& e)
    {
        showWarning(e.what());
    }
}


void DAQ_API::histogram(int x)
{
    QList<int> columns;
    columns.append(x);
    histogram(columns);
}

void DAQ_API::histogram(QList<int> x)
{
    histogramDetails(x);
}

void DAQ_API::histogram(QString x)
{
    QList<QString> columns;
    if(isTuple(x))
    {
        columns = tupleToColumnNames(x);
    }
    else
    {
        columns.append(x);
    }

    histogramDetails(columns);
}

void DAQ_API::savePlotAsImage(QString filename)
{
    Plot * plot = m_mainWindow->activePlot();
    if(plot)
    {
        plot->saveAs(filename);
    }
}

void DAQ_API::savePlotAsImage(QString filename, QSize size)
{
    Plot * plot = m_mainWindow->activePlot();
    if(plot)
    {
        plot->saveAs(filename, size);
    }
}

template<class T>
void DAQ_API::histogramDetails(QList<T> x)
{
    try
    {
        Table * table = m_mainWindow->activeTable();
        if(table)
        {
            PlotHistogramWrapper histogram = m_mainWindow->plotFactory().buildPlotHistogram(*table, x.toVector());
            m_mainWindow->openWindow(histogram.get());
        }
    }
    catch(PlotFactoryException& e)
    {
        showWarning(e.what());
    }
}


bool DAQ_API::isTuple(const QString &str)
{
    if(str.size() == 0)
        return false;

    if((str.startsWith('(') && str.endsWith(')')) ||
       (str.startsWith('[') && str.endsWith(']')))
        return true;

    return false;
}

QList<QString> DAQ_API::tupleToColumnNames(const QString &str)
{
    QString modified = str;
    modified = modified.remove('(').remove(')').remove('[').remove(']').remove(' ');

    QStringList badNames = modified.split(",");
    QList<QString> goodNames;
    foreach(QString s, badNames)
    {
        goodNames.append(s.remove('\''));
    }

    return goodNames;
}

TableModel::FillType DAQ_API::qStringToFillType(const QString &str)
{
    if(str == "row_numbers")
        return TableModel::RowNumbers;
    if(str == "random")
        return TableModel::RandomValues;
    if(str == "normal_random")
        return TableModel::NormalRandomValues;

    return TableModel::RowNumbers;
}

void DAQ_API::showWarning(const QString & msg)
{
    QMessageBox::warning(0, tr("Warning"), msg, QMessageBox::Ok);
}

