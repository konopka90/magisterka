#include "ScriptWorker.h"
#include "../core/MainWindow.h"

ScriptWorker::ScriptWorker(ScriptingEnv & scriptingEnv):
    m_scriptingEnv(scriptingEnv)
{

}

void ScriptWorker::start(const QString code)
{
    m_code = code;
    QThread::start();
    qDebug() << "ScriptWorker started..";
}

void ScriptWorker::stop()
{
    m_scriptingEnv.stop();
}

void ScriptWorker::run()
{
    m_scriptingEnv.execute(m_code);
    emit workFinished();
}
