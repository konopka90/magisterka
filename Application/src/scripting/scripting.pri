RESOURCES +=

INCLUDEPATH += scripting/

HEADERS += \  
    scripting/ScriptWindow.h \
    scripting/ScriptEditor.h \
    scripting/ScriptingConsole.h \
    scripting/DecoratorDataRow.h \
    scripting/DecoratorTable.h \
    scripting/PythonSyntaxHighlighter.h \
    scripting/ThreadedPythonContext.h \
    scripting/ScriptingEnv.h \
    scripting/ScriptingPython.h \
    scripting/ScriptingEnvCreator.h \
    scripting/ScriptingMuParser.h \
    scripting/DAQ_API.h \
    scripting/ScriptWorker.h

SOURCES += \  
    scripting/ScriptWindow.cpp \
    scripting/ScriptEditor.cpp \
    scripting/ScriptingConsole.cpp \
    scripting/DecoratorDataRow.cpp \
    scripting/DecoratorTable.cpp \
    scripting/PythonSyntaxHighlighter.cpp \
    scripting/ScriptingEnv.cpp \
    scripting/ScriptingPython.cpp \
    scripting/ScriptingEnvCreator.cpp \
    scripting/ScriptingMuParser.cpp \
    scripting/DAQ_API.cpp \
    scripting/ScriptWorker.cpp
