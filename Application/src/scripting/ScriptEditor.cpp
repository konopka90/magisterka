#include "ScriptEditor.h"
#include "../core/MainWindow.h"

ScriptEditor::ScriptEditor(QWidget *parent)
    : QPlainTextEdit(parent),
      TAB_WIDTH(4)
{
    init();
}

ScriptEditor::ScriptEditor(QSharedPointer<ScriptingEnv> env, QWidget * parent)
    :
      QPlainTextEdit(parent),
      m_scriptEnv(env),
      m_filepath(""),
      TAB_WIDTH(4)
{
    init();
}

void ScriptEditor::init()
{
    m_syntaxHighlighter = new PythonSyntaxHighlighter(this);
    m_syntaxHighlighter->setDocument(this->document());
    m_lineNumberArea = new LineNumberArea(this);

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(matchParentheses()));

    setupFont();
    updateLineNumberAreaWidth(0);
}

void ScriptEditor::load(const QString &filename, bool showPopup)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        if(showPopup)
            QMessageBox::information(this, tr("Unable to open file"), file.errorString());
        return;
    }

    QTextStream in(&file);

    while(!in.atEnd())
    {
        QString line = in.readLine();
        this->appendPlainText(line);
    }

    file.close();
    m_filepath = filename;
}

void ScriptEditor::save(const QString &filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, tr("Unable to open file"), file.errorString());
        return;
    }

    QTextStream out(&file);

    QString plainTextEditContents = this->toPlainText();
    QStringList lines = plainTextEditContents.split("\n");

    foreach(QString str, lines)
    {
        out << str << endl;
    }

    file.close();

    m_filepath = filename;
}

void ScriptEditor::setLineAreaVisibile(bool visible)
{
    if(visible)
        m_lineNumberArea->show();
    else
        m_lineNumberArea->hide();
}

void ScriptEditor::insertFunction(const QString &name, const QString &args)
{
    QTextCursor cursor = textCursor();
    QString markedText = cursor.selectedText();
    cursor.insertText(name+"("+markedText+args+")");
    if(markedText.isEmpty()){
        // if no text is selected, place cursor inside the ()
        // instead of after it
        cursor.movePosition(QTextCursor::PreviousCharacter,QTextCursor::MoveAnchor,1);
        // the next line makes the selection visible to the user
        // (the line above only changes the selection in the
        // underlying QTextDocument)
        setTextCursor(cursor);
    }
}

void ScriptEditor::insertText(const QString &name)
{
    QTextCursor cursor = textCursor();
    QString markedText = cursor.selectedText();
    cursor.insertText(name);
    if(markedText.isEmpty()){
        // if no text is selected, place cursor inside the ()
        // instead of after it
        cursor.movePosition(QTextCursor::PreviousCharacter,QTextCursor::MoveAnchor,1);
        // the next line makes the selection visible to the user
        // (the line above only changes the selection in the
        // underlying QTextDocument)
        setTextCursor(cursor);
    }
}


bool ScriptEditor::isEmpty()
{
    if(m_filepath.isEmpty() && this->toPlainText().isEmpty())
        return true;

    return false;
}

int ScriptEditor::lineNumberAreaWidth()
 {
     int digits = 1;
     int max = qMax(1, blockCount());
     while (max >= 10) {
         max /= 10;
         ++digits;
     }

     int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits;

     return space;
}

const QString &ScriptEditor::getFilepath()
{
    return m_filepath;
}

QSharedPointer<ScriptingEnv> ScriptEditor::getScriptEnv()
{
    return m_scriptEnv;
}

void ScriptEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void ScriptEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        m_lineNumberArea->scroll(0, dy);
    else
        m_lineNumberArea->update(0, rect.y(), m_lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void ScriptEditor::setupFont()
{
    QFont font;
    font.setFamily("Courier");
    font.setStyleHint(QFont::Monospace);
    font.setFixedPitch(true);
    font.setPointSize(10);

    this->setFont(font);

    QFontMetrics metrics(font);
    this->setTabStopWidth(TAB_WIDTH * metrics.width(' '));
}

void ScriptEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    m_lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}


void ScriptEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(m_lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, m_lineNumberArea->width(), fontMetrics().height(),
            Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

void ScriptEditor::matchParentheses()
{
    QList<QTextEdit::ExtraSelection> selections;
    setExtraSelections(selections);

    TextBlockData *data = static_cast<TextBlockData *>(textCursor().block().userData());

    if (data) {
        QVector<ParenthesisInfo *> infos = data->parentheses();

        int pos = textCursor().block().position();
        for (int i = 0; i < infos.size(); ++i) {
            ParenthesisInfo *info = infos.at(i);

            int curPos = textCursor().position() - textCursor().block().position();
            if (info->position == curPos - 1 && info->character == '(') {
                if (matchLeftParenthesis(textCursor().block(), i + 1, 0))
                    createParenthesisSelection(pos + info->position);
            } else if (info->position == curPos - 1 && info->character == ')') {
                if (matchRightParenthesis(textCursor().block(), i - 1, 0))
                    createParenthesisSelection(pos + info->position);
            }
        }
    }
}

bool ScriptEditor::matchLeftParenthesis(QTextBlock currentBlock, int i, int numLeftParentheses)
{
    TextBlockData *data = static_cast<TextBlockData *>(currentBlock.userData());
    QVector<ParenthesisInfo *> infos = data->parentheses();

    int docPos = currentBlock.position();
    for (; i < infos.size(); ++i) {
        ParenthesisInfo *info = infos.at(i);

        if (info->character == '(') {
            ++numLeftParentheses;
            continue;
        }

        if (info->character == ')' && numLeftParentheses == 0) {
            createParenthesisSelection(docPos + info->position);
            return true;
        } else
            --numLeftParentheses;
    }

    currentBlock = currentBlock.next();
    if (currentBlock.isValid())
        return matchLeftParenthesis(currentBlock, 0, numLeftParentheses);

    return false;
}

bool ScriptEditor::matchRightParenthesis(QTextBlock currentBlock, int i, int numRightParentheses)
{
    TextBlockData *data = static_cast<TextBlockData *>(currentBlock.userData());
    QVector<ParenthesisInfo *> parentheses = data->parentheses();

    int docPos = currentBlock.position();
    for (; i > -1 && parentheses.size() > 0; --i) {
        ParenthesisInfo *info = parentheses.at(i);
        if (info->character == ')') {
            ++numRightParentheses;
            continue;
        }
        if (info->character == '(' && numRightParentheses == 0) {
            createParenthesisSelection(docPos + info->position);
            return true;
        } else
            --numRightParentheses;
    }

    currentBlock = currentBlock.previous();
    if (currentBlock.isValid())
        return matchRightParenthesis(currentBlock, 0, numRightParentheses);

    return false;
}

void ScriptEditor::createParenthesisSelection(int pos)
{
    QList<QTextEdit::ExtraSelection> selections = extraSelections();

    QTextEdit::ExtraSelection selection;
    QTextCharFormat format = selection.format;
    format.setBackground(Qt::lightGray);
    //format.setForeground (Qt::red);
    selection.format = format;

    QTextCursor cursor = textCursor();
    cursor.setPosition(pos);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
    selection.cursor = cursor;

    selections.append(selection);

    setExtraSelections(selections);
}
