#ifndef SCRIPTWORKER_H
#define SCRIPTWORKER_H

#include <QThread>
#include "ScriptingEnv.h"
#include "ScriptingPython.h"

class ScriptWorker : public QThread
{
    Q_OBJECT

public:
    ScriptWorker(ScriptingEnv & scriptingEnv);

    ~ScriptWorker()
    {
        int i = 0;
    }

    void start(const QString code);
    void stop();
    void run();

signals:
    void workFinished();

private:

    ScriptingPython * p;
    ScriptingEnv & m_scriptingEnv;
    QString m_code;
};

#endif // SCRIPTWORKER_H
