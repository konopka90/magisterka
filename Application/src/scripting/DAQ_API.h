#ifndef DAQ_H
#define DAQ_H

#include <QObject>

class MainWindow;
class ScriptingPython;

#include "../table/TableModel.h"

#define RETURN_IF_NO_TABLE if(!m_mainWindow->activeTable()) return;

class DAQ_API : public QObject
{
    Q_OBJECT


public :

    // Constructor for wrapper
    DAQ_API()
        :   m_isRunning(true)
    { }

    // Constructor for shared object between C++ and Python
    DAQ_API(MainWindow * window)
        :   m_mainWindow(window),
            m_isRunning(true)
    { }

    ~DAQ_API() {}

public slots:

    // Tables

    int rows() const;
    double get(int row, int col) const;
    void set(int row, int col, double val);
    void setRows(int rows);

    // Type - 'row_numbers', 'random', 'normal_random'
    void fillColumn(QString name, QString type);
    void fillColumn(int index, QString type);

    // Script

    bool isRunning() const;

//signals:
    void startCapture();
    void stopCapture();

    // Tables


    void setColumnName(int col, QString name);
    void setColumnFormula(QString name, QString formula);
    void setColumnFormula(int col, QString formula);
    void setColumnConstant(QString name, QString constant, double value);
    void setColumnConstant(int col, QString constant, double value);

    void clearColumn(QString name);
    void clearColumn(int index);
    void clearTable();
    void hideColumn(QString name);
    void hideColumn(int index);


    void showColumn(QString name);
    void showColumn(int index);
    void showColumns();
    void sortColumnAscending(QString name);
    void sortColumnAscending(int index);
    void sortColumnDescending(QString name);
    void sortColumnDescending(int index);
    void normalizeColumn(QString name);
    void normalizeColumn(int index);
    void normalizeTable();
    void recalculateTable();
    void recalculateColumn(QString name);
    void recalculateColumn(int index);


    // Windows

    void setWindowName(QString name);
    void selectWindow(QString name);
    void closeWindow();
    void closeWindow(QString name);

    // Plots

    void setTitle(QString title);
    void setAxisXTitle(QString title);
    void setAxisYTitle(QString title);


    void plot(int x, QList<int> y);
    void plot(int x, int y);
    void plot(QString x, QString y);

    void histogram(QList<int> x);
    void histogram(int x);
    void histogram(QString x);

    void savePlotAsImage(QString filename);
    void savePlotAsImage(QString filename, QSize size);
private:

    void setIsRunning(bool state) { m_isRunning = state; }

    bool isTuple(const QString & str);
    QList<QString> tupleToColumnNames(const QString & str);
    TableModel::FillType qStringToFillType(const QString & str);

    template<class T>
    void plotDetails(T x, QList<T> y);

    template<class T>
    void histogramDetails(QList<T> x);

    void showWarning(const QString & msg);

    MainWindow * m_mainWindow;
    bool m_isRunning;

    friend class ScriptingPython;
};

#endif // DAQ_H
