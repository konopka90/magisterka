#ifndef PLOTFACTORY_H
#define PLOTFACTORY_H

#include <QString>
#include <QVector>
#include <qwt_scale_engine.h>

#include "../core/PtrWrapper.h"
#include "PlotMode.h"
#include "PlotType.h"
#include "PlotLine.h"
#include "PlotHistogram.h"
#include "PlotFactoryException.h"

typedef PtrWrapper<Plot> PlotWrapper;
typedef PtrWrapper<PlotLine> PlotLineWrapper;
typedef PtrWrapper<PlotHistogram> PlotHistogramWrapper;

class MainWindow;
class PlotFactory
{
public:

    PlotFactory(MainWindow * mainWindow);

    PlotLineWrapper buildPlotLine(Table & table, const QString & xColumn, const QVector<QString> & yColumns);
    PlotLineWrapper buildPlotLine(Table & table, int xColumn, const QVector<int> & yColumns);

    PlotHistogramWrapper buildPlotHistogram(Table & table, const QVector<QString> & columns);
    PlotHistogramWrapper buildPlotHistogram(Table & table, const QVector<int> & columns);

    QVector<Plot*> buildPlotsFromSelection(Table& table, PlotType::Type type, PlotMode::Mode mode);

private:
    PlotLineWrapper buildPlotLine(Table & table, TableColumnPtr xColumn, const QVector<TableColumnPtr > & yColumns);
    PlotHistogramWrapper buildPlotHistogram(Table & table, const QVector<TableColumnPtr > & columns);


    TableColumnPtr getColumnX(Table & table, const QString& name);
    QVector<TableColumnPtr> getColumnsByName(Table & table, const QVector<QString> & columnsNames);
    QVector<TableColumnPtr> getColumnsByIndexes(Table & table, const QVector<int> & columnsIndexes);
    Table * getActiveTable() throw(PlotFactoryException);
    void verifyPlotLineSelection(Table&) throw(PlotFactoryException);
    void verifyPlotHistogramSelection(Table&) throw(PlotFactoryException);

    QVector<Plot*> createPlotsLineAndAddCurves(Table&, PlotMode::Mode);
    QVector<Plot*> createPlotsHistogramAndAddCurves(Table&, PlotMode::Mode);
private:
     MainWindow& m_mainWindow;

};



#endif // PLOTFACTORY_H
