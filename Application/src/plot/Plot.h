#ifndef PLOT_H
#define PLOT_H

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_zoomer.h>
#include <qwt_legend.h>
#include <qwt_text_label.h>
#include <qwt_scale_widget.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_scale_engine.h>
#include <qwt_plot_legenditem.h>

#include <QPalette>
#include <QPen>
#include <QMenu>
#include <QtPrintSupport/qprinter.h>


#include "TitlePicker.h"
#include "ScalePicker.h"
#include "GlobalPicker.h"
#include "Curve.h"
#include "FitLine.h"

#include "../options/Options.h"
#include "../core/MyMath.h"
#include "../core/MdiSubWindow.h"
#include "dialogs/PlotDetails.h"
#include "../dialogs/TextEditorDialog.h"
#include "../analysis/FitStructs.h"




class Table;
class Line;

class Plot : public MdiSubWindow
{
    Q_OBJECT
public:

    explicit Plot(MainWindow *parent, MdiSubWindowType type);
    virtual ~Plot()
    {
        m_zoomer->blockSignals(true);
        m_plot.blockSignals(true);
    }

    virtual int numPlots() const = 0;
    bool isMaximumPlotsReached();
    QwtPlot & qwtPlot();
    void saveAs();
    void saveAs(const QString & filepath);
    void saveAs(const QString & filepath, QSize size);

    void save(QDataStream &stream);
    void load(QDataStream &stream);
    void addFitCurve(const FitResult & data);
    void deleteFits();


    void deselectWidgets();
    void setTitle(const QString & title);
    void setAxisXTitle(const QString & title);
    void setAxisYTitle(const QString & title);
    void addLegend();
    void setBackground();
    void setupPlot();
    void setupZoomer();
    void preparePens();
    void createContextMenu();
    void createPickers();
    virtual QVector<Curve*> getCurves() = 0;

private:
    void updateSettings();

protected:

    static const unsigned int TITLE_FONT_SIZE = 12;
    static const unsigned int AXIS_FONT_SIZE = 10;
    static const float RESOLUTION;


    QMenu * m_contextMenu;

    Options& m_options;

    unsigned int m_curvesCounter;
    QVector<QwtPlotCurve*> m_curves;
    QwtPlot m_plot;
    QwtPlotZoomer * m_zoomer;
    QSize m_legendMarkerSize;
    TitlePicker * m_titlePicker;
    ScalePicker * m_scalePicker;
    GlobalPicker * m_globalPicker;
    QFont m_font;
    int m_fontSize;
    QwtText m_qwtTitle;
    QwtText m_leftAxisTitle;
    QwtText m_rightAxisTitle;
    QwtText m_topAxisTitle;
    QwtText m_bottomAxisTitle;

    QMap<QwtPlot::Axis, bool> m_axisVisibility;
    QMap<QwtPlot::Axis, QwtText> m_axisTitles;
    QMap<QwtPlot::Axis, QFont> m_axisFonts;

    QVector<FitLine*> m_fitCurves;

    friend class TextEditorDialog;


signals:

public slots:
    void updateZoomer();
    void showProperties();
    void showContextMenu(const QPoint &);

private slots:
    void onScaleDivChanged();
    void openTextEditor();
    void openTextEditor(QwtScaleWidget*);
    void refresh();
};

QDataStream & operator>>(QDataStream & stream, QwtText & text);
QDataStream & operator<<(QDataStream & stream, const QwtText & text);
#endif // PLOT_H
