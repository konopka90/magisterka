#include "FitLine.h"

FitLine::FitLine(const FitResult& fitData)
    : Line(0,-1,-1),
      m_fitData(fitData)

{
    computeDataset();
    this->setSamples(m_x, m_y);
}


const QString FitLine::getTitle() const
{
    return "yee";
}

double FitLine::minX()
{
    return 0;
}

double FitLine::maxX()
{
    return 0;
}

double FitLine::minY()
{
    return 0;
}

double FitLine::maxY()
{
    return 0;
}

int FitLine::points()
{
    return 2;
}

double FitLine::x(int position)
{
    return 0;
}

double FitLine::y(int position)
{
    return 0;
}

void FitLine::computeDataset()
{
    ScriptingMuParser parser;

    foreach(const Parameter & p, m_fitData.parameters)
    {
        parser.addConstant(p.name, p.value);
    }

    parser.setFormula(m_fitData.formula);

    double dx = (m_fitData.to - m_fitData.from) / m_fitData.points;

    m_x.reserve(m_fitData.points);
    m_y.reserve(m_fitData.points);

    double x;
    parser.addVariable("x", x);
    for(x = m_fitData.from ; x <= m_fitData.to ; x+=dx )
    {
        m_x.append(x);

        try
        {
            m_y.append(parser.execute());
        }
        catch(mu::Parser::exception_type & e)
        {
            qDebug() << "Some kind of exception: " << QString(e.GetMsg().c_str());
            break;
        }
    }

}

