#ifndef PLOTLINE_H
#define PLOTLINE_H

#include "Plot.h"
#include "QwtTableColumnSeriesData.h"
#include "Line.h"

class Table;

class PlotLine : public Plot
{
    Q_OBJECT

public:
    explicit PlotLine(MainWindow *parent, ::Table * table);

    void addCurve(int columnX, int columnY, TableSelectionRange range);

    void save(QDataStream &stream);
    void load(QDataStream &stream);

    QVector<Curve *> getCurves();
    int numPlots() const;

    ::Table* table() const;

public slots:
    void updateRangesBecauseOfRemovedRows(int row, int count);

private slots:
    void tableClose(MdiSubWindow*);

private:
    void assignToTable();
    void addCurve(Line * line);
    Line * createCurve(int columnX, int columnY, TableSelectionRange range);
    void setCurveAttributes(Line * line);

private:
    QVector< Line* > m_lines;
    ::Table * m_table;
};

#endif // PLOTLINE_H
