#include "QwtPlotGappedCurve.h"

using namespace boost::math;

QwtPlotGappedCurve::QwtPlotGappedCurve()
:	QwtPlotCurve(),
    m_gapValue(NAN)
{
}

QwtPlotGappedCurve::QwtPlotGappedCurve(const QwtText &title)
:	QwtPlotCurve(title),
    m_gapValue(NAN)
{
}

QwtPlotGappedCurve::QwtPlotGappedCurve(const QString &title)
:	QwtPlotCurve(title),
    m_gapValue(NAN)
{
}

void QwtPlotGappedCurve::drawSeries(QPainter *painter, const QwtScaleMap &xMap,
                                    const QwtScaleMap &yMap, const QRectF &canvRect, int from, int to) const
{
    if ( !painter || dataSize() <= 0 )
        return;

    if (to < 0)
        to = dataSize() - 1;

    int i = from;
    while (i < to)
    {
        // If data begins with missed values,
        // we need to find first non-missed point.
        double x = data()->sample(i).x();
        double y = data()->sample(i).y();
        while ((i < to) && IS_NAN(y) || IS_NAN(x))
        {
            ++i;
            x = data()->sample(i).x();
            y = data()->sample(i).y();
        }
        // First non-missed point will be the start of curve section.
        int start = i;
        x = data()->sample(i).x();
        y = data()->sample(i).y();
        // Find the last non-missed point, it will be the end of curve section.
        while ((i < to) && IS_NOT_NAN(y) && IS_NOT_NAN(x))
        {
            ++i;
            y = data()->sample(i).y();
            x = data()->sample(i).x();
        }
        // Correct the end of the section if it is at missed point
        int end = IS_NAN(y) || IS_NAN(x) ? i - 1 : i;

        // Draw the curve section
        if (start <= end)
            QwtPlotCurve::drawSeries(painter, xMap, yMap, canvRect, start, end);
    }
}
