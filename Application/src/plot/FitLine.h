#ifndef FITLINE_H
#define FITLINE_H

#include "Line.h"
#include "../analysis/FitStructs.h"
#include "../scripting/ScriptingMuParser.h"

class FitLine : public Line
{
    Q_OBJECT
public:
    FitLine(const FitResult& fitData);

    // Curve interface
public:
    const QString getTitle() const;
    double minX();
    double maxX();
    double minY();
    double maxY();
    int points();
    double x(int position);
    double y(int position);

private:
    void computeDataset();

    const FitResult & m_fitData;
    QVector<double> m_x;
    QVector<double> m_y;

};

#endif // FITLINE_H
