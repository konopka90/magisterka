#include "PlotHistogram.h"
#include "../table/Table.h"
using namespace std;



PlotHistogram::PlotHistogram(MainWindow *parent)
 :Plot(parent, MdiSubWindow::PlotHistogram)
{
    m_plot.setTitle("Histogram");
    addLegend();
}


void PlotHistogram::addCurve(TableColumnPtr column, TableSelectionRange range)
{
    try
    {
        Histogram & histogram = createNewHistogram(column, range);
        setupOutlookForHistogram(histogram);
        histogram.attach(&m_plot);
        updateZoomer();
        m_curvesCounter++;
    }
    catch(QException & e)
    {
        qDebug() << "Can't add histogram curve: " << tr(e.what());
    }
}

int PlotHistogram::numPlots() const
{
    return m_histograms.size();
}


void PlotHistogram::setupOutlookForHistogram(Histogram& histogram)
{
    const QColor& color = m_options.getColor(m_histograms.size() - 1);

    histogram.setLegendIconSize(m_legendMarkerSize);
    histogram.setPen(color,0.0,Qt::SolidLine);
    histogram.setBrush(QBrush(color, Qt::BDiagPattern));

}


void PlotHistogram::saveHistograms(QDataStream &stream)
{
    int size = m_histograms.size();

    stream << size;

    for(int i = 0 ; i < size ; ++i)
    {
        stream << *m_histograms[i].data();
    }
}

void PlotHistogram::loadHistograms(QDataStream &stream)
{
    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        Histogram & histogram = createNewHistogram();
        stream >> histogram;
        histogram.attach(&m_plot);
        updateZoomer();
    }
}




Histogram &PlotHistogram::createNewHistogram(TableColumnPtr column, TableSelectionRange selection)
{
    m_histograms.push_back(QSharedPointer<Histogram>(new Histogram(column, selection)));
    return *m_histograms.last();
}

Histogram &PlotHistogram::createNewHistogram()
{
    m_histograms.push_back(QSharedPointer<Histogram>(new Histogram()));
    return *m_histograms.last();
}

void PlotHistogram::save(QDataStream &stream)
{
    Plot::save(stream);
    saveHistograms(stream);
}

void PlotHistogram::load(QDataStream &stream)
{
    Plot::load(stream);
    loadHistograms(stream);
}

QVector<Curve *> PlotHistogram::getCurves()
{
    QVector<Curve*> curves;
    foreach(const QSharedPointer<Histogram> & histogram, m_histograms)
    {
        curves.append(histogram.data());
    }

    return curves;
}
