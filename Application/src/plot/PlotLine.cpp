#include "PlotLine.h"
#include "../table/Table.h"

PlotLine::PlotLine(MainWindow *parent, ::Table * table)
    :Plot(parent, MdiSubWindow::PlotLine),
      m_table(table)
{
    addLegend();
    assignToTable();
}

void PlotLine::addCurve(int columnX, int columnY, TableSelectionRange range)
{
    double min_X = m_table->columnMin(columnX, range);
    double min_Y = m_table->columnMin(columnY, range);

    if(IS_NAN(min_X) || IS_NAN(min_Y))
        return;

    Line* curve = createCurve(columnX, columnY, range);
    setCurveAttributes(curve);

    m_lines.append(curve);

    updateZoomer();
}

int PlotLine::numPlots() const
{
    return m_lines.size();
}

Table *PlotLine::table() const
{
    return m_table;
}

void PlotLine::updateRangesBecauseOfRemovedRows(int row, int count)
{
    foreach(Line* curve, m_lines)
    {
        TableSelectionRange actualRange = curve->getRange();

        // Count how many rows are deleted before minimum of actual range
        int rowsBeforeA = 0;
        int rowsBeforeB = 0;
        int A = actualRange.minimum;
        int B = actualRange.maximum;
        if(A - row > 0)
        {
            if(A - row + count < A)
                rowsBeforeA = row + count;
            else
                rowsBeforeA = A - row;
        }

        if(B - row >= 0)
        {
            if(row + count > B)
                B = B - (B - row) - 1;
            else
                B = B - count;
        }

        A = A - rowsBeforeA;
        B = B - rowsBeforeB;


        TableSelectionRange range(A,B);
        curve->setRange(range);
    }

    updateZoomer();
}

void PlotLine::tableClose(MdiSubWindow *)
{
    this->confirmClose(false);
}

void PlotLine::assignToTable()
{
    QObject::connect(m_table,SIGNAL(closedWindow(MdiSubWindow*)), this,SLOT(closeWithoutAsking()));
    QObject::connect(m_table,SIGNAL(signalRemoveRows(int,int)), this, SLOT(updateRangesBecauseOfRemovedRows(int,int)), Qt::QueuedConnection);
    QObject::connect(m_table, SIGNAL(dataChangedSignal()), this, SLOT(updateZoomer()), Qt::QueuedConnection);
}

void PlotLine::addCurve(Line *curve)
{
    setCurveAttributes(curve);
    m_lines.append(curve);
    m_curvesCounter++;
}

Line *PlotLine::createCurve(int columnX, int columnY, TableSelectionRange range)
{
    Line * curve = new Line(m_table, columnX, columnY);
    QwtTableColumnSeriesData * proxy = new QwtTableColumnSeriesData(columnX, columnY, range, m_table->model());
    curve->setData(proxy);

    return curve;
}

void PlotLine::setCurveAttributes(Line *curve)
{
    const QColor& color = m_options.getColor(m_lines.size());
    QPen pen = QPen(color);
    QString curveTitle = m_table->columns().at(curve->columnY())->curveTitle();

    curve->setTable(m_table);
    curve->setPen(pen);
    curve->setTitle(curveTitle);
    curve->attach(&m_plot);
    curve->setLegendIconSize(m_legendMarkerSize);
}

void PlotLine::save(QDataStream &stream)
{
    Plot::save(stream);

    int size = m_lines.size();
    stream << size;

    for(int i = 0 ; i < size ; ++i)
    {
        stream << m_lines[i]->columnX();
        stream << m_lines[i]->columnY();
        stream << m_lines[i]->getRange();
        stream << *m_lines[i];

    }
}

void PlotLine::load(QDataStream &stream)
{
    Plot::load(stream);

    int size;
    stream >> size;

    for(int i = 0 ; i < size ; ++i)
    {
        int columnX, columnY;
        TableSelectionRange range;

        stream >> columnX;
        stream >> columnY;
        stream >> range;

        Line* currentCurve = new Line(m_table, columnX, columnY);
        QwtTableColumnSeriesData * proxy = new QwtTableColumnSeriesData(columnX, columnY, range, m_table->model());
        currentCurve->setData(proxy);

        stream >> *currentCurve;

        addCurve(currentCurve);
    }
}


QVector<Curve *> PlotLine::getCurves()
{
    QVector<Curve*> curves;
    foreach(Line * line, m_lines)
    {
        curves.append(dynamic_cast<Curve*>(line));
    }

    return curves;
}
