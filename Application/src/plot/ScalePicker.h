#ifndef SCALEPICKER_H
#define SCALEPICKER_H

#include <QObject>
#include <qwt_scale_widget.h>

class Plot;

class ScalePicker : public QObject
{
    Q_OBJECT
public:
    ScalePicker(Plot * plot);
    bool eventFilter(QObject *, QEvent *);
    void deselect();

private:
    void selectTitle(QwtScaleWidget *scale);

signals:
    void doubleClicked(QwtScaleWidget*);

private:
    Plot * m_plot;
};

#endif // SCALEPICKER_H
