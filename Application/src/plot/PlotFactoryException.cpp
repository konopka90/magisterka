#include "PlotFactoryException.h"
#include <QDebug>
PlotFactoryException::PlotFactoryException(const QString& message)
    : m_messageStd(message.toStdString())
{
}

const char *PlotFactoryException::what() const throw()
{
    return m_messageStd.c_str();
}
