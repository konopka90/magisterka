
INCLUDEPATH += plot/ \
               plot/dialogs/

HEADERS +=  \
    plot/Plot.h \
    plot/PlotLine.h \
    plot/PlotFactory.h \
    plot/PlotFactoryException.h \
    plot/PlotMode.h \
    plot/PlotHistogram.h \
    plot/PlotType.h \
    plot/QwtTableColumnSeriesData.h \
    plot/QwtPlotGappedCurve.h \
    plot/dialogs/PlotDetails.h \
    plot/TitlePicker.h \
    plot/ScalePicker.h \
    plot/GlobalPicker.h \
    plot/Histogram.h \
    plot/Line.h \
    plot/Curve.h \
    plot/FitLine.h


SOURCES +=  \
    plot/Plot.cpp \
    plot/PlotLine.cpp \
    plot/PlotFactory.cpp \
    plot/PlotFactoryException.cpp \
    plot/PlotHistogram.cpp \
    plot/QwtTableColumnSeriesData.cpp \
    plot/QwtPlotGappedCurve.cpp \
    plot/dialogs/PlotDetails.cpp \
    plot/TitlePicker.cpp \
    plot/ScalePicker.cpp \
    plot/GlobalPicker.cpp \
    plot/Histogram.cpp \
    plot/Line.cpp \
    plot/Curve.cpp \
    plot/FitLine.cpp
