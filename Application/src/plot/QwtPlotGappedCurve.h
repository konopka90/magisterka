#ifndef QWTPLOTGAPPEDCURVE_H
#define QWTPLOTGAPPEDCURVE_H

#include <QString>
#include <boost/math_fwd.hpp>

#include "../Precompiled.h"
#include "qwt_plot_curve.h"
#include "qwt_text.h"
#include "qwt_plot_curve.h"
#include "qwt_scale_map.h"

#include <cmath>
#include <limits>


////////////////////////////////////////////////////////////////////////////////
/// This class is a plot curve that can have gaps at points
/// with specific Y value.
/// This specific "gap" value can be passed to constructors,
/// it's zero by default.
class QwtPlotGappedCurve: public QwtPlotCurve
{
public:
    QwtPlotGappedCurve();
    QwtPlotGappedCurve(const QwtText &title);
    QwtPlotGappedCurve(const QString &title);

    /// Override draw method to create gaps for missed Y values
    void drawSeries(QPainter *painter, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &canvasRect, int from, int to) const;

private:
    /// Value that denotes missed Y data at point
    double m_gapValue;

};

#endif // QWTPLOTGAPPEDCURVE_H
