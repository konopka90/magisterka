#include "PlotFactory.h"
#include "../core/MainWindow.h"



PlotFactory::PlotFactory(MainWindow *mainWindow)
    : m_mainWindow(*mainWindow)
{}

PlotLineWrapper PlotFactory::buildPlotLine(Table &table,const QString &xColumn, const QVector<QString> & yColumns)
{
    TableColumnPtr columnX = getColumnX(table,xColumn);
    QVector<TableColumnPtr> columnsY = getColumnsByName(table, yColumns);

    return buildPlotLine(table, columnX, columnsY);
}


PlotLineWrapper PlotFactory::buildPlotLine(Table &table,int xColumn, const QVector<int> & yColumns)
{
    int xIdx = xColumn - 1;
    if(xIdx < 0 || xIdx > table.columns().size() - 1)
        return PlotLineWrapper();

    TableColumnPtr columnX = table.columns().at(xIdx);
    QVector<TableColumnPtr> columnsY = getColumnsByIndexes(table, yColumns);

    return buildPlotLine(table, columnX, columnsY);
}

PlotLineWrapper PlotFactory::buildPlotLine(Table & table, TableColumnPtr xColumn, const QVector<TableColumnPtr > & yColumns)
{
    TableSelectionRange range = table.fullRange();

    PlotLine* plot = new PlotLine(&m_mainWindow, &table);
    foreach(TableColumnPtr yColumn, yColumns)
    {
        plot->addCurve(xColumn->logicalIndex(),yColumn->logicalIndex(),range);
    }

    return plot;
}

PlotHistogramWrapper PlotFactory::buildPlotHistogram(Table &table, const QVector<QString> &columns)
{
    QVector<TableColumnPtr> _columns = getColumnsByName(table,columns);
    return buildPlotHistogram(table,_columns);
}

PlotHistogramWrapper PlotFactory::buildPlotHistogram(Table &table, const QVector<int> &columns)
{
    QVector<TableColumnPtr> _columns = getColumnsByIndexes(table,columns);
    return buildPlotHistogram(table,_columns);
}

PlotHistogramWrapper PlotFactory::buildPlotHistogram(Table & table, const QVector<TableColumnPtr > & columns)
{
    TableSelectionRange range = table.fullRange();

    PlotHistogramWrapper plot = new PlotHistogram(&m_mainWindow);
    foreach(TableColumnPtr column, columns)
    {
        plot->addCurve(column, range);
    }


    return plot;
}


QVector<Plot*> PlotFactory::buildPlotsFromSelection(Table& table, PlotType::Type type, PlotMode::Mode mode)
{
    QVector<Plot*> plots;
    switch(type)
    {
        case PlotType::LINE:
            verifyPlotLineSelection(table);
            plots = createPlotsLineAndAddCurves(table, mode);
            break;

        case PlotType::HISTOGRAM:
            verifyPlotHistogramSelection(table);
            plots = createPlotsHistogramAndAddCurves(table, mode);
            break;
    }

    return plots;
}

TableColumnPtr PlotFactory::getColumnX(Table &table, const QString& name)
{
    TableColumnPtr columnX = table.getColumnByName(name);
    if(!columnX)
        throw PlotFactoryException("No column with name \"" + name + "\"");

    return columnX;
}

QVector<TableColumnPtr> PlotFactory::getColumnsByName(Table &table, const QVector<QString> & columnsNames)
{
    QVector<TableColumnPtr> columns;
    foreach(QString colName, columnsNames)
    {
        TableColumnPtr column = table.getColumnByName(colName);
        if(column)
            columns.append(column);
    }

    return columns;
}

QVector<TableColumnPtr> PlotFactory::getColumnsByIndexes(Table &table, const QVector<int> &columnsIndexes)
{
    QVector<TableColumnPtr> columns;
    foreach(int colIndex, columnsIndexes)
    {
        int logicalIndex = colIndex - 1;
        if(logicalIndex < 0 || logicalIndex > table.columns().size() - 1)
            continue;

        TableColumnPtr column = table.columns().at(logicalIndex);
        columns.append(column);
    }

    return columns;
}

Table * PlotFactory::getActiveTable() throw(PlotFactoryException)
{
    Table * table = m_mainWindow.activeTable();
    if(!table)
    {
        throw PlotFactoryException("Select table!");
    }

    return table;
}

void PlotFactory::verifyPlotLineSelection(Table& table) throw(PlotFactoryException)
{
    QString message("Select at least one X and one Y column!");
    QVector<TableColumnPtr> columns = table.selectedFullyOrNotFullyColumns();
    int columnSize = columns.size();

    if(columnSize < 2)
        throw PlotFactoryException(message);

    bool isColumnYFound = false;
    foreach(TableColumnPtr column, columns)
    {

        if(column->type() == TableColumn::Y)
        {
            isColumnYFound = true;
        }
    }

    if(!isColumnYFound)
        throw PlotFactoryException(message);
}

void PlotFactory::verifyPlotHistogramSelection(Table& table) throw(PlotFactoryException)
{
    QString message("Select at least one column!");
    QVector<TableColumnPtr> columns = table.selectedFullyOrNotFullyColumns();

    if(columns.size() < 1)
        throw PlotFactoryException(message);
}

QVector<Plot*> PlotFactory::createPlotsLineAndAddCurves(Table& table, PlotMode::Mode mode)
{
    TableColumnPtr columnX;
    TableSelectionRange range = table.selectedRange();
    QVector<TableColumnPtr> columns = table.selectedFullyOrNotFullyColumns();
    QVector<Plot*> plots;

    if(mode == PlotMode::TOGETHER)
    {
        plots.push_back(new PlotLine(&m_mainWindow, &table));
    }

    // Find first X
    int size = columns.size();
    for(int i = 0 ; i < size ; ++i)
    {
        if(columns.at(i)->type() == TableColumn::X)
        {
            columnX = columns.at(i);
        }
    }

    // Iterate columns - ordered by view
    for(int i = 0 ; i < size ; ++i)
    {
        TableColumnPtr column = columns.at(i);
        switch(column->type())
        {
            case TableColumn::X :
                columnX = column;
                break;

            case TableColumn::Y :

                if(columnX)
                {
                    if(mode == PlotMode::SEPARATELY)
                    {
                        plots.push_back(new PlotLine(&m_mainWindow, &table));
                    }

                    PlotLine* plot = dynamic_cast<PlotLine*>(plots.last());

                    if(plot->isMaximumPlotsReached())
                    {
                        break;
                    }

                    TableColumnPtr columnY = column;
                    plot->addCurve(columnX->logicalIndex(),columnY->logicalIndex(),range);
                    //else
                    //  plot->addCurve(columnY,range);

                }
                break;
        }
    }

    return plots;
}

QVector<Plot*> PlotFactory::createPlotsHistogramAndAddCurves(Table& table, PlotMode::Mode mode)
{
    QVector<Plot*> plots;
    TableSelectionRange range = table.selectedRange();
    QVector<TableColumnPtr> columns = table.selectedFullyOrNotFullyColumns();

    if(mode == PlotMode::TOGETHER)
    {
        plots.push_back(new PlotHistogram(&m_mainWindow));
    }

    // Iterate columns - ordered by view
    int size = columns.size();
    for(int i = 0 ; i < size ; ++i)
    {
        PlotHistogram* plot;
        if(mode == PlotMode::SEPARATELY)
        {
            plots.push_back(new PlotHistogram(&m_mainWindow));
        }

        plot = dynamic_cast<PlotHistogram*>(plots.last());

        if(plot->isMaximumPlotsReached())
        {
            break;
        }

        TableColumnPtr column = columns.at(i);
        plot->addCurve(column, range);
    }

    return plots;
}

