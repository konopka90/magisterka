#include "Plot.h"
#include "FitLine.h"
#include "PlotLine.h"
#include "../table/Table.h"

const float Plot::RESOLUTION = 85.0f;

QDataStream & operator<<(QDataStream & stream, const QwtText & text)
{
    stream << text.color();
    stream << text.text();
    stream << text.borderPen();
    stream << text.font();
    stream << text.renderFlags();

    return stream;
}

QDataStream & operator>>(QDataStream & stream, QwtText & text)
{
    QColor color;
    QString string;
    QPen pen;
    QFont font;
    int flags;

    stream >> color;
    stream >> string;
    stream >> pen;
    stream >> font;
    stream >> flags;

    text.setColor(color);
    text.setText(string);
    text.setBorderPen(pen);
    text.setFont(font);
    text.setRenderFlags(flags);

    return stream;
}


Plot::Plot(MainWindow *parent, MdiSubWindowType type) :
    MdiSubWindow(parent, type), m_options(Options::instance()), m_curvesCounter(0), m_legendMarkerSize(32,2)
{
    setBackground();
    setupPlot();
    setupZoomer();
    createContextMenu();
    createPickers();
}

bool Plot::isMaximumPlotsReached()
{
    return m_curvesCounter >= Options::MAX_PLOTS;
}

QwtPlot &Plot::qwtPlot()
{
    return m_plot;
}

void Plot::saveAs()
{
    QString filename = QFileDialog::getSaveFileName(
            this,
            tr("Save plot"),
            QDir::currentPath(),
            tr("Bitmap (*.bmp);;JPG (*.jpg);;Portable Network Graphics (*.png);;All files (*.*)") );

    if( !filename.isNull() )
    {
        this->saveAs(filename);
    }
}

void Plot::saveAs(const QString &filepath)
{
    QSize imageSize = m_options.getSaveAsImageSize();
    saveAs(filepath, imageSize);
}

void Plot::saveAs(const QString &filepath, QSize size)
{
    this->deselectWidgets();

    float x = (float)size.width()/RESOLUTION * 25.4f;
    float y = (float)size.height()/RESOLUTION * 25.4f;

    QSizeF sizeMM(x,y);
    QwtPlotRenderer renderer;
    renderer.renderDocument(&m_plot, filepath, sizeMM, RESOLUTION);
}

void Plot::deselectWidgets()
{
    m_scalePicker->deselect();
    m_titlePicker->deselect();
}

void Plot::setTitle(const QString &title)
{
    m_plot.setTitle(title);
}

void Plot::setAxisXTitle(const QString &title)
{
    m_plot.setAxisTitle(QwtPlot::xBottom, title);
    m_plot.setAxisTitle(QwtPlot::xTop, title);
}

void Plot::setAxisYTitle(const QString &title)
{
    m_plot.setAxisTitle(QwtPlot::yLeft, title);
    m_plot.setAxisTitle(QwtPlot::yRight, title);
}

void Plot::addLegend()
{
    QwtPlotLegendItem * item = new QwtPlotLegendItem();
    item->setBorderDistance(20);
    item->setBorderPen(QPen(Qt::black));
    item->setAlignment(Qt::AlignLeft);
    item->setMaxColumns(1);
    item->setBackgroundBrush(QBrush(QColor(Qt::white)));
    item->attach(&m_plot);
}

void Plot::updateZoomer()
{
    m_zoomer->setZoomBase();
}

void Plot::showProperties()
{
    PlotDetails pt(this);
    pt.exec();
}

void Plot::onScaleDivChanged()
{
    m_plot.setAxisScaleDiv(QwtPlot::yRight, m_plot.axisScaleDiv(QwtPlot::yLeft));
    m_plot.setAxisScaleDiv(QwtPlot::xTop, m_plot.axisScaleDiv(QwtPlot::xBottom));
}

void Plot::openTextEditor()
{
    TextEditorDialog dialog(this);
    dialog.exec();
}

void Plot::openTextEditor(QwtScaleWidget * scaleWidget)
{
    TextEditorDialog dialog(this,scaleWidget);
    dialog.exec();
}

void Plot::refresh()
{
    uint zoomIndex = m_zoomer->zoomRectIndex();
    if(zoomIndex == 0)
    {
        for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
            m_plot.setAxisAutoScale(i);
    }
}

void Plot::setBackground()
{
    QPalette background(palette());
    background.setColor(QPalette::Background, Qt::white);
    setAutoFillBackground(true);
    setPalette(background);
}

void Plot::setupPlot()
{
    m_axisTitles[QwtPlot::yLeft] = QwtText("Y Axis");
    m_axisTitles[QwtPlot::yRight] = QwtText("Y Axis");
    m_axisTitles[QwtPlot::xTop] = QwtText("X Axis");
    m_axisTitles[QwtPlot::xBottom] = QwtText("X Axis");

    for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
    {
        QwtPlot::Axis index = static_cast<QwtPlot::Axis>(i);
        m_axisVisibility[index] = m_options.getAxisVisibility(index);
        m_axisFonts[index] = m_plot.axisFont(index);
    }

    m_qwtTitle = QwtText("Plot");
    m_font = m_plot.font();
    m_fontSize = TITLE_FONT_SIZE;
    m_font.setPointSize(m_fontSize);

    m_plot.setStyleSheet("QwtPlot { margin: 20px }");
    m_plot.setCanvasBackground( Qt::white );
    m_plot.setAutoReplot(true);

    updateSettings();

    ///for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
    ////    m_plot.setAxisAutoScale(i);

    QwtScaleWidget * scaleWidgetLeft = m_plot.axisWidget(QwtPlot::yLeft);
    QwtScaleWidget * scaleWidgetBottom = m_plot.axisWidget(QwtPlot::xBottom);

    connect(scaleWidgetLeft, SIGNAL(scaleDivChanged()), this, SLOT(onScaleDivChanged()));
    connect(scaleWidgetBottom, SIGNAL(scaleDivChanged()), this, SLOT(onScaleDivChanged()));

    this->setWidget(&m_plot);
    this->resize(m_options.getGeometryPlotSize());
}

void Plot::setupZoomer()
{
    m_zoomer = new QwtPlotZoomer(m_plot.canvas());
    m_zoomer->setKeyPattern( QwtEventPattern::KeyRedo, Qt::Key_I, Qt::ShiftModifier );
    m_zoomer->setKeyPattern( QwtEventPattern::KeyUndo, Qt::Key_O, Qt::ShiftModifier );
    m_zoomer->setKeyPattern( QwtEventPattern::KeyHome, Qt::Key_Home );
    m_zoomer->setTrackerMode(QwtPicker::AlwaysOn);

    connect(m_zoomer, SIGNAL(zoomed(QRectF)), this, SLOT(refresh()));
}


void Plot::createContextMenu()
{
    m_plot.setContextMenuPolicy(Qt::CustomContextMenu);

    m_contextMenu = new QMenu();
    QAction * actionProperties = m_contextMenu->addAction(tr("Properties..."));
    m_contextMenu->addSeparator();
    QAction * actionClose = m_contextMenu->addAction(tr("Close Window"));


    connect(&m_plot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
    connect(actionProperties, SIGNAL(triggered()), this, SLOT(showProperties()));
    connect(actionClose, SIGNAL(triggered()), this, SLOT(close()));

}

void Plot::createPickers()
{
    m_titlePicker = new TitlePicker(this);
    m_scalePicker = new ScalePicker(this);
    m_globalPicker = new GlobalPicker(this);
    connect(m_titlePicker, SIGNAL(doubleClicked()), this, SLOT(openTextEditor()));
    connect(m_scalePicker, SIGNAL(doubleClicked(QwtScaleWidget*)), this, SLOT(openTextEditor(QwtScaleWidget*)));
}

void Plot::updateSettings()
{
    QwtText axisTitle;

    for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
    {
        QwtPlot::Axis index = static_cast<QwtPlot::Axis>(i);

        (m_axisVisibility[index] ? axisTitle = m_axisTitles[index] : axisTitle = QwtText());
        axisTitle.setFont(m_axisFonts[index]);
        m_plot.setAxisTitle(index, axisTitle);
        m_plot.enableAxis(index,m_axisVisibility[index]);
        m_plot.setAxisAutoScale(i);
    }

    m_qwtTitle.setFont(m_font);
    m_plot.titleLabel()->setFont(m_font);
    m_plot.setTitle(m_qwtTitle);
}


void Plot::showContextMenu(const QPoint& pos)
{
    QwtPlotCanvas * canvas = qobject_cast<QwtPlotCanvas*>(this->childAt(pos));
    if(!canvas)
        m_contextMenu->exec(QCursor::pos());
}


void Plot::save(QDataStream &stream)
{
    MdiSubWindow::save(stream);

    stream << m_plot.title();
    stream << m_plot.titleLabel()->font();

    for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
    {
        stream << m_plot.axisTitle(i);
        stream << m_plot.axisEnabled(i);
        stream << m_plot.axisTitle(i).font();
    }

}

void Plot::load(QDataStream &stream)
{
    MdiSubWindow::load(stream);

    stream >> m_qwtTitle;
    stream >> m_font;

    for(int i = 0 ; i < QwtPlot::axisCnt ; ++i)
    {
        QwtPlot::Axis index = static_cast<QwtPlot::Axis>(i);
        stream >> m_axisTitles[index];
        stream >> m_axisVisibility[index];
        stream >> m_axisFonts[index];
    }

    updateSettings();
}

void Plot::addFitCurve(const FitResult &data)
{
    FitLine * line = new FitLine(data);
    m_fitCurves.append(line);

    const QColor& color = m_options.getColor(m_curvesCounter);
    QPen pen = QPen(color);

    line->setPen(pen);
    line->setTitle("NonlinearFit_" + QString::number(m_fitCurves.size()));
    line->attach(&m_plot);

    m_curvesCounter++;
}

void Plot::deleteFits()
{
    int size = m_fitCurves.size();

    foreach(FitLine * line, m_fitCurves)
    {
        line->detach();
        delete line;
    }

    m_fitCurves.clear();

    m_curvesCounter -= size;
}
