#include "QwtTableColumnSeriesData.h"
#include "../table/Table.h"

QwtTableColumnSeriesData::QwtTableColumnSeriesData(int columnX, int columnY, const TableSelectionRange& range, TableModel & model)
    : m_columnX(columnX), m_columnY(columnY), m_model(model), m_range(range)
{
    m_size = computeSize(m_range);
}

size_t QwtTableColumnSeriesData::size() const
{
    return m_size;
}

QPointF QwtTableColumnSeriesData::sample(size_t i) const
{
    double x = m_model.data(i + m_range.minimum, m_columnX);
    double y = m_model.data(i + m_range.minimum, m_columnY);

    return QPointF(x,y);
}


QRectF QwtTableColumnSeriesData::boundingRect() const
{
    using namespace boost::math;

    if(m_size <= 0)
        return QRectF(0,0,-1,-1);

    double maxY = sample(0).y();
    double minY = sample(0).y();
    double maxX = sample(0).x();
    double minX = sample(0).x();

    for(uint64_t i = 1 ; i < size() ; ++i)
    {
        QPointF p = sample(i);
        if(p.y() > maxY && !(isnan)(p.y()))
            maxY = p.y();

        if(p.y() < minY && !(isnan)(p.y()))
            minY = p.y();

        if(p.x() > maxX && !(isnan)(p.x()))
            maxX = p.x();

        if(p.x() < minX && !(isnan)(p.x()))
            minX = p.x();


    }

    //d_boundingRect = qwtBoundingRect( *this );
    d_boundingRect.setRect(minX,minY,maxX - minX, maxY - minY);


    return d_boundingRect;
}

TableSelectionRange QwtTableColumnSeriesData::getRange() const
{
    return m_range;
}

void QwtTableColumnSeriesData::setRange(const TableSelectionRange &range)
{
    m_range = range;
    m_size = computeSize(m_range);
}

int QwtTableColumnSeriesData::computeSize(const TableSelectionRange & range)
{
    return range.maximum - range.minimum + 1;
}
