#ifndef CURVE_H
#define CURVE_H

#include <QObject>

class Plot;

class Curve : public QObject
{
    Q_OBJECT

public:
    explicit Curve(QObject *parent = 0);

    virtual const QString getTitle() const = 0;

    virtual double minX() { return 0.0; }
    virtual double maxX() { return 30.0; }
    virtual double minY() { return 0.0; }
    virtual double maxY() { return 30.0; }

    virtual int points() = 0;
    virtual double x(int position) = 0;
    virtual double y(int position) = 0;
};

Q_DECLARE_METATYPE(Curve*)

#endif // CURVE_H
