#ifndef PLOTHISTOGRAM_H
#define PLOTHISTOGRAM_H

#include <qwt_plot_histogram.h>
#include <qwt_samples.h>
#include <cmath>
#include <map>

#include "Plot.h"
#include "../table/Table.h"
#include "Histogram.h"

class PlotHistogram : public Plot
{
    Q_OBJECT

public:

    explicit PlotHistogram(MainWindow *parent);
    void addCurve(TableColumnPtr column, TableSelectionRange range);
    void save(QDataStream &stream);
    void load(QDataStream &stream);
    QVector<Curve *> getCurves();
    int numPlots() const;

private:

    Histogram& createNewHistogram(TableColumnPtr column, TableSelectionRange selection);
    Histogram& createNewHistogram();
    void setupOutlookForHistogram(Histogram& histogram);

    void saveHistograms(QDataStream & stream);
    void loadHistograms(QDataStream & stream);

private:
    QVector< QSharedPointer<Histogram> > m_histograms;
};



#endif // PLOTHISTOGRAM_H
