#include "Line.h"
#include "../table/Table.h"

QDataStream & operator << (QDataStream& stream, const Line & line)
{
    stream << line.m_columnXIndex;
    stream << line.m_columnYIndex;
    stream << line.m_range;

    return stream;
}

QDataStream & operator >> (QDataStream& stream, Line & line)
{
    stream >> line.m_columnXIndex;
    stream >> line.m_columnYIndex;
    stream >> line.m_range;

    return stream;
}

Line::Line(Table * table, int columnX, int columnY)
    : m_table(table),
      m_columnXIndex(columnX),
      m_columnYIndex(columnY)
{

}


const QString Line::getTitle() const
{
    return this->title().text();
}

double Line::minX()
{
    const QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
    return data->sample(0).x();
}

double Line::maxX()
{
    QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());

    return data->sample(data->size() - 1).x();
}

double Line::minY()
{
     QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
     return data->sample(0).y();
}

double Line::maxY()
{
    QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
    return data->sample(data->size() - 1).y();
}

int Line::points()
{
    QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
    return data->size();
}

double Line::x(int position)
{
    QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
    return data->sample(position).x();
}

double Line::y(int position)
{
    QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
    return data->sample(position).y();
}
