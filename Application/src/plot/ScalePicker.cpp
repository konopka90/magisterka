#include "ScalePicker.h"
#include "../plot/Plot.h"

ScalePicker::ScalePicker(Plot * plot) :
    QObject(plot),
    m_plot(plot)
{
    for ( uint i = 0; i < QwtPlot::axisCnt; i++ )
    {
        QwtScaleWidget *scale = (QwtScaleWidget *)m_plot->qwtPlot().axisWidget(i);
        if (scale)
        {
            scale->installEventFilter(this);
        }
    }
}


bool ScalePicker::eventFilter(QObject * object, QEvent * e)
{
    QwtScaleWidget *scale = qobject_cast<QwtScaleWidget *>(object);
    if (!scale)
    {
        return QObject::eventFilter(object, e);
    }

    if (e->type() == QEvent::MouseButtonDblClick)
    {
        emit doubleClicked(scale);
        return true;
    }


    if(e->type() == QEvent::MouseButtonPress)
    {
        m_plot->deselectWidgets();
        selectTitle(scale);
        return true;
    }

    return QObject::eventFilter(object, e);
}

void ScalePicker::deselect()
{
    for(uint i = 0; i < QwtPlot::axisCnt ; ++i)
    {
        QwtScaleWidget * axis = m_plot->qwtPlot().axisWidget(i);
        QwtText title = axis->title();
        title.setBackgroundBrush(QBrush(Qt::NoBrush));
        axis->setTitle(title);
    }
}

void ScalePicker::selectTitle(QwtScaleWidget *scale)
{
    if (!scale)
        return;

    QwtText title = scale->title();
    title.setBackgroundBrush(QBrush(Options::HIGHLIGHT_WIDGET_COLOR));

    scale->setTitle(title);
}
