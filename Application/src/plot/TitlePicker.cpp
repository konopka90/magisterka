#include "TitlePicker.h"
#include "Plot.h"
TitlePicker::TitlePicker(Plot * plot) :
    QObject(plot),
    m_plot(plot)
{

    m_titleLabel = plot->qwtPlot().titleLabel();
    if(m_titleLabel)
        m_titleLabel->installEventFilter(this);
}


bool TitlePicker::eventFilter(QObject * object, QEvent * e)
{
    if(object != qobject_cast<QObject*>(m_titleLabel))
        return false;

    if ( object->inherits("QwtTextLabel") && e->type() == QEvent::MouseButtonDblClick)
    {
        emit doubleClicked();
        return true;
    }

    if ( object->inherits("QwtTextLabel") && e->type() == QEvent::MouseButtonPress)
    {
        m_plot->deselectWidgets();
        selectTitle();
        return true;
    }

    return QObject::eventFilter(object, e);
}

void TitlePicker::selectTitle()
{
    QwtText text = m_titleLabel->text();
    text.setBackgroundBrush(QBrush(Options::HIGHLIGHT_WIDGET_COLOR));
    m_titleLabel->setText(text);
}

void TitlePicker::deselect()
{
    QwtText text = m_titleLabel->text();
    text.setBackgroundBrush(QBrush(Qt::NoBrush));
    m_titleLabel->setText(text);
}
