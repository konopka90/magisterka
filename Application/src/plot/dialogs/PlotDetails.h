#ifndef PLOTDETAILS_H
#define PLOTDETAILS_H

#include <QtWidgets>
#include <QTreeView>
#include <QGridLayout>

class Plot;

class PlotDetails : public QDialog
{
public:
    PlotDetails(Plot * parent);

private:
    Plot * m_plot;
    QGridLayout m_gridLayout;
    QTreeView * m_treeView;
};


#endif
