#include "PlotDetails.h"
#include "../Plot.h"

PlotDetails::PlotDetails(Plot *parent)
    : QDialog(parent),
      m_plot(parent)
{
    setWindowTitle("Plot Details");
    setModal(true);

    m_treeView = new QTreeView();

    m_gridLayout.addWidget(m_treeView,0,0,0,0);

}
