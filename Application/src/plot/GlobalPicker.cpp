#include "GlobalPicker.h"
#include "Plot.h"

GlobalPicker::GlobalPicker(Plot *plot) :
    QObject(plot),
    m_plot(plot)
{
    m_plot->installEventFilter(this);
}

bool GlobalPicker::eventFilter(QObject * object, QEvent * e)
{
    Plot * plot = qobject_cast<Plot*>(object);

    if(plot && e->type() == QEvent::MouseButtonPress)
    {
        m_plot->deselectWidgets();
    }

    return QObject::eventFilter(object,e);
}
