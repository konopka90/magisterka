#ifndef PLOTTYPE_H
#define PLOTTYPE_H

class PlotType
{
public:
    enum Type
    {
        LINE,
        HISTOGRAM
    };

private:
    PlotType() {}
    PlotType(const PlotMode&) {}
    PlotType& operator=(const PlotType&);
};

#endif // PLOTTYPE_H
