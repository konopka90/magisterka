#ifndef QWTTABLECOLUMNSERIESDATA_H
#define QWTTABLECOLUMNSERIESDATA_H

#include "../table/TableModel.h"
#include "../table/TableColumn.h"
#include "../table/TableSelectionRange.h"

#include "qwt_series_data.h"

class QWT_EXPORT QwtTableColumnSeriesData: public QwtSeriesData<QPointF>
{
public:
    QwtTableColumnSeriesData(int columnX, int columnY, const TableSelectionRange& range, TableModel & model);

    size_t size() const;
    QPointF sample(size_t i) const;
    QRectF boundingRect() const;
    TableSelectionRange getRange() const;
    void setRange(const TableSelectionRange & range);

private:

    int computeSize(const TableSelectionRange & range);

    int m_columnX;
    int m_columnY;
    TableModel & m_model;
    TableSelectionRange m_range;
    int m_size;

};

#endif // QWTTABLECOLUMNSERIESDATA_H
