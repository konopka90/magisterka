#ifndef TITLEPICKER_H
#define TITLEPICKER_H

#include <QObject>
#include <qwt_text_label.h>

class Plot;
class TitlePicker : public QObject
{
    Q_OBJECT
public:
    TitlePicker(Plot *plot);
    void deselect();

private:
    QwtTextLabel * m_titleLabel;
    Plot * m_plot;

    bool eventFilter(QObject *, QEvent *);
    void selectTitle();

signals:
    void doubleClicked();
};

#endif // TITLEPICKER_H
