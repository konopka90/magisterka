#ifndef PLOTMODE_H
#define PLOTMODE_H

class PlotMode
{
public:
    enum Mode
    {
        TOGETHER,
        SEPARATELY
    };

private:
    PlotMode() {}
    PlotMode(const PlotMode&) {}
    PlotMode& operator=(const PlotMode&);
};

#endif // PLOTMODE_H
