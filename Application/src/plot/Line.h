#ifndef LINE_H
#define LINE_H

#include <qwt_plot_curve.h>
#include "QwtPlotGappedCurve.h"
#include "QwtTableColumnSeriesData.h"
#include "../table/TableSelectionRange.h"
#include "Curve.h"

class Table;

class QWT_EXPORT Line : public QwtPlotGappedCurve, public Curve
{
public:
    Line(Table * table, int columnX, int columnY);

    TableSelectionRange getRange()
    {
        QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
        if(data)
            return data->getRange();

        return TableSelectionRange();
    }

    void setRange(TableSelectionRange range)
    {
        m_range = range;
        QwtTableColumnSeriesData * data = dynamic_cast<QwtTableColumnSeriesData*>(this->data());
        if(data)
            data->setRange(range);
    }

    void setTable(Table * table)
    {
        m_table = table;
    }

    int columnX() {
        return m_columnXIndex;
    }

    int columnY() {
        return m_columnYIndex;
    }

    friend QDataStream & operator << (QDataStream& stream, const Line & line);
    friend QDataStream & operator >> (QDataStream& stream, Line & line);

private:

    Table * m_table;
    int m_columnXIndex;
    int m_columnYIndex;
    TableSelectionRange m_range;
    QwtTableColumnSeriesData * m_series;

    // Curve interface
public:
    const QString getTitle() const;
    double minX();
    double maxX();
    double minY();
    double maxY();
    int points();
    double x(int position);
    double y(int position);
};

#endif // LINE_H
