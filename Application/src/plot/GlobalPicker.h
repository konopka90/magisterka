#ifndef GLOBALPICKER_H
#define GLOBALPICKER_H

#include <QObject>

class Plot;

class GlobalPicker : public QObject
{
    Q_OBJECT
public:
    explicit GlobalPicker(Plot * plot);
    bool eventFilter(QObject *, QEvent *);

private:
    Plot * m_plot;

};

#endif // GLOBALPICKER_H
