#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "qwt_plot_histogram.h"
#include "../core/MyMath.h"
#include "../table/Table.h"
#include <QException>
#include <QString>
#include <Curve.h>

typedef QVector<QwtIntervalSample> HistogramSamples;

class QWT_EXPORT Histogram : public QwtPlotHistogram, public Curve
{
public:

    enum BinsType
    {
        None,
        SquareRoot,
        SturgesFormula,
        RiceRule,
        ExplicitValue
    };


    typedef std::map<Histogram::BinsType, QString> BinsTypeMap;

    static QString binsTypeToQString(BinsType type);
    static BinsType qStringToBinsType(const QString& name);
    static QVector<QString> binsTypes();

    explicit Histogram( TableColumnPtr column, TableSelectionRange range, const QString &getTitle = QString::null );
    explicit Histogram();

    friend QDataStream & operator<< (QDataStream& stream, const Histogram & histogram);
    friend QDataStream & operator>> (QDataStream& stream, Histogram & histogram);

private:
    bool isTableSelectionValid();
    void getNumberOfBins();
    void findBinRanges();
    void generateHistogramValues();

private:
    TableColumnPtr m_column;
    TableSelectionRange m_range;
    double m_min;
    double m_max;
    int m_rowsCount;
    int m_bins;
    QVector<double> m_binRanges;
    QVector<double> m_values;
    HistogramSamples m_samples;
    int m_samplesSize;

    static BinsTypeMap m_binsTypeMap;

    // Curve interface
public:
    const QString getTitle() const;
    double minX();
    double maxX();
    double minY();
    double maxY();
    int points();
    double x(int position);
    double y(int position);

private:
    inline double middleX(int position) {

        double min = m_samples.at(position).interval.minValue();
        double max = m_samples.at(position).interval.maxValue();
        double mid = (max + min) / 2;

        return mid;
    }
};

#endif // HISTOGRAM_H
