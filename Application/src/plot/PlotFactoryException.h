#ifndef PLOTFACTORYEXCEPTION_H
#define PLOTFACTORYEXCEPTION_H

#include <exception>
#include <string>
#include <QString>

class PlotFactoryException : public std::exception
{
public:
    PlotFactoryException(const QString& message);
    ~PlotFactoryException() throw() {}
    const char* what() const throw();

private:
    std::string m_messageStd;
};

#endif // PLOTFACTORYEXCEPTION_H
