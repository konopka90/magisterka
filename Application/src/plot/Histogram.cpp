#include "Histogram.h"
#include "boost/assign.hpp"
#include "../table/Table.h"
using namespace boost::assign;
using namespace std;

QDataStream & operator<<(QDataStream& stream, const QwtInterval& sample)
{
    stream << sample.minValue();
    stream << sample.maxValue();

    return stream;
}

QDataStream & operator<<(QDataStream& stream, const QwtIntervalSample& sample)
{
    stream << sample.value;
    stream << sample.interval;
    return stream;
}

QDataStream & operator<<(QDataStream& stream, const HistogramSamples& samples)
{
    int size = samples.size();
    stream << size;

    for(int i = 0 ; i < size ; ++i)
        stream << samples[i];
    return stream;
}

QDataStream & operator>>(QDataStream& stream, QwtInterval& sample)
{
    double value;
    stream >> value;
    sample.setMinValue(value);

    stream >> value;
    sample.setMaxValue(value);

    return stream;
}

QDataStream & operator>>(QDataStream& stream, QwtIntervalSample& sample)
{
    stream >> sample.value;
    stream >> sample.interval;
    return stream;
}

QDataStream & operator>>(QDataStream& stream, HistogramSamples& samples)
{
    int size;
    stream >> size;

    samples.resize(size);
    for(int i = 0 ; i < size ; ++i)
        stream >> samples[i];

    return stream;
}

map<Histogram::BinsType, QString> Histogram::m_binsTypeMap = map_list_of
        (Histogram::SquareRoot, "Square-root, k = sqrt(n)")
        (Histogram::SturgesFormula, "Struges' Formula, k = ceil(log2(n) + 1)")
        (Histogram::RiceRule, "Rice Rule, k = ceil(2n^1/3)")
        (Histogram::ExplicitValue,"Explicit value");

Histogram::Histogram(TableColumnPtr column, TableSelectionRange range,const QString &title)
    : QwtPlotHistogram(title),
      m_column(column),
      m_range(range),
      m_rowsCount(range.maximum - range.minimum + 1)
{
    if(!isTableSelectionValid())
        throw QException();

    getNumberOfBins();
    findBinRanges();
    generateHistogramValues();
    setSamples(m_samples);
    setTitle(column->curveTitle());
}

Histogram::Histogram()
    : QwtPlotHistogram()
{}

QDataStream & operator<<(QDataStream &stream, const Histogram &histogram)
{
    stream << histogram.m_min;
    stream << histogram.m_max;
    stream << histogram.m_range;
    stream << histogram.m_rowsCount;
    stream << histogram.m_bins;
    stream << histogram.m_binRanges;
    stream << histogram.m_values;
    stream << histogram.m_samples;
    stream << histogram.getTitle();
    stream << histogram.pen();
    stream << histogram.brush();
    stream << histogram.legendIconSize();
    return stream;
}

QDataStream & operator>>(QDataStream &stream, Histogram &histogram)
{
    QString title;
    QPen pen;
    QBrush brush;
    QSize legendSize;

    stream >> histogram.m_min;
    stream >> histogram.m_max;
    stream >> histogram.m_range;
    stream >> histogram.m_rowsCount;
    stream >> histogram.m_bins;
    stream >> histogram.m_binRanges;
    stream >> histogram.m_values;
    stream >> histogram.m_samples;
    stream >> title;
    stream >> pen;
    stream >> brush;
    stream >> legendSize;

    histogram.setSamples(histogram.m_samples);
    histogram.setTitle(title);
    histogram.setPen(pen);
    histogram.setBrush(brush);
    histogram.setLegendIconSize(legendSize);

    return stream;
}

bool Histogram::isTableSelectionValid()
{
    ::Table & table = m_column->parent();
    m_min = table.columnMin(m_column->logicalIndex(), m_range);
    m_max = table.columnMax(m_column->logicalIndex(), m_range);

    if(IS_NAN(m_min) || IS_NAN(m_max))
        return false;

    return true;
}

void Histogram::getNumberOfBins()
{
    double n = m_rowsCount;
    Options & options = Options::instance();
    BinsType type = Histogram::qStringToBinsType(options.getBinsType());

    switch(type)
    {

        case SquareRoot:
            m_bins = sqrt(n);
            break;
        case SturgesFormula:
            m_bins = ceil(MyMath::Log2(n) + 1.0);
            break;
        case RiceRule:
            m_bins = ceil(2.0*(pow (n, 12.0)));
            break;
        case ExplicitValue:
            m_bins = options.getNumberOfBins();
            break;

        default:
            m_bins = sqrt(n);
    }
}

#include <gsl/gsl_sf_bessel.h>

void Histogram::findBinRanges()
{
    m_binRanges.clear();

    // First is minimum
    m_binRanges.push_back(m_min);

    double interval = (m_max - m_min)/(double)m_bins;

    for(int i = 0 ; i < m_bins - 1 ; ++i)
    {
        m_binRanges.push_back(m_min + interval*(i+1));
    }

    // Last is maximum
    m_binRanges.push_back(m_max);
}

void Histogram::generateHistogramValues()
{
    const TableModel& model = m_column->parent().model();
    int col = m_column->logicalIndex();
    double divider = (m_max - m_min) / m_bins;

    m_values.resize(m_bins);
    for(int i = m_range.minimum ; i <= m_range.maximum ; ++i)
    {
        double value = model.data_nocheck(i, col);
        int binNumber = (int)((value-m_min) / divider);

        if(binNumber == m_bins)
            binNumber--;
        m_values[binNumber]++;
    }

    m_samples.clear();
    for(int i = 0 ; i < m_bins ; ++i)
    {
       m_samples.push_back(QwtIntervalSample(m_values[i],m_binRanges[i],m_binRanges[i+1]));
    }
    m_samplesSize = m_samples.size();
}

QString Histogram::binsTypeToQString(Histogram::BinsType type)
{
    return m_binsTypeMap[type];
}

Histogram::BinsType Histogram::qStringToBinsType(const QString &name)
{
    BinsTypeMap::const_iterator it;

    for (it = m_binsTypeMap.begin(); it != m_binsTypeMap.end(); ++it )
        if (it->second == name)
            return it->first;

    return Histogram::None;
}

QVector<QString> Histogram::binsTypes()
{
    BinsTypeMap::const_iterator it;
    QVector<QString> types;

    for (it = m_binsTypeMap.begin(); it != m_binsTypeMap.end(); ++it )
        types.append(it->second);

    return types;
}


const QString Histogram::getTitle() const
{
    return this->title().text();
}

double Histogram::minX()
{
    return x(0);
}

double Histogram::maxX()
{
    return x(m_samplesSize - 1);
}

double Histogram::minY()
{
    return y(0);
}

double Histogram::maxY()
{
    return y(m_samplesSize - 1);
}

int Histogram::points()
{
    return m_samplesSize;
}

double Histogram::x(int position)
{
    return middleX(position);
}

double Histogram::y(int position)
{
    return m_samples.at(position).value;
}
