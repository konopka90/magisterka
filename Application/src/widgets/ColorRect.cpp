#include "ColorRect.h"

ColorRect::ColorRect(QWidget *parent) :
    QWidget(parent),
    m_color(Qt::black)
{
    this->setMinimumSize(14,14);
    this->setMaximumSize(14,14);
}

void ColorRect::setColor(const QColor &color)
{
    m_color = color;
}

const QColor ColorRect::getColor() const
{
    return m_color;
}


void ColorRect::paintEvent(QPaintEvent * event)
{
    QRect rect = event->rect();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(m_color);
    painter.setBrush(QBrush(m_color));
    painter.drawRect(rect);
}


void ColorRect::mouseReleaseEvent(QMouseEvent *)
{
    QColorDialog dialog;
    if(dialog.exec())
    {
        setColor(dialog.selectedColor());
        emit colorChanged(dialog.selectedColor());
    }
}
