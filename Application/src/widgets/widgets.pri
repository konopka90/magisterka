
INCLUDEPATH += widgets/

HEADERS +=  widgets/ColorRect.h \
    widgets/ColorWidget.h \
    widgets/DoubleSpinBox.h

SOURCES +=  widgets/ColorRect.cpp \
    widgets/ColorWidget.cpp \
    widgets/DoubleSpinBox.cpp
