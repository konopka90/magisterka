#ifndef COLORRECT_H
#define COLORRECT_H

#include <QtWidgets>

class ColorRect : public QWidget
{
    Q_OBJECT
public:
    ColorRect(QWidget *parent = 0);
    void setColor(const QColor& color);
    const QColor getColor() const;
private:
    QColor m_color;

signals:
    void colorChanged(QColor);

protected:
    void paintEvent(QPaintEvent *);
    void mouseReleaseEvent(QMouseEvent *);
};

#endif // COLORRECT_H
