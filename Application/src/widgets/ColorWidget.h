#ifndef COLORWIDGET_H
#define COLORWIDGET_H

#include <QtWidgets>
#include <QColorDialog>

#include "ColorRect.h"

class ColorWidget : public QWidget
{
    Q_OBJECT

public:
    ColorWidget();
    void setText(const QString & text);
    void setColor(const QColor & color);
    const QColor getColor() const;

private slots:
    void selectColor();

private:
    QLabel * m_text;
    ColorRect * m_colorRect;
    QPushButton * m_button;
};

#endif // COLORWIDGET_H
