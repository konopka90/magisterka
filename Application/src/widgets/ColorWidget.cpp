#include "ColorWidget.h"

ColorWidget::ColorWidget()
{
    m_text = new QLabel(tr("Color"));
    m_colorRect = new ColorRect();
    m_button = new QPushButton(tr("Change..."));

    QHBoxLayout * layout = new QHBoxLayout();
    layout->addWidget(m_text);
    layout->addWidget(m_colorRect);
    layout->addWidget(m_button);

    connect(m_button, SIGNAL(clicked()), this, SLOT(selectColor()));

    setLayout(layout);

}

void ColorWidget::setText(const QString &text)
{
    m_text->setText(text);

}

void ColorWidget::setColor(const QColor &color)
{
    m_colorRect->setColor(color);
}

const QColor ColorWidget::getColor() const
{
    return m_colorRect->getColor();
}

void ColorWidget::selectColor()
{
    QColorDialog dialog;
    if(dialog.exec())
    {
        m_colorRect->setColor(dialog.selectedColor());
    }
}
