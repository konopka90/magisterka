###############################
## Check if debug or release ##
###############################

DEBUG_EXT =

CONFIG(debug, debug|release) {
    DEFINES += DEBUG_MODE
    DEBUG_EXT = _d
}

CONFIG(release, debug|release) {
    DEFINES += RELEASE_MODE
    DEBUG_EXT =
}



win32 {
    !contains(QMAKE_TARGET.arch, x86_64) {
        ## Windows x86 (32bit) specific build here
        DEFINES += "ARCH_X86"
        ARCH_DIR = x86

    } else {
        ## Windows x64 (64bit) specific build here
        DEFINES += "ARCH_X64"
        ARCH_DIR = x64
    }

}

ROOT = ..
!include( $$ROOT/build.conf ) {
  message( "Setup all variables listed below in build.conf." )
}

##################
## Configurable ##
##################


LIBS += -L$$LIBS_DIR -lpython27 -lpythonqt -lpythonqt_qtall -lmuparser -lqwt
LIBS += libgsl$${DEBUG_EXT}.lib
LIBS += libgslcblas$${DEBUG_EXT}.lib

message($${LIBS})

#win32:LIBS += -lgslcblas
win32:LIBS += -lwpcap
win32:LIBS += -lws2_32
unix:LIBS += -lpcap

##################

TEMPLATE = app
QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport
TARGET = DAQ

message($$INCLUDEPATH)

include( analysis/analysis.pri )
include( core/core.pri )
include( widgets/widgets.pri )
include( dialogs/dialogs.pri )
include( options/options.pri )
include( scripting/scripting.pri )
include( table/table.pri )
include( plot/plot.pri )

HEADERS  += Precompiled.h

RESOURCES += resources/resources.qrc

PRECOMPILED_HEADER = Precompiled.h

unix:DEFINES += PLATFORM_UNIX
win32:DEFINES += PLATFORM_WIN32
win32:DEFINES += NOMINMAX
win32:DEFINES += WIN32_LEAN_AND_MEAN
win32:QMAKE_LFLAGS_DEBUG += /INCREMENTAL:NO




