#ifndef PRECOMPILED_H
#define PRECOMPILED_H

#include <cmath>
#include <limits>
#include <boost/math/special_functions/fpclassify.hpp>
#include <boost/math_fwd.hpp>

#include <QDialog>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QtWidgets>


#ifndef NAN
    #define NAN std::numeric_limits<double>::quiet_NaN()
#endif

#define IS_NAN(x) ((boost::math::isnan)(x))
#define IS_NOT_NAN(x) !((boost::math::isnan)(x))
#define MAX_PRECISION 50

#endif // PRECOMPILED_H
