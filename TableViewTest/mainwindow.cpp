#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    model(0)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    int rows = ui->spinRows->value();
    int cols = ui->spinCols->value();

    if(model != NULL)
        delete model;

    model = new MyModel(rows, cols, this);
    ui->tableView->setModel(model);
    ui->tableView->verticalHeader()->setDefaultSectionSize(20);

}
