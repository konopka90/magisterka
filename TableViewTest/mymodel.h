#ifndef MYMODEL_H
#define MYMODEL_H

#include <QAbstractItemModel>
#include <sstream>
#include <vector>
#include <list>
#include <iterator>
#include "datarow.h"

/************************/
/* Wybor typu kontenera */

#define DATATYPE_VECTOR
//#define DATATYPE_LIST

/************************/

#ifdef DATATYPE_VECTOR
    typedef std::vector<DataRow*> myDataType;
#else
    typedef std::list<DataRow*> myDataType;
#endif


class MyModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit MyModel(int rows, int cols, QObject *parent = 0);
    ~MyModel();

    int rowCount(const QModelIndex& parent) const;
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QModelIndex	index ( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
    QModelIndex parent(const QModelIndex &child) const;

private:
    myDataType myData;
    int rows;
    int cols;

signals:

public slots:


};

#endif // MYMODEL_H
