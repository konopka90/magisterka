#-------------------------------------------------
#
# Project created by QtCreator 2014-01-07T19:40:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TableViewTest
TEMPLATE = app


SOURCES += main.cpp\
        datarow.cpp \
        mainwindow.cpp\
        mymodel.cpp


HEADERS  += mainwindow.h\
            mymodel.h \
    datarow.h

FORMS    += mainwindow.ui
