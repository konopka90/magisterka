#include "mymodel.h"

MyModel::MyModel(int rows, int cols, QObject *parent) :
    QAbstractItemModel(parent), rows(rows), cols(cols)
{
    for(int i = 0 ; i < rows; ++i)
    {
       myData.push_back(new DataRow());
    }
}

MyModel::~MyModel()
{
    for(myDataType::iterator iter=myData.begin(); iter != myData.end(); iter++ )
    {
        delete *iter;
    }
}

int MyModel::rowCount(const QModelIndex &parent) const
{
    return rows;
}

int MyModel::columnCount(const QModelIndex &parent) const
{
    return cols;
}

QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
            return QVariant();

    if(role == Qt::DisplayRole)
    {
#ifdef DATATYPE_VECTOR
        return myData[index.row()]->values[index.column()];
#else
        myDataType::const_iterator it = std::next(myData.begin(), index.row());
        return (*it)->values[index.column()];
#endif
    }

    return QVariant::Invalid;
}

QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        std::stringstream ss;
        if(orientation == Qt::Horizontal)
        {
        ss << "H_" << section;
        return QString(ss.str().c_str());
        }
        else if(orientation == Qt::Vertical)
        {
          ss << "V_" << section;
          return QString(ss.str().c_str());
        }
    }

    return QVariant::Invalid;
}

QModelIndex MyModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column);
}

QModelIndex MyModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}
