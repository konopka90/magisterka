#ifndef DATAROW_H
#define DATAROW_H

#include <cstring>

class DataRow
{
public:
    DataRow();

public:

    static const unsigned int SIZE = 20;

    double values[SIZE];
};

#endif // DATAROW_H
