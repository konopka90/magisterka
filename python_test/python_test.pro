#-------------------------------------------------
#
# Project created by QtCreator 2014-02-20T00:11:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = python_test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += "C:\\Python27\\include"
LIBS += "C:\\Python27\\libs\\python27.lib"

SOURCES += main.cpp
